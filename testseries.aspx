﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="testseries.aspx.cs" Inherits="testseries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .btn{

    background: none;
    border: none;
    color:rgb(54, 54, 56);
    text-decoration: underline;
    cursor: pointer;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="contact">
		<div class="container">
            <center><h3><asp:Label ID="lbltest" runat="server"></asp:Label></h3></center>
            <br />
            <div class="row">
                <div class="col-md-12" style="text-align:center">
                    <asp:Literal ID="literal1" runat="server"></asp:Literal>
                </div>
            </div><br />
            <div class="row">
                <div class="col-md-12">
                <asp:Repeater ID="Repeater1" runat="server">
                    <HeaderTemplate>
                        <div class="row" style=" border-style:solid; border-bottom:none;">
                            <div class="col-md-1">Sno</div>
                            <div class="col-md-4" style="text-align:center">
                                Test
                            </div>
                            <div class="col-md-2">
                                Total qs
                            </div>
                            <div class="col-md-5"></div>
                        </div>
                    </HeaderTemplate>
        <ItemTemplate>
						 <div class="row"  style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
                             <div class="col-md-1">
                                  <asp:Label ID="Label1" runat="server" Text='<%#Container.ItemIndex+1 %>'></asp:Label>
                             </div>
                            
                             <div class="col-md-4" style="text-align:center">
                               <strong><asp:Label ID="lblno" runat="server" Text='<%#Eval("name") %>'></asp:Label></strong>  
                             </div>
                             <div class="col-md-2">
                                <strong><asp:Label ID="lbltotalqs" runat="server" Text='<%#Eval("COUNT") %>'></asp:Label></strong>
                             </div>
                            
                                 <%--<div class="col-md-1">--%>
                                     <%--   Max marks:
                                 </div>
                             <div class="col-md-1">
                                 <asp:Label ID="lblmaxmarks" runat="server" Text='<%#Eval("Setno") %>'></asp:Label>
                             </div>--%>
                             <div class="col-md-5" style="text-align:right">
                                        <a href="sscOnline.aspx?set=<%#Eval("name")%>&section=<%#Eval("set") %>">Start quiz</a>
                </div>
                         </div>
            
    </ItemTemplate>
                </asp:Repeater></div>
            </div>
            <div class="row">
                <div class="col-md-11">
                <asp:Repeater ID="Repeater2" runat="server">
                    <HeaderTemplate>
                        <div class="row"  style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
                             <div class="col-md-1">Sno
                             </div>
                            
                             <div class="col-md-1" style="text-align:center">Test
                             </div>
                             <div class="col-md-2">No. of questions
                                 
                             </div>
                            
                                 <%--<div class="col-md-1">--%>
                                     <%--   Max marks:
                                 </div>
                             <div class="col-md-1">
                                 <asp:Label ID="lblmaxmarks" runat="server" Text='<%#Eval("Setno") %>'></asp:Label>
                             </div>--%>
                             <div class="col-md-1">
                                        Total Marks
                </div>
                            <div class="col-md-1">Negative marks%</div>
                            <div class="col-md-1">Time</div>
                            <div class="col-md-1">Scored</div>
                            <div class="col-md-2">Actions</div>
                            <div class="col-md-1">Pay for it</div>
                            <div class="col-md-1"></div>
                         </div>
                    </HeaderTemplate>
        <ItemTemplate>
						 <div class="row"  style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
                             <div class="col-md-1">
                                  <asp:Label ID="Label1" runat="server" Text='<%#Container.ItemIndex+1 %>'></asp:Label>
                             </div>
                            
                             <div class="col-md-1" style="text-align:center">
                               <strong>
                                  
                                   <asp:Label ID="lblno" runat="server" Text='<%#Eval("name") %>'></asp:Label></strong>  
                             </div>
                             <div class="col-md-2" style="text-align:center">
                                 <strong><asp:Label ID="lbltotalqs" runat="server" Text='<%#Eval("count") %>'></asp:Label></strong>
                             </div>
                              <div class="col-md-1">
                               <strong><asp:Label ID="totalmarks" runat="server" Text='<%#Eval("totalmarks") %>'></asp:Label></strong>
                             </div>
                             <div class="col-md-1">
                                 <strong><asp:Label ID="lblnegative" runat="server" Text="25%"></asp:Label></strong>
                             </div>
                              <div class="col-md-1">
                                <strong><asp:Label ID="lbltime" runat="server" Text='<%#Eval("totaltime") %>'></asp:Label></strong>
                             </div>
                             <div class="col-md-1">
                                 <strong><asp:Label ID="lblobt" runat="server" Text='<%#Eval("TotalMarksObt") %>'></asp:Label></strong>
                             </div>
                             <div class="col-md-2">
                                 <strong><asp:Label ID="lblaction" runat="server" Text="Atempted"></asp:Label></strong>
                             </div>
                             <div class="col-md-1">
                                 <strong><asp:Label ID="lblpay" runat="server" Text="Free"></asp:Label></strong>
                             </div>
                             <div class="col-md-1">
                                 <a href="solution.aspx?ssconline=<%#Eval("name") %>" target="_blank">View Solution</a>
                             </div>
                                 <%--<div class="col-md-1">--%>
                                     <%--   Max marks:
                                 </div>
                             <div class="col-md-1">
                                 <asp:Label ID="lblmaxmarks" runat="server" Text='<%#Eval("Setno") %>'></asp:Label>
                             </div>--%>
                             
                            
                         </div>
           

    </ItemTemplate>
                </asp:Repeater></div>
                <div class="col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="pnl1" runat="server"></asp:Panel>
                </div>
            </div>
           
					</div>
				
			</div>

   
     <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
   
</asp:Content>
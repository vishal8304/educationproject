﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class Jobdesc : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        contentbind();
    }
    public void contentbind()
    {
        try
        {
            var a = Request.QueryString["tile"];
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            if (a != null)
            {
                SqlCommand cmd = new SqlCommand("select * from Jobs where tile='" + a + "'", con);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    string str = dr["Content"].ToString();
                    literal1.Text = System.Net.WebUtility.HtmlDecode(str);
                }
                dr.Close();
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }
}
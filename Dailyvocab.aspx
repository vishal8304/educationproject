﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Dailyvocab.aspx.cs" Inherits="Dailyvocab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .pnl{
            background-color:rgba(132, 128, 128, 0.67);
            border-style:groove;
        }
        .label2{
  font-size: 15px;
  vertical-align: middle;
}
        .label3{
  font-size: 30px;
  vertical-align: top;
}
        .rpt{
           
            width:833px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row" >
            <div class="col-md-12" style="text-align:center; border-bottom-style:groove; border-bottom-color:black"">
                <asp:Literal ID="literal1" runat="server"></asp:Literal>
            </div>
            <br />
        <div class="row">
            <div class="col-md-9">
                    <div class="rpt" style="margin-left:14%;margin-top: 2%;">
                <asp:Repeater ID="rptvocabqs" runat="server" OnItemDataBound="rptvocabqs_ItemDataBound" >
                    <ItemTemplate >
                        <div class="row">
                            <div class="col-md-1">
                                <strong>
                                <asp:Label ID="sno" Text='<%#Container.ItemIndex+1 %>' runat="server"></asp:Label></strong>
                            </div>
                                <%--<asp:Label ID="lblid" ClientIDMode="Static" runat="server" Text='<%#Eval("vocabId") %>'></asp:Label>--%>
                            <div class="col-md-11">
                                 <asp:HiddenField ClientIDMode="Static"  runat="server" Value='<%#Eval("vocabId") %>' ID="hdn" />
                                <strong><asp:Label ID="lblqs" ClientIDMode="Static" Text='<%#Eval("Question") %>' runat="server"></asp:Label></strong>
                            </div>
                        </div><br />
                        <div class="colored">
                        <div class="row">
                            <div class="mainRow">
                            <div class="col-md-1">
                                <asp:RadioButton ID="rdb1" ClientIDMode="Static" CssClass="label2" runat="server" value="A" GroupName="a" />
                                <%--<input type="radio" id="rdb1" class="label2" value="A" vocabId="<%#Eval("vocabId") %>" name="a" />--%>
                                <input type="hidden" id="hfld" value="<%#Eval("vocabId") %>"/>
                            </div>
                            <div class="col-md-5 a" runat="server" id="divs">
                                <asp:Label ID="lblopt1" ClientIDMode="Static" runat="server" Text='<%#Eval("val1") %>'></asp:Label>
                            </div></div>
                            <div class="mainRow">
                            <div class="col-md-1">
                        <asp:RadioButton ID="rdb2" ClientIDMode="Static" runat="server" CssClass="label2" value="B" GroupName="a"  />
                                <%--<input type="radio" id="rdb2" class="label2" value="B" vocabId="<%#Eval("vocabId") %>" name="a" />--%>
                                <input type="hidden" id="hfld" value="<%#Eval("vocabId") %>"/>
                        </div>
                            <div class="col-md-5 b" runat="server" id="divt">
                                  <asp:Label ID="lblopt2" ClientIDMode="Static" runat="server"  Text='<%#Eval("val2") %>'></asp:Label>
                            </div></div>
                        </div><br />
                        <div class="row">
                            <div class="mainRow">
                            <div class="col-md-1">
                        <asp:RadioButton ID="rdb3" ClientIDMode="Static" runat="server" CssClass="label2" value="C" GroupName="a"  />
                                <%--<input type="radio" id="rdb3" class="label2" value="C" vocabId="<%#Eval("vocabId") %>" name="a" />--%>
                                <input type="hidden" id="hfld" value="<%#Eval("vocabId") %>"/>
                        </div>
                            <div class="col-md-5 c" runat="server" id="divm">
                                  <asp:Label ID="lblopt3"  ClientIDMode="Static" runat="server"  Text='<%#Eval("val3") %>'></asp:Label>
                            </div>
                            </div>
                            <div class="mainRow">
                            <div class="col-md-1">
                        <asp:RadioButton ID="rdb4"  runat="server" ClientIDMode="Static"  CssClass="label2" value="D"  GroupName="a"  />
                                <input type="hidden" id="hfld" value="<%#Eval("vocabId") %>"/>
                        </div>
                            <div class="col-md-5 d" runat="server" id="divn">
                                  <asp:Label ID="lblopt4" ClientIDMode="Static" runat="server" Text='<%#Eval("val4") %>'></asp:Label>
                            </div>
                                </div>
                        </div><br />
                       </div>
                    </ItemTemplate>
                </asp:Repeater>
                </div>
                </div>
              <%--  <div class="row" style="text-align:center;">
                    <div class="col-md-12">
                        <input type="button" id="btnSave" value="Save" style="border-style:outset; background-color:black; color:white;" />
                    </div>
                </div>--%>
           
       <%--     <div class="col-md-3" style="margin-top:2%;">
                
              <aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
                   <asp:Panel ID="pnlword" runat="server" CssClass="pnl">
                   <asp:Repeater ID="rptwords" runat="server">
                       <ItemTemplate>
                           <div class="row">
                               <div class="col-md-10">
                                      <b> <asp:Label ID="lblword" runat="server" Text='<%#Eval("word") %>' Font-Size="Medium"></asp:Label></b>
                               </div>
                               <div class="col-md-2">
                                   <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="fa fa-sort-desc"></span>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li>
                                <p><asp:Label ID="lbldesc" runat="server" Text='<%#Eval("worddesc")%>'></asp:Label></p></li>
                        </ul>
                               </div>
                           </div>
                       </ItemTemplate>
                   </asp:Repeater> </asp:Panel>
            
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
            </div>--%>
       
            </div>
        </div>
    </div>
      <script src="Admin/js/jquery.min.js"></script>
    
    <script>
        //$("#btnSave").click(function () {
        $("input[type='radio']").click(function () {
            var current = $(this);
            var colored = current.parents(".colored");
            var mainDiv = current.parents(".mainRow");
            var targetDiva = colored.find(".a");
            var targetDivb = colored.find(".b");
            var targetDivc = colored.find(".c");
            var targetDivd = colored.find(".d");
                var opt = current.val();
                var vocabId = current.parent().siblings("#hfld").val();
               var Email = "";
             var cookieArray = document.cookie.split("; ");
             for (var i = 0; i < cookieArray.length; i++) {
                 var nameValueArray = cookieArray[i].split("=");
                 if (nameValueArray[0] == "Email") {
                     Email = nameValueArray[1];
                 }
             }
                
                //console.log("vocabId:" + vocabId + ", opt : " + opt + " , Email : " + Email);
            $.ajax({
        type: "POST",
        url: "Dailyvocab.aspx/SaveAnswer",
        data: JSON.stringify({ opt: opt, Email: Email, vocabId: vocabId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Result) {
            debugger
            colored.find("input[type='radio']").attr("disabled", true);
            //current.parent().siblings("span").attr("disabled", true);
            if (Result.d == 'B')
            {

                targetDivb.css("background-color", "rgba(9, 206, 9, 0.6)");
                targetDiva.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivc.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivd.css("background-color", "rgba(255, 11, 11, 0.72)");
            }
            else if(Result.d=='A') {
                targetDiva.css("background-color", "rgba(9, 206, 9, 0.6)");
                targetDivb.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivc.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivd.css("background-color", "rgba(255, 11, 11, 0.72)");
            }
            else if (Result.d == 'C') {
                targetDivc.css("background-color", "rgba(9, 206, 9, 0.6)");
                targetDivb.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDiva.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivd.css("background-color", "rgba(255, 11, 11, 0.72)");
            }
            else if (Result.d == 'D') {
                targetDivd.css("background-color", "rgba(9, 206, 9, 0.6)");
                targetDivb.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDivc.css("background-color", "rgba(255, 11, 11, 0.72)");
                targetDiva.css("background-color", "rgba(255, 11, 11, 0.72)");
            }
               
            else if (Result.d == "Aa") {
               
                targetDiva.css("background-color", "rgba(9, 206, 9, 0.6)");
            }
            else if (Result.d == "Ba") {
              
                targetDivb.css("background-color", "rgba(9, 206, 9, 0.6)");
            }
            else if (Result.d == "Ca") {
                
                targetDivc.css("background-color", "rgba(9, 206, 9, 0.6)");
            }
            else if (Result.d == "Da") {
                
                targetDivd.css("background-color", "rgba(9, 206, 9, 0.6)");
            }
        },
        failure: function(response) {
            alert("Error");

        }
            });
        });
        </script>
</asp:Content>


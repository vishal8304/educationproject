﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class UserProfile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindrpt();
            connect();
        }
    }

    public void bindrpt()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            var a = Request.QueryString["Email"];
            SqlDataAdapter upload = new SqlDataAdapter("select * from [User] where [Email]='" + a + "'", con);
            DataTable dt = new DataTable();
            upload.Fill(dt);
            rptprofile.DataSource = dt;
            rptprofile.DataBind();
        }
        catch (Exception ex2)
        {
            lblupdate.Visible = true;
            lblupdate.Text = ex2.ToString();
        }
    }
    public void connect()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var a = Request.QueryString["Email"];
        SqlCommand cmd = new SqlCommand("select * from Followers where Username='" + a + "' and Followers ='" + Request.Cookies["Email"].Value + "'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            btnfollow.Text = "Unfollow";
        }
        else
        {
            btnfollow.Text = "Follow";
        }
        dr.Close();
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }

    protected void btnfollow_Click(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var a = Request.QueryString["Email"];
        SqlCommand cmd = new SqlCommand("select * from Followers where Username='" + a + "' and Followers ='" + Request.Cookies["Email"].Value + "'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            dr.Close();
            SqlCommand cmd1 = new SqlCommand("delete from Followers where Username='" + a + "' and Followers ='" + Request.Cookies["Email"].Value + "'", con);
            int i = cmd1.ExecuteNonQuery();
            if (i == 1)
            {
                btnfollow.Text = "Follow";
            }
        }
        else
        {
            dr.Close();
            SqlCommand cmd1 = new SqlCommand("insert into Followers values('" + a + "','" + Request.Cookies["Email"].Value + "')", con);
            int i = cmd1.ExecuteNonQuery();
            if (i == 1)
            {
                btnfollow.Text = "Unfollow";
            }
        }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }
}
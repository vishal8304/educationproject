﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Choicepop.aspx.cs" Inherits="Choicepop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style type="text/css">
#overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter:alpha(opacity=70);
-moz-opacity:1;
-khtml-opacity: 1;
opacity: 1;
z-index: 100;
display: none;
}
.content a{
text-decoration: none;
}
.popup{
width: 100%;
margin: 0 auto;
display: none;
position: fixed;
z-index: 101;
}
.content{
min-width: 600px;
width: 600px;
min-height: 150px;
margin: 10px auto;
background: #f3f3f3;
position: relative;
z-index: 103;
padding: 10px;
border-radius: 5px;
box-shadow: 0 2px 5px #000;
}
.content p{
clear: both;
color: #555555;
text-align: justify;
}
.content p a{
color: #d91900;
font-weight: bold;
}
.content .x{
float: right;
height: 35px;
left: 22px;
position: relative;
top: -25px;
width: 34px;
}
.content .x:hover{
cursor: pointer;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <div class='popup'>
<div class='content'>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <h2>Select your exam</h2>
        </div>
    </div><br />
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            <strong><asp:RadioButton GroupName="a" ID="rdbssc" runat="server" />SSC</strong>
        </div>
        <div class="col-md-6" >
            <strong><asp:RadioButton GroupName="a" ID="rdbbank" runat="server" />Bank</strong>
        </div>
    </div><br />
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
        </div>
    </div><br />
    </div></div>
               <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type='text/javascript'>
    $(function () {
        var overlay = $('<div id="overlay"></div>');
        $('.close').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
        $('.x').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });

        $(document).ready(function(){
            overlay.show();
            overlay.appendTo(document.body);
            $('.popup').show();
            return false;
        });
    });
</script>
   
</asp:Content>


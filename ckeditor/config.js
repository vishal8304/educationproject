﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};

config.extraPlugins = 'FMathEditor';

config.extraPlugins = 'Simage';

config.extraPlugins = 'pastefromword';


config.font_names = 'Kruti Dev 010/"kruti dev 010";' + config.font_names;


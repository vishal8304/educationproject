﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Globalization;
using EducationDataAccess.Data;
using EducationDataAccess.Models;
using System.Web.Script.Serialization;
using System.Web.Script.Services;

public partial class Home : System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    public List<Banners> Banner = new List<Banners>();
    string strname = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            text();
            text1();
        }

        con.Open();
        SqlCommand cmd = new SqlCommand("select Top 5 * from Banner", con);
        SqlDataReader sdr = cmd.ExecuteReader();
        while (sdr.Read())
        {
            Banners sol = new Banners
            {
                Id = Convert.ToInt32(sdr["Id"]),
                Banner = sdr["Banner"].ToString(),
            };
            Banner.Add(sol);
            //var data = new JavaScriptSerializer();
            //BannerImages = data.Serialize(Banner.Select(a => a.Banner));
        }
        con.Close();
    }


    public class Banners
    {
        public int Id { get; set; }
        public string Banner { get; set; }
    }

    //[WebMethod]
    //public static askmodelDto fetchpostquery(string Email, int PageNumber, int PageSize)
    //{
    //    DataTable dt = new DataTable();
    //    askmodelDto objdept = new askmodelDto();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("spGetPosts", con))

    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
    //            cmd.Parameters.Add(new SqlParameter()
    //            {
    //                ParameterName = "@PageNumber",
    //                Value = PageNumber
    //            });

    //            cmd.Parameters.Add(new SqlParameter()
    //            {
    //                ParameterName = "@PageSize",
    //                Value = PageSize
    //            });

    //            //SqlDataAdapter da = new SqlDataAdapter(cmd);

    //            //da.Fill(dt);
    //            SqlDataReader sdr = cmd.ExecuteReader();
    //            while (sdr.HasRows)
    //            {
    //                var mcqQuery = new List<mcqqueryDto>();
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    var mcq = new mcqqueryDto
    //                    {
    //                        Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
    //                        Ques = dt.Rows[i]["Ques"].ToString(),
    //                        opta = dt.Rows[i]["opta"].ToString(),
    //                        optb = dt.Rows[i]["optb"].ToString(),
    //                        optc = dt.Rows[i]["optc"].ToString(),
    //                        optd = dt.Rows[i]["optd"].ToString(),
    //                        opte = dt.Rows[i]["opte"].ToString(),
    //                        corval = dt.Rows[i]["corval"].ToString(),
    //                        exam = dt.Rows[i]["exam"].ToString(),
    //                        subject = dt.Rows[i]["subject"].ToString(),
    //                        picpath = dt.Rows[i]["picpath"].ToString(),
    //                        Date = dt.Rows[i]["Date"].ToString(),
    //                        Email = dt.Rows[i]["Email"].ToString(),
    //                        Name = dt.Rows[i]["Name"].ToString(),
    //                        profilepath = dt.Rows[i]["profilepath"].ToString()
    //                    };
    //                    JavaScriptSerializer js = new JavaScriptSerializer();
    //                    //Context.Response.Write(js.Serialize(mcqQuery));

    //                    var id = mcq.Id;
    //                    using (SqlCommand cmds = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con))
    //                    {
    //                        if (con.State == ConnectionState.Closed)
    //                        {
    //                            con.Open();
    //                        }
    //                        SqlDataAdapter das = new SqlDataAdapter(cmds);
    //                        DataTable dts = new DataTable();
    //                        das.Fill(dts);
    //                        if (dts.Rows.Count > 0)
    //                        {
    //                            var asklike = new List<asklikeDto>();
    //                            for (int t = 0; t < dts.Rows.Count; t++)
    //                            {
    //                                var likeq = new asklikeDto
    //                                {
    //                                    count = Convert.ToInt16(dts.Rows[t]["count"].ToString())
    //                                };
    //                                asklike.Add(likeq);
    //                            }
    //                            mcq.asklike = asklike;
    //                        }
    //                    }

    //                    using (SqlCommand cmdk = new SqlCommand("select count(*) as commentscount from ask where Id='" + id + "'", con))
    //                    {
    //                        if (con.State == ConnectionState.Closed)
    //                        {
    //                            con.Open();
    //                        }
    //                        SqlDataAdapter das = new SqlDataAdapter(cmdk);
    //                        DataTable dts = new DataTable();
    //                        das.Fill(dts);
    //                        if (dts.Rows.Count > 0)
    //                        {
    //                            var com = new List<comDto>();
    //                            for (int t = 0; t < dts.Rows.Count; t++)
    //                            {
    //                                var comdto = new comDto
    //                                {
    //                                    commentcount = Convert.ToInt16(dts.Rows[t]["commentscount"].ToString())
    //                                };
    //                                com.Add(comdto);
    //                            }
    //                            mcq.com = com;
    //                        }
    //                    }
    //                    using (SqlCommand cmd1 = new SqlCommand("select a.*,b.profilepath from ask a inner join [user] b on a.Email=b.Email where a.Id='" + id + "' order by a.commentId desc", con))
    //                    {
    //                        if (con.State == ConnectionState.Closed)
    //                        {
    //                            con.Open();
    //                        }
    //                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
    //                        DataTable dt1 = new DataTable();
    //                        da1.Fill(dt1);
    //                        if (dt1.Rows.Count > 0)
    //                        {
    //                            var ask = new List<askDto>();
    //                            for (int j = 0; j < dt1.Rows.Count; j++)
    //                            {
    //                                var askq = new askDto
    //                                {
    //                                    commentId = Convert.ToInt16(dt1.Rows[j]["commentId"].ToString()),
    //                                    Comments = dt1.Rows[j]["Comments"].ToString(),
    //                                    comimg = dt1.Rows[j]["comimg"].ToString(),
    //                                    profile = dt1.Rows[j]["profilepath"].ToString()
    //                                };
    //                                var commentId = askq.commentId;
    //                                using (SqlCommand cmd2 = new SqlCommand("select * from ChildCommentTable where commentId='" + commentId + "' order by ChildCommentID desc", con))
    //                                {
    //                                    if (con.State == ConnectionState.Closed)
    //                                    {
    //                                        con.Open();
    //                                    }
    //                                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
    //                                    DataTable dt2 = new DataTable();
    //                                    da2.Fill(dt2);
    //                                    if (dt2.Rows.Count > 0)
    //                                    {
    //                                        var ChildCommentTable = new List<ChildCommentTableDto>();
    //                                        for (int k = 0; k < dt2.Rows.Count; k++)
    //                                        {
    //                                            ChildCommentTable.Add(new ChildCommentTableDto
    //                                            {
    //                                                //ChildCommentID = Convert.ToInt16(dt2.Rows[k]["ChildCommentID"].ToString()),
    //                                                CommentMessage = dt2.Rows[k]["CommentMessage"].ToString()
    //                                            });
    //                                        }
    //                                        askq.ChildCommentTable = ChildCommentTable;
    //                                        askq.commentId = commentId;
    //                                    }
    //                                    //ask.Add(askq);
    //                                }
    //                                using (SqlCommand cmd2 = new SqlCommand("select count(*) as commentscount from comlike where comId=" + commentId, con))
    //                                {
    //                                    if (con.State == ConnectionState.Closed)
    //                                    {
    //                                        con.Open();
    //                                    }
    //                                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
    //                                    DataTable dt2 = new DataTable();
    //                                    da2.Fill(dt2);
    //                                    if (dt2.Rows.Count > 0)
    //                                    {
    //                                        var comlike = new List<comlikeDto>();
    //                                        for (int k = 0; k < dt2.Rows.Count; k++)
    //                                        {
    //                                            comlike.Add(new comlikeDto
    //                                            {
    //                                                tot = Convert.ToInt16(dt2.Rows[k]["commentscount"].ToString())
    //                                            });
    //                                        }
    //                                        askq.comlike = comlike;

    //                                    }
    //                                    ask.Add(askq);
    //                                }
    //                            }
    //                            mcq.ask = ask;
    //                        }
    //                        mcqQuery.Add(mcq);
    //                    }
    //                }

    //                //int recordPerPage = 5;
    //                int totalRecords = mcqQuery.Count;
    //                //int totalPages = Convert.ToInt32(Math.Ceiling((double)totalRecords / recordPerPage));
    //                //mcqQuery = mcqQuery.Skip(recordPerPage * (page - 1)).Take(recordPerPage).ToList();
    //                objdept.mcqquery = mcqQuery;
    //                //objdept.TotalPages = totalPages;

    //                using (SqlCommand cmd8 = new SqlCommand("select * from [User] where Email='" + Email + "' and expert='True'", con))
    //                {
    //                    SqlDataReader dr = cmd8.ExecuteReader();
    //                    dr.Read();
    //                    if (dr.HasRows)
    //                    {
    //                        objdept.expert = "True";
    //                        dr.Close();
    //                    }
    //                    else
    //                    {
    //                        objdept.expert = "False";
    //                        dr.Close();
    //                    }
    //                }
    //            }
    //        }
    //        con.Close();
    //        return objdept;
    //    }
    //}

    //[WebMethod]
    //public static askmodel fetchquery(string btnval,string Email)
    //{
    //    DataTable dt = new DataTable();
    //    askmodel objdept = new askmodel();
    //    int start = Convert.ToInt16(btnval);
    //    int stop = start + 5;
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id desc) AS Row#,a.*,b.Name from mcqquery a inner join [User] b on a.Email=b.Email) select * from MyTable where Row# between '"+start+"' and '"+stop+"'", con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                var mcqQuery = new List<mcqquery>();
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    var mcq = new mcqquery
    //                    {
    //                        Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
    //                        Ques = dt.Rows[i]["Ques"].ToString(),
    //                        opta = dt.Rows[i]["opta"].ToString(),
    //                        optb = dt.Rows[i]["optb"].ToString(),
    //                        optc = dt.Rows[i]["optc"].ToString(),
    //                        optd = dt.Rows[i]["optd"].ToString(),
    //                        opte = dt.Rows[i]["opte"].ToString(),
    //                        corval = dt.Rows[i]["corval"].ToString(),
    //                        exam = dt.Rows[i]["exam"].ToString(),
    //                        subject = dt.Rows[i]["subject"].ToString(),
    //                        picpath = dt.Rows[i]["picpath"].ToString(),
    //                        Date = dt.Rows[i]["Date"].ToString(),
    //                        Email = dt.Rows[i]["Email"].ToString(),
    //                        Name = dt.Rows[i]["Name"].ToString()
    //                    };

    //                    var id = mcq.Id;
    //                    using (SqlCommand cmds = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con))
    //                    {
    //                        if (con.State == ConnectionState.Closed)
    //                        {
    //                            con.Open();
    //                        }
    //                        SqlDataAdapter das = new SqlDataAdapter(cmds);
    //                        DataTable dts = new DataTable();
    //                        das.Fill(dts);
    //                        if (dts.Rows.Count > 0)
    //                        {
    //                            var asklike = new List<asklike>();
    //                            for (int t = 0; t < dts.Rows.Count; t++)
    //                            {
    //                                var likeq = new asklike
    //                                {
    //                                    count = Convert.ToInt16(dts.Rows[t]["count"].ToString())
    //                                };
    //                                asklike.Add(likeq);
    //                            }
    //                            mcq.asklike = asklike;
    //                        }

    //                    }
    //                    using (SqlCommand cmd1 = new SqlCommand("select * from ask where Id='" + id + "' order by commentId desc", con))
    //                    {
    //                        if (con.State == ConnectionState.Closed)
    //                        {
    //                            con.Open();
    //                        }
    //                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
    //                        DataTable dt1 = new DataTable();
    //                        da1.Fill(dt1);
    //                        if (dt1.Rows.Count > 0)
    //                        {
    //                            var ask = new List<ask>();
    //                            for (int j = 0; j < dt1.Rows.Count; j++)
    //                            {
    //                                var askq = new ask
    //                                {
    //                                    commentId = Convert.ToInt16(dt1.Rows[j]["commentId"].ToString()),
    //                                    Comments = dt1.Rows[j]["Comments"].ToString()
    //                                };
    //                                var commentId = askq.commentId;
    //                                //using (SqlCommand cmd8 = new SqlCommand("select * from [User] where Email='" + Email + "' and expert='True'", con))
    //                                //{
    //                                //    SqlDataReader dr = cmd8.ExecuteReader();
    //                                //    dr.Read();
    //                                //    if (dr.HasRows)
    //                                //    {
    //                                //        var ask1 = new ask
    //                                //        {
    //                                //            expert = "True"
    //                                //        };
    //                                //        ask.Add(ask1);
    //                                //        dr.Close();
    //                                //    }
    //                                //    else
    //                                //    {
    //                                //        var ask1 = new ask
    //                                //        {
    //                                //            expert = "False"
    //                                //        };
    //                                //        ask.Add(ask1);
    //                                //        dr.Close();
    //                                //    }
    //                                //}
    //                                using (SqlCommand cmd2 = new SqlCommand("select * from ChildCommentTable where commentId='" + commentId + "' order by ChildCommentID desc", con))
    //                                {
    //                                    if (con.State == ConnectionState.Closed)
    //                                    {
    //                                        con.Open();
    //                                    }
    //                                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
    //                                    DataTable dt2 = new DataTable();
    //                                    da2.Fill(dt2);
    //                                    if (dt2.Rows.Count > 0)
    //                                    {
    //                                        var ChildCommentTable = new List<ChildCommentTable>();
    //                                        for (int k = 0; k < dt2.Rows.Count; k++)
    //                                        {
    //                                            ChildCommentTable.Add(new ChildCommentTable
    //                                            {
    //                                                ChildCommentID = Convert.ToInt16(dt2.Rows[k]["ChildCommentID"].ToString()),
    //                                                CommentMessage = dt2.Rows[k]["CommentMessage"].ToString()
    //                                            });
    //                                        }
    //                                        askq.ChildCommentTable = ChildCommentTable;
    //                                    }
    //                                    ask.Add(askq);

    //                                }

    //                            }
    //                            mcq.ask = ask;
    //                        }
    //                        mcqQuery.Add(mcq);
    //                    }
    //                }
    //                objdept.mcqquery = mcqQuery;
    //            }
    //        }
    //        con.Close();
    //        return objdept;
    //    }
    //}

    //[WebMethod]
    //public static List<mcqquery> mcqans(string val, string Email, string qid)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    List<mcqquery> mcqquerys = new List<mcqquery>();
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        UserEmail = dr["Email"].ToString();
    //    }
    //    dr.Close();
    //    SqlCommand cmd = new SqlCommand("insert into Usermcq(selectedval,Email,Id) values (@selectedval,@Email,@Id)", con);
    //    cmd.Parameters.AddWithValue("@selectedval", val);
    //    cmd.Parameters.AddWithValue("@Email", UserEmail);
    //    cmd.Parameters.AddWithValue("@Id", qid);
    //    int Result = cmd.ExecuteNonQuery();
    //    if (Result == 1)
    //    {

    //        using (SqlCommand cmd1 = new SqlCommand("select * from mcqquery where Id='" + qid + "'"))
    //        {
    //            cmd1.Connection = con;
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            using (SqlDataReader sdr = cmd1.ExecuteReader())
    //            {
    //                while (sdr.Read())
    //                {
    //                    mcqquerys.Add(new mcqquery
    //                    {
    //                        opta = sdr["opta"].ToString(),
    //                        optb = sdr["optb"].ToString(),
    //                        optc = sdr["optc"].ToString(),
    //                        optd = sdr["optd"].ToString(),
    //                        opte = sdr["opte"].ToString(),
    //                        corval = sdr["corval"].ToString()
    //                    });
    //                }
    //            }

    //        }
    //    }
    //    con.Close();
    //    return mcqquerys;
    //}

    //[WebMethod]
    //public static string uploadfile(string img, string Email, string id, string textcom)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    string returnval = "";
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        UserEmail = dr["Email"].ToString();
    //    }
    //    dr.Close();
    //    string str1 = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
    //    string images = "/comments/" + img;
    //    SqlCommand cmd = new SqlCommand("insert into ask(Email,CommentDate,Id,comimg,Comments) values('" + UserEmail + "','" + str1 + "','" + id + "','" + images + "','" + textcom + "')", con);
    //    int i = cmd.ExecuteNonQuery();
    //    if (i == 1)
    //    {
    //        returnval = "true";
    //    }
    //    return returnval;
    //}

    //[WebMethod]
    //public static string xpert(string Email)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    string returnval = "";
    //    var expert = "";
    //    SqlCommand cmd2 = new SqlCommand("select expert from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        expert = dr["expert"].ToString();
    //        if (expert == "True")
    //        {
    //            returnval = "true";
    //        }
    //        else
    //        {
    //            returnval = "false";
    //        }
    //    }
    //    dr.Close();
    //    return returnval;
    //}

    //[WebMethod]
    //public static string showcoms(string Email)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    string returnval = "";

    //    SqlCommand cmd = new SqlCommand("select * from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        returnval = dr["profilepath"].ToString();
    //    }
    //    return returnval;
    //}

    //[WebMethod]
    //public static string SaveCom(string Email, string id, string textcom)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    string returnval = "";
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    string str1 = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
    //    SqlCommand cmd = new SqlCommand("insert into ask(Email,CommentDate,Id,Comments) values('" + UserEmail + "','" + str1 + "','" + id + "','" + textcom + "')", con);
    //    int i = cmd.ExecuteNonQuery();
    //    if (i == 1)
    //    {
    //        SqlCommand cmd1 = new SqlCommand("select * from [User] where Email='" + UserEmail + "'", con);
    //        SqlDataReader dr = cmd1.ExecuteReader();
    //        dr.Read();
    //        if (dr.HasRows)
    //        {
    //            returnval = dr["profilepath"].ToString();
    //        }
    //    }
    //    return returnval;
    //}

    //[WebMethod]
    //public static List<mcqquery> populatelink()
    //{
    //    DataTable dt1 = new DataTable();
    //    List<mcqquery> objdept1 = new List<mcqquery>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#,* FROM mcqquery) select * from MyTable", con))
    //        {
    //            con.Open();
    //            SqlDataAdapter da = new SqlDataAdapter(cmd1);
    //            da.Fill(dt1);
    //            if (dt1.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt1.Rows.Count; i++)
    //                {
    //                    objdept1.Add(new mcqquery
    //                    {
    //                        Id = Convert.ToInt32(dt1.Rows[i]["Row#"])

    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept1;
    //        }
    //    }

    //}

    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    var value = Request.Cookies["Email"].Value;
    //    try
    //    {
    //        string str1 = System.DateTime.Now.ToString();
    //        con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //        con.Open();
    //        var UserEmail = "";
    //        SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + value + "' or Mob='" + value + "'", con);
    //        SqlDataReader dr = cmd2.ExecuteReader();
    //        dr.Read();
    //        if (dr.HasRows)
    //        {
    //            UserEmail = dr["Email"].ToString();
    //        }
    //        dr.Close();
    //        string qry = "insert into Askquery(Post,exam,subject,Email,picpath) values(@t1,@t2,@t3,@t4,@t5) insert into sort(askquery,[Date]) values(@t9,@t10)";
    //        SqlCommand cmd = new SqlCommand(qry, con);
    //        cmd.Parameters.AddWithValue("@t1", Request.Form["Text1"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t2", Request.Form["exam"]);
    //        cmd.Parameters.AddWithValue("@t3", Request.Form["sub"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t4", UserEmail);
    //        if (FileUpload1.HasFile)
    //        {
    //            strname = "/upload/" + FileUpload1.FileName.ToString();
    //            FileUpload1.PostedFile.SaveAs(Server.MapPath("/upload/") + FileUpload1.FileName.ToString());
    //        }
    //        cmd.Parameters.AddWithValue("@t5", strname);
    //        cmd.Parameters.AddWithValue("@t9", Request.Form["Text1"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t10", str1);
    //        int i = cmd.ExecuteNonQuery();
    //    }
    //    catch (Exception ex)
    //    {
    //        Console.Write("", ex);
    //    }
    //    con.Close();
    //}

    //protected void btnSavemcq_Click(object sender, EventArgs e)
    //{
    //    var value = Request.Cookies["Email"].Value;
    //    string str1 = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
    //    string radio = Request.Form["exam2"];
    //    string txtmcq = Request.Form["Text1"];
    //    con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    con.Open();
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + value + "' or Mob='" + value + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        UserEmail = dr["Email"].ToString();
    //    }
    //    dr.Close();
    //    string qry2 = "insert into mcqquery(Ques,opta,optb,optc,optd,corval,exam,subject,picpath,opte,Date,Email) values(@t1,@t2,@t3,@t4,@t5,@t6,@t7,@t8,@t9,@opte,@date,@Email)";
    //    SqlCommand cmd1 = new SqlCommand(qry2, con);
    //    cmd1.Parameters.AddWithValue("@t1", txtmcq);
    //    cmd1.Parameters.AddWithValue("@t2", Request.Form["txta"]);
    //    cmd1.Parameters.AddWithValue("@t3", Request.Form["txtb"]);
    //    cmd1.Parameters.AddWithValue("@t4", Request.Form["txtc"]);
    //    cmd1.Parameters.AddWithValue("@t5", Request.Form["txtd"]);
    //    cmd1.Parameters.AddWithValue("@t6", Request.Form["correctvalmcq"]);
    //    cmd1.Parameters.AddWithValue("@t7", radio);
    //    cmd1.Parameters.AddWithValue("@t8", Request.Form["mcqsub"]);
    //    String strfile2 = "";
    //    if (fileupload2.HasFile)
    //    {
    //        string a = fileupload2.FileName.ToString();
    //        a = a.Replace(" ", "%20");
    //        strfile2 = "/mcqupload/" + a;
    //        fileupload2.PostedFile.SaveAs(Server.MapPath("~/mcqupload/") + a);
    //    }
    //    cmd1.Parameters.AddWithValue("@t9", strfile2);
    //    cmd1.Parameters.AddWithValue("@opte", Request.Form["txte"]);
    //    cmd1.Parameters.AddWithValue("@date", str1);
    //    cmd1.Parameters.AddWithValue("@Email", UserEmail);
    //    //cmd1.Parameters.AddWithValue("@t10", Request.Form["txtmcq1"].TrimEnd().TrimStart());
    //    //cmd1.Parameters.AddWithValue("@t11",str);
    //    int j = cmd1.ExecuteNonQuery();
    //    con.Close();
    //}

    //protected void btninfosave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //    con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    con.Open();
    //    string qry1 = "insert into shareinfo(info,ibank,isubject,picpath) values(@t1,@t2,@t3,@t4)";
    //    SqlCommand cmd2 = new SqlCommand(qry1, con);
    //    cmd2.Parameters.AddWithValue("@t1", Request.Form["txtinfor"].TrimEnd().TrimStart());
    //    cmd2.Parameters.AddWithValue("@t2", Request.Form["optbank"]);
    //    cmd2.Parameters.AddWithValue("@t3", Request.Form["mcqsub"]);
    //    String strfile3 = "";
    //    if (fileupload3.HasFile)
    //    {
    //        strfile3 = "~/infoupload/" + fileupload3.FileName.ToString();
    //        fileupload3.PostedFile.SaveAs(Server.MapPath("~/infoupload/") + fileupload3.FileName.ToString());
    //    }
    //    cmd2.Parameters.AddWithValue("@t4", strfile3);
    //    string qry2 = "insert into sort(shareinfo,Date) values(@t9,@t10)";
    //    SqlCommand cmd = new SqlCommand(qry2, con);
    //    cmd.Parameters.AddWithValue("@t9", Request.Form["txtinfor"].TrimEnd().TrimStart());
    //    cmd.Parameters.AddWithValue("@t10", System.DateTime.Now.ToString());
    //    int i = cmd.ExecuteNonQuery();
    //    int k = cmd2.ExecuteNonQuery();

    //    }catch(Exception ex2)
    //    {
    //        Console.Write("", ex2);
    //    }
    //}

    //[WebMethod]
    //public static string SaveAnswer(string opt, string Email, string id)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        UserEmail = dr["Email"].ToString();
    //    }
    //    dr.Close();
    //    SqlCommand cmd = new SqlCommand("insert into Usermcq(selectedval,Email,Id) values (@selectedval,@Email,@Id)", con);
    //    cmd.Parameters.AddWithValue("@selectedval", opt);
    //    cmd.Parameters.AddWithValue("@Email", UserEmail);
    //    cmd.Parameters.AddWithValue("@Id", id);
    //    int Result = cmd.ExecuteNonQuery();
    //    if (Result == 1)
    //    {
    //        string qry3 = "update b set [status]='passed' FROM mcqquery a inner join Usermcq b on a.Id = b.Id where a.corval=b.selectedval select top 1 case when[status] is null then 0 else 1 end from uservocab order by id desc";
    //        SqlCommand cmd3 = new SqlCommand(qry3, con);
    //        int j = Convert.ToInt32(cmd3.ExecuteScalar());
    //        returnvalue = j == 1 ? "true" : "false";
    //    }
    //    con.Close();
    //    return returnvalue;
    //}


    //[WebMethod]
    //public static string Savepost(string Email, string id)
    //{
    //    string returnvalue = "False";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd1 = new SqlCommand("select * from Savepost where Email='" + UserEmail + "' and Askid =" + id, con);
    //    SqlDataReader dr = cmd1.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        returnvalue = "False";
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("insert into Savepost(Askid,Email) values (@Askid,@Email)", con);
    //        cmd.Parameters.AddWithValue("@Email", UserEmail);
    //        cmd.Parameters.AddWithValue("@Askid", id);
    //        int i = cmd.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "True";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string Savesharepost(string Email, string id)
    //{
    //    string returnvalue = "False";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    SqlCommand cmd1 = new SqlCommand("select * from Savepost where Email='" + Email + "' and shareId =" + id, con);
    //    SqlDataReader dr = cmd1.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("delete from Savepost where shareId=" + id, con);
    //        int i = cmd.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "False";
    //        }
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("insert into Savepost(shareId,Email) values (@shareId,@Email)", con);
    //        cmd.Parameters.AddWithValue("@Email", Email);
    //        cmd.Parameters.AddWithValue("@shareId", id);
    //        int i = cmd.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "True";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string btnsavemcqpost(string Email, string id)
    //{
    //    string returnvalue = "False";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }

    //    SqlCommand cmd1 = new SqlCommand("select * from Savepost where Email='" + Email + "' and mcqId =" + id, con);
    //    SqlDataReader dr = cmd1.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        returnvalue = "False";
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("insert into Savepost(mcqId,Email) values (@mcqId,@Email)", con);
    //        cmd.Parameters.AddWithValue("@Email", Email);
    //        cmd.Parameters.AddWithValue("@mcqId", id);
    //        int i = cmd.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "True";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string getbuttonval(string Email, string followee)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd1 = new SqlCommand("select * from Followers where Followers='" + UserEmail + "' and Username ='" + followee + "'", con);
    //    SqlDataReader dr = cmd1.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        returnvalue = "False";
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("insert into Followers(Username,Followers) values (@Username,@Followers)", con);
    //        cmd.Parameters.AddWithValue("@Username", followee);
    //        cmd.Parameters.AddWithValue("@Followers", UserEmail);
    //        int Result = cmd.ExecuteNonQuery();
    //        if (Result == 1)
    //        {
    //            returnvalue = "true";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}
    //[WebMethod]
    //public static string sharebutton(string Email, string Id)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    SqlCommand cmd1 = new SqlCommand("select * from Share where PostId='" + Id + "' and Followee ='" + Email + "'", con);
    //    SqlDataReader dr = cmd1.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        returnvalue = "False";
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd = new SqlCommand("insert into Share(PostId,Followee) values (@PostId,@Followee)", con);
    //        cmd.Parameters.AddWithValue("@PostId", Id);
    //        cmd.Parameters.AddWithValue("@Followee", Email);
    //        int Result = cmd.ExecuteNonQuery();
    //        if (Result == 1)
    //        {
    //            returnvalue = "true";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}
    //[WebMethod]
    //public static string btnlikesharepost(string Email, string id)
    //{
    //    string returnvalue = "False";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    SqlCommand cmd = new SqlCommand("select * from asklike where Email=@t1 and shareinfo=@t2", con);
    //    cmd.Parameters.AddWithValue("@t1", Email);
    //    cmd.Parameters.AddWithValue("@t2", id);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        dr.Close();
    //        SqlCommand cmd1 = new SqlCommand("delete from asklike where Email=@t3 and shareinfo=@t4", con);
    //        cmd1.Parameters.AddWithValue("@t4", id);
    //        cmd1.Parameters.AddWithValue("@t3", Email);
    //        int i = cmd1.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "False";
    //        }
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd2 = new SqlCommand("insert into asklike(Email,shareinfo) values(@t3,@t4)", con);
    //        cmd2.Parameters.AddWithValue("@t3", Email);
    //        cmd2.Parameters.AddWithValue("@t4", id);
    //        int i = cmd2.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            returnvalue = "True";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}
    //[WebMethod]
    //public static string replycomment(string txt, string Email, string commentId)
    //{
    //    string returnvalue = "False";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd = new SqlCommand("insert into ChildCommentTable values(@Email,@CommentMessage,@CommentDate,@commentId)", con);
    //    cmd.Parameters.AddWithValue("@Email", UserEmail);
    //    cmd.Parameters.AddWithValue("@CommentMessage", txt);
    //    cmd.Parameters.AddWithValue("@CommentDate", System.DateTime.Now.ToString());
    //    cmd.Parameters.AddWithValue("@commentId", commentId);
    //    int i = cmd.ExecuteNonQuery();
    //    if (i == 1)
    //    {
    //        returnvalue = "True";
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string commentlike(string comid, string Email)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd = new SqlCommand("select * from comlike where Email=@t1 and comId=@t2", con);
    //    cmd.Parameters.AddWithValue("@t1", UserEmail);
    //    cmd.Parameters.AddWithValue("@t2", comid);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        dr.Close();
    //        SqlCommand cmd1 = new SqlCommand("delete from comlike where Email=@t3 and comId=@t4", con);
    //        cmd1.Parameters.AddWithValue("@t4", comid);
    //        cmd1.Parameters.AddWithValue("@t3", UserEmail);
    //        int i = cmd1.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            SqlCommand comd = new SqlCommand("select count (*) as likecount from comlike where comId='" + comid + "'", con);
    //            SqlDataReader dr2 = comd.ExecuteReader();
    //            dr2.Read();
    //            if (dr2.HasRows)
    //            {
    //                var val1 = dr2["likecount"].ToString();
    //                returnvalue = val1 + " votes";
    //            }
    //            dr2.Close();
    //        }
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd3 = new SqlCommand("insert into comlike(Email,comId) values(@t3,@t4)", con);
    //        cmd3.Parameters.AddWithValue("@t3", Email);
    //        cmd3.Parameters.AddWithValue("@t4", comid);
    //        int i = cmd3.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            SqlCommand comd = new SqlCommand("select count (*) as count from comlike where comId='" + comid + "'", con);
    //            SqlDataReader dr2 = comd.ExecuteReader();
    //            dr2.Read();
    //            if (dr2.HasRows)
    //            {
    //                var val1 = dr2["count"].ToString();
    //                returnvalue = val1 + " votes";
    //            }
    //            dr2.Close();
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string Likepost(string Email, string id)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd = new SqlCommand("select * from asklike where Email=@t1 and postId=@t2", con);
    //    cmd.Parameters.AddWithValue("@t1", UserEmail);
    //    cmd.Parameters.AddWithValue("@t2", id);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        dr.Close();
    //        SqlCommand cmd1 = new SqlCommand("delete from asklike where Email=@t3 and postId=@t4", con);
    //        cmd1.Parameters.AddWithValue("@t4", id);
    //        cmd1.Parameters.AddWithValue("@t3", UserEmail);
    //        int i = cmd1.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            SqlCommand comd = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con);
    //            SqlDataReader dr2 = comd.ExecuteReader();
    //            dr2.Read();
    //            if (dr2.HasRows)
    //            {
    //                var val1 = dr2["count"].ToString();
    //                returnvalue = val1 + "votes ";
    //            }
    //            dr2.Close();
    //            SqlCommand comd2 = new SqlCommand("select count (*) as countcom from ask where Id='" + id + "'", con);
    //            SqlDataReader dr3 = comd2.ExecuteReader();
    //            dr3.Read();
    //            if (dr3.HasRows)
    //            {
    //                var val2 = dr3["countcom"].ToString();
    //                returnvalue += val2 + "comments";
    //            }
    //        }
    //    }
    //    else
    //    {
    //        dr.Close();
    //        SqlCommand cmd3 = new SqlCommand("insert into asklike(Email,postId) values(@t3,@t4)", con);
    //        cmd3.Parameters.AddWithValue("@t3", UserEmail);
    //        cmd3.Parameters.AddWithValue("@t4", id);
    //        int i = cmd3.ExecuteNonQuery();
    //        if (i == 1)
    //        {
    //            SqlCommand comd = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con);
    //            SqlDataReader dr2 = comd.ExecuteReader();
    //            dr2.Read();
    //            if (dr2.HasRows)
    //            {
    //                var val1 = dr2["count"].ToString();
    //                returnvalue = val1 + "votes ";
    //            }
    //            dr2.Close();
    //            SqlCommand comd2 = new SqlCommand("select count (*) as countcom from ask where Id='" + id + "'", con);
    //            SqlDataReader dr3 = comd2.ExecuteReader();
    //            dr3.Read();
    //            if (dr3.HasRows)
    //            {
    //                var val2 = dr3["countcom"].ToString();
    //                returnvalue += val2 + "comments";
    //            }
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static string comment(string Email, string id, string textcom)
    //{
    //    string returnvalue = "";
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + Email + "' or Mob='" + Email + "'", con);
    //    SqlDataReader drs = cmd2.ExecuteReader();
    //    drs.Read();
    //    if (drs.HasRows)
    //    {
    //        UserEmail = drs["Email"].ToString();
    //    }
    //    drs.Close();
    //    SqlCommand cmd = new SqlCommand("Insert into ask(Email, Comments, CommentDate,Id) values(@Username, @CommentMessage, @date,@Id)", con);
    //    cmd.Parameters.AddWithValue("@Username", UserEmail);
    //    cmd.Parameters.AddWithValue("@CommentMessage", textcom);
    //    cmd.Parameters.AddWithValue("@date", System.DateTime.Now.ToString());
    //    cmd.Parameters.AddWithValue("@Id", id);
    //    int i = cmd.ExecuteNonQuery();
    //    if (i == 1)
    //    {
    //        SqlCommand comd = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con);
    //        SqlDataReader dr2 = comd.ExecuteReader();
    //        dr2.Read();
    //        if (dr2.HasRows)
    //        {
    //            var val1 = dr2["count"].ToString();
    //            returnvalue = val1 + "votes";
    //        }
    //        dr2.Close();
    //        SqlCommand comd2 = new SqlCommand("select count (*) as countcom from ask where Id='" + id + "'", con);
    //        SqlDataReader dr3 = comd2.ExecuteReader();
    //        dr3.Read();
    //        if (dr3.HasRows)
    //        {
    //            var val2 = dr3["countcom"].ToString();
    //            returnvalue += val2 + "comments";
    //        }
    //    }
    //    con.Close();
    //    return returnvalue;
    //}

    //[WebMethod]
    //public static List<ask> show(string id)
    //{
    //    DataTable dt = new DataTable();
    //    List<ask> objdept = new List<ask>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("select TOP 1 * from ask where Id='" + id + "' order by commentId desc", con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    objdept.Add(new ask
    //                    {
    //                        Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
    //                        Comments = dt.Rows[i]["Comments"].ToString(),
    //                        commentId = Convert.ToInt16(dt.Rows[i]["commentId"].ToString()),
    //                        Email = dt.Rows[i]["Email"].ToString(),
    //                        CommentDate = dt.Rows[i]["CommentDate"].ToString()
    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept;
    //        }
    //    }
    //}


    public void text()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Top 1 * from AboutMaster", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["About"].ToString();
                Literal1.Text = System.Net.WebUtility.HtmlDecode(str);
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        finally
        {
            con.Close();
        }
    }

    public void text1()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Top 1 * from FeatureMaster", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["Feature"].ToString();
                Literal2.Text = System.Net.WebUtility.HtmlDecode(str);
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        finally
        {
            con.Close();
        }
    }
}
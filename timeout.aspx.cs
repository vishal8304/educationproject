﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class timeout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["index"] != null)
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da=new SqlDataAdapter("SELECT distinct section, COUNT(status) as statuscount FROM onusertest where status = 'passed' and Email = '" + Request.Cookies["Email"].Value + "' GROUP BY status,section", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                rptsection.Visible = true;
                rptsection.DataSource = dt;
                rptsection.DataBind();
                SqlCommand cmd = new SqlCommand("select COUNT(Email) from onusertest where Email='" + Request.Cookies["Email"].Value + "' group by Email", con);
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    lblscore.Text = count.ToString()+"Questions";
                lblscoretext.Visible = true;
                lblscore.Visible = true;
                lbltimeout.Visible = false;
                lbltest.Visible = true;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("ssc");
                Session.Remove("Bank");
                Session.Remove("Current");
            }
            else if (Session["Quiz"] != null)
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter("SELECT distinct s.name, COUNT(b.status) as statuscount FROM onlinetest b inner join test s on s.Id=b.Id where b.status = 'passed' and b.Email ='" + Request.Cookies["Email"].Value + "' GROUP BY b.status,s.name", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                rptsection.Visible = true;
                rptsection.DataSource = dt;
                rptsection.DataBind();
                SqlCommand cmd = new SqlCommand("select COUNT(Email) from onusertest where Email='" + Request.Cookies["Email"].Value + "' group by Email", con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                lblscore.Text = count.ToString() + "Questions";

                lblscoretext.Visible = true;
                lblscore.Visible = true;
                lbltimeout.Visible = false;
                lbltest.Visible = true;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("Bank");
                Session.Remove("Current");
            }
            else if (Session["ssc"] != null)
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd1 = new SqlCommand("SELECT COUNT(status) as statuscount FROM sscuser where status = 'passed' and Email = '" + Request.Cookies["Email"].Value + "' GROUP BY status", con);
                int status = Convert.ToInt32(cmd1.ExecuteScalar());
                lblcount.Text = status.ToString() +"Questions";
                SqlCommand cmd = new SqlCommand("select COUNT(Email) from sscuser where Email='" + Request.Cookies["Email"].Value + "' group by Email", con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                lblscore.Text = count.ToString() + "Questions";
                lblscoretext.Visible = true;
                lblscore.Visible = true;
                lbltimeout.Visible = false;
                lbltest.Visible = true;
                lblcount.Visible = true;
                lblstatuscount.Visible = true;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("ssc");
                Session.Remove("Bank");
                Session.Remove("Current");
                rptsection.Visible = false;
            }
            else if (Session["Bank"] != null)
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd1 = new SqlCommand("SELECT COUNT(status) as statuscount FROM userbank where status = 'passed' and Email = '" + Request.Cookies["Email"].Value + "' GROUP BY status", con);
                int status = Convert.ToInt32(cmd1.ExecuteScalar());
                lblcount.Text = status.ToString() + "Questions";
                SqlCommand cmd = new SqlCommand("select COUNT(Email) from userbank where Email='" + Request.Cookies["Email"].Value + "' group by Email", con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                lblscore.Text = count.ToString() + "Questions";
                lblscoretext.Visible = true;
                lblscore.Visible = true;
                lbltimeout.Visible = false;
                lbltest.Visible = true;
                lblcount.Visible = true;
                lblstatuscount.Visible = true;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("ssc");
                Session.Remove("Bank");
                Session.Remove("Current");
                rptsection.Visible = false;
            }
            else if (Session["Current"] != null)
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd1 = new SqlCommand("SELECT COUNT(status) as statuscount FROM usercaffair where status = 'passed' and Email = '" + Request.Cookies["Email"].Value + "' GROUP BY status", con);
                int status = Convert.ToInt32(cmd1.ExecuteScalar());
                lblcount.Text = status.ToString() + "Questions";
                SqlCommand cmd = new SqlCommand("select COUNT(Email) from usercaffair where Email='" + Request.Cookies["Email"].Value + "' group by Email", con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                lblscore.Text = count.ToString() + "Questions";
                lblscoretext.Visible = true;
                lblscore.Visible = true;
                lbltimeout.Visible = false;
                lbltest.Visible = true;
                lblcount.Visible = true;
                lblstatuscount.Visible = true;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("ssc");
                Session.Remove("Bank");
                Session.Remove("Current");
                rptsection.Visible = false;
            }
            else if (Session["timeup"] != null)
            {
                lbltimeout.Visible = true;
                lbltest.Visible = false;
                rptsection.Visible = false;
                lblscore.Visible = false;
                lblscoretext.Visible = false;
                Session.Remove("index");
                Session.Remove("timeup");
                Session.Remove("ssc");
                Session.Remove("Bank");
                Session.Remove("Current");
            }
        }
    }
}
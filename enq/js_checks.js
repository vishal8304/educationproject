// JavaScript Document

String.prototype.trim = function() 
{ return this.replace(/^\s+|\s+$/, ''); }

Array.prototype.unique =
  function() {
    var a = [];
    var l = this.length;
    for(var i=0; i<l; i++) {
      for(var j=i+1; j<l; j++) {
        // If this[i] is found later in the array
        if (this[i] === this[j])
          j = ++i;
      }
      a.push(this[i]);
    }
    return a;
  };

Array.prototype.destroy = function(obj){
    // Return null if no objects were found and removed
    var destroyed = null;

    for(var i = 0; i < this.length; i++){

        // Use while-loop to find adjacent equal objects
        while(this[i] === obj){

            // Remove this[i] and store it within destroyed
            destroyed = this.splice(i, 1)[0];
        }
    }

    return destroyed;
}

String.prototype.capitalize = function()
{
   return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
}

function getid(elmnt)
{
	try 
	{ 
		return document.getElementById(elmnt); 
	}catch(e) 
	{ return null; }
}


function popup(URL,wd,ht) 
{
eval("page" + " = window.open(URL, '"  + "', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=0,width='+wd+',height='+ht+',left = NaN,top = 0');");
}

function disable_b(elem_id)
{
	getid(elem_id).style.visible = false;	 
	$("#submit_b").switchClass("button_blue","loading", 10 );	
}

function popup2(URL) 
{
	if (parseInt(navigator.appVersion)>3) {
	 if (navigator.appName=="Netscape") {
	  winW = window.innerWidth;
	  winH = window.innerHeight;
	 }
	 
	 if (navigator.appName.indexOf("Microsoft")!=-1) {
	  winW = document.body.offsetWidth;
	  winH = document.body.offsetHeight;
	 }
	}    	// alert(winW +"_"+ winH);
	if(winH<400)	winH=600;
 eval("page" + " = window.open(URL, '"  + "', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1,width='+winW+',height='+winH+',left = NaN,top = 0');");
}

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

// onkeypress="return number(event);"
function number(e){
  var key;
  var keychar;
  if(window.event){
    key = window.event.keyCode;
  }else if (e){
    key = e.which;
  }
  else
    return true;
  if((key == 8) || (key == 0))
    return true;
 //alert(key)	;
  keychar = String.fromCharCode(key);
  keychar = keychar.toLowerCase();
  if(((key > 47) && (key < 58)) || (key==46))
  {
    return true;
  }else
      return false;
}

function toggleMe(a){
  var e=document.getElementById(a);
  if(!e)return true;
  if(e.style.display=="none"){
    e.style.display="block"
  } else {
    e.style.display="none"
  }
  return true;
}


function checkall(elem_id,name,total)
{		
	var total=parseInt(total);
	if(total>0)
	{
		if(getid(elem_id).checked==true)
		{	for(a=1;a<=total;a++)		getid(name+a).checked=true;			
		}
		else
		{	for(a=1;a<=total;a++)		getid(name+a).checked=false;
		}		
	}
}


function selection(status,name,total)
{		
	if(total>0)
	{
		if(status=='select')
		{	for(a=1;a<=total;a++)		getid(name+a).checked=true;			
		}
		else if(status=='deselect')
		{	for(a=1;a<=total;a++)		getid(name+a).checked=false;
		}		
	}
}


function check_box_value(elem_id,elem_val,elem_str,max_val,hid_id,hid_nm,disp_id,box_name)
{
	// Get Previous values 		
	var hid_id_str=getid(hid_id).value.trim();  	var hid_nm_str=getid(hid_nm).value.trim();
	var hid_id_arr=Array(); 						var hid_nm_arr=Array();
	
	if(hid_id_str!='') {
		hid_id_arr=hid_id_str.split(",");		var hid_nm_arr=hid_nm_str.split(",");
		hid_id_arr=hid_id_arr.unique();			hid_id_arr=hid_id_arr.unique();
		// Make them unique	
	}
	//alert(hid_id_arr.length);
	
	if(hid_id_arr.length<max_val || getid(elem_id).checked==false)
	{		
		if(getid(elem_id).checked==true)	
		{
			hid_id_arr.push(elem_val);		hid_nm_arr.push(elem_str); 
		}
		else if(getid(elem_id).checked==false)	
		{			
			hid_id_arr.destroy(elem_val); 	hid_nm_arr.destroy(elem_str); 
		}	
		
		hid_id_str=hid_id_arr.toString();      hid_nm_str=hid_nm_arr.toString();
		
		getid(hid_id).value=hid_id_str;	   getid(hid_nm).value=hid_nm_str;	

		if(hid_nm_str!='')	{	// disp_content(disp_id,'block');	 
			var box_top='<div class="div_box"><div class="box_top"> <a onclick="de_selection(\''+box_name+'\',\''+hid_id+'\',\''+hid_nm+'\',\''+disp_id+'\');">Remove All</a> </div>';
			var box_bot='</div>';				
			getid(disp_id).innerHTML=box_top+hid_nm_str+box_bot;			
			$('#'+disp_id).show( "drop",300 );
		}
		else 	{	$('#'+disp_id).hide( "drop",300 );	 	}
	
	} else { getid(elem_id).checked=false;  }
}

function de_selection(box_id,select_id,select_nm,disp_id)
{
	var al;	 var hid_id_str=getid(select_id).value;
	if(hid_id_str!='')	{	hid_id_arr=hid_id_str.split(",");  var al=hid_id_arr.length;	
							for(var a=0;a<al;a++)	 getid(box_id+'_'+hid_id_arr[a]).checked=false;	
							$('#'+disp_id).hide( "drop",300 ); 	getid(select_id).value='';	   getid(select_nm).value='';	
	}
}

function chkmail(elem)
{
	elem=elem.trim();
	var testresults=true;
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
	if(!filter.test(elem.trim()))  		testresults=false;		    
	return(testresults);
}

 // Refresh the parent page After Closing.
function closewin()
{
	window.close();
	if (window.opener && !window.opener.closed) 
		window.opener.location.reload();
	 
}
function fresh()
{
	<!-- Codes by Quackit.com -->
	window.location.reload(true);

}
function disp_content(matterid,idprop)
{	 
	if(getid(matterid)) getid(matterid).style.display=idprop;
}

function show_row(f,l,elem_id)
{		 
	var first=parseInt(f); var last=parseInt(l); var a;
	for(a=first;a<=last;a++)
	{		 
		disp_content(elem_id+a,'block');	
	}
}

function alphanumber(e){
  var key;
  var keychar;
  if (window.event){
    key = window.event.keyCode;
  }else if (e){
    key = e.which;
  }
  else
    return true;
  if((key == 8) || (key == 0))
    return true;
  keychar = String.fromCharCode(key);
  keychar = keychar.toLowerCase();

  if(key!=32){
    invalids = "`~@#$%^*-()_+=\|{}[].:;'\"<>&?/!,\\";
      for(i=0; i< invalids.length; i++){
      if(keychar != 0){
        if(keychar.indexOf(invalids.charAt(i)) >= 0 || keychar==false){
          return false;
        }
      }
      }
  }
  return true;
}


function validate_editor(form) {  
	for (var i in CKEDITOR.instances)
    {
            CKEDITOR.instances[i].updateElement();
    }	
    $('.ckeditor').toggle();
    validated = $(form).validationEngine('validate');	
    $('.ckeditor').toggle();
    return validated;
}


function imageformat(file) 
{
   extArray = new Array(".jpg", ".jpeg", ".gif", ".png");
   fnm=file.value;
	
	if( (fnm.lastIndexOf(".jpg")==-1) &&  (fnm.lastIndexOf(".jpeg")==-1) &&  (fnm.lastIndexOf(".gif")==-1)  &&  (fnm.lastIndexOf(".png")==-1) )
	{
   		alert("Please upload only Image files i.e (.jpg, .jpeg, .gif, .png) extention format.\nUpload Again");
	  		return false;
	}
	else
	  		return true;
}


function check_image_file(field, rules, i, options)
{	
    fnm=field.val();	
	if( (fnm.lastIndexOf(".jpg")==-1) &&  (fnm.lastIndexOf(".jpeg")==-1) &&  (fnm.lastIndexOf(".gif")==-1)  &&  (fnm.lastIndexOf(".png")==-1) )
	{
   		return "Please upload only Image files i.e (.jpg, .jpeg, .gif, .png) extention format."; 
	}
}

function check_resume_file(field, rules, i, options)
{	
    fnm=field.val();	
	if( (fnm.lastIndexOf(".doc")==-1) &&  (fnm.lastIndexOf(".docx")==-1))
	{
   		return "Upload Word files i.e with extensions (.doc, .docx )"; 
	}
}

function check_radio(btn,err_id,msg) 
{
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) 
	{
        if (btn[i].checked) 
		{			cnt = i; i = -1;		}
    }
	
	// alert(cnt);
	if(cnt==-1) 
	{
		eid=getid(err_id);
		eid.innerHTML=" <div class='errortext'> "+msg+"  not selected. </div> ";		
		return false;
	}
	else
	{	eid=getid(err_id);		eid.innerHTML="";				return true;		}
	
}


function textCounter(textarea_box,counter_disp,maxlimit) 
{
	getid(counter_disp).style.display='block';
	var data=getid(textarea_box).value.trim();
	var len = data.length;
	var left=0;
	left=maxlimit-len;
	
	if(left<=0)
	{
        data = data.substring(0,maxlimit);
		len = data.length;
		left=maxlimit-len;
		getid(textarea_box).value=data;
		getid(counter_disp).innerHTML='<label class="counter_box_red"><b>'+left+'</b> remaining. Total entered <b>'+maxlimit+'</b> characters. </label>';
        return false;
	}
	else
	{
			getid(counter_disp).innerHTML='<b>'+left+'</b> characters remaining. ';
	}
	
	if(len==0)	getid(counter_disp).style.display='none';	 
}


function ajax_call(req_type,req_page,req_param,display_id)
{
    var http2 = false;  var rmsg=0;
	if(navigator.appName == "Microsoft Internet Explorer")   http2 = new ActiveXObject("Microsoft.XMLHTTP"); 
	else 				  http2 = new XMLHttpRequest();
  
  http2.abort();
  http2.open(req_type, req_page+"?"+req_param, true);
  http2.onreadystatechange=function() {
    if(http2.readyState == 4) {  rmsg=1;
      document.getElementById(display_id).innerHTML = http2.responseText;
	  
    }
	else
	{
		document.getElementById(display_id).innerHTML = '&nbsp; <img src="loader.gif" alt="Loading.." border="0" align="absmiddle" width="20"/>';
	}
  }
  http2.send(null);
  return rmsg;
} 



/* */////////////////////////////////////////////////////////////////////////
function getRadioVal(form, name) {
    var val;
    // get list of radio buttons with specified name
    var radios = form.elements[name];
    
    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}

function show_poll_display(disp_id, status) {
		if(disp_id!='')   ajax_call('GET','backend-query.php','req_param=Get_Poll&&status='+status,disp_id);	
}

function cast_vote(pollid, status) {
	var val = getRadioVal( document.getElementById('form1'), 'poll_val' );
	 
	if(val==undefined) alert("Choose an Option ");
	else {
			ajax_call('GET','backend-query.php','req_param=Cast_Poll&&pollid='+pollid+'&&val='+val+'&&status='+status,'poll_display');		
	}		 
} 

function show_poll_results(disp_id, pollid, status) {
		if(disp_id!='')   ajax_call('GET','backend-query.php','req_param=Cast_Poll&&pollid='+pollid+'&&status='+status,disp_id);	
}

function show_stats(disp_id) {
		if(disp_id!='')   ajax_call('GET','backend-query.php','req_param=Stats',disp_id);	
}

function post_comment() {
 	var name=getid('cname').value;  var email=getid('cemail').value;  var comment=getid('ccomment').value;  var news_id=getid('news_id').value;
	
	jq_ajax('GET','backend-query.php','req_param=Post_Comment&&name='+name+'&&email='+email+'&&comment='+comment+'&&news_id='+news_id,'comment_msg','comment_form');  
}

function jq_ajax(req_type,req_page,req_data,display_id,form_id='') {
	 
	var request = $.ajax({ 
		// The URL for the request
		url:  req_page , 
		data: req_data , 
		type: "GET" , 
		dataType : "html",
	})
	   
	  request.done(function( json ) { 
		 var j = jQuery.parseJSON( json); 
		 $("#"+display_id).html(j.message);
		 if(j.status==1)  $("#"+form_id)[0].reset();
	  })
	   
	  request.fail(function( xhr, status, errorThrown ) { 
		/*console.log( "Error: " + errorThrown );
		console.log( "Status: " + status );
		console.dir( xhr );*/
	  })
	  // Code to run regardless of success or failure;
	  /* always(function( xhr, status ) {
		alert( "The request is complete!" );
	  }); */
}
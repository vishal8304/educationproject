﻿<%@ WebHandler Language="C#" Class="FileUploadHandler" %>

using System;
using System.Web;
using System.IO;

public class FileUploadHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context)
    {
        string val = "";
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname = context.Server.MapPath("~/comments/" + file.FileName);
                file.SaveAs(fname);
                val = "true";
            }
        }

        //context.Response.ContentType = "text/json";
        //context.Response.Write(val);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}
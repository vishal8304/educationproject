﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

public partial class Master2 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["Email"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void lbtnLogout_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["Email"] != null)
            {
                Response.Cookies["Email"].Expires = DateTime.Now.AddDays(-1);
            }
            Response.Redirect("Index.aspx");
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
     }
    
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        //Session["Search"] = Search.Text;
        Response.Redirect("Search.aspx");
    }
    //protected void btnSavemcq_Click(object sender, EventArgs e)
    //{
    //    var value = Request.Cookies["Email"].Value;
    //    string str1 = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
    //    string radio = Request.Form["exam2"];
    //    string txtmcq = Request.Form["Text1"];
    //   SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    con.Open();
    //    var UserEmail = "";
    //    SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + value + "' or Mob='" + value + "'", con);
    //    SqlDataReader dr = cmd2.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        UserEmail = dr["Email"].ToString();
    //    }
    //    dr.Close();
    //    string qry2 = "insert into mcqquery(Ques,opta,optb,optc,optd,corval,exam,subject,picpath,opte,Date,Email) values(@t1,@t2,@t3,@t4,@t5,@t6,@t7,@t8,@t9,@opte,@date,@Email)";
    //    SqlCommand cmd1 = new SqlCommand(qry2, con);
    //    cmd1.Parameters.AddWithValue("@t1", txtmcq);
    //    cmd1.Parameters.AddWithValue("@t2", Request.Form["txta"]);
    //    cmd1.Parameters.AddWithValue("@t3", Request.Form["txtb"]);
    //    cmd1.Parameters.AddWithValue("@t4", Request.Form["txtc"]);
    //    cmd1.Parameters.AddWithValue("@t5", Request.Form["txtd"]);
    //    cmd1.Parameters.AddWithValue("@t6", Request.Form["correctvalmcq"]);
    //    cmd1.Parameters.AddWithValue("@t7", radio);
    //    cmd1.Parameters.AddWithValue("@t8", Request.Form["mcqsub"]);
    //    String strfile2 = "";
    //    if (fileupload2.HasFile)
    //    {
    //        string a = fileupload2.FileName.ToString();
    //        a = a.Replace(" ", "%20");
    //        strfile2 = "/mcqupload/" + a;
    //        fileupload2.PostedFile.SaveAs(Server.MapPath("~/mcqupload/") + a);
    //    }
    //    cmd1.Parameters.AddWithValue("@t9", strfile2);
    //    cmd1.Parameters.AddWithValue("@opte", Request.Form["txte"]);
    //    cmd1.Parameters.AddWithValue("@date", str1);
    //    cmd1.Parameters.AddWithValue("@Email", UserEmail);
    //    //cmd1.Parameters.AddWithValue("@t10", Request.Form["txtmcq1"].TrimEnd().TrimStart());
    //    //cmd1.Parameters.AddWithValue("@t11",str);
    //    int j = cmd1.ExecuteNonQuery();
    //    con.Close();
    //}

    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    var value = Request.Cookies["Email"].Value;
    //    try
    //    {
    //        string str1 = System.DateTime.Now.ToString();
    //        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //        con.Open();
    //        var UserEmail = "";
    //        string strname = "";
    //        SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + value + "' or Mob='" + value + "'", con);
    //        SqlDataReader dr = cmd2.ExecuteReader();
    //        dr.Read();
    //        if (dr.HasRows)
    //        {
    //            UserEmail = dr["Email"].ToString();
    //        }
    //        dr.Close();
    //        string qry = "insert into Askquery(Post,exam,subject,Email,picpath) values(@t1,@t2,@t3,@t4,@t5) insert into sort(askquery,[Date]) values(@t9,@t10)";
    //        SqlCommand cmd = new SqlCommand(qry, con);
    //        cmd.Parameters.AddWithValue("@t1", Request.Form["Text1"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t2", Request.Form["exam"]);
    //        cmd.Parameters.AddWithValue("@t3", Request.Form["sub"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t4", UserEmail);
    //        if (FileUpload1.HasFile)
    //        {
    //            strname = "/upload/" + FileUpload1.FileName.ToString();
    //            FileUpload1.PostedFile.SaveAs(Server.MapPath("/upload/") + FileUpload1.FileName.ToString());
    //        }
    //        cmd.Parameters.AddWithValue("@t5", strname);
    //        cmd.Parameters.AddWithValue("@t9", Request.Form["Text1"].TrimEnd().TrimStart());
    //        cmd.Parameters.AddWithValue("@t10", str1);
    //        int i = cmd.ExecuteNonQuery();
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        Console.Write("", ex);
    //    }

    //}

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["Email"] != null)
            {
                Response.Cookies["Email"].Expires = DateTime.Now.AddDays(-1);
            }
            Response.Redirect("Index.aspx");
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
}

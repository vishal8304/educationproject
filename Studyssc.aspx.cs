﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.Services;

public partial class Studyssc : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindssc();
        }
    }

    public void bindssc()
    {
        var section = Request.QueryString["section"];
        if(section== "Quantitative Aptitude" || section==""|| section== null) { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from SSC where section='Quantitative Aptitude' group by Chapter,sscId,section,Topic,content order by sscId", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        }
        else if (section == "English")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select * from SSC where section='English' group by Chapter,sscId,section,Topic,content order by sscId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        else if (section == "Reasoning")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select * from SSC where section='Reasoning' group by Chapter,sscId,section,Topic,content order by sscId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        //else if(section=="General Studies")
        //{
        //    if (con.State == ConnectionState.Closed)
        //    {
        //        con.Open();
        //    }
        //    SqlDataAdapter da = new SqlDataAdapter("select * from SSC where section='General Studies' group by Chapter,sscId,section,Topic,content order by sscId", con);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    Repeater1.DataSource = dt;
        //    Repeater1.DataBind();
        //}


    }

    public void GeneralStudies()
    {

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from SSC where section='General Studies' group by Chapter,sscId,section,Topic,content order by sscId", con);


    }


}
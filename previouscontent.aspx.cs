﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
public partial class previouscontent : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindssccontent();
        }
    }
    public void bindssccontent()
    {
        try { 
        var a = Request.QueryString["exam"];
        var b= Request.QueryString["bankexam"];
        if (a != null && b==null)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("select top 1 * from previousppr where exam=@t1 order by Id desc", con);
            cmd1.Parameters.AddWithValue("@t1", a);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            dr1.Read();
            if (dr1.HasRows)
            {
                string str = dr1["content"].ToString();
                literal2.Text = System.Net.WebUtility.HtmlDecode(str);
            }
            dr1.Close();
        }

        if (b != null && a==null)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("select top 1 * from previousppr where exam=@t1 order by Id desc", con);
            cmd1.Parameters.AddWithValue("@t1", b);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            dr1.Read();
            if (dr1.HasRows)
            {
                string str = dr1["content"].ToString();
                literal2.Text = System.Net.WebUtility.HtmlDecode(str);
            }
            dr1.Close();
        }
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }
   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Jobs : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //connect();
            //grid();
            rpttiles();
            repeatAdmit();
            repeatResult();
            repeatJobs();
        }
    }
    //Jobs Section Start
    public void repeatAdmit() //For Admit Card Repeater
    {
        try {
            if (con != null && con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select * from NewJob where Section = 'Admit Card'", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                Repeater1.DataSource = sdr;
                Repeater1.DataBind();
            }
        }
        catch(Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }

    public void repeatResult() //For Result Repeater
    {
        try
        {
            if (con != null && con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select * from NewJob where Section = 'Result'", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                Repeater2.DataSource = sdr;
                Repeater2.DataBind();
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }

    public void repeatJobs() //For Jobs Repeater
    {
        try
        {
            if (con != null && con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select * from NewJob where Section = 'New Job'", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                Repeater3.DataSource = sdr;
                Repeater3.DataBind();
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }
    //End Job Section

    //Tiles Repeater
    public void rpttiles()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select * from Jobs where tile is not null", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rptjobs.DataSource = dt;
            rptjobs.DataBind();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }




}
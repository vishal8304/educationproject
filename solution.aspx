﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="solution.aspx.cs" Inherits="solution" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style>
    body {
        font-family: Arial;
    }

    .wraps {
        word-wrap: break-word;
    }

    .btn {
        background: #c3c3c3;
        color: white;
    }
    /* Style the tab */
    .tab {
        overflow: hidden;
        background-color: #f1f1f1;
    }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current tablink class */
            .tab button.active {
                background-color: #ccc;
            }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
    <style type="text/css">
        .btns {
            background-color: black;
            color: white;
        }


        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12" style="text-align:center">
<div class="tab" style="margin-top: 7%;">
 
    <%--perfomance Button--%>
  <input type="button" class="tablinks btn" onclick="openCity(event, 'London')" value="My Performance" style="background-color:palevioletred;"/>
    <%--Solution --%>
  <input type="button" class="tablinks btn" onclick="openCity(event, 'Paris')"  value="Solution" style="background-color:palevioletred"/>
  <%--Feedback --%>
  <input type="button" class="tablinks btn" onclick="openCity(event, 'Tokyo')" value="Feedback" style="background-color:palevioletred"/>
</div>
            
</div>
    </div>
<div id="London" class="tabcontent">
  <h3 style=" text-align: center;
    margin-top: 2%;"">Performance</h3>
  <div class="row" style="margin-left: 18%; margin-top: 2%;">
        <div class="col-md-4" style="text-align:center">
        
        </div>
      <div class="col-md-2" style="text-align:center">
          <p>OVERALL SCORE</p>
      </div>
      <div class="col-md-2" style="text-align:center">
          <p>RANK</p>
      </div>
        <div class="col-md-4" style="text-align:center">
        
        </div>
  </div>
    <div class="row" style="margin-left: 18%; margin-top: 2%;">
        <div class="col-md-4" style="text-align:center">
        
        </div>
        <div class="col-md-2" style="text-align:center">
              <asp:Label ID="lblscore" runat="server"></asp:Label>
        </div>
        <div class="col-md-2" style="text-align:center">
            <asp:Label ID="lblrank" runat="server" ></asp:Label>
        </div>
           <div class="col-md-4" style="text-align:center">
        
        </div>
        
    </div>
</div>

<div id="Paris" class="tabcontent">
 <div class="row">
               
                <div class="col-md-12">
                     <asp:Panel ID="pnlsol" ClientIDMode="Static" runat="server" BorderStyle="Solid"><br />
                    </asp:Panel> </div>
            </div><br />
</div>

<div id="Tokyo" class="tabcontent"><br />
   <div class="row">
        <div class="col-md-5">
        </div>
        <div class="col-md-2">
          <h3 style="text-align:center">Feedback:</h3>
        </div>
            <div class="col-md-5">
        </div>
  </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <textarea id="txtfeedback" style="width: 611px; height: 62px;" name="txtfeedback"></textarea>
        </div>
     <div class="col-md-4">
        </div>
       
    </div>
    <div class="row">
        <div class="col-md-5">
        </div>
         <div class="col-md-2">
          <input type="button" id="btnsubmit" class="btns" value="Send" style="width:200px" />
              <div id="lblsave" style="color:red" hidden="hidden"></div>
        </div>
            <div class="col-md-5">
        </div>
  </div>
</div>
    <div class="row">
        <div style="margin-left:40%; margin-right:40%;">
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/onlinetiles.aspx">Back to Test Series</asp:LinkButton>
    </div>
        </div>
   
            
      
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    function openCity(evt, cityName) {
        debugger
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
    <script>
        $("#btnsubmit").click(function () {
            debugger
            var current = $(this);
            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            var test = "";
            var decode = "";
            //Start SSC Section
            var set = getUrlVars()["ssconline"];
            if (set != null) {
                test = "SSC Online";
                decode = decodeURI(set);
            }
            var set = getUrlVars()["sscquiz"];
            if (set != null) {
                test = "SSC Quiz";
                decode = decodeURI(set);
            }
            //End SSC Section

            //Start Bank Section
            var banksol = getUrlVars()["Bankonline"];
            if (banksol != null) {
                test = "Bank Online";
                decode = decodeURI(banksol);
            }
            var set = getUrlVars()["bankquiz"];
            if (set != null) {
                test = "Bank Quiz";
                decode = decodeURI(set);
            }
            //End Bank Section


            //Start UPSC Section
            var upscsol = getUrlVars()["upsconline"];
            if (upscsol != null) {
                test = "UPSC Online";
                decode = decodeURI(upscsol);
            }
            var set = getUrlVars()["upscquiz"];
            debugger
            if (set != null) {
                test = "UPSC Quiz";
                decode = decodeURI(set);
            }
            //End UPSC Section

            //Feedback Form 
            var txtfeed = $("#txtfeedback").val();
            var Email = "";
            var cookieArray = document.cookie.split("; ");
            for (var i = 0; i < cookieArray.length; i++) {
                var nameValueArray = cookieArray[i].split("=");
                if (nameValueArray[0] == "Email") {
                    Email = nameValueArray[1];
                }
            }
            $.ajax({

                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "solution.aspx/sendData",
                data: "{'txtfeed':'" + txtfeed + "','test':'" + test + "','decode':'" + decode + "','Email':'" + Email + "'}",
                dataType: "json",
                success: function (result) {
                    debugger
                    if (result.d == 'true') {
                        current.next("#lblsave").show();
                        current.next("#lblsave").text("Sent Successfully");
                    }
                    else {
                        $("#lblsave").show();
                        $("#lblsave").val = "Error Occurred";
                    }
                }
            });
        });
    </script>

   <%--SSC Solution Section--%>
     <script>
       
         var counter = 1;
         $(document).ready(function () {
             debugger
             function getUrlVars() {

                 var vars = [], hash;
                 var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                 for (var i = 0; i < hashes.length; i++) {
                     hash = hashes[i].split('=');
                     vars.push(hash[0]);
                     vars[hash[0]] = hash[1];
                 }
                 return vars;
             }
             var set = getUrlVars()["ssconline"];
             var decode = "";
             if (set != null) {
                 decode = decodeURI(set);
             }
             $.ajax({
                 type: "Post",
                 contentType: "application/Json; Charset=utf-8",
                 url: "solution.aspx/divsol",
                 data: "{'decode':'" + decode + "'}",
                 dataType: "json",
                 success: function (result) {
                     $.each(result.d, function (key, value) {

                         if (value.Question != "" || value.Question != null) {
                             var $label1 = $('<div class="row"></div><br/>').text('Question: ' + value.Question);
                             $('#pnlsol').append($label1);
                         }
                         if (value.descr != "") {
                             var $label = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                             $('#pnlsol').append($label);
                         }
                         counter++;
                     });
                 },
                 error: function ajaxError(result) {
                 }
             });
         });
   </script>
    <%--Bank Solution Section--%>
    <script>
        var counter = 1;
        $(document).ready(function () {
            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }
            var decodebank = "";
            var banksol = getUrlVars()["Bankonline"];
            if (banksol != null) {
                decodebank = decodeURI(banksol);
            }
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "solution.aspx/divbanksol",
                data: "{'decodebank':'" + decodebank + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        if (value.Question != "") {
                            var $label1 = $('<div class="row"></div><br/>').text('Question: ' + value.Question);
                            $('#pnlsol').append($label1);
                        }
                        if (value.descr != "") {
                            var $label = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                            $('#pnlsol').append($label);
                        }
                        counter++;
                    });
                },
                error: function ajaxError(result) {
                }
            });
        });
   </script>
    <%--UPSC Solution Section--%>
     <script>
         var counter = 1;
         $(document).ready(function () {
             function getUrlVars() {
                 var vars = [], hash;
                 var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                 for (var i = 0; i < hashes.length; i++) {
                     hash = hashes[i].split('=');
                     vars.push(hash[0]);
                     vars[hash[0]] = hash[1];
                 }
                 return vars;
             }

             var decodeupsc = "";
             var upscsol = getUrlVars()["upsconline"];
             if (upscsol != null) {
                 decodeupsc = decodeURI(upscsol);
             }
             $.ajax({
                 type: "Post",
                 contentType: "application/Json; Charset=utf-8",
                 url: "solution.aspx/divupscsol",
                 data: "{'decodeupsc':'" + decodeupsc + "'}",
                 dataType: "json",
                 success: function (result) {
                     $.each(result.d, function (key, value) {
                         if (value.Question != "") {
                             var $label1 = $('<div class="row"></div><br/>').text('Question: ' + value.Question);
                             $('#pnlsol').append($label1);
                         }

                         if (value.descr != "") {
                             var $label = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                             $('#pnlsol').append($label);
                         }
                         counter++;
                     });
                 },
                 error: function ajaxError(result) {
                 }
             });
         });
   </script>

     <script>
        $(document).ready(function () {
            function getUrlVars() {

                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }
            var decode = "";
            var set = getUrlVars()["sscquiz"];
            if (set != null) {
                decode = decodeURI(set);
            }
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "solution.aspx/sscquizsol",
                data: "{'decode':'" + decode + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        if (value.Ques != "") {
                            var $label1 = $('<div class="row"<div class="col-md-12">').text('Question:' + value.Ques + '</div> </div>');
                            $('#pnlsol').append($label1);
                        }
                        if (value.desc != "") {
                            var $label = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                            $('#pnlsol').append($label);
                        }
                    });
                },
                error: function ajaxError(result) {
                }
            });
        });
   </script>
     <script>
         var counter = 1;
         $(document).ready(function () {
             function getUrlVars() {

                 var vars = [], hash;
                 var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                 for (var i = 0; i < hashes.length; i++) {
                     hash = hashes[i].split('=');
                     vars.push(hash[0]);
                     vars[hash[0]] = hash[1];
                 }
                 return vars;
             }
             var set = getUrlVars()["bankquiz"];
             var decode = "";
             if (set != null) {
                 decode = decodeURI(set);
             }
             $.ajax({
                 type: "Post",
                 contentType: "application/Json; Charset=utf-8",
                 url: "solution.aspx/btnbankquiz",
                 data: "{'decode':'" + decode + "'}",
                 dataType: "json",
                 success: function (result) {
                     $.each(result.d, function (key, value) {
                         if (value.Ques != "") {
                             var $label5 = $('<div class="row"></div><br/>').text('Question: ' + value.Ques);
                             $('#pnlsol').append($label5);
                         }
                         if (value.descr != "") {
                             var $label6 = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                             $('#pnlsol').append($label6);
                         }
                         counter++;

                     });
                 },
                 error: function ajaxError(result) {

                 }
             });
         });
   </script>


    <script>
         var counter = 1;
         $(document).ready(function () {
             function getUrlVars() {

                 var vars = [], hash;
                 var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                 for (var i = 0; i < hashes.length; i++) {
                     hash = hashes[i].split('=');
                     vars.push(hash[0]);
                     vars[hash[0]] = hash[1];
                 }
                 return vars;
             }
             var set = getUrlVars()["upscquiz"];
             var decode = "";
             if (set != null) {
                 decode = decodeURI(set);
             }
             $.ajax({
                 type: "Post",
                 contentType: "application/Json; Charset=utf-8",
                 url: "solution.aspx/btnupscquiz",
                 data: "{'decode':'" + decode + "'}",
                 dataType: "json",
                 success: function (result) {
                     $.each(result.d, function (key, value) {
                         if (value.Ques != "") {
                             var $label5 = $('<div class="row"></div><br/>').text('Question: ' + value.Ques);
                             $('#pnlsol').append($label5);
                         }
                         if (value.descr != "") {
                             var $label6 = $('<div class="row"></div><br/>').text('Solution: ' + value.descr);
                             $('#pnlsol').append($label6);
                         }
                         counter++;

                     });
                 },
                 error: function ajaxError(result) {

                 }
             });
         });
   </script>
     <script>
         // Get the modal
         var modal = document.getElementById('myModal');
         var btn = document.getElementById("myBtn");
         var span = document.getElementsByClassName("close")[0];
         btn.onclick = function () {
             modal.style.display = "block";
         }
         span.onclick = function () {
             modal.style.display = "none";
         }
         window.onclick = function (event) {
             if (event.target == modal) {
                 modal.style.display = "none";
             }
         }
</script>
</asp:Content>


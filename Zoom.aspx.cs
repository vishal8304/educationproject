﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Zoom : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindimg();
        }
    }
    public void bindimg()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var img = Request.QueryString["imageid"];
        if (img != null)
        {
            SqlCommand cmd = new SqlCommand("select * from mcqquery where Id="+img, con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                myImg.Src = dr["picpath"].ToString();
            }
        }
    }
}
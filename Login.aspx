﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .rfv {
            background: none !important;
            color: inherit;
            border: none;
            padding: 0 !important;
            font: inherit;
          
            border-bottom: 1px solid #444;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server" defaultbutton="btnSubmit" defaultfocus="btnSubmit">
        <div class="contact">
            <div class="container">
                <div class="w3layouts_mail_grid">
                    <div class="agileits_mail_grid_right agileits_w3layouts_mail_grid_right">
                        <h3 class="w3l_header w3_agileits_header1">Login<span>Form</span>
                        </h3>
                        <div class="container">
                            <br />
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-9" style="text-align: center">
                                    <asp:Label ID="lblInfo" runat="server" Text="Label" Visible="False"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    Enter Username:
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtUsrnm" runat="server" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3">
                                    Enter Password:
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6" style="text-align: right">
                                    <asp:Button ID="btnforgotpwd" CssClass="" Text="Forgot Password?" runat="server" OnClick="btnforgotpwd_Click" />
                                </div>
                                <div class="col-md-6">
                                    <asp:Button ID="btnSubmit" type="submit" CssClass="btn btn-primary" runat="server" Text="Login" OnClick="btnSubmit_Click" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/jquery-ui.js"></script>

</asp:Content>


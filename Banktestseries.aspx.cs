﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Banktestseries : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindgrid();
        }
    }

    public void bindgrid()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            var a = Request.QueryString["set"];
            SqlCommand cmd = new SqlCommand("Select name,[set],sum(marks) as totalmarks,sum(time) as totaltime, count(Question) as count, dbo.getbankonlinevalues(@a, name, @email) as TotalMarksObt from onbanktest where [set] = @a GROUP BY[set], name", con);
            cmd.Parameters.Add(new SqlParameter("@a", a));
            cmd.Parameters.Add(new SqlParameter("@email", Request.Cookies["Email"].Value));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            grid.DataSource = dt;
            grid.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }

    protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var a = Request.QueryString["set"];
        DataRowView drv = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {

            }
            else
            {
                HyperLink Hlink = (HyperLink)e.Row.FindControl("Hlink");
                Label lbl = (Label)e.Row.FindControl("lblobt");
                Label lblname = (Label)e.Row.FindControl("lblname");
                if (lbl.Text == "")
                {
                    Hlink.NavigateUrl = "Bankinstruct.aspx?set=" + lblname.Text + "&section=" +a;
                    Hlink.Text = "Start Test";
                }
                else
                {
                    Hlink.NavigateUrl = "solution.aspx?Bankonline="+lblname.Text;
                    Hlink.Text = "View Solution";
                }
            }

        }

    }
    
    public class onbanktest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Correctval { get; set; }
        public string path { get; set; }
        public string solimg { get; set; }
        public string solved { get; set; }
        public string descr { get; set; }
    }
   
}
﻿<%@ Application Language="C#" CodeBehind="~/App_Code/Global.asax.cs" Inherits="Global" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">
    void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);
    }
   
    static void RegisterRoutes(RouteCollection routes)
    {
        
        routes.MapPageRoute("Blogs", "Blog", "~/Blog.aspx");
        routes.MapPageRoute("Blogs", "Blog/{Url}", "~/BlogDetails.aspx");
    }
</script>

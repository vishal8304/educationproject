﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .rfv
    {
        padding-left:10px;
        color:#B50128;
        font-size:12px;
        font-family: Verdana, Tahoma, Arial;
        font-weight:bold;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="contact">
		<div class="container">
    <div class="w3layouts_mail_grid">
       
   <h3 class="w3l_header w3_agileits_header1">Register<span></span></h3>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <asp:Label ID="lblConfirm" runat="server" Text="Label" ForeColor="Red" Visible="False"></asp:Label></div>
                </div><br />
                <div class="row">
                    <div class="col-md-3">Name:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtName" required="" runat="server" placeholder="Enter Your Name" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-md-3">Email Id:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtEmail" runat="server" required="" placeholder="Enter Your Email" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox></div>
                </div><br />
         <div class="row">
                    <div class="col-md-3">Mobile No:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtmob" runat="server" required="" placeholder="Enter Your Mobile No" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox></div>
                </div><br />
         <div class="row">
                    <div class="col-md-3">Address:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtadrr" runat="server" required="" placeholder="Enter Your Address" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox></div>
                </div><br />
                 <div class="row">
                    <div class="col-md-3">Password:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtPwd" ClientIDMode="Static" required="" runat="server" placeholder="Enter Your Password" onkeyup="checkPass(); return false;" TextMode="Password" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox></div>
                </div><br />
                 <div class="row">
                    <div class="col-md-3">Confirm Password:</div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtcpwd" ClientIDMode="Static" required="" runat="server" placeholder="Confirm Password" onkeyup="checkPass(); return false;" TextMode="Password" BorderStyle="Groove" ForeColor="#212121" Font-Size="14px" Width="85%" Height="37px"></asp:TextBox></div>
                </div>
        <div class="row">
            <div class="col-md-3"></div>
                     <div class="col-md-9">
                    <div id="error-nwl"></div>
                </div>
                 </div><br />
                <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSubmit" runat="server" Text="Register" OnClick="btnSubmit_Click1"/>
                            <asp:Label ID="lblrequired" runat="server" Text="Label" Visible="False" ForeColor="#FF3300"></asp:Label>
                        </div>
                        </div><br />
                </div>
      
            </div>
       </div>
   <script>
// just for the demos, avoids form submit
    function checkPass() {
        var pass1 = document.getElementById('txtPwd');
        var pass2 = document.getElementById('txtcpwd');
        var message = document.getElementById('error-nwl');
        var goodColor = "#66cc66";
        var badColor = "#ff6666";

        if (pass1.value.length > 5) {
            pass1.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "character number ok!"
        }
        else {
            pass1.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = " you have to enter at least 6 digit!"
            return;
        }

        if (pass1.value == pass2.value) {
            pass2.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "ok!"
        }
        else {
            pass2.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = " These passwords don't match"
        }
    }
</script>
</asp:Content>


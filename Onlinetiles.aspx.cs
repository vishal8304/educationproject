﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Onlinetiles : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindrpttiles();
            bindbanktiles();
        }
    }
    public void bindrpttiles()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct [set] from ontimertest where [set] is not null", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rpttiles.DataSource = dt;
        rpttiles.DataBind();
    }
    public void bindbanktiles()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct [set] from onbanktest where [set] is not null", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptbanktiles.DataSource = dt;
        rptbanktiles.DataBind();
    }
}
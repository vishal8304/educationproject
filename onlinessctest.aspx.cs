﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class onlinessctest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public class ontimertest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string desc { get; set; }
    }

    [WebMethod]
    public static List<ontimertest> getenglishqs(string English)
    {
        DataTable dt = new DataTable();
        List<ontimertest> objdept = new List<ontimertest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='Set1' and [set]='SSC CGL' and section='English') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new ontimertest
                        {
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="contact">
		<div class="container">
    <div class="w3layouts_mail_grid">
        <div class="agileits_mail_grid_right agileits_w3layouts_mail_grid_right">
            <div class="row">
                <div class="col-md-4">
                    <asp:Label ID="lblupdate" runat="server" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
  <asp:Repeater ID="rptprofile" runat="server">
      <ItemTemplate>
                <div class="row">
                    <div class="col-md-12">
                        <strong><asp:Label ID="lblupdate" runat="server" Text='<%#Eval("Name") %>'></asp:Label></strong>
                    </div>
                 </div>
          <div class="row">
                    <div class="col-md-12">
                        <asp:Image ID="imgprofile" BorderStyle="Solid" ImageUrl='<%#Eval("profilepath")%>' Height="200px" Width="200px" runat="server" />
                    </div>
                     </div>
          </ItemTemplate>
  </asp:Repeater>
                    </div>
                <div class="col-md-9">
                    <asp:Button ID="btnfollow" runat="server" Text="Follow" OnClick="btnfollow_Click" />
                </div>
           </div>
           </div> 
      </div>
            </div>
         </div>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
        $('#password, #confirm_password').on('keyup', function () {
            if ($('#password').val() == $('#confirm_password').val()) {
                $('#message').html('Matching').css('color', 'green');
            } else
                $('#message').html('Not Matching').css('color', 'red');
        });
    </script>
   
     <script>
    $(function () {
    populatelist();
   
});
    function populatelist() {
      var Email = "";
             var cookieArray = document.cookie.split("; ");
             for (var i = 0; i < cookieArray.length; i++) {
                 var nameValueArray = cookieArray[i].split("=");
                 if (nameValueArray[0] == "Email") {
                     Email = nameValueArray[1];
                 }
             }
        $.ajax({
            type: "Post",
            contentType: "application/Json; Charset=utf-8",
            url: "Myprofile.aspx/Savedpost",
            data: "{'Email': '" + Email + "'}",
            dataType: "json",
            success: function (result) {
                var $post = $('<div class="row"></div><br/>').text('Shared Posts:').css("font-weight", "600");
                $('#divqs').append($post);
                $.each(result.d, function (key, value) {
                    var $div1 = $('<div class="row"></div><br/>').text('Question: ' + value.Ques);
                    $('#divqs').append($div1);
                    if (value.picpath.length > 0) {
                        var img = '<img src="' + value.picpath + '" height="200px" width="800px"/>';
                        $('#divqs').append(img);
                    }
                    if (value.opta.length > 0) {
                        var $div2 = $('<div class="row"></div><br/>').text('1. ' + value.opta);
                        $('#divqs').append($div2);
                    }
                    if (value.optb.length > 0) {
                        var $div3 = $('<div class="row"></div><br/>').text('2. ' + value.optb);
                        $('#divqs').append($div3);
                    }
                    if (value.optc.length > 0) {
                        var $div4 = $('<div class="row"></div><br/>').text('3. ' + value.optc);
                        $('#divqs').append($div4);
                    }
                    if (value.optd.length > 0) {
                        var $div5 = $('<div class="row"></div><br/>').text('4. ' + value.optd);
                        $('#divqs').append($div5);
                    }
                    if (value.opte.length > 0) {
                        var $div6 = $('<div class="row"></div><br/>').text('5. ' + value.opte);
                        $('#divqs').append($div6);
                    }
                });
            },
            error: function ajaxError(result) {

            }
        });
    }
        </script>
</asp:Content>


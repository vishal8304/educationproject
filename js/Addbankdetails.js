﻿
populatelist();
populateqs();
populateqsfromtab();
reviewclick();
clearresponseclick();

function populatelist() {
    function getUrlVars() {

        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var set = getUrlVars()["set"];
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    var decodeset = decodeURI(set);
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "bankOnline.aspx/populatelist",
        data: "{'set':'" + decodeset + "','section':'" + decode + "'}",
        dataType: "json",
        success: function (result) {
            $.each(result.d, function (key, value) {
                //$('#lblqs').append('<input type="text">').val(value.Id).text(value.Question);
                var $label = $('<label></label>').text(value.Question);
                //Create the input element
                //var $input = $('<input type="text">').attr({ id: value.Id, name: value.Question });
                $('#lblqs').append($label);
                var $labelopt1 = $('<label></label>').text(value.Optionval1);
                //Create the input element
                //var $inputopt1 = $('<input type="text">').attr({ id: value.Id, name: value.Optionval1 });
                $('#divopt1').append($labelopt1);
                //alert(result.status + ':' + result.statusText);
                var $labelopt2 = $('<label></label>').text(value.Optionval2);
                $('#divopt2').append($labelopt2);
                var $labelopt3 = $('<label></label>').text(value.Optionval3);
                $('#divopt3').append($labelopt3);
                var $labelopt4 = $('<label></label>').text(value.Optionval4);
                $('#divopt4').append($labelopt4);
                var $labelopt5 = $('<label></label>').text(value.Optionval5);
                $('#divopt5').append($labelopt5);
                if (value.path.length > 0) {
                    var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                    $('#divimg').show();
                    $('#divimg').html(img);
                }
                else {
                    $('#divimg').hide();
                }
                var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                $('#divrdb1').html($rdb1);
                var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                $('#divrdb2').html($rdb2);
                var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                $('#divrdb3').html($rdb3);
                var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                $('#divrdb4').html($rdb4);
                var $rdb5 = ' <input type="radio" id="rdbE" name="val" value="E"/>';
                $('#divrdb5').html($rdb5);
                var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                $('#hdn').html($hdn);
                var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                $('#hdn1').html($hdn1);
            });
        },
        error: function ajaxError(result) {

        }
    });
}

function populateqs() {
    var counter = 1;
    function getUrlVars() {

        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var set = getUrlVars()["set"];
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    var decodeset = decodeURI(set);
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "bankOnline.aspx/populateqs",
        data: "{'set':'" + decodeset + "','section':'" + decode + "'}",
        dataType: "json",
        success: function (result) {
            $.each(result.d, function (key, value) {

                var $btn = '<input type="button" class="myClass" name="Button" value="' + value.Id + '" id="' + counter + '" />';
                counter++;
                $('#divqsno').append($btn);
            });
            buttonclick();
        },
        error: function ajaxError(result) {

        }
    });
}

function buttonclick() {
    $("input[name='Button']").click(function () {
        function getUrlVars() {

            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var set = getUrlVars()["set"];
        var section = getUrlVars()["section"];
        var decode = decodeURI(section);
        var decodeset = decodeURI(set);
        var current = $(this);
        var val = current.val();
        $.ajax({
            type: "Post",
            contentType: "application/Json; Charset=utf-8",
            url: "bankOnline.aspx/getqsfromlist",
            data: "{'val':'" + val + "','set':'" + decodeset + "','section':'" + decode + "'}",
            dataType: "json",
            success: function (result) {
                //$('#lblqs').empty();
                //$('#lblqs').append('<input type="text">');
                $.each(result.d, function (key, value) {
                    //$('#lblqs').append('<input type="text">').val(value.Id).text(value.Question);
                    var $label = $('<label></label>').text(value.Question);
                    //Create the input element
                    //var $input = $('<input type="text">').attr({ id: value.Id, name: value.Question });
                    $('#lblqs').html($label);
                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
                    //Create the input element
                    //var $inputopt1 = $('<input type="text">').attr({ id: value.Id, name: value.Optionval1 });
                    $('#divopt1').html($labelopt1);
                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
                    $('#divopt2').html($labelopt2);
                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
                    $('#divopt3').html($labelopt3);
                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
                    $('#divopt4').html($labelopt4);
                    var $labelopt5 = $('<label></label>').text(value.Optionval5);
                    $('#divopt5').append($labelopt5);
                    if (value.path.length > 0) {
                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                        $('#divimg').show();
                        $('#divimg').html(img);
                    }
                    else {
                        $('#divimg').hide();
                    }
                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                    $('#divrdb1').html($rdb1);
                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                    $('#divrdb2').html($rdb2);
                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                    $('#divrdb3').html($rdb3);
                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                    $('#divrdb4').html($rdb4);
                    var $rdb5 = ' <input type="radio" id="rdbE" name="val" value="E"/>';
                    $('#divrdb5').html($rdb5);
                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                    $('#hdn').html($hdn);
                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                    $('#hdn1').html($hdn1);
                });
            },
            error: function ajaxError(result) {

            }
        });
    });
}
function populateqsfromtab() {
    function getUrlVars() {

        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var set = getUrlVars()["set"];
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    var decodeset = decodeURI(set);
    $("input[name='section']").click(function () {
        var current = $(this);
        if (current.val() == 'English Comprehension') {
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "bankOnline.aspx/getenglishqs",
                data: "{'English':' English ','set':'" + decodeset + "','section':'" + decode + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        var $label = $('<label></label>').text(value.Question);
                        $('#lblqs').html($label);
                        var $labelopt1 = $('<label></label>').text(value.Optionval1);
                        $('#divopt1').html($labelopt1);
                        var $labelopt2 = $('<label></label>').text(value.Optionval2);
                        $('#divopt2').html($labelopt2);
                        var $labelopt3 = $('<label></label>').text(value.Optionval3);
                        $('#divopt3').html($labelopt3);
                        var $labelopt4 = $('<label></label>').text(value.Optionval4);
                        $('#divopt4').html($labelopt4);
                        var $labelopt5 = $('<label></label>').text(value.Optionval5);
                        $('#divopt5').html($labelopt5);
                        if (value.path.length > 0) {
                            var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                            $('#divimg').show();
                            $('#divimg').html(img);
                        }
                        else {
                            $('#divimg').hide();
                        }
                        var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                        $('#divrdb1').html($rdb1);
                        var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                        $('#divrdb2').html($rdb2);
                        var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                        $('#divrdb3').html($rdb3);
                        var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                        $('#divrdb4').html($rdb4);
                        var $rdb5 = ' <input type="radio" id="rdbE" name="val" value="E"/>';
                        $('#divrdb5').html($rdb5);
                        var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                        $('#hdn').html($hdn);
                        var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                        $('#hdn1').html($hdn1);
                    });
                },
                error: function ajaxError(result) {

                }
            });
        }
        else if (current.val() == 'General Intelligence And Reasoning') {
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "bankOnline.aspx/getintlliqs",
                data: "{'General_Intelligence':' General Intelligence ','set':'" + decodeset + "','section':'" + decode + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        var $label = $('<label></label>').text(value.Question);
                        $('#lblqs').html($label);
                        var $labelopt1 = $('<label></label>').text(value.Optionval1);
                        $('#divopt1').html($labelopt1);
                        var $labelopt2 = $('<label></label>').text(value.Optionval2);
                        $('#divopt2').html($labelopt2);
                        var $labelopt3 = $('<label></label>').text(value.Optionval3);
                        $('#divopt3').html($labelopt3);
                        var $labelopt4 = $('<label></label>').text(value.Optionval4);
                        $('#divopt4').html($labelopt4);
                        var $labelopt5 = $('<label></label>').text(value.Optionval5);
                        $('#divopt5').html($labelopt5);
                        if (value.path.length > 0) {
                            var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                            $('#divimg').show();
                            $('#divimg').html(img);
                        }
                        else {
                            $('#divimg').hide();
                        }
                        var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                        $('#divrdb1').html($rdb1);
                        var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                        $('#divrdb2').html($rdb2);
                        var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                        $('#divrdb3').html($rdb3);
                        var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                        $('#divrdb4').html($rdb4);
                        var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                        $('#hdn').html($hdn);
                        var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                        $('#hdn1').html($hdn1);
                    });
                },
                error: function ajaxError(result) {

                }
            });
        }
        else if (current.val() == 'General Knowledge And General Awareness') {
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "bankOnline.aspx/getreasoningqs",
                data: "{'Reasoning':'Reasoning','set':'" + decodeset + "','section':'" + decode + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        var $label = $('<label></label>').text(value.Question);
                        $('#lblqs').html($label);
                        var $labelopt1 = $('<label></label>').text(value.Optionval1);
                        $('#divopt1').html($labelopt1);
                        var $labelopt2 = $('<label></label>').text(value.Optionval2);
                        $('#divopt2').html($labelopt2);
                        var $labelopt3 = $('<label></label>').text(value.Optionval3);
                        $('#divopt3').html($labelopt3);
                        var $labelopt4 = $('<label></label>').text(value.Optionval4);
                        $('#divopt4').html($labelopt4);
                        var $labelopt5 = $('<label></label>').text(value.Optionval5);
                        $('#divopt5').html($labelopt5);
                        if (value.path.length > 0) {
                            var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                            $('#divimg').show();
                            $('#divimg').html(img);
                        }
                        else {
                            $('#divimg').hide();
                        }
                        var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                        $('#divrdb1').html($rdb1);
                        var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                        $('#divrdb2').html($rdb2);
                        var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                        $('#divrdb3').html($rdb3);
                        var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                        $('#divrdb4').html($rdb4);
                        var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                        $('#hdn').html($hdn);
                        var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                        $('#hdn1').html($hdn1);
                    });
                },
                error: function ajaxError(result) {

                }
            });
        }
        else if (current.val() == 'Quantitative Aptitude') {
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "bankOnline.aspx/getaptitudeqs",
                data: "{'Quantitative_Aptitude':'Quantitative Aptitude','set':'" + decodeset + "','section':'" + decode + "'}",
                dataType: "json",
                success: function (result) {
                    $.each(result.d, function (key, value) {
                        var $label = $('<label></label>').text(value.Question);
                        $('#lblqs').html($label);
                        var $labelopt1 = $('<label></label>').text(value.Optionval1);
                        $('#divopt1').html($labelopt1);
                        var $labelopt2 = $('<label></label>').text(value.Optionval2);
                        $('#divopt2').html($labelopt2);
                        var $labelopt3 = $('<label></label>').text(value.Optionval3);
                        $('#divopt3').html($labelopt3);
                        var $labelopt4 = $('<label></label>').text(value.Optionval4);
                        $('#divopt4').html($labelopt4);
                        var $labelopt5 = $('<label></label>').text(value.Optionval5);
                        $('#divopt5').html($labelopt5);
                        if (value.path.length > 0) {
                            var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                            $('#divimg').show();
                            $('#divimg').html(img);
                        }
                        else {
                            $('#divimg').hide();
                        }
                        var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                        $('#divrdb1').html($rdb1);
                        var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                        $('#divrdb2').html($rdb2);
                        var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                        $('#divrdb3').html($rdb3);
                        var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                        $('#divrdb4').html($rdb4);
                        var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                        $('#hdn').html($hdn);
                        var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                        $('#hdn1').html($hdn1);
                    });
                },
                error: function ajaxError(result) {

                }
            });
        }
    });
}

function reviewclick() {
    $("#inp_revise").click(function () {
        var current = $(this);
        var qs = current.parent().parent().parent().find("#hidden1").val();
        $('input[name="Button"][value="' + qs + '"]').css("background-color", "rgb(132, 95, 187)");
    });
}

function clearresponseclick() {
    $('input[type="Button"][class="CRBtn"]').click(function () {
        var current = $(this);
        var qs = current.parent().parent().parent().find("#hidden1").val();
        $('input[name="Button"][value="' + qs + '"]').css("background-color", "#e4edf7");
    });
}
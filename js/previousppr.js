﻿$(function () {
    populatelist();
    populatessc();
});

function populatelist() {
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "Previousyrppr.aspx/populatelist",
        data: "{}",
        dataType: "json",
        success: function (result) {
            //$('#lblqs').empty();
            //$('#lblqs').append('<input type="text">');
            $.each(result.d, function (key, value) {
                var $label = $('<input type="button" name="button" value=' + value.exam + 'class="btn"/>');
                $('#banklist').append($label);
                $("input[name='button']").click(function () {
                    window.location.href = "../Previousyrppr.aspx?exam=" + value.exam;
                });
            });
               
        },
        error: function ajaxError(result) {

        }
    });
};

function populatessc() {
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "Previousyrppr.aspx/populatessc",
        data: "{}",
        dataType: "json",
        success: function (result) {
            //$('#lblqs').empty();
            //$('#lblqs').append('<input type="text">');
            $.each(result.d, function (key, value) {
                var $label = $('<input type="button" name="button" value=' + value.exam + ' class="btn"/>');
                $('#ssclist').append($label);
                $("input[name='button']").click(function () {
                    window.location.href = "../Previousyrppr.aspx?exam=" + value.exam;
                });
            });

        },
        error: function ajaxError(result) {

        }
    });
};
﻿$(function () {
    $('.pop').show();
    var overlay = $('<div id="overlay"></div>');
    $('.close').click(function () {
        $('.pop').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });
    $('.fa fa-times').click(function () {
        $('.pop').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });
});

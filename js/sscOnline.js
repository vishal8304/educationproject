﻿onloadqs();
populateqsfromtab();
populateqs();
reviewclick();
clearresponseclick();

function onloadqs() {
    function getUrlVars() {

        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var rowNo = 0;
    var set = decodeURI(getUrlVars()["set"]);
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "sscOnline.aspx/GetQuestionsBySections",
        data: "{'section1':'','set':'" + set + "','section':'" + decode + "'}",
        dataType: "json",
        success: function (result) {
            $.each(result.d, function (key, value) {
                rowNo++;
                var $p = $('<p></p>').text(value.rowNo);
                $('qsnO').html($p);
                var $label = $('<label></label>').text(value.Question);
                $('#lblqs').html($label);
                var $labelopt1 = $('<label></label>').text(value.Optionval1);
                $('#divopt1').html($labelopt1);
                var $labelopt2 = $('<label></label>').text(value.Optionval2);
                $('#divopt2').html($labelopt2);
                var $labelopt3 = $('<label></label>').text(value.Optionval3);
                $('#divopt3').html($labelopt3);
                var $labelopt4 = $('<label></label>').text(value.Optionval4);
                $('#divopt4').html($labelopt4);
                if (value.path.length > 0) {
                    var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                    $('#divimg').show();
                    $('#divimg').html(img);
                }
                else {
                    $('#divimg').hide();
                }
                var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                $('#divrdb1').html($rdb1);
                var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                $('#divrdb2').html($rdb2);
                var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                $('#divrdb3').html($rdb3);
                var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                $('#divrdb4').html($rdb4);
                var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                $('#hdn').html($hdn);
                var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                $('#hdn1').html($hdn1);
            });
        },
        error: function ajaxError(result) {

        }
    });
}

function populateqsfromtab() {
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var set = decodeURI(getUrlVars()["set"]);
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    $(".btnSection").click(function () {
        var section1 = $(this).val();
        $.ajax({
            type: "Post",
            contentType: "application/Json; Charset=utf-8",
            url: "sscOnline.aspx/GetQuestionsBySections",
            data: "{'section1':'"+section1+"','set':'" + set + "','section':'" + decode + "'}",
            dataType: "json",
            success: function (result) {

                if (result.d.length<1)
                {
                    alert("This section does not contain any questions");
                    return false;
                }

                $.each(result.d, function (key, value) {
                    var $label = $('<label></label>').text(value.Question);
                    $('#lblqs').html($label);
                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
                    $('#divopt1').html($labelopt1);
                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
                    $('#divopt2').html($labelopt2);
                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
                    $('#divopt3').html($labelopt3);
                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
                    $('#divopt4').html($labelopt4);
                    if (value.path.length > 0) {
                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                        $('#divimg').show();
                        $('#divimg').html(img);
                    }
                    else {
                        $('#divimg').hide();
                    }
                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                    $('#divrdb1').html($rdb1);
                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                    $('#divrdb2').html($rdb2);
                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                    $('#divrdb3').html($rdb3);
                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                    $('#divrdb4').html($rdb4);
                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                    $('#hdn').html($hdn);
                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                    $('#hdn1').html($hdn1);
                });
            },
            error: function ajaxError(result) {

            }
        });
    });


    //$(".btnSection").click(function () {
      
    //    var current = $(this);
    //    if (current.val() == 'English Comprehension') {
    //        alert("english me gaya");

    //        $.ajax({
    //            type: "Post",
    //            contentType: "application/Json; Charset=utf-8",
    //            url: "sscOnline.aspx/getenglishqs",
    //            data: "{'English':' English ','set':'" + set + "','section':'" + decode + "'}",
    //            dataType: "json",
    //            success: function (result) {
    //                debugger
    //                $.each(result.d, function (key, value) {
    //                    var $label = $('<label></label>').text(value.Question);
    //                    $('#lblqs').html($label);
    //                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
    //                    $('#divopt1').html($labelopt1);
    //                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
    //                    $('#divopt2').html($labelopt2);
    //                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
    //                    $('#divopt3').html($labelopt3);
    //                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
    //                    $('#divopt4').html($labelopt4);
    //                    if (value.path.length > 0) {
    //                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
    //                        $('#divimg').show();
    //                        $('#divimg').html(img);
    //                    }
    //                    else {
    //                        $('#divimg').hide();
    //                    }
    //                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
    //                    $('#divrdb1').html($rdb1);
    //                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
    //                    $('#divrdb2').html($rdb2);
    //                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
    //                    $('#divrdb3').html($rdb3);
    //                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
    //                    $('#divrdb4').html($rdb4);
    //                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
    //                    $('#hdn').html($hdn);
    //                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
    //                    $('#hdn1').html($hdn1);
    //                });
    //            },
    //            error: function ajaxError(result) {

    //            }
    //        });
    //    }
    //    else if (current.val() == 'General Intelligence And Reasoning') {
    //        alert("General Intelligence And Reasoning")
    //        $.ajax({
    //            type: "Post",
    //            contentType: "application/Json; Charset=utf-8",
    //            url: "sscOnline.aspx/getintlliqs",
    //            data: "{'General_Intelligence':' General Intelligence ','set':'" + set + "','section':'" + decode + "'}",
    //            dataType: "json",
    //            success: function (result) {
    //                $.each(result.d, function (key, value) {
    //                    var $label = $('<label></label>').text(value.Question);
    //                    $('#lblqs').html($label);
    //                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
    //                    $('#divopt1').html($labelopt1);
    //                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
    //                    $('#divopt2').html($labelopt2);
    //                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
    //                    $('#divopt3').html($labelopt3);
    //                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
    //                    $('#divopt4').html($labelopt4);
    //                    if (value.path.length > 0) {
    //                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
    //                        $('#divimg').show();
    //                        $('#divimg').html(img);
    //                    }
    //                    else {
    //                        $('#divimg').hide();
    //                    }
    //                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
    //                    $('#divrdb1').html($rdb1);
    //                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
    //                    $('#divrdb2').html($rdb2);
    //                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
    //                    $('#divrdb3').html($rdb3);
    //                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
    //                    $('#divrdb4').html($rdb4);
    //                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
    //                    $('#hdn').html($hdn);
    //                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
    //                    $('#hdn1').html($hdn1);
    //                });
    //            },
    //            error: function ajaxError(result) {

    //            }
    //        });
    //    }
    //    else if (current.val() == 'General Knowledge And General Awareness') {
    //        $.ajax({
    //            type: "Post",
    //            contentType: "application/Json; Charset=utf-8",
    //            url: "sscOnline.aspx/getreasoningqs",
    //            data: "{'Reasoning':'Reasoning','set':'" + set + "','section':'" + decode + "'}",
    //            dataType: "json",
    //            success: function (result) {
    //                $.each(result.d, function (key, value) {
    //                    var $label = $('<label></label>').text(value.Question);
    //                    $('#lblqs').html($label);
    //                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
    //                    $('#divopt1').html($labelopt1);
    //                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
    //                    $('#divopt2').html($labelopt2);
    //                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
    //                    $('#divopt3').html($labelopt3);
    //                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
    //                    $('#divopt4').html($labelopt4);
    //                    if (value.path.length > 0) {
    //                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
    //                        $('#divimg').show();
    //                        $('#divimg').html(img);
    //                    }
    //                    else {
    //                        $('#divimg').hide();
    //                    }
    //                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
    //                    $('#divrdb1').html($rdb1);
    //                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
    //                    $('#divrdb2').html($rdb2);
    //                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
    //                    $('#divrdb3').html($rdb3);
    //                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
    //                    $('#divrdb4').html($rdb4);
    //                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
    //                    $('#hdn').html($hdn);
    //                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
    //                    $('#hdn1').html($hdn1);
    //                });
    //            },
    //            error: function ajaxError(result) {

    //            }
    //        });
    //    }
    //    else if (current.val() == 'Quantitative Aptitude') {
    //        $.ajax({
    //            type: "Post",
    //            contentType: "application/Json; Charset=utf-8",
    //            url: "sscOnline.aspx/getaptitudeqs",
    //            data: "{'Quantitative_Aptitude':'Quantitative Aptitude','set':'" + set + "','section':'" + decode + "'}",
    //            dataType: "json",
    //            success: function (result) {
    //                $.each(result.d, function (key, value) {
    //                    var $label = $('<label></label>').text(value.Question);
    //                    $('#lblqs').html($label);
    //                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
    //                    $('#divopt1').html($labelopt1);
    //                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
    //                    $('#divopt2').html($labelopt2);
    //                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
    //                    $('#divopt3').html($labelopt3);
    //                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
    //                    $('#divopt4').html($labelopt4);
    //                    if (value.path.length > 0) {
    //                        var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
    //                        $('#divimg').show();
    //                        $('#divimg').html(img);
    //                    }
    //                    else {
    //                        $('#divimg').hide();
    //                    }
    //                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
    //                    $('#divrdb1').html($rdb1);
    //                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
    //                    $('#divrdb2').html($rdb2);
    //                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
    //                    $('#divrdb3').html($rdb3);
    //                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
    //                    $('#divrdb4').html($rdb4);
    //                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
    //                    $('#hdn').html($hdn);
    //                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
    //                    $('#hdn1').html($hdn1);
    //                });
    //            },
    //            error: function ajaxError(result) {

    //            }
    //        });
    //    }
    //});
}


function populateqs() {
    var counter = 1;
    $(".question_number").text(counter);
    var qNo = $("#"+counter+"").css("border", "1px solid green");
  
    function getUrlVars() {

        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var set = decodeURI(getUrlVars()["set"]);
    var section = getUrlVars()["section"];
    var decode = decodeURI(section);
    $.ajax({
        type: "Post",
        contentType: "application/Json; Charset=utf-8",
        url: "sscOnline.aspx/populateqs",
        data: "{'set':'" + set + "','section':'" + decode + "'}",
        dataType: "json",
        success: function (result) {
            $.each(result.d, function (key, value) {
                var $btn = '<input type="button" class="myClass" name="Button" value="' + value.Id + '" id="' + counter + '" />'
                counter++;
                $('#divqsno').append($btn);
                
            });
            buttonclick(); 
        },
        error: function ajaxError(result) {

        }
    });
}

function buttonclick() {
    $("input[name='Button']").click(function () {
        function getUrlVars() {

            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var set = decodeURI(getUrlVars()["set"]);
        var section = getUrlVars()["section"];
        var decode = decodeURI(section);
       
        var current = $(this);
        var val = current.val();

        $.ajax({
            type: "Post",
            contentType: "application/Json; Charset=utf-8",
            url: "sscOnline.aspx/getqsfromlist",
            data: "{'val':'" + val + "','set':'" + set + "','section':'" + decode + "'}",
            dataType: "json",
            success: function (result) {
                console.log(JSON.stringify(result.d));
                var value = result.d;
                    var $label = $('<label></label>').text(value.Question);
                    $('#lblqs').html($label);
                    var $labelopt1 = $('<label></label>').text(value.Optionval1);
                    //var $hdn = ' <input type="hidden" id="hidden" value="' + value.Id + '"/>';
                    //$('#hdn').html($hdn);
                    var $hdn1 = ' <input type="hidden" id="hidden1" value="' + value.RowNumber + '"/>';
                    $('#hdn1').html($hdn1);
                    $('#divopt1').html($labelopt1);
                    //alert(result.status + ':' + result.statusText);
                    var $labelopt2 = $('<label></label>').text(value.Optionval2);
                    $('#divopt2').html($labelopt2);
                    var $labelopt3 = $('<label></label>').text(value.Optionval3);
                    $('#divopt3').html($labelopt3);
                    var $labelopt4 = $('<label></label>').text(value.Optionval4);
                    $('#divopt4').html($labelopt4);
                    var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                    $('#divimg').html(img);
                    var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                    $('#divrdb1').html($rdb1);
                    var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                    $('#divrdb2').html($rdb2);
                    var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                    $('#divrdb3').html($rdb3);
                    var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                    $('#divrdb4').html($rdb4);
                    var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                    $('#hdn').html($hdn);
                
            },
            error: function ajaxError(result) {

            }
        });
    });
}

function reviewclick() {
    debugger
    var counter = 1;
    $("#inp_revise").click(function () {
        
        var current = $(this);
        var qs = current.parent().parent().parent().find("#hidden1").val();
       
        $('input[name="Button"][value="' + qs + '"]').css("background-color", "rgb(132, 95, 187)");
        qs++;
        if (qs < 2) {
            qs++;
        }
        function getUrlVars() {

            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var set = decodeURI(getUrlVars()["set"]);
        var section = getUrlVars()["section"];
        var decode = decodeURI(section);
        var hidden = current.parent().parent().parent().find("input[name=myfieldname]").val();
        $.ajax({
            type: 'POST',
            url: "sscOnline.aspx/Skip",
            data: "{'hidden':'" + hidden + "','counter':'" + counter + "','set':'" + set + "','decode':'" + decode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result.d.length <= 0) {
                    window.location.href = 'testseries.aspx?set=' + decode;
                }
                else {
                    $.each(result.d, function (key, value) {
                        counter++;
                        if (counter < 300) {
                            debugger
                            var $label = $('<label></label>').text(value.Question);
                            $('#lblqs').html($label);
                            var $labelopt1 = $('<label></label>').text(value.Optionval1);
                            $('#divopt1').html($labelopt1);
                            var $labelopt2 = $('<label></label>').text(value.Optionval2);
                            $('#divopt2').html($labelopt2);
                            var $labelopt3 = $('<label></label>').text(value.Optionval3);
                            $('#divopt3').html($labelopt3);
                            var $labelopt4 = $('<label></label>').text(value.Optionval4);
                            $('#divopt4').html($labelopt4);
                            if (value.path.length > 0) {
                                var img = '<img src="' + value.path + '" height="200px" width="325px"/>';
                                $('#divimg').show();
                                $('#divimg').html(img);
                            }
                            else {
                                $('#divimg').hide();
                            }
                            var $rdb1 = ' <input type="radio" id="rdbA" name="val" value="A"/>';
                            $('#divrdb1').html($rdb1);
                            var $rdb2 = ' <input type="radio" id="rdbB" name="val" value="B"/>';
                            $('#divrdb2').html($rdb2);
                            var $rdb3 = ' <input type="radio" id="rdbC" name="val" value="C"/>';
                            $('#divrdb3').html($rdb3);
                            var $rdb4 = ' <input type="radio" id="rdbD" name="val" value="D"/>';
                            $('#divrdb4').html($rdb4);
                            var $hdn = '<input type="hidden" name="myfieldname" value="' + value.Id + '" />';
                            $('#hdn').html($hdn);
                        } 
                    });
                }
            },

            error: function ajaxError(result) {

            }

        });
    });
}

function clearresponseclick() {
    $('input[type="Button"][class="CRBtn"]').click(function () {
        debugger
        var current = $(this);
        var qs = current.parent().parent().parent().find("#hidden1").val();
        $('input[name="val"][type="radio"]').prop("checked", false);
    });
}

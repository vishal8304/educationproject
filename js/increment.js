﻿function incrementValue() {
    var value = parseInt(document.getElementById('txtid').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('txtid').value = value;
    document.getElementById('ddlmin').value = "";
    document.getElementById('ddlsec').value = "";
}

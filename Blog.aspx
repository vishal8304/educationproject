﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.master" AutoEventWireup="true" CodeFile="Blog.aspx.cs" Inherits="Blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="styleBlog.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnplace" Runat="Server">
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="special featured">
        <div class="container" style="width: 100% !important;">
            <div class="w3_agileits_services_grids">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>

                        <div id="content" class="site-content">
                            <div class="container">
                                <div class="inner-wrapper">
                                    <div id="primary" class="content-area">
                                        <main id="main" class="site-main" role="main">
                                            <article id="post-30" class="post-30 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorised tag-best-economics-teacher-in-ghaziabad tag-cbse-coaching-classes-in-ghaziabad tag-cbse-coaching-courses-in-ghaziabad tag-economics-coaching-in-delhi-ncr">
                                                <div class="featured-thumb">
                                                    <a href="">
                                                        <img width="370" height="235" src="<%# Eval("BlogImage")%>" class="attachment-business-point-small size-business-point-small wp-post-image" alt="Registrations open"></a>
                                                </div>
                                                <div class="content-wrap content-with-image">
                                                    <div class="content-wrap-inner">
                                                        <header class="entry-header">
                                                            <div class="entry-meta">
                                                            </div>
                                                            <h2 class="entry-title"><a href="" rel="bookmark"><%# Eval("Heading")%></a></h2>
                                                        </header>
                                                        <!-- .entry-header -->

                                                        <div class="entry-content">
                                                            <p><%# Eval("Short")%></p>

                                                            <div class="entry-footer">
                                                                <span class="posted-on"><a href="" rel="bookmark"><time class="entry-date published" datetime="2019-03-18T06:31:39+00:00"><%# Eval("Date")%></time><time class="updated" datetime="2019-03-18T10:00:17+00:00"><%# Eval("Date")%></time></a></span><span class="byline"> <span class="author vcard"><a class="url fn n" href="">admin</a></span></span>
                                                            </div>
                                                            <a class="readmore-content" href="BlogDetails.aspx?Url=<%# Eval("Url")%>" rel="bookmark">Continue Reading <i class="fa fa-angle-double-right"></i></a>
                                                        </div>
                                                        <!-- .entry-content -->
                                                    </div>
                                                </div>

                                            </article>
                                            <!-- #post-## -->

                                        </main><!-- #main -->
                                    </div>
                                    <!-- #primary -->
                    </ItemTemplate>
                </asp:Repeater>



            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contentplace3" Runat="Server">
</asp:Content>


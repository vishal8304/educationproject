﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="sscinstruct.aspx.cs" Inherits="sscinstruct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .btn{
          background-color:#060692;
          color:white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <b style="text-align:center; margin-top:3%">
            <asp:Literal ID="literal1" runat="server">
            </asp:Literal>
                </b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input id="chk" required="required" name="chk" type="checkbox" />
            Please check this checkbox if you agree to above Terms and Conditions.
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" style="text-align:right; margin-bottom:2%;">
            <asp:Button ID="btnlaunch" CssClass="btn" runat="server" OnClick="btnlaunch_Click" Text="Launch Test" />
        </div>
    </div>
    
</asp:Content>

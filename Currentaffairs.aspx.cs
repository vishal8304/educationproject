﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Currentaffairs : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    int a, b;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            valMembers();
            bindrepeater();
        }
    }

    public void valMembers()
    {
        var a = Request.QueryString["section"];
        var b = Request.QueryString["Date"];
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        if (a != null) { 
        SqlCommand cmd = new SqlCommand("select top 1 * FROM CurrentAffair where section='"+a+"' order by Date desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["content"].ToString();
            literal1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        dr.Close();
        }
        else if (b != null)
        {
            SqlCommand cmd = new SqlCommand("select top 1 * FROM CurrentAffair where Date='" + b + "' order by Date desc", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["content"].ToString();
                literal1.Text = System.Net.WebUtility.HtmlDecode(str);
            }
            dr.Close();
        }
    }

    public void bindrepeater()
    {
        var a = Request.QueryString["section"];
        var b = Request.QueryString["Date"];
        if (a != null)
        { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da=new SqlDataAdapter("select * FROM CurrentAffair where section='" + a + "' order by Id desc", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptwords.DataSource = dt;
        rptwords.DataBind();
        }
        if (b != null)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select section from CurrentAffair where Date='" + b + "' and section='English'", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                dr.Close();
                SqlDataAdapter da = new SqlDataAdapter("select * FROM CurrentAffair where section='English' order by Id desc", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                rptwords.DataSource = dt;
                rptwords.DataBind();
            }
            else
            {
                dr.Close();
                SqlDataAdapter da = new SqlDataAdapter("select * FROM CurrentAffair where section='Hindi' order by Date", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                rptwords.DataSource = dt;
                rptwords.DataBind();
            }
           
        }
    }

    //[WebMethod]
    //public static string SaveAnswer(string opt, string Email, string id)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    con.Open();
    //    SqlCommand cmd = new SqlCommand("insert into usercaffair(selectedval,Email,Id) values (@selectedval,@Email,@Id)", con);
    //    cmd.Parameters.AddWithValue("@selectedval", opt);
    //    cmd.Parameters.AddWithValue("@Email", Email);
    //    cmd.Parameters.AddWithValue("@Id", id);
    //    int Result = cmd.ExecuteNonQuery();
    //    if (Result == 1)
    //    {
    //        string qry3 = "update b set [status]='passed' FROM CurrentAffair a inner join usercaffair b on a.Id = b.Id where a.correctval=b.selectedval";
    //        SqlCommand cmd3 = new SqlCommand(qry3, con);
    //        int j = cmd3.ExecuteNonQuery();
    //        //if (j == 1)
    //        //{
    //        //    //Session.Remove("timeout");
    //        //    con.Open();
    //        //    //string qry = "select count(*) from onlinetest where [status]='passed' and Email='ruchikaj6@ndinfotech.co.in'";
    //        //    //SqlCommand cmd1 = new SqlCommand(qry, con);
    //        //    //int count = Convert.ToInt16(cmd1.ExecuteScalar());
    //        //    //lblshow.Visible = true;
    //        //    //lblresult.Visible = true;
    //        //    //lblresult.Text = count.ToString();
    //        //}
    //        //return j.ToString();
    //    }
    //    return Result > 0 ? "true": "error";
    //}
}
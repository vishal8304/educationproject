﻿using EducationDataAccess.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    public List<Banners> Banner = new List<Banners>();
    public string BannerImages;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            text();
            text1();
        }
        con.Open();
        SqlCommand cmd = new SqlCommand("select Top 5 * from Banner", con);
        SqlDataReader sdr = cmd.ExecuteReader();
        while (sdr.Read())
        {
            Banners sol = new Banners
            {
                Id = Convert.ToInt32(sdr["Id"]),
                Banner = sdr["Banner"].ToString(),
            };
            Banner.Add(sol);
            //var data = new JavaScriptSerializer();
            //BannerImages = data.Serialize(Banner.Select(a => a.Banner));
        }
        con.Close();
    }

    [WebMethod]
    public static string register(string name, string email1, string pwd, string number, string addr)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        string returnvalue = "";
        try
        {

            string num = "0123456789";
            int len = num.Length;
            string otp = string.Empty;
            //How many digits mention below
            int otpdigit = 5;
            string finaldigit;
            int getindex;
            for (int i = 0; i < otpdigit; i++)
            {
                do
                {
                    getindex = new Random().Next(0, len);
                    finaldigit = num.ToCharArray()[getindex].ToString();
                } while (otp.IndexOf(finaldigit) != -1);
                otp += finaldigit;
            }

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }


            SqlCommand newcmd = new SqlCommand("select Email from [User] where Email='" + email1 + "' ", con);
            SqlDataReader dr = newcmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                returnvalue = otp;
            }
            else
            {
                dr.Close();
                string qry = "insert into [User] (Name,Email,[password],Mob,Addr,registered) values(@t1,@t2,@t3,@t4,@t5,@t6)";
                SqlCommand cmd = new SqlCommand(qry, con);
                cmd.Parameters.AddWithValue("@t1", name);
                cmd.Parameters.AddWithValue("@t2", email1);
                cmd.Parameters.AddWithValue("@t3", pwd);
                cmd.Parameters.AddWithValue("@t4", number);
                cmd.Parameters.AddWithValue("@t5", addr);
                cmd.Parameters.AddWithValue("@t6", "False");
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    SqlCommand cmds = new SqlCommand("insert into otp(Usernum,otp) values(@Usernum,@otp)", con);
                    cmds.Parameters.AddWithValue("@Usernum", number);
                    cmds.Parameters.AddWithValue("@otp", otp);
                    cmds.ExecuteNonQuery();
                    SendSms(number, otp);
                    returnvalue = otp;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
        con.Close();

        return returnvalue;
    }

    [WebMethod]
    public static string otpsubmit(string otp, string number)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        string returnvalue = "";
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string qry = "select * from otp where otp='" + otp + "' and Usernum='" + number + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                dr.Close();
                SqlCommand cmd2 = new SqlCommand("update [User] set registered='True' where Mob='" + number + "'", con);
                int j = cmd2.ExecuteNonQuery();
                if (j == 1)
                {
                    returnvalue = "True";
                }
            }
            else
            {
                returnvalue = "False";
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
        con.Close();

        return returnvalue;
    }


    public class Banners
    {
        public int Id { get; set; }
        public string Banner { get; set; }
    }


    //protected void btnsubmit_Click1(object sender, EventArgs e)
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    string qry = "select * from [User] where [Email]=@t1 or Mob=@t1 and [password]=@t2 and registered='True'";
    //    SqlCommand cmd = new SqlCommand(qry, con);
    //    cmd.Parameters.AddWithValue("@t1", Request.Form["email"]);
    //    cmd.Parameters.AddWithValue("@t2", Request.Form["password"]);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        Session["usr"] = dr["Email"].ToString();
    //        Response.Redirect("Home.aspx");
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Username and Password mismatch')", true);
    //    }
    //    dr.Close();
    //    con.Close();
    //}

    [WebMethod]
    public static string Login(string Email, string pwd)
    {
        EducationalhubEntities ent = new EducationalhubEntities();
        var user = ent.Users.FirstOrDefault(a => (a.Email == Email || a.Mob == Email) && a.password == pwd);
        if (user == null)
            return "False";
        if (user.registered == false)
            return "notRegistered";
        return "True";
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        //if (con.State == ConnectionState.Closed)
        //{
        //    con.Open();
        //}
        //string returnval = "";
        //string qry = "select * from [User] where ([Email]=@t1 or Mob=@t1) and [password]=@t2 and registered=1";
        //SqlCommand cmd = new SqlCommand(qry, con);
        //cmd.Parameters.AddWithValue("@t1",Email);
        //cmd.Parameters.AddWithValue("@t2", pwd);
        //SqlDataReader dr = cmd.ExecuteReader();
        //dr.Read();
        //if (dr.HasRows)
        //{
        //    returnval = "True";
        //}
        //else
        //{
        //    returnval = "False";
        //}
        //dr.Close();
        //con.Close();
    }

    public static void SendSms(string number, string otp)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string mobileNumber = number;
        string m = "Your OTP is:" + otp;
        string authKey = "134832AN9sHl5pu585e3153";
        //Multiple mobiles numbers separated by comma
        //Sender ID,While using route4 sender id should be 6 characters long.
        string senderId = "SPDOM";
        //Your message to send, Add URL encoding here.
        string message = HttpUtility.UrlEncode(m);
        //Prepare you post parameters
        StringBuilder sbPostData = new StringBuilder();
        sbPostData.AppendFormat("authkey={0}", authKey);
        sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        sbPostData.AppendFormat("&message={0}", message);
        sbPostData.AppendFormat("&sender={0}", senderId);
        sbPostData.AppendFormat("&route={0}", "4");
        try
        {
            //Call Send SMS API
            string sendSMSUri = "https://control.msg91.com/api/sendhttp.php";
            //Create HTTPWebrequest
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
            //Prepare and Add URL Encoded data
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(sbPostData.ToString());
            //Specify post method
            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/x-www-form-urlencoded";
            httpWReq.ContentLength = data.Length;
            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            //Get the response
            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string responseString = reader.ReadToEnd();

            //Close the response
            reader.Close();
            response.Close();
        }
        catch
        {

        }
        con.Close();
    }


    public void text()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Top 1 * from AboutMaster", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["About"].ToString();
                Literal1.Text = System.Net.WebUtility.HtmlDecode(str);
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        finally
        {
            con.Close();
        }
    }

    public void text1()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Top 1 * from FeatureMaster", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["Feature"].ToString();
                Literal2.Text = System.Net.WebUtility.HtmlDecode(str);
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        finally
        {
            con.Close();
        }
    }
}
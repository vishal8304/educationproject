﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="ssctestseries.aspx.cs" Inherits="ssctestseries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .btn{

    background: none;
    border: none;
    color:rgb(54, 54, 56);
    text-decoration: underline;
    cursor: pointer;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="contact">
		<div class="container">
            <center><h3 style="margin-top: 2%;"><asp:Label ID="lbltest" runat="server"></asp:Label></h3></center>
            <br />
           <div class="row" style="position:relative">
                
               <div class="col-md-12" style="top:0;position:relative">
            <div class="row">
                <div class="col-md-12"style="margin-top:5%; margin-left:5%;">
                    <asp:GridView ID="grid" Width="90%" CssClass="table table-bordered" runat="server" AutoGenerateColumns="False" DataKeyNames="name" OnRowDataBound="grid_RowDataBound">
                
                <Columns>
                     <asp:TemplateField ItemStyle-Width="120px" HeaderText="Name" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">

<ItemTemplate>
    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name").ToString()%>'/>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" HeaderText="Total Questions" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
   <asp:Label ID="lbltotalqs" runat="server" Text='<%#Eval("COUNT") %>'></asp:Label>
</ItemTemplate>
                      
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" HeaderText="Total Marks" ControlStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="totalmarks" runat="server" Text='<%#Eval("totalmarks") %>'></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                     <asp:TemplateField ItemStyle-Width="120px" HeaderText="Negative Marks" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lblnegative" runat="server" Text="25%"></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Total Time" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lbltime" runat="server" Text='<%#Eval("totaltime") %>'></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" HeaderText="Marks Obtained" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lblobt" runat="server" Text='<%#Eval("TotalMarksObt") %>'></asp:Label>
</ItemTemplate>
                     
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:HyperLink ID="Hlink" runat="server"></asp:HyperLink>
</ItemTemplate>
</asp:TemplateField>
                     
                </Columns>
            </asp:GridView>
                </div>
            </div></div>
       <%--  <div class="col-md-3" style="position:absolute;bottom:0;right:0">
            <div class="row">
                <div class="col-md-12">
                    <asp:panel ID="pnl" runat="server" style="border-style:solid;border-color:black">
                    <div class="row">
                        <div class="col-md-12" style="text-align:center">
                            <strong>
                            Feedback
                                </strong>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                        <textarea name="txtfeedback" style="height:100px" required="required" cols="22"></textarea>
                       
                    </div></div>
                        <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lblsave" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align:center">
                            <asp:Button ID="btnSend" runat="server" Text="Send" OnClick="btnSend_Click" />
                        </div>
                    </div>
                        </asp:panel>
                </div>
            </div></div>--%>
            </div>
					</div>
			</div>

   
     <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  
</asp:Content>
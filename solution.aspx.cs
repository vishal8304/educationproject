﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class solution : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            getquizssc();
            getquizbank();
            getssconline();
            getbankonline();
            getquizupsc();
            getupsconline();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    public class ontimertest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string descr { get; set; }
        public string solimg { get; set; }
        public string solved { get; set; }
    }
    public class onbanktest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Optionval5 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string descr { get; set; }
        public string solimg { get; set; }
        public string solved { get; set; }
    }

    public class onupsctest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string val1 { get; set; }
        public string val2 { get; set; }
        public string val3 { get; set; }
        public string val4 { get; set; }

        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string descr { get; set; }
        public string solimg { get; set; }
        public string solved { get; set; }
    }

    public class test
    {
        public int Id { get; set; }
        public string Ques { get; set; }
        public string val1 { get; set; }
        public string val2 { get; set; }
        public string val3 { get; set; }
        public string val4 { get; set; }
        public string path { get; set; }
        public string descr { get; set; }
        public string qspath { get; set; }
        public string solvedio { get; set; }
    }

    //public void getperformance()
    //{
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    var set = Request.QueryString["section"];
    //    var url=Request.QueryString["ssconline"];
    //    SqlCommand cmd = new SqlCommand("Select name,[set],sum(marks) as totalmarks,sum(time) as totaltime, count(Question) as count, dbo.GetTotalMarksBySet('"+set+"', name , '"+Session["usr"].ToString()+"') as TotalMarksObt from ontimertest where [set] = '"+set+"' and name='"+url+"' GROUP BY[set], name", con);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    dr.Read();
    //    if (dr.HasRows)
    //    {
    //        decimal TotalMarksObt = Convert.ToDecimal(dr["TotalMarksObt"].ToString());
    //        decimal totalmarks= Convert.ToDecimal(dr["totalmarks"].ToString());
    //        lblscore.Text = TotalMarksObt + "/" + totalmarks;
    //    }
    //    dr.Close();
    //    con.Close();
    //}
    public void getquizssc()
    {

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var ssctest = Request.QueryString["sscquiz"];
        if (ssctest != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from onlinetest where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + ssctest + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getranksscquiz]() where name='" + ssctest + "' group by name ", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from test where name='" + ssctest + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;

            dr2.Close();
            dr.Close();
            con.Close();

        }
    }
    public void getssconline()
    {
        //SSC Online Open
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var ssconline = Request.QueryString["ssconline"];
        var section = Request.QueryString["section"];
        if (ssconline != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from onusertest where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + ssconline + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            //SSC Online Closed

            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getssconlinerank]() where name='" + ssconline + "' group by name", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from ontimertest where name='" + ssconline + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;
            dr2.Close();
            dr.Close();
            con.Close();
        }
    }

    public void getquizbank()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var bankquiz = Request.QueryString["bankquiz"];
        if (bankquiz != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from userbankquiz where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + bankquiz + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getrankbankquiz]() where name='" + bankquiz + "' group by name", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from Bankquiz where name='" + bankquiz + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;
            dr2.Close();
            dr.Close();
            con.Close();
        }
    }
    public void getbankonline()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var Bankonline = Request.QueryString["Bankonline"];
        var section = Request.QueryString["section"];
        if (Bankonline != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from useronbanktest where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + Bankonline + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getbankonlinerank]() where name='" + Bankonline + "' group by name ", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from onbanktest where name='" + Bankonline + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;
            dr2.Close();
            dr.Close();
            con.Close();
        }
    }
    public void getquizupsc()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var upscquiz = Request.QueryString["upscquiz"];
        if (upscquiz != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from UpscquizTest where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + upscquiz + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getrankupscquiz]() where name='" + upscquiz + "' group by name", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from UpscQuiz where name='" + upscquiz + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;
            dr2.Close();
            dr.Close();
            con.Close();
        }
    }
    public void getupsconline()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var upsconline = Request.QueryString["upsconline"];
        var section = Request.QueryString["section"];
        if (upsconline != null)
        {
            var Rank = "";
            var sums = "";
            var marksobt = "";
            var totmarks = "";

            SqlCommand cmd = new SqlCommand("SELECT sum(marksobt) as marksobt, Email, name, DENSE_RANK() OVER(partition by name order by sum(marksobt) desc) AS RankByMarks from UpscquizTest where Email = '" + Request.Cookies["Email"].Value + "' and name = '" + upsconline + "' group by Email, name", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Rank = dr["RankByMarks"].ToString();
                marksobt = dr["marksobt"].ToString();
            }
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("SELECT sum(counts) as sums,name from [dbo].[getbankonlinerank]() where name='" + upsconline + "' group by name ", con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();
            if (dr2.HasRows)
            {
                sums = dr2["sums"].ToString();
            }
            dr2.Close();
            SqlCommand cmd3 = new SqlCommand("select max(marks) as totmarks,name from onbanktest where name='" + upsconline + "' group by name", con);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            dr3.Read();
            if (dr3.HasRows)
            {
                totmarks = dr3["totmarks"].ToString();
            }
            lblrank.Text = Rank + "/" + sums;
            lblscore.Text = marksobt + "/" + totmarks;
            dr2.Close();
            dr.Close();
            con.Close();
        }
    }
    [WebMethod]
    public static List<ontimertest> divsol(string decode)
    {
        DataTable dt = new DataTable();
        List<ontimertest> objdept = new List<ontimertest>();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decode != null)
            {
                SqlCommand cmd = new SqlCommand("select * from ontimertest where name='" + decode + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objdept.Add(new ontimertest
                    {
                        descr = dt.Rows[i]["descr"].ToString(),
                        Question = dt.Rows[i]["Question"].ToString(),
                        path = dt.Rows[i]["path"].ToString(),
                        solimg = dt.Rows[i]["solimg"].ToString(),
                        solved = dt.Rows[i]["solved"].ToString()
                    });
                }
            }
            con.Close();
            return objdept;
        }
    }
    [WebMethod]
    public static List<onbanktest> divbanksol(string decodebank)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decodebank != null)
            {
                SqlCommand cmd = new SqlCommand("select * from onbanktest where name='" + decodebank + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objdept.Add(new onbanktest
                    {
                        descr = dt.Rows[i]["descr"].ToString(),
                        Question = dt.Rows[i]["Question"].ToString(),
                        path = dt.Rows[i]["path"].ToString(),
                        solimg = dt.Rows[i]["solimg"].ToString(),
                        solved = dt.Rows[i]["solved"].ToString()
                    });
                }
            }
            con.Close();
            return objdept;
        }
    }
    [WebMethod]
    public static List<onupsctest> divupscsol(string decodeupsc)
    {
        DataTable dt = new DataTable();
        List<onupsctest> objdept = new List<onupsctest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decodeupsc != null)
            {
                SqlCommand cmd = new SqlCommand("select * from UpscQuiz where name='" + decodeupsc + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objdept.Add(new onupsctest
                    {
                        descr = dt.Rows[i]["descr"].ToString(),
                        Question = dt.Rows[i]["Ques"].ToString(),
                        solved = dt.Rows[i]["correctoption"].ToString()
                    });
                }
            }
            con.Close();
            return objdept;
        }
    }


    public class upscquiz
    {
        public int Id { get; set; }
        public string Ques { get; set; }
        public string solpath { get; set; }
        public string qspath { get; set; }
        public string descr { get; set; }
        public string solvideo { get; set; }
    }
    [WebMethod]
    public static List<upscquiz> btnupscquiz(string decode)
    {
        DataTable dt = new DataTable();
        List<upscquiz> objdept = new List<upscquiz>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decode != null)
            {
                SqlCommand cmd = new SqlCommand("select * from UpscQuiz where name='" + decode + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new upscquiz
                        {
                            Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
                            Ques = dt.Rows[i]["Ques"].ToString(),
                            solpath = dt.Rows[i]["solpath"].ToString(),
                            qspath = dt.Rows[i]["qspath"].ToString(),
                            descr = dt.Rows[i]["descr"].ToString(),
                            solvideo = dt.Rows[i]["solvideo"].ToString()
                        });
                    }
                }
                con.Close();
                return objdept;
            }
        }
    }
    [WebMethod]
    public static List<test> sscquizsol(string decode)
    {
        DataTable dt = new DataTable();
        List<test> objdept = new List<test>();

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decode != null)
            {
                SqlCommand cmd = new SqlCommand("select * from test where name='" + decode + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objdept.Add(new test
                    {
                        descr = dt.Rows[i]["descr"].ToString(),
                        Ques = dt.Rows[i]["Ques"].ToString(),
                        path = dt.Rows[i]["path"].ToString(),
                        qspath = dt.Rows[i]["qspath"].ToString(),
                        solvedio = dt.Rows[i]["solvedio"].ToString()
                    });
                }
            }
            con.Close();
            return objdept;
        }
    }
    public class Bankquiz
    {
        public int Id { get; set; }
        public string Ques { get; set; }
        public string solpath { get; set; }
        public string qspath { get; set; }
        public string descr { get; set; }
        public string solvideo { get; set; }
    }
    [WebMethod]
    public static List<Bankquiz> btnbankquiz(string decode)
    {
        DataTable dt = new DataTable();
        List<Bankquiz> objdept = new List<Bankquiz>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (decode != null)
            {
                SqlCommand cmd = new SqlCommand("select * from Bankquiz where name='" + decode + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new Bankquiz
                        {
                            Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
                            Ques = dt.Rows[i]["Ques"].ToString(),
                            solpath = dt.Rows[i]["solpath"].ToString(),
                            qspath = dt.Rows[i]["qspath"].ToString(),
                            descr = dt.Rows[i]["descr"].ToString(),
                            solvideo = dt.Rows[i]["solvideo"].ToString()
                        });
                    }
                }
                con.Close();
                return objdept;
            }
        }
    }
    [WebMethod]
    public static string sendData(string txtfeed, string test, string decode, string Email)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        string returnval = "";
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("insert into feedback(feedback,Email,test) values(@t1,@t2,@t3)", con);
        cmd.Parameters.AddWithValue("@t1", txtfeed);
        cmd.Parameters.AddWithValue("@t2", Email);
        cmd.Parameters.AddWithValue("@t3", test);
        try
        {
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                returnval = "true";
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
        con.Close();
        return returnval;

    }
}
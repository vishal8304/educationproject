﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Search : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            valMembers();
            valcomments();
        }
    }
    public void valMembers()
    {
        string a = Session["search"].ToString();
        //Select u.Userphoto,u.Username,s.ServiceName,s.ServiceField,s.ServiceCost FROM Users as u inner join Services as s on u.userId = s.userId"
        da = new SqlDataAdapter("select u.Name,a.Post,a.exam,a.subject,a.picpath from [User] as u inner join Askquery as a on (u.Email=a.Email) and a.Post like '%" + a + "%' or a.Post like '" + a + "%' or u.Name like '%" + a + "%' or u.Name like '" + a + "%' or a.exam like '" + a + "%' or a.exam like '%" + a + "%' or a.[subject] like '" + a + "%' or a.[subject] like '%" + a + "%'", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptqs.DataSource = dt;
        rptqs.DataBind();
    }
    public void valcomments()
    {
        da = new SqlDataAdapter("select c.Comments from ask as c inner join Askquery as q on c.Aid=q.Id", con);
        DataTable dt1 = new DataTable();
        da.Fill(dt1);
        rptcom.DataSource = dt1;
        rptcom.DataBind();
    }

    protected void btnshare_Click(object sender, EventArgs e)
    {

    }

    protected void btnEnter_Click(object sender, EventArgs e)
    {
        con.Open();
        string qry = "insert into ask values(@t1,@t2,@t3)";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.Parameters.AddWithValue("@t1", Request.Form["txtcom"]);
        cmd.Parameters.AddWithValue("@t2", Session["usr"].ToString());
        //cmd.Parameters.AddWithValue("@t3",);
    }

    protected void btnlike_Click(object sender, EventArgs e)
    {
        con.Open();
        string sqlquery = "insertlikes";
        SqlCommand com = new SqlCommand(sqlquery, con);
        com.CommandType = CommandType.StoredProcedure;
        com.CommandText = "insertlikes";
        com.Parameters.AddWithValue("@Emailid", SqlDbType.VarChar).Value = Session["usr"].ToString();
        //com.Parameters.AddWithValue("@Postid", SqlDbType.VarChar).Value
        //com.Parameters.AddWithValue("@t1", );
        int i = com.ExecuteNonQuery();
    }
}
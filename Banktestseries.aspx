﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Banktestseries.aspx.cs" Inherits="Banktestseries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .btn{

    background: none;
    border: none;
    color:rgb(54, 54, 56);
    text-decoration: underline;
    cursor: pointer;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="contact">
		<div class="container">
               
            <center><h3 style="margin-top:2%;">Bank Test Series</h3></center><br />
            <div class="row">
                <div class="col-md-12" style="margin-left:5%;">
                    <asp:GridView ID="grid" Width="90%" CssClass="table table" runat="server" AutoGenerateColumns="False" DataKeyNames="name" OnRowDataBound="grid_RowDataBound">
                
                <Columns>
                    
                  
                 
                     <asp:TemplateField ItemStyle-Width="120px" HeaderText="Name" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name").ToString()%>'/>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" HeaderText="Total qs" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
   <asp:Label ID="lbltotalqs" runat="server" Text='<%#Eval("count") %>'></asp:Label>
</ItemTemplate>
                      
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" HeaderText="Total Marks" ControlStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="totalmarks" runat="server" Text='<%#Eval("totalmarks") %>'></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                     <asp:TemplateField ItemStyle-Width="120px" HeaderText="Negative Marks" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lblnegative" runat="server" Text="25%"></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Total Time" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lbltime" runat="server" Text='<%#Eval("totaltime") %>'></asp:Label>
</ItemTemplate>
                       
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" HeaderText="Marks Obtained" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:Label ID="lblobt" runat="server" Text='<%#Eval("TotalMarksObt") %>'></asp:Label>
</ItemTemplate>
                     
</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="120px" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
<ItemTemplate>
    <asp:HyperLink ID="Hlink" runat="server"></asp:HyperLink>
</ItemTemplate>
                     
</asp:TemplateField>
                   
                    
                     
                </Columns>
            </asp:GridView>
                </div>
            </div>
            
            
            <br />
           
					</div>
				
			</div>
     <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    
</asp:Content>
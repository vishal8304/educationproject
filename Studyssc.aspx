﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Studyssc.aspx.cs" Inherits="Studyssc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        a:hover{
            color:blue;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <h3>SSC</h3>
        </div>
    </div><br />
      <div class="row">
            <div class="col-sm-3">
                <a href="Studyssc.aspx?section=Quantitative Aptitude"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#ffb52f;text-align:center"><br /><br />
                        Quantitative Aptitude
                    </div>
                </a>
            </div>
          <div class="col-sm-3">
                <a href="Studyssc.aspx?section=English"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#33cc99;text-align:center"><br /><br />
                        English
                    </div>
                </a>
            </div>
          <div class="col-sm-3">
                <a href="Studyssc.aspx?section=Reasoning"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#eb5424;text-align:center"><br /><br />
                        Reasoning
                    </div>
                </a>
            </div>
          <div class="col-sm-3">
                <a href="Studyssc.aspx?section=General Studies"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#55c2e2;text-align:center"><br /><br />
                        General Studies
                    </div>
                </a>
            </div>
      </div><br /><br />
     <div class="row">
         
         <%#Eval ("Chapter") %>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                          <div class="col-sm-3">
                           

                <a href="ssc.aspx?sscId=<%#Eval("sscId") %>"  style="text-decoration: none !important;">
                     
                    <div style="width:200px;height:150px;background-color:#55c2e2;text-align:center"><br /><br />
<div>  </div> 
<%#Eval ("Topic") %>

                                                   </div>
                </a>
            </div>
                    </ItemTemplate>
            
                    <AlternatingItemTemplate>
                                <div class="col-sm-3">
                <a href="#"  style="text-decoration: none !important;">
                    <div style="width:200px;height:150px;background-color:cadetblue;text-align:center"><br /><br />
                        <%#Eval ("Chapter") %>
                                            </div>
                </a>
            </div>
                    </AlternatingItemTemplate>
                </asp:Repeater>


     </div>

           <br />

</asp:Content>


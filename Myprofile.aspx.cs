﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Myprofile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string pwd = txtpwd.Text;
            if (pwd == null || pwd == "")
            {

            }
            else
            {
                SqlCommand cmd = new SqlCommand("select * from [User] where [password]=@t2 and [Email]=@t1 ", con);
                cmd.Parameters.AddWithValue("@t2", pwd);
                cmd.Parameters.AddWithValue("@t1", Request.Cookies["Email"].Value);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    pnlchpwd.Visible = true;
                    pnlpwd.Visible = false;
                }
                else
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Incorrect Password";
                }
            }
        }
        catch (Exception ex)
        {
            lblmsg.Visible = true;
            lblmsg.Text = ex.ToString();
        }
    }

    protected void btnchng_Click(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string txt1 = Request.Form["password"];
            string txt2 = Request.Form["confirm_password"];
            if(txt1 == "" && txt2 == "")
            {
                lblchng.Visible = true;
                lblchng.Text = "Password and Confirm Password can't be blank";
            }
            if(txt1 == "" && txt2 != "")
            {
                lblchng.Visible = true;
                lblchng.Text = "Confirm Password can't be blank";
            }
            if (txt1 != "" && txt2 == "")
            {
                lblchng.Visible = true;
                lblchng.Text = "Password can't be blank";
            }
            if (txt1 == txt2)
            {
                SqlCommand cmd1 = new SqlCommand("Update [User] set password=@tp where [Email]=@t3", con);
                cmd1.Parameters.AddWithValue("@tp", txt2);
                cmd1.Parameters.AddWithValue("@t3", Request.Cookies["Email"].Value);
                int i = cmd1.ExecuteNonQuery();
                if (i == 1)
                {
                    lblchng.Visible = true;
                    lblchng.Text = "Password Updated";
                }
            }
            else
            {
                lblchng.Visible = true;
                lblchng.Text = "Password mismatch";
            }
        }
        catch (Exception ex)
        {
            lblchng.Visible = true;
            lblchng.Text = ex.ToString();
        }
    }

    //protected void btnUpdate_Click(object sender, EventArgs e)
    //{
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    SqlCommand cmdprofile = new SqlCommand("Update [User] set profilepath=@tprofile where [Email]=@t3", con);
    //    string fp;
    //    if (profile.HasFile)
    //    {
    //        fp = "/User/" + profile.FileName.ToString();
    //        profile.PostedFile.SaveAs(Server.MapPath("/User/") + profile.FileName.ToString());
    //        cmdprofile.Parameters.AddWithValue("@tprofile", fp);
    //        cmdprofile.Parameters.AddWithValue("@t3", Request.Cookies["Email"].Value);
    //        cmdprofile.ExecuteScalar();
    //        Response.Redirect("MyProfile.aspx");
    //    }
    //}

    public class follow
    {
        public string Following { get; set; }
    }


    public class mcqquery
    {
        public int Id { get; set; }
        public string Ques { get; set; }
        public string opta { get; set; }
        public string optb { get; set; }
        public string optc { get; set; }
        public string optd { get; set; }
        public string opte { get; set; }
        public string picpath { get; set; }
        public string Email { get; set; }


    }


    [WebMethod]
    public static List<mcqquery> Savedpost(string Email)
    {
        DataTable dt = new DataTable();
        List<mcqquery> objdept = new List<mcqquery>();


        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("select a.* from mcqquery a inner join Savepost b on a.Id=b.shareId", con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new mcqquery
                        {
                            Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
                            picpath = dt.Rows[i]["picpath"].ToString(),
                            Ques = dt.Rows[i]["Ques"].ToString(),
                            opta = dt.Rows[i]["opta"].ToString(),
                            optb = dt.Rows[i]["optb"].ToString(),
                            optc = dt.Rows[i]["optc"].ToString(),
                            optd = dt.Rows[i]["optd"].ToString(),
                            opte = dt.Rows[i]["opte"].ToString()
                        });
                    }
                }

            }

            return objdept;

        }

    }


    //public void Test()
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
    //    con.Open();
    //    SqlCommand cmd = new SqlCommand("select Count(a.Followers) as Followers from Followers a inner join mcqquery b on a.Username=b.Email where a.Followers=" + Request.Cookies["Email"].Value, con);
    //    SqlDataReader sdr = cmd.ExecuteReader();
    //    if (sdr.HasRows)
    //    {
    //        Label1.Text = sdr["Followers"].ToString();
    //    }

    //}


    //[WebMethod]
    //public static List<onusertest> query(string Email)
    //{
    //    DataTable dt = new DataTable();
    //    List<onusertest> objdept = new List<onusertest>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("select * from onusertest where Email='"+Email+"'", con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                      using (SqlCommand cmd2 = new SqlCommand("select count(Email) as count from onusertest where Email='"+Email+"'", con))
    //                    {
    //                        objdept.Add(new onusertest
    //                        {
    //                            Email = dt.Rows[i]["count"].ToString()
    //                        });
    //                    }
    //                }
    //            }
    //            return objdept;
    //        }
    //    }
    //}

}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Signin.aspx.cs" Inherits="Signin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Classy Register Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
    <script type="application/x-javascript"> 
    addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } 
    </script>

<!-- Meta tag Keywords -->
<!-- css files -->
    
    <link href="css/classystyle.css" rel="stylesheet" />
<!-- //css files -->
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Cuprum:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
</head>
<body>
<div class="header">
	<h1>Login Form</h1>
</div>
<div class="w3-main">
	<!-- Main -->
	<div class="about-bottom main-agile book-form">
		<div class="alert-close"> </div>
		<h2 class="tittle">Login Here</h2>
		<form action="#" method="post" runat="server">
			<div class="form-date-w3-agileits">
				<label> Username: </label>
				<input type="text" name="username" placeholder="Your Username" required="" />
				<label> Password </label>
				<input type="password" name="pwd" placeholder="Your Password" required=""/>
				
			</div>
			<div class="make wow shake">
				  <asp:button runat="server" ID="btnsubmit" OnClick="btnsubmit_Click" type="submit" Text="Login" value="Register"/>
			</div>
            <div>
                <asp:Label ID="lblConfirm" runat="server" Visible="false" Font-Bold="True" ForeColor="White"></asp:Label>
            </div>
		</form>
	</div>
	<!-- //Main -->
</div>
<!-- footer -->

<!-- //footer -->
<!-- js-scripts-->
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		
		<script>$(document).ready(function(c) {
		$('.alert-close').on('click', function(c){
			$('.main-agile').fadeOut('slow', function(c){
				$('.main-agile').remove();
			});
		});	  
	});
	</script>
<!-- //js-scripts-->
</body></html>

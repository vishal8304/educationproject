﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .labelz {
            font-style: italic;
            font-size: 14px;
        }
	
.flex-caption {
  width: 96%;
  padding: 2%;
  left: 0;
  bottom: 0;
  background: rgba(0,0,0,.5);
  color: #fff;
  text-shadow: 0 -1px 0 rgba(0,0,0,.3);
  font-size: 14px;
  line-height: 18px;
}

    </style>
	<script>// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});</script>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnplace" runat="Server">
	<a class="active" href="#" data-toggle="modal" data-target="#myModal2" style="margin-left: 76%"><i class="fa fa-sign-in" aria-hidden="true" ></i>Sign In</a>
    <a href="#" data-toggle="modal" data-target="#myModal3"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Sign Up</a> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
                        <h3 class="agileinfo_sign">Sign In</h3>

                        <div class="login-form">
                            <form action="#" id="forms" method="post" runat="server"  >

                                <input type="text" id="email" name="email" placeholder="E-mail/Mobile Number" required="required" />

                                <input type="password" id="password" name="password" placeholder="Password" required="required" />
                                <div class="tp">
                                    <span id="msg"></span>
                                </div>
                                <div class="tp">
                                    <input  type="button" class="btn btn-primary" id="signin" value="Signin" style="width:100%" runat="server" autofocus="autofocus" onkeydown="cliked()" onclick="clicked()" />
                                </div>
                            </form>
                        </div>
                        <div class="login-social-grids">
                        </div>

                        <p><a href="#" data-toggle="modal" data-target="#myModal3">Don't have an account?</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- Modal2 -->
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <div class="signin-form profile">
                        <h3 class="agileinfo_sign">Sign Up</h3>
                        <div class="login-form" id="formk">
                            <form action="#" method="post">
                                <input type="text" id="name" name="name" placeholder="Username" required="" />
                                <input type="email" id="email1" name="email1" placeholder="Email" required="" />
                                <input type="password" id="pwd" name="pwd" placeholder="Password" onkeyup="checkPass(); return false;" required="" />
                                <input type="password" name="pwd2" id="pwd2" onkeyup="checkPass(); return false;" placeholder="Confirm Password" required="" />
                                <input type="text" placeholder="Mobile Number" name="number" id="number" required="" />
                                <input type="text" placeholder="City/Town" id="addr" name="addr" required="" />
                                <label class="labelz">*You will get an otp after Sign Up. Please confirm that otp for successful registration</label>
                                <input type="submit" id="register" name="register" data-toggle="modal" data-target="#myModal4" value="Sign Up" /><br />
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <div id="error-nwl"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <p><a href="#">By clicking register, I agree to your terms</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
                        <h3 class="agileinfo_sign">OTP</h3>
                        <div class="login-form">
                            <form action="#" method="post">
                                <input type="text" placeholder="OTP" name="txtotp" id="txtotp" />
                                <div class="tp">
                                    <input type="submit" id="otpsubmit" name="otpsubmit" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <!-- banner -->
	<div class="w3ls-banner">
		<!-- banner-text -->
			<div class="flexslider">
				<ul class="slides">
                    <%foreach (var i in Banner)
                        {%>
                     <li>
      <img src="<%=i.Banner %>" />
    </li>
                    <%} %>
   
   <%-- <li>
      <img src="/images/slide2.jpg" />
   
    </li>
    <li>
      <img src="images/slide3.jpg" />
    
    </li>--%>
  
  </ul>
			</div>    
		</div> <BR>
		<!-- //banner-text -->  
	<!-- //banner -->
	<!-- services -->
	<div class="services">
		<div class="container">   
			<h3 class="agileits-title">Top Courses</h3>
			
			
			<div class="w3-services-grids">
				<div class="col-md-3 col-xs-6 w3l-services-grid">
					<img src="/images/c1.jpg">
				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/c2.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C3.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C4.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C5.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C6.jpg">

				</div>
				
				
				
				
				
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
	<!-- //services -->
	<!-- about-slid -->
	<div class="about-w3slid jarallax">
		<div class="subscribe-agileinfo"> 
			<div class="container">  
				<h3>WELCOME TO SPEEDOMETER COACHING
</h3>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
				<%--<p>We counsel students on regular basis. Also, inform them on upcoming exams in which students can appear. Our dedicated career service team contact students and help them if they are facing any problems. Students can easily reach our career service team.</p> 
				<p>World Class comprehensive content covering all important concepts in detail. Students will also get comprehensive analysis and detailed reports for all the tests taken. Students can analyze their performance, identify strengths and weakness and maximize scores through our detailed reports and graphs. Our content will help you to crack actual exams as pattern of the tests are same as the actuals exams with varied level of difficulty.</p>--%>
			</div>
		</div>
	</div><BR><hr><BR>
	<div class="about-w3slid jarallax">
		<div class="subscribe-agileinfo"> 
			<div class="container">  
				<h3>OUR FEATURES</h3>
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
<%--<p>Our exceptional features distinguish us from the rest of the crowd. We have the best of everything assembled together to deliver finesse results.</p>
					<div class="w3-services-grids">
				<div class="col-md-3 col-xs-6 w3l-services-grid">
					<img src="/images/t1.png" style="height: 100px;" ><h4>SKILLED TEACHER</h4>
					<P>we have well-qualified and experienced teachers who impart necessary knowledge and proper guidance for the all-round professional growth of individual students.</P>
				</div>
				
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/t3.png" style="height: 100px;"><h4>STUDY MATERIAL</h4>
					<P>Well researched update & detailed study material imparting immense value to student & aspirants.Exhaustive and impeccable study material for thorough/better understanding </P>

				</div>
						<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/t2.png" style="height: 100px;"><h4>ONLINE TESTS</h4>
					<P>Test Series & quizes with explicit focus on the various competetive exam.We ace the field of online tests with our extensive series of tests.</P>

				</div>


			</div>--%>
		</div>
	</div>
		</div>
	<!-- //about-slid --> 
	<!-- welcome -->
	<div class="welcome" > 
		<div class="container">
			<h3>Speedometer Benifits </h3>
<p >Pellentesque habitant morbi tristique senectus et netus et malesuada fames rutrum fringilla fermentum ac turpis egestas auris rutrum fringilla fermentum. Donec tincidunt, eros quis. </p>			
			<div class="welcome-agileinfo">
				<div class="col-md-7 agile-welcome-left"> 
					<div class="col-sm-6 col-xs-6 welcome-w3imgs">
						<figure class="effect-chico">
							<img src="HomeDesign/images/g3.jpg" alt=" " />
							<figcaption>
								<h4>Staff Selection Commission</h4>
								<p>The Staff Selection Commission SSC conducts the various examinations such as SSC CGL, SSC CHSL etc./p>
							</figcaption>			
						</figure>
						<figure class="effect-chico welcome-img2">
							<img src="HomeDesign/images/g4.jpg" alt=" " />
							<figcaption>
								<h4>Banking</h4>
								<p>A bank is a financial institution that accepts deposits from the public and creates credit.</p>
							</figcaption>			
						</figure>
					</div>
					<div class="col-sm-6 col-xs-6 welcome-w3imgs">
						<figure class="effect-chico">
							<img src="HomeDesign/images/g2.jpg" alt=" " />
							<figcaption>
								<h4>Railway</h4>
								<p>The first railway on Indian sub-continent ran over a stretch of 21 miles from Bombay to Thane.</p>
							</figcaption>			
						</figure>
						<figure class="effect-chico welcome-img2">
							<img src="HomeDesign/images/g1.jpg" alt=" " />
							<figcaption>
								<h4>CTET</h4>
								<p>Central Teacher Eligibility Test (CTET) will be conducted by Central Board of Secondary Education Delhi.</p>
							</figcaption>			
						</figure>
					</div>
				<div class="clearfix"> </div> 
			</div>
			<div class="col-md-5 agile-welcome-right">
				<h4>World Class comprehensive content covering all important concepts in detail. Students will also get comprehensive analysis and detailed reports for all the tests taken. Students can analyze their performance, identify strengths and weakness and maximize scores through our detailed reports and graphs. Our content will help you to crack actual exams as pattern of the tests are same as the actuals exams with varied level of difficulty. 
				<p>Personal attention is given to each student. We have team of mentors who provide regular guidance to all the students and focus on their performance. We provide  doubt clearing sessions on Maths and also Mock Test doubt clearing session by Examination Cell to the students. </p>
	
			</div>
			<div class="clearfix"> </div>
		</div>
	</div> 		
	</div>
	<!-- //welcome -->
	<!-- about-slid -->
	<div class="about-w3slid jarallax">
		<div class="sub-agileinfo">
			<div class="container">
				<h3 class="agileits-title w3title1">Get our free newsletter</h3>
				<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est consectetur adipisci velit sed quia non numquam eius.</p>
				<form>
					<input type="email" name="email" placeholder="Email Address" class="user" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div> 
		</div>
	</div>
	<!-- //about-slid --> 
	<!-- services-bottom -->
	<div class="services-bottom">
		<div class="container">
			<div class="agileits-heading">
				<h3 class="agileits-title">Featured Services</h3>
			</div>
			<div class="wthree-services-bottom-grids">
				<div class="col-md-6 wthree-services-left">
					<img src="HomeDesign/images/g8.jpg" alt="" />
				</div>
				<div class="col-md-6 wthree-services-right">
					<div class="wthree-services-right-top">
						<h4>Bank Probationary Officer</h4>
						<p>This is a managerial position in a bank. After the essential 1 to 2-year training, candidates are designated as Assistant Managers (AM) or Deputy Managers (DM)</p>
					</div>
					<div class="wthree-services-right-bottom">
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-bell" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Skilled Faculty</h5>
								<p>We have a dedicated group of faculty that is inclined towards the all-round professional growth of the students. They provide aspirants with necessary guidance and knowledge to scale Bank Exams with ease.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-asterisk" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Best Online Tests</h5>
								<p>We provide our students with a real time feel of the examinations conducted through our well-designed Test Series.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="wthree-services-bottom-grids w3-services-bottom">
				<div class="col-md-6 wthree-services-right">
					<div class="wthree-services-right-top">
						<h4>SSC CGL 2019 Exam Pattern</h4>
						<p>SSC CGL Tier-1 will be conducted tentatively from 04th June to 19th June 2019. The exam will be conducted online comprising of 4 sections with about a 100 questions in total and with maximum marks of 200. The entire exam is needed to be completed in a time span of 60 minutes.</p>
					</div>
					<div class="wthree-services-right-bottom">
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-bell" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Free Demo Classes</h5>
								<p>We at Career Power provide with free demo classes to students to help them chose from the classes they want to attend.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-asterisk" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Extra Doubt Clearing Sessions</h5>
								<p>We provide aspirants with extra sessions apart from regular classes to clear their doubts and queries so that they can prepare efficiently for their upcoming exams.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree-services-left">
                    <img src="HomeDesign/images/g6.jpg" />
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //services-bottom -->
    <script>
        var btn = document.querySelector('input');
        btn.addEventListener('onclick', cliked);
            function cliked() {
            debugger
            var btn = $(this);
            btn.attr("disabled", "disabled");
            var msg = $("#msg");
            msg.text("Wait please...").css('color', 'grey');
            var current = $(this);
            var Email = $('#email').val();
            var pwd = $('#password').val();
            if (Email.length < 1 || pwd.length < 1)
            {
                msg.text("Email Id or Password can not be blank").css('color', 'red');
                btn.removeAttr('disabled');
                return false;
            }
            $.ajax({
                
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "Index.aspx/Login",
                data: "{'Email':'"+ Email +"', 'pwd': '"+ pwd +"'}",
                dataType: "json",
                success: function (result) {
                 
                    if (result.d == 'True') {
                        document.cookie = "Email=" + document.getElementById("email").value;
                        window.location.href = "Home.aspx";
                    }
                    else if(result.d=='notRegistered')
                    {
                        msg.text('You are awaited for approval.').css('color','red');
                    }
                    else{
                        msg.text('Email Id and Password mismatch').css('color','red');
                    }
                    btn.removeAttr('disabled');

                },
                error: function (err) {
                    msg.text("Server Error").css('color', 'red');
                    console.log(err.statusText);
                    btn.removeAttr('disabled');

                }

            });
            btn.removeAttr('disabled');
            }
        

            var btn = document.querySelector('input');
            btn.addEventListener('onclick', cliked);
            function cliked() {
                debugger
                var btn = $(this);
                btn.attr("disabled", "disabled");
                var msg = $("#msg");
                msg.text("Wait please...").css('color', 'grey');
                var current = $(this);
                var Email = $('#email').val();
                var pwd = $('#password').val();
                if (Email.length < 1 || pwd.length < 1) {
                    msg.text("Email Id or Password can not be blank").css('color', 'red');
                    btn.removeAttr('disabled');
                    return false;
                }
                $.ajax({

                    type: "Post",
                    contentType: "application/Json; Charset=utf-8",
                    url: "Index.aspx/Login",
                    data: "{'Email':'" + Email + "', 'pwd': '" + pwd + "'}",
                    dataType: "json",
                    success: function (result) {

                        if (result.d == 'True') {
                            document.cookie = "Email=" + document.getElementById("email").value;
                            window.location.href = "Home.aspx";
                        }
                        else if (result.d == 'notRegistered') {
                            msg.text('You are awaited for approval.').css('color', 'red');
                        }
                        else {
                            msg.text('Email Id and Password mismatch').css('color', 'red');
                        }
                        btn.removeAttr('disabled');

                    },
                    error: function (err) {
                        msg.text("Server Error").css('color', 'red');
                        console.log(err.statusText);
                        btn.removeAttr('disabled');

                    }

                });
                btn.removeAttr('disabled');
            }


    </script>

    <script>
        $("#formk").on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var name = $("#name").val();
            var email1 = $("#email1").val();
            var pwd = $("#pwd").val();
            var number = $("#number").val();
            var addr = $("#addr").val();
            sessionStorage.setItem("number", number);
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "Index.aspx/register",
                data: "{'name': '" + name + "', 'email1': '" + email1 + "','pwd': '" + pwd + "','number': '" + number + "','addr': '" + addr + "'}",
                dataType: "json",
                success: function (result) {
                    if (resuld.d != null) {
                        $("#myModal4").show();
                    }
                },
                error: function ajaxError(result) {

                }
            });
        });
        
function myFunction() {
    var otp = prompt("Please enter your OTP:", "");
    if (otp == null || otp == "") {
        alert("You have to enter  an otp");
    } else {
        alert(otp);
    }
    document.getElementById("demo").innerHTML = txt;
}

        $("#otpsubmit").click(function () {
    var number = sessionStorage.getItem("number");
            var otp = $("#txtotp").val();
            $.ajax({
                type: "Post",
                contentType: "application/Json; Charset=utf-8",
                url: "Index.aspx/otpsubmit",
                data: "{'otp': '" + otp + "','number': '" + number + "'}",
                dataType: "json",
                success: function (result) {
                    if (result.d == "True") {
                        alert("Registered Successfully");
                    }
                    else {
                        alert("Wrong Otp");
                    }
                },
                error: function ajaxError(result) {

                }
            });
       });
    </script>
    <script>
// just for the demos, avoids form submit
    function checkPass() {
        var pass1 = document.getElementById('pwd');
        var pass2 = document.getElementById('pwd2');
        var message = document.getElementById('error-nwl');
        var goodColor = "#66cc66";
        var badColor = "#ff6666";

        if (pass1.value.length > 5) {
            pass1.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "character number ok!"
        }
        else {
            pass1.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = " you have to enter at least 6 digit!"
            return;
        }

        if (pass1.value == pass2.value) {
            pass2.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "ok!"
        }
        else {
            pass2.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = "These passwords don't match"
        }
    }
    </script>

    <script>
        var x = document.getElementById("signin").autofocus;
    </script>
</asp:Content>


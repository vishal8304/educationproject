﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <link href="css/popup.css" rel="stylesheet" />
 
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
<style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
    <style>
        #FileUploadsz{
    display:none
}
    </style>
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(208)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<style>
  article, aside, figure, footer, header, hgroup, 
  menu, nav, section { display: block; }
</style>
    <link href="css/Homepage.css" rel="stylesheet" />
  
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript">
        //GridView Comment 
        var prevComment = [];
        function showReply(n) {
            $("#divReply" + n).show();
            return false;
        }
        function closeReply(n) {
            $("#divReply" + n).hide();
            return false;
        }
    </script>
   <style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
     
	 <!-- banner -->
	<div class="w3ls-banner">
		<!-- banner-text -->
			<div class="flexslider">
				<ul class="slides">
   <%foreach (var i in Banner)
                        {%>
                     <li>
      <img src="<%=i.Banner %>" />
    </li>
                    <%} %>
  
  </ul>
			</div>    
		</div> <BR>
		<!-- //banner-text -->  
	<!-- //banner -->
	<!-- services -->
	<div class="services">
		<div class="container">   
			<h3 class="agileits-title">Top Courses</h3>
			
			
			<div class="w3-services-grids">
				<div class="col-md-3 col-xs-6 w3l-services-grid">
					<img src="/images/c1.jpg">
				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/c2.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C3.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C4.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C5.jpg">

				</div>
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/C6.jpg">

				</div>
				
				
				
				
				
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
	<!-- //services -->
	<!-- about-slid -->
	<div class="about-w3slid jarallax">
		<div class="subscribe-agileinfo"> 
			<div class="container">  
				<h3>WELCOME TO SPEEDOMETER COACHING
</h3>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
				<%--<p>We counsel students on regular basis. Also, inform them on upcoming exams in which students can appear. Our dedicated career service team contact students and help them if they are facing any problems. Students can easily reach our career service team.</p> 
				<p>World Class comprehensive content covering all important concepts in detail. Students will also get comprehensive analysis and detailed reports for all the tests taken. Students can analyze their performance, identify strengths and weakness and maximize scores through our detailed reports and graphs. Our content will help you to crack actual exams as pattern of the tests are same as the actuals exams with varied level of difficulty.</p>--%>
			</div>
		</div>
	</div><BR><hr><BR>
	<div class="about-w3slid jarallax">
		<div class="subscribe-agileinfo"> 
			<div class="container">  
				<h3>OUR FEATURES</h3>
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
<%--<p>Our exceptional features distinguish us from the rest of the crowd. We have the best of everything assembled together to deliver finesse results.</p>
					<div class="w3-services-grids">
				<div class="col-md-3 col-xs-6 w3l-services-grid">
					<img src="/images/t1.png" style="height: 100px;" ><h4>SKILLED TEACHER</h4>
					<P>we have well-qualified and experienced teachers who impart necessary knowledge and proper guidance for the all-round professional growth of individual students.</P>
				</div>
				
				<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/t3.png" style="height: 100px;"><h4>STUDY MATERIAL</h4>
					<P>Well researched update & detailed study material imparting immense value to student & aspirants.Exhaustive and impeccable study material for thorough/better understanding </P>

				</div>
						<div class="col-md-3 col-xs-6 w3l-services-grid">
										<img src="/images/t2.png" style="height: 100px;"><h4>ONLINE TESTS</h4>
					<P>Test Series & quizes with explicit focus on the various competetive exam.We ace the field of online tests with our extensive series of tests.</P>

				</div>


			</div>--%>
		</div>
	</div>
		</div>
	<!-- //about-slid --> 
	<!-- welcome -->
	<div class="welcome" > 
		<div class="container">
			<h3>Speedometer Benifits </h3>
<p >Pellentesque habitant morbi tristique senectus et netus et malesuada fames rutrum fringilla fermentum ac turpis egestas auris rutrum fringilla fermentum. Donec tincidunt, eros quis. </p>			
			<div class="welcome-agileinfo">
				<div class="col-md-7 agile-welcome-left"> 
					<div class="col-sm-6 col-xs-6 welcome-w3imgs">
						<figure class="effect-chico">
							<img src="HomeDesign/images/g3.jpg" alt=" " />
							<figcaption>
								<h4>Staff Selection Commission</h4>
								<p>The Staff Selection Commission SSC conducts the various examinations such as SSC CGL, SSC CHSL etc./p>
							</figcaption>			
						</figure>
						<figure class="effect-chico welcome-img2">
							<img src="HomeDesign/images/g4.jpg" alt=" " />
							<figcaption>
								<h4>Banking</h4>
								<p>A bank is a financial institution that accepts deposits from the public and creates credit.</p>
							</figcaption>			
						</figure>
					</div>
					<div class="col-sm-6 col-xs-6 welcome-w3imgs">
						<figure class="effect-chico">
							<img src="HomeDesign/images/g2.jpg" alt=" " />
							<figcaption>
								<h4>Railway</h4>
								<p>The first railway on Indian sub-continent ran over a stretch of 21 miles from Bombay to Thane.</p>
							</figcaption>			
						</figure>
						<figure class="effect-chico welcome-img2">
							<img src="HomeDesign/images/g1.jpg" alt=" " />
							<figcaption>
								<h4>CTET</h4>
								<p>Central Teacher Eligibility Test (CTET) will be conducted by Central Board of Secondary Education Delhi.</p>
							</figcaption>			
						</figure>
					</div>
				<div class="clearfix"> </div> 
			</div>
			<div class="col-md-5 agile-welcome-right">
				<h4>World Class comprehensive content covering all important concepts in detail. Students will also get comprehensive analysis and detailed reports for all the tests taken. Students can analyze their performance, identify strengths and weakness and maximize scores through our detailed reports and graphs. Our content will help you to crack actual exams as pattern of the tests are same as the actuals exams with varied level of difficulty. 
				<p>Personal attention is given to each student. We have team of mentors who provide regular guidance to all the students and focus on their performance. We provide  doubt clearing sessions on Maths and also Mock Test doubt clearing session by Examination Cell to the students. </p>
	
			</div>
			<div class="clearfix"> </div>
		</div>
	</div> 		
	</div>
	<!-- //welcome -->
	<!-- about-slid -->
	<div class="about-w3slid jarallax">
		<div class="sub-agileinfo">
			<div class="container">
				<h3 class="agileits-title w3title1">Get our free newsletter</h3>
				<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est consectetur adipisci velit sed quia non numquam eius.</p>
				<form>
					<input type="email" name="email" placeholder="Email Address" class="user" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div> 
		</div>
	</div>
	<!-- //about-slid --> 
	<!-- services-bottom -->
	<div class="services-bottom">
		<div class="container">
			<div class="agileits-heading">
				<h3 class="agileits-title">Featured Services</h3>
			</div>
			<div class="wthree-services-bottom-grids">
				<div class="col-md-6 wthree-services-left">
					<img src="HomeDesign/images/g8.jpg" alt="" />
				</div>
				<div class="col-md-6 wthree-services-right">
					<div class="wthree-services-right-top">
						<h4>Bank Probationary Officer</h4>
						<p>This is a managerial position in a bank. After the essential 1 to 2-year training, candidates are designated as Assistant Managers (AM) or Deputy Managers (DM)</p>
					</div>
					<div class="wthree-services-right-bottom">
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-bell" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Skilled Faculty</h5>
								<p>We have a dedicated group of faculty that is inclined towards the all-round professional growth of the students. They provide aspirants with necessary guidance and knowledge to scale Bank Exams with ease.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-asterisk" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Best Online Tests</h5>
								<p>We provide our students with a real time feel of the examinations conducted through our well-designed Test Series.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="wthree-services-bottom-grids w3-services-bottom">
				<div class="col-md-6 wthree-services-right">
					<div class="wthree-services-right-top">
						<h4>SSC CGL 2019 Exam Pattern</h4>
						<p>SSC CGL Tier-1 will be conducted tentatively from 04th June to 19th June 2019. The exam will be conducted online comprising of 4 sections with about a 100 questions in total and with maximum marks of 200. The entire exam is needed to be completed in a time span of 60 minutes.</p>
					</div>
					<div class="wthree-services-right-bottom">
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-bell" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Free Demo Classes</h5>
								<p>We at Career Power provide with free demo classes to students to help them chose from the classes they want to attend.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="services-right-bottom-bottom">
							<div class="services-bottom-icon">
								<i class="fa fa-asterisk" aria-hidden="true"></i>
							</div>
							<div class="services-bottom-info">
								<h5>Extra Doubt Clearing Sessions</h5>
								<p>We provide aspirants with extra sessions apart from regular classes to clear their doubts and queries so that they can prepare efficiently for their upcoming exams.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree-services-left">
                    <img src="HomeDesign/images/g6.jpg" />
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //services-bottom -->
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Globalization;

public partial class Dailyvocab : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            connect();
            bindcomp();
        }
    }

    public void connect()
    {
        //var now = DateTime.Now.ToShortDateString();
        string datestring = DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
        DateTime now = Convert.ToDateTime(datestring);
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            da = new SqlDataAdapter("SELECT * FROM VOCAB where Convert(Date,Dated)= Convert(Date,GETDATE())", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rptvocabqs.DataSource = dt;
            rptvocabqs.DataBind();
            //SqlDataAdapter da1 = new SqlDataAdapter("select * from Vocabwords where [date]=(select top 1 [Date] from Vocabwords order by [Date] desc)", con);
            //DataTable dt1 = new DataTable();
            //da1.Fill(dt1);
            //rptwords.DataSource = dt1;
            //rptwords.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    public void bindcomp()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select top 1 * from vocabcomprehension order by Id desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Comprehension"].ToString();
            literal1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        con.Close();
        dr.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    [WebMethod]
    public static string SaveAnswer(string opt, string Email, string vocabId)
    {
        string returnvalue = "";
        try { 
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        
            SqlCommand cmd = new SqlCommand("insert into uservocab(selectedval,Email,vocabId) values(@selectedval,@Email,@Id)", con);
            cmd.Parameters.AddWithValue("@selectedval", opt);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Id", vocabId);
            int Result = cmd.ExecuteNonQuery();
            if (Result == 1)
            {
                //string qry3 = "update b set [status]='passed' FROM Vocab a inner join uservocab b on a.vocabId = b.vocabId where a.correctval=b.selectedval select top 1 case when[status] is null then 0 else 1 end from uservocab order by id desc";
                string qry3 = "select correctval from Vocab where vocabId='" + vocabId + "'";
                SqlCommand cmd3 = new SqlCommand(qry3, con);
                SqlDataReader dr1 = cmd3.ExecuteReader();
                dr1.Read();
                if (dr1.HasRows)
                {
                var corval= dr1["correctval"].ToString();
                if (corval == opt)
                {
                    returnvalue = opt+'a';
                }
                else
                {
                    returnvalue = dr1["correctval"].ToString();
                }
                
             }
                
            }
        
        con.Close();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
        return returnvalue;
    }

    protected void rptvocabqs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
           con.Open();
        }
        HtmlGenericControl div1 = e.Item.FindControl("divs") as HtmlGenericControl;
        HtmlGenericControl div2 = e.Item.FindControl("divt") as HtmlGenericControl;
        HtmlGenericControl div3 = e.Item.FindControl("divm") as HtmlGenericControl;
        HtmlGenericControl div4 = e.Item.FindControl("divn") as HtmlGenericControl;
        RadioButton rdo = (RadioButton)e.Item.FindControl("rdb1");
        RadioButton rdo1 = (RadioButton)e.Item.FindControl("rdb2");
        RadioButton rdo2 = (RadioButton)e.Item.FindControl("rdb3");
        RadioButton rdo3 = (RadioButton)e.Item.FindControl("rdb4");
        Label lblopt1 = (Label)e.Item.FindControl("lblopt1");
        Label lblopt2 = (Label)e.Item.FindControl("lblopt2");
        Label lblopt3 = (Label)e.Item.FindControl("lblopt3");
        Label lblopt4 = (Label)e.Item.FindControl("lblopt4");
        HiddenField hdn = (HiddenField)e.Item.FindControl("hdn");
        SqlCommand cmd = new SqlCommand("select a.*,b.* from uservocab a inner join Vocab b on a.vocabId=b.vocabId where a.Email='" + Request.Cookies["Email"].Value + "' and a.vocabId='" + hdn.Value + "'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            rdo.Enabled = false;
            rdo1.Enabled = false;
            rdo2.Enabled = false;
            rdo3.Enabled = false;
                var selected = dr["selectedval"].ToString().Trim();
                var correct= dr["correctval"].ToString().Trim();
                if (selected == correct)
                {
                    if (selected == "A")
                    {
                    div1.Style.Add("background-color", ConsoleColor.Green.ToString());
                    }
                    else if (selected == "B")
                    {
                    div2.Style.Add("background-color", ConsoleColor.Green.ToString());
                }
                    else if (selected == "C")
                    {
                    div3.Style.Add("background-color", ConsoleColor.Green.ToString());
                }
                    else if (selected == "D")
                    {
                    div4.Style.Add("background-color", ConsoleColor.Green.ToString());
                }
                }
                else
                {
                    if (correct == "A")
                    {
                   
                    div2.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div3.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div4.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div1.Style.Add("background-color", ConsoleColor.Green.ToString());
                    }
                    else if (correct == "B")
                    {
                  
                    div1.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div3.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div4.Style.Add("background-color", ConsoleColor.Red.ToString());
                        div2.Style.Add("background-color", ConsoleColor.Green.ToString());
                    }
                    else if (correct == "C")
                    {
                    div2.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div1.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div4.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div3.Style.Add("background-color", ConsoleColor.Green.ToString());
                }
                    else if (correct == "D")
                    {
                    div2.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div1.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div3.Style.Add("background-color", ConsoleColor.Red.ToString());
                    div4.Style.Add("background-color", ConsoleColor.Green.ToString());
                }
                }
            }
            con.Close();
        dr.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
  
}
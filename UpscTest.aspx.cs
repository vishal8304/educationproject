﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UpscTest : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindgrid();
            bindinstruct();
        }
    }


    public void bindgrid()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("Select name,max(marks) as totalmarks,max([time]) as totaltime, count(Ques) as count, [dbo].[GetUpscquizmarks](name, '" + Request.Cookies["Email"].Value + "') as TotalMarksObt from UpscQuiz GROUP BY name", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            grid.DataSource = dt;
            grid.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
    public void bindinstruct()

    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select top 1 * from quizinstruction where section='UPSC' order by Id desc", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["Content"].ToString();
                lblinstruct.Text = System.Net.WebUtility.HtmlDecode(str);
            }
            con.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
    protected void grid_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {

            }
            else
            {
                HyperLink Hlink = (HyperLink)e.Row.FindControl("Hlink");
                Label lbl = (Label)e.Row.FindControl("lblobt");
                Label lblname = (Label)e.Row.FindControl("lblname");
                if (lbl.Text == "")
                {
                    var plainTextBytes = HttpUtility.HtmlEncode(lblname.Text);
                    Hlink.NavigateUrl = "UpscQuiz.aspx?name=" + plainTextBytes + "";
                    Hlink.Text = "Start Quiz";
                }
                else
                {
                    Hlink.NavigateUrl = "solution.aspx?upscquiz=" + lblname.Text + "";
                    Hlink.Text = "View Solution";
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class sscinstruct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            literal();
        }
    }

    public void literal()
    {
        try { 
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select Top 1 * from onlineinstruct where section='SSC' order by Id desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Content"].ToString();
            literal1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        con.Close();
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btnlaunch_Click(object sender, EventArgs e)
    {
        var set=Request.QueryString["set"];
        var section=Request.QueryString["section"];
        Response.Redirect("sscOnline.aspx?set=" + set + "&section=" + section);
    }
}
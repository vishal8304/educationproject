<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Jobs.aspx.cs" Inherits="Jobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <%-- Jobs Tiles--%>
     <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="text-align:center;margin-top: 2%;"><strong>
                            New Jobs</strong>
                        </h2>
                    </div>
                    </div><br />
         <div class="row">
                    <asp:Repeater ID="rptjobs" runat="server">
            <ItemTemplate>
                    <div class="col-md-3 " id="box">
                                 <a href="Jobdesc.aspx?tile=<%#Eval("tile") %>" style="text-decoration: none !important; text-align:center; margin-top:50px;">
                    <div style="width:250px;height:100px;background-color:#33cc99;margin-top: 8% !important;"><br />
                        <%#Eval("tile") %>
                    </div>
                </a>
                             </div>
                       </ItemTemplate>
                        </asp:Repeater>    
                   
                </div>
        <br />
      
             <%--   <div class="row">
                    <div class="col-md-12">
                        <h4>
                            Current Openings:
                        </h4>
                    </div>
                    </div>
                <div class="row">
                    <asp:Repeater ID="rptjobs" runat="server">
            <ItemTemplate>
                    <div class="col-md-3">
                             
                                 <a href="Jobdesc.aspx?Field=<%#Eval("Field") %>" style="text-decoration: none !important; text-align:center; margin-top:50px;">
                    <div style="width:250px;height:100px;background-color:rgba(193, 193, 193, 0.45)"><br />
                        <%#Eval("Field") %>
                    </div>
                </a>--%>
                              <%--<a href="Jobdesc.aspx?Field=<%#Eval("Field") %>"><%#Eval("Field") %></a>--%> 
                      <%--  </ItemTemplate>
                        </asp:Repeater>    
                   
                </div>
        <br />
        <div class="row">
            <div class="col-md-12">
               <strong>Some important links:</strong> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-4">
                <asp:GridView ID="GridView1" Width="100%" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Jobid">
                     <Columns>
                        <asp:hyperlinkfield headertext="Result"
      datatextfield="Result" datanavigateurlformatstring="http://{0}" DataNavigateUrlFields="Result" target="_blank"/>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </div>
            <div class="col-md-4">
                <asp:GridView ID="grddetails" Width="100%" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Jobid">
                    <Columns>
                        <asp:hyperlinkfield headertext="Admit Card"
      datatextfield="Admitcard" datanavigateurlformatstring="http://{0}" DataNavigateUrlFields="Admitcard" target="_blank"/>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </div>
            <div class="col-md-4">
                <asp:GridView ID="GridView2" Width="100%" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Jobid">
                    <Columns>
                        <asp:hyperlinkfield headertext="Latest Jobs"
      datatextfield="Latestjobs" datanavigateurlformatstring="http://{0}" DataNavigateUrlFields="Latestjobs" target="_blank"/>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </div>
           
            </div>
        <br />--%>
          </div>
    <div class="row" style="     margin: auto;
    width: 80%; margin-bottom:2%;" >
            <div class="col-md-4" style="border:5px; border-color:crimson">
                <div class="row" style="border:2px solid; border-color:darkblue" >
                <div class="row" style="background-color:red">
       <h3 style="color:white; text-align:center; ">Admit Card</h3>
                    </div>
        <asp:Repeater ID="Repeater1" runat="server">
            
            <ItemTemplate>
              <div class="row">
               <h4 style="text-align:center"> <a target="_blank" href="<%#Eval("Link") %>"><%#Eval("Text") %></a></h4>
                  </div>
            </ItemTemplate>
        </asp:Repeater>
                    </div>
            </div>
             <div class="col-md-4" style="border:5px; border-color:crimson">
                <div class="row" style="border:2px solid; border-color:darkblue" >
                <div class="row" style="background-color:red">
       <h3 style="color:white; text-align:center; ">New Jobs</h3>
                    </div>
        <asp:Repeater ID="Repeater2" runat="server">
            
            <ItemTemplate>
              <div class="row">
               <h4 style="text-align:center"> <a target="_blank" href="<%#Eval("Link") %>"><%#Eval("Text") %></a></h4>
                  </div>
            </ItemTemplate>
        </asp:Repeater>
                    </div>
            </div>
             <div class="col-md-4" style="border:5px; border-color:crimson">
                <div class="row" style="border:2px solid; border-color:darkblue" >
                <div class="row" style="background-color:red">
       <h3 style="color:white; text-align:center; ">Results</h3>
                    </div>
        <asp:Repeater ID="Repeater3" runat="server">
            
            <ItemTemplate>
              <div class="row">
               <h4 style="text-align:center"> <a target="_blank" href="<%#Eval("Link") %>"><%#Eval("Text") %></a></h4>
                  </div>
            </ItemTemplate>
        </asp:Repeater>
                    </div>
            </div>
            </div>
            <style>
        #box{
                color: white;
    border-radius: 24%;
        }
    </style>     
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="timeout.aspx.cs" Inherits="timeout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container-fluid">
        <div class="row" style="text-align:center">
            <div class="col-md-12">
                <h2><asp:Label ID="lbltimeout" runat="server" Text="Time out"></asp:Label></h2>
            </div>
            
        </div>
      
        <br />
        <div class="row" style="text-align:center">
        <asp:Label ID="lbltest" runat="server" Text="Congrats you completed the test!!" Visible="false" ForeColor="#FF3300" Font-Italic="True"></asp:Label>
            </div>
         <div class="row">
             <div class="col-md-2">
                <asp:Label ID="lblscoretext" Visible="false" runat="server" Text=" You Attempted:"></asp:Label>
             </div>
             <div class="col-md-10">
                <strong><asp:Label ID="lblscore" runat="server" Visible="false"></asp:Label></strong> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="lblstatuscount" Text="You Answered correctly:" runat="server" Visible="false"></asp:Label>
             </div>
             <div class="col-md-9">
                 <asp:Label ID="lblcount" runat="server" Visible="false"></asp:Label>
             </div>
         </div>
         <br />
         <asp:Repeater ID="rptsection" runat="server" visible="false">
              <ItemTemplate>

         <div class="row">
                     <div class="col-md-3">
                 You Answered Correctly:
             </div>
             <div class="col-md-1">
                 <strong>
                 <asp:Label ID="lblscoresub" runat="server" Text='<%#Eval("statuscount") %>'></asp:Label></strong>
             </div>
         </div>
              </ItemTemplate>
             </asp:Repeater>
         
    </div>
</asp:Content>


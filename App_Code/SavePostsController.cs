﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EducationDataAccess.Data;
using EducationDataAccess.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public class SavePostsController : ApiController
{
    EducationalhubEntities ent = new EducationalhubEntities();

    [HttpPost]
    public askmodelDto fetchpostquery(Userz usr)
    {
        DataTable dt = new DataTable();
        askmodelDto objdept = new askmodelDto();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmdd = new SqlCommand("select Email from [User] where Email='"+usr.Email+"'", con);
            SqlDataReader drr = cmdd.ExecuteReader();
            drr.Read();
            if (drr.HasRows)
            {
                drr.Close();
                objdept.message = new Message();
                objdept.message.msg = "Ok";
               
                using (SqlCommand cmd = new SqlCommand("select a.*,b.Name,b.profilepath from mcqquery a inner join [User] b on a.Email=b.Email order by a.Id desc", con))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        var mcqQuery = new List<mcqqueryDto>();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var mcq = new mcqqueryDto
                            {
                                Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
                                Ques = dt.Rows[i]["Ques"].ToString(),
                                opta = dt.Rows[i]["opta"].ToString(),
                                optb = dt.Rows[i]["optb"].ToString(),
                                optc = dt.Rows[i]["optc"].ToString(),
                                optd = dt.Rows[i]["optd"].ToString(),
                                opte = dt.Rows[i]["opte"].ToString(),
                                corval = dt.Rows[i]["corval"].ToString(),
                                exam = dt.Rows[i]["exam"].ToString(),
                                subject = dt.Rows[i]["subject"].ToString(),
                                picpath = dt.Rows[i]["picpath"].ToString(),
                                Date = dt.Rows[i]["Date"].ToString(),
                                Email = dt.Rows[i]["Email"].ToString(),
                                Name = dt.Rows[i]["Name"].ToString(),
                                profilepath = dt.Rows[i]["profilepath"].ToString()
                            };

                            var id = mcq.Id;
                            using (SqlCommand cmds = new SqlCommand("select count (*) as count from asklike where postId='" + id + "'", con))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                SqlDataAdapter das = new SqlDataAdapter(cmds);
                                DataTable dts = new DataTable();
                                das.Fill(dts);
                                if (dts.Rows.Count > 0)
                                {
                                    var asklike = new List<asklikeDto>();
                                    for (int t = 0; t < dts.Rows.Count; t++)
                                    {
                                        var likeq = new asklikeDto
                                        {
                                            count = Convert.ToInt16(dts.Rows[t]["count"].ToString())
                                        };
                                        asklike.Add(likeq);
                                    }
                                    mcq.asklike = asklike;
                                }
                            }

                            using (SqlCommand cmdi = new SqlCommand("select * from Savepost where Email = '" + usr.Email + "' and shareId = " + id, con))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                SqlDataReader dr = cmdi.ExecuteReader();
                                dr.Read();
                                if (dr.HasRows)
                                {
                                    dr.Close();
                                    mcq.share = "True";
                                }
                                else
                                {
                                    dr.Close();
                                    mcq.share = "False";
                                }
                            }

                            using (SqlCommand cmdk = new SqlCommand("select count(*) as commentscount from ask where Id='" + id + "'", con))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                SqlDataAdapter das = new SqlDataAdapter(cmdk);
                                DataTable dts = new DataTable();
                                das.Fill(dts);
                                if (dts.Rows.Count > 0)
                                {
                                    var com = new List<comDto>();
                                    for (int t = 0; t < dts.Rows.Count; t++)
                                    {
                                        var comdto = new comDto
                                        {
                                            commentcount = Convert.ToInt16(dts.Rows[t]["commentscount"].ToString())
                                        };
                                        com.Add(comdto);
                                    }
                                    mcq.com = com;
                                }
                            }

                            using (SqlCommand cmd1 = new SqlCommand("select a.*,b.profilepath from ask a inner join [user] b on a.Email=b.Email where a.Id='" + id + "' order by a.commentId desc", con))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                                DataTable dt1 = new DataTable();
                                da1.Fill(dt1);
                                if (dt1.Rows.Count > 0)
                                {
                                    var ask = new List<askDto>();
                                    for (int j = 0; j < dt1.Rows.Count; j++)
                                    {
                                        var askq = new askDto
                                        {
                                            commentId = Convert.ToInt16(dt1.Rows[j]["commentId"].ToString()),
                                            Comments = dt1.Rows[j]["Comments"].ToString(),
                                            comimg = dt1.Rows[j]["comimg"].ToString(),
                                            profile = dt1.Rows[j]["profilepath"].ToString()
                                        };
                                        var commentId = askq.commentId;
                                        using (SqlCommand cmd2 = new SqlCommand("select * from ChildCommentTable where commentId='" + commentId + "' order by ChildCommentID desc", con))
                                        {
                                            if (con.State == ConnectionState.Closed)
                                            {
                                                con.Open();
                                            }
                                            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                                            DataTable dt2 = new DataTable();
                                            da2.Fill(dt2);
                                            if (dt2.Rows.Count > 0)
                                            {
                                                var ChildCommentTable = new List<ChildCommentTableDto>();
                                                for (int k = 0; k < dt2.Rows.Count; k++)
                                                {
                                                    ChildCommentTable.Add(new ChildCommentTableDto
                                                    {
                                                        //ChildCommentID = Convert.ToInt16(dt2.Rows[k]["ChildCommentID"].ToString()),
                                                        CommentMessage = dt2.Rows[k]["CommentMessage"].ToString()
                                                    });
                                                }
                                                askq.ChildCommentTable = ChildCommentTable;
                                                askq.commentId = commentId;
                                            }
                                            //ask.Add(askq);
                                        }
                                        using (SqlCommand cmd2 = new SqlCommand("select count(*) as commentscount from comlike where comId=" + commentId, con))
                                        {
                                            if (con.State == ConnectionState.Closed)
                                            {
                                                con.Open();
                                            }
                                            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                                            DataTable dt2 = new DataTable();
                                            da2.Fill(dt2);
                                            if (dt2.Rows.Count > 0)
                                            {
                                                var comlike = new List<comlikeDto>();
                                                for (int k = 0; k < dt2.Rows.Count; k++)
                                                {
                                                    comlike.Add(new comlikeDto
                                                    {
                                                        tot = Convert.ToInt16(dt2.Rows[k]["commentscount"].ToString())
                                                    });
                                                }
                                                askq.comlike = comlike;

                                            }
                                            ask.Add(askq);
                                        }
                                    }
                                    mcq.ask = ask;
                                }
                                mcqQuery.Add(mcq);
                            }
                        }
                        int recordPerPage = 10;
                        int totalRecords = mcqQuery.Count;
                        int totalPages = Convert.ToInt32(Math.Ceiling((double)totalRecords / recordPerPage));
                        mcqQuery = mcqQuery.Skip(recordPerPage * (usr.page-1)).Take(10).ToList();
                        objdept.mcqquery = mcqQuery;
                        objdept.TotalPages = totalPages;
                        using (SqlCommand cmd8 = new SqlCommand("select * from [User] where Email='" + usr.Email + "' and expert='True'", con))
                        {
                            SqlDataReader dr = cmd8.ExecuteReader();
                            dr.Read();
                            if (dr.HasRows)
                            {
                                objdept.expert = "True";
                                dr.Close();
                            }
                            else
                            {
                                objdept.expert = "False";
                                dr.Close();
                            }
                        }
                    }
                }
            }
            else
            {
                objdept.message = new Message();
                objdept.message.msg = "Invalid Email Id";
            }
            con.Close();
            return objdept;
        }
    }

    public Task<HttpResponseMessage> Posted()
    {
        Message msg = new Message();
        
        List<string> savedFilePath = new List<string>();
        if (!Request.Content.IsMimeMultipartContent())
        {
            throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        }
        string fileRelativePath = "";
        string Ques= "";
        string opta = "";
        string optb = "";
        string optc = "";
        string optd = "";
        string opte = "";
        string corval = "";
        string exam = "";
        string subject = "";
        string Email = "";
      
        string rootPath = HttpContext.Current.Server.MapPath("/mcqupload");
        var provider = new MultipartFormDataStreamProvider(rootPath);
        var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                if (t.IsCanceled || t.IsFaulted)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }
            
                foreach (MultipartFileData item in provider.FileData)
                {
                    try
                    {
                        string newFileName = "";
                        string name = item.Headers.ContentDisposition.FileName.Replace("\"", "");
                        string[] allowedExtensions = { ".jpg", ".jpeg", ".gif", ".png" };
                        string ext = Path.GetExtension(name);
                        if (!allowedExtensions.Contains(ext))
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                        }
                        string filename = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                        newFileName = filename + ext;
                        //string newFileName = Guid.NewGuid() + Path.GetExtension(name);
                        File.Move(item.LocalFileName, Path.Combine(rootPath, newFileName));

                        Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                        fileRelativePath = "/mcqupload/" + newFileName;
                        Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute(fileRelativePath));
                        savedFilePath.Add(fileFullPath.ToString());
                        
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {

                        if (key == "Ques" && !string.IsNullOrEmpty(val))
                            Ques = val;
                        if (key == "opta" && !string.IsNullOrEmpty(val))
                            opta = val;
                        if (key == "optb" && !string.IsNullOrEmpty(val))
                            optb = val;
                        if (key == "optc" && !string.IsNullOrEmpty(val))
                            optc = val;
                        if (key == "optd" && !string.IsNullOrEmpty(val))
                            optd = val;
                        if (key == "opte" && !string.IsNullOrEmpty(val))
                            opte = val;
                        if (key == "corval" && !string.IsNullOrEmpty(val))
                            corval = val;
                        if (key == "exam" && !string.IsNullOrEmpty(val))
                            exam = val;
                        if (key == "subject" && !string.IsNullOrEmpty(val))
                            subject = val;
                        if (key == "Email" && !string.IsNullOrEmpty(val))
                            Email = val;
                    }
                }
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd1 = new SqlCommand("select Email from [User] where Email='"+Email+ "' and Email!=''", con);
                SqlDataReader dr = cmd1.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    dr.Close();
                    SqlCommand cmd = new SqlCommand("insert into mcqquery(Ques,opta,optb,optc,optd,opte,corval,exam,subject,Date,Email,picpath) values(@Ques,@opta,@optb,@optc,@optd,@opte,@corval,@exam,@subject,@Date,@Email,@picpath)", con);
                    cmd.Parameters.AddWithValue("@Ques", Ques);
                    cmd.Parameters.AddWithValue("@opta", opta);
                    cmd.Parameters.AddWithValue("@optb", optb);
                    cmd.Parameters.AddWithValue("@optc", optc);
                    cmd.Parameters.AddWithValue("@optd", optd);
                    cmd.Parameters.AddWithValue("@opte", opte);
                    cmd.Parameters.AddWithValue("@corval", corval);
                    cmd.Parameters.AddWithValue("@exam", exam);
                    cmd.Parameters.AddWithValue("@subject", subject);
                    cmd.Parameters.AddWithValue("@Date", System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Email", Email);
                    cmd.Parameters.AddWithValue("@picpath", fileRelativePath);
                    try
                    {
                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            msg.msg = "Ok";
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("error", ex.ToString());
                    }
                }
                else
                {
                    msg.msg = "Invalid Credentials";
                }
                return Request.CreateResponse(HttpStatusCode.Created, msg);
            });
        
        return task;
    }

}


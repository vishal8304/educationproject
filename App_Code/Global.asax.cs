﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Web.Http;
using EducationDataAccess.Data;

public class Global : System.Web.HttpApplication
{

    void Application_Start(object sender, EventArgs e)
  {
        RouteTable.Routes.MapHttpRoute(
                  name: "DefaultApi",
                  routeTemplate: "api/{controller}/{action}/{id}",
                  defaults: new { id = System.Web.Http.RouteParameter.Optional }

        );

        GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
     .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        GlobalConfiguration.Configuration.Formatters
            .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
       
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        Session.Timeout =1000;
    }

    void Session_End(object sender, EventArgs e)
    {
        try { 
        if (Session["usr"] == null)
        { 
            Response.Redirect("Index.aspx");
        }
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using EducationDataAccess.Models;
using EducationDataAccess.Data;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using System.IO;

public class CommentController : ApiController
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);

    [HttpPost]
    public returns commentlike(comlikeDto comlikes)
    {
        var rm = new returns();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var UserEmail = "";
        SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + comlikes.Email + "' or Mob='" + comlikes.Email + "'", con);
        SqlDataReader drs = cmd2.ExecuteReader();
        drs.Read();
        if (drs.HasRows)
        {
            UserEmail = drs["Email"].ToString();
        }
        drs.Close();
        SqlCommand cmd = new SqlCommand("select * from comlike where Email=@t1 and comId=@t2", con);
        cmd.Parameters.AddWithValue("@t1", UserEmail);
        cmd.Parameters.AddWithValue("@t2", comlikes.Id);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            dr.Close();
            SqlCommand cmd1 = new SqlCommand("delete from comlike where Email=@t3 and comId=@t4", con);
            cmd1.Parameters.AddWithValue("@t4", comlikes.Id);
            cmd1.Parameters.AddWithValue("@t3", UserEmail);
            int i = cmd1.ExecuteNonQuery();
            if (i == 1)
            {
                SqlCommand comd = new SqlCommand("select count (*) as likecount from comlike where comId='" + comlikes.Id + "'", con);
                SqlDataReader dr2 = comd.ExecuteReader();
                dr2.Read();
                if (dr2.HasRows)
                {
                    var val1 = dr2["likecount"].ToString();
                    rm.returnval = val1;
                    rm.message = "Unliked";
                }
                dr2.Close();
            }
        }
        else
        {
            dr.Close();
            SqlCommand cmd3 = new SqlCommand("insert into comlike(Email,comId) values(@t3,@t4)", con);
            cmd3.Parameters.AddWithValue("@t3", comlikes.Email);
            cmd3.Parameters.AddWithValue("@t4", comlikes.Id);
            int i = cmd3.ExecuteNonQuery();
            if (i == 1)
            {
                SqlCommand comd = new SqlCommand("select count (*) as count from comlike where comId='" + comlikes.Id + "'", con);
                SqlDataReader dr2 = comd.ExecuteReader();
                dr2.Read();
                if (dr2.HasRows)
                {
                    var val1 = dr2["count"].ToString();
                    rm.returnval = val1;
                    rm.message = "Liked";
                }
                dr2.Close();
            }
        }
        con.Close();
        return rm;
    }

    [HttpPost]
    public ChildCommentTableDto replycom(ChildCommentTableDto reply)
    {
        var model = new ChildCommentTableDto();
        string comment = System.DateTime.Now.ToString();
        string commentDate = System.DateTime.Now.ToString();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var UserEmail = "";
        SqlCommand cmd2 = new SqlCommand("select Email from [User] where Email='" + reply.Email + "' or Mob='" + reply.Email + "'", con);
        SqlDataReader drs = cmd2.ExecuteReader();
        drs.Read();
        if (drs.HasRows)
        {
            UserEmail = drs["Email"].ToString();
        }
        drs.Close();
        SqlCommand cmd = new SqlCommand("insert into ChildCommentTable values(@Email,@CommentMessage,@CommentDate,@commentId)", con);
        cmd.Parameters.AddWithValue("@Email", UserEmail);
        cmd.Parameters.AddWithValue("@CommentMessage", reply.CommentMessage);
        cmd.Parameters.AddWithValue("@CommentDate", commentDate);
        cmd.Parameters.AddWithValue("@commentId", reply.commentId);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            model.CommentMessage = reply.CommentMessage;
            model.Email = reply.Email;
            model.commentId = reply.commentId;
            model.CommentDate = commentDate;
        }
        con.Close();
        return model;
    }
}

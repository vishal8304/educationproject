﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EducationDataAccess.Data;
using EducationDataAccess.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public class VocabController : ApiController
{
    public IEnumerable<Vocab> Getqs()
    {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var a = entities.Vocabs.ToList();
            return a;
        }
    }

    public IEnumerable<Vocabword> getwords()
    {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var b = entities.Vocabwords.SqlQuery("select * from Vocabwords where [date]=(select top 1 [Date] from Vocabwords order by [Date] desc)").ToList();
            return b;
        }
    }

    public IEnumerable<vocabcomprehension> getcomp()
    {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var b = entities.vocabcomprehensions.SqlQuery("select top 1 * from vocabcomprehension order by Id desc").ToList();
            return b;
        }
    }

    [HttpPost]
    public dailyVocab answervocab(uservocab vocab)
    {
        var rm = new dailyVocab();
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insert into uservocab(selectedval,Email,vocabId) values(@selectedval,@Email,@Id)", con);
            cmd.Parameters.AddWithValue("@selectedval", vocab.selectedval);
            cmd.Parameters.AddWithValue("@Email", vocab.Email);
            cmd.Parameters.AddWithValue("@Id", vocab.vocabId);
            int Result = cmd.ExecuteNonQuery();
            if (Result == 1)
            {
                //string qry3 = "update b set [status]='passed' FROM Vocab a inner join uservocab b on a.vocabId = b.vocabId where a.correctval=b.selectedval select top 1 case when[status] is null then 0 else 1 end from uservocab order by id desc";
                string qry3 = "select correctval from Vocab where vocabId='" + vocab.vocabId + "'";
                SqlCommand cmd3 = new SqlCommand(qry3, con);
                SqlDataReader dr1 = cmd3.ExecuteReader();
                dr1.Read();
                if (dr1.HasRows)
                {
                    var corval = dr1["correctval"].ToString();
                    if (corval == vocab.selectedval)
                    {
                        rm.Message = vocab.selectedval+"true";
                    }
                    else
                    {
                        rm.Message= dr1["correctval"].ToString();
                    }
                }

            }

            con.Close();
        }
        catch(Exception ex)
        {
            rm.Message = ex.ToString();
        }
        return rm;
    }
}

﻿using EducationDataAccess.Data;
using EducationDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SigninController : ApiController
{
    [HttpPost]
    public Login Signin(User usr)
    {
        var rm = new Login();
        try
        {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var r1 = entities.Users.FirstOrDefault(a => a.Email == usr.Email || a.Mob==usr.Email && a.password == usr.password && a.registered==true);
            if (r1 != null)
            {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlCommand cmd = new SqlCommand("select * from [User] where Email='" + usr.Email + "' or Mob='"+usr.Email+"'", con);
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    if (dr.HasRows)
                    {
                        var a = dr["Email"].ToString();
                        rm.user.Email = a;
                        rm.user.password = dr["password"].ToString();
                        rm.user.Addr = dr["Addr"].ToString();
                        rm.user.Mob = dr["Mob"].ToString();
                        rm.user.Name = dr["Name"].ToString();
                        rm.user.profilepath = dr["profilepath"].ToString();
                        if (dr["expert"].ToString() == "True")
                        {
                            rm.user.expert = true;
                        }
                        else
                        {
                            rm.user.expert = false;
                        }
                        if (dr["freetest"].ToString() == "True")
                        {
                            rm.user.freetest = true;
                        }
                        else
                        {
                            rm.user.freetest = false;
                        }
                    }

                    //rm.user.Email = user.Email;
                    
                    rm.Message = "ok";
                }
                else
                {
                    rm.Message = "User is not registered";
            }
                return rm;
        }
    }catch(Exception ex)
        {
            rm.Message = ex.Message;
        }
        return rm;
    }
}

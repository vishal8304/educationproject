﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using EducationDataAccess;
using System.Data.Entity;
using EducationDataAccess.Data;
using EducationDataAccess.Models;

public class JobsController : ApiController
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);

    public jobDto getjobtile()
    {
        jobDto obj = new jobDto();
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select * from Jobs where tile is not null", con);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                var jobDto = new List<tile>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var jobs = new tile
                    {
                        tiles = dt.Rows[i]["tile"].ToString()
                    };
                    jobDto.Add(jobs);
                }
                obj.tiled = jobDto;
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
        return obj;
    }

    [HttpPost]
    public jobdescDto getdetails(tile tile)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        jobdescDto obj = new jobdescDto();
        try
        {
            SqlCommand cmd = new SqlCommand("select * from Jobs where tile='" + tile.tiles + "'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            obj.Descrption = dr["Content"].ToString();
        }
        dr.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }
        return obj;
    }

}

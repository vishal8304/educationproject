﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using EducationDataAccess.Models;
using EducationDataAccess.Data;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using System.IO;

public class LikecomController : ApiController
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);

    [HttpPost]
    public returns Likepost(mcqqueryDto likes)
    {
        var rm = new returns();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from asklike where Email=@t1 and postId=@t2", con);
        cmd.Parameters.AddWithValue("@t1", likes.Email);
        cmd.Parameters.AddWithValue("@t2", likes.Id);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            dr.Close();
            SqlCommand cmd1 = new SqlCommand("delete from asklike where Email=@t3 and postId=@t4", con);
            cmd1.Parameters.AddWithValue("@t4", likes.Id);
            cmd1.Parameters.AddWithValue("@t3", likes.Email);
            int i = cmd1.ExecuteNonQuery();
            if (i == 1)
            {
                SqlCommand comd = new SqlCommand("select count (*) as count from asklike where postId='" + likes.Id + "'", con);
                SqlDataReader dr2 = comd.ExecuteReader();
                dr2.Read();
                if (dr2.HasRows)
                {
                    var val1 = dr2["count"].ToString();
                    rm.returnval = val1;
                    rm.message = "Unliked";
                }
                dr2.Close();
            }
        }
        else
        {
            dr.Close();
            SqlCommand cmd2 = new SqlCommand("insert into asklike(Email,postId) values(@t3,@t4)", con);
            cmd2.Parameters.AddWithValue("@t3", likes.Email);
            cmd2.Parameters.AddWithValue("@t4", likes.Id);
            int i = cmd2.ExecuteNonQuery();
            if (i == 1)
            {
                SqlCommand comd = new SqlCommand("select count (*) as count from asklike where postId='" + likes.Id + "'", con);
                SqlDataReader dr2 = comd.ExecuteReader();
                dr2.Read();
                if (dr2.HasRows)
                {
                    var val1 = dr2["count"].ToString();
                    rm.returnval = val1;
                    rm.message = "Liked";
                }
                dr2.Close();
            }
        }
        con.Close();
        return rm;
    }

    public Task<HttpResponseMessage> submitcom()
    {
        //Message msg = new Message();
        responsecom rc = new responsecom();
        List<string> savedFilePath = new List<string>();
        if (!Request.Content.IsMimeMultipartContent())
        {
            throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        }
        string Image = "";
        string Email = "";
        string CommentDate = "";
        string Id = "";
        string Comments = "";
        string fileRelativePath = "";
        string rootPath = HttpContext.Current.Server.MapPath("/comments/");
        var provider = new MultipartFormDataStreamProvider(rootPath);
        var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                if (t.IsCanceled || t.IsFaulted)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                foreach (MultipartFileData item in provider.FileData)
                {
                    try
                    {
                        string newFileName = "";
                        string name = item.Headers.ContentDisposition.FileName.Replace("\"", "");
                        string[] allowedExtensions = { ".jpg", ".jpeg", ".gif", ".png" };
                        string ext = Path.GetExtension(name);
                        if (!allowedExtensions.Contains(ext))
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable);
                        }
                        string filename = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                        newFileName = filename + ext;
                        //string newFileName = Guid.NewGuid() + Path.GetExtension(name);
                        File.Move(item.LocalFileName, Path.Combine(rootPath, newFileName));
                        Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                        fileRelativePath = "/comments/" + newFileName;
                        Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute(fileRelativePath));
                        savedFilePath.Add(fileFullPath.ToString());
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        if (key == "Comments" && !string.IsNullOrEmpty(val))
                            Comments = val;
                        if (key == "Email" && !string.IsNullOrEmpty(val))
                            Email = val;
                        if (key == "Id" && !string.IsNullOrEmpty(val))
                            Id = val;
                        CommentDate = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                        if (fileRelativePath != "")
                        {
                            Image = fileRelativePath;
                        }
                    }
                }
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd1 = new SqlCommand("select Email from [User] where Email='" + Email + "' and Email!=''", con);
                SqlDataReader dr = cmd1.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    dr.Close();
                    SqlCommand cmd = new SqlCommand("insert into ask(Email,CommentDate,Id,comimg,Comments) values('" + Email + "','" + CommentDate + "','" + Id + "','" + fileRelativePath + "','" + Comments + "')", con);
                    try
                    {
                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            SqlCommand comd2 = new SqlCommand("select count (*) as countcom from ask where Id='" + Id + "'", con);
                            SqlDataReader dr3 = comd2.ExecuteReader();
                            dr3.Read();
                            if (dr3.HasRows)
                            {
                                var val2 = dr3["countcom"].ToString();
                                rc.returnval = val2;
                            }
                            //msg.msg = "Ok";
                            rc.comment = Comments;
                            rc.Image = Image;
                            rc.Id = Id;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("error", ex.ToString());
                    }
                }
                else
                {
                    //msg.msg = "Invalid Credentials";
                }
                return Request.CreateResponse(HttpStatusCode.Created, rc);
            });

        return task;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using EducationDataAccess;
using System.Data.Entity;
using EducationDataAccess.Data;
using EducationDataAccess.Models;

public class RegistrationsController : ApiController
{
    public IEnumerable<User> Get()
    {
        using(EducationalhubEntities entities=new EducationalhubEntities())
        {
            var a = entities.Users.ToList();
            return a;
        }
    }
    
    public User Get(string Email)
    {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var a= entities.Users.FirstOrDefault(e => e.Email == Email);
            return a;
        }
    }
    
    [HttpPost]
    public UserReturnModel RegisterUser(User user)
    {
        var rm = new UserReturnModel();
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string qry = "select Email from [User] where Email='" + user.Email + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                rm.Message = "Email already exists";
            }
            else { 
            using (EducationalhubEntities entities=new EducationalhubEntities())
        {
            string num = "0123456789";
            int len = num.Length;
            string otp1 = string.Empty;
            int otpdigit = 5;
            string finaldigit;
            int getindex;
            for (int i = 0; i < otpdigit; i++)
            {
                do
                {
                    getindex = new Random().Next(0, len);
                    finaldigit = num.ToCharArray()[getindex].ToString();
                } while (otp1.IndexOf(finaldigit) != -1);
                otp1 += finaldigit;
            }
            entities.Users.Add(user);
            entities.SaveChanges();
            
                var otpz = new otp();
                otpz.otp1 = Convert.ToInt64(otp1);
                otpz.Usernum = user.Mob;
                entities.otps.Add(otpz);
                entities.SaveChanges();
                SendSms(user.Mob, otp1);

                rm.Message = "ok";
                rm.User = user;    
        }
        }
        }
        catch (Exception ex)
        {
            rm.Message = ex.Message; 
        }
        
        return rm;
        
    }

    [HttpGet]
    public otp getotpdetails(int otp)
    {
        using (EducationalhubEntities entities = new EducationalhubEntities())
        {
            var a = entities.otps.FirstOrDefault(e => e.otp1 == otp);
            return a;
        }
    }


    [HttpPost]
    public OtpModel otpsubmit(otp otpk)
    {
        var rc = new OtpModel();
        try {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string qry = "select * from otp where otp='" + otpk.otp1 + "' and Usernum='" + otpk.Usernum + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                dr.Close();
                SqlCommand cmd2 = new SqlCommand("update [User] set registered='True' where Mob='" + otpk.Usernum + "'", con);
                int i = cmd2.ExecuteNonQuery();
            }
            con.Close();
            rc.Message = "ok";
            rc.otp = otpk;
        }
        catch (Exception ex)
        {
            rc.Message = ex.Message;
        }
        return rc;
        
        }

    public static void SendSms(string number, string otp)
    {
       
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string mobileNumber = number;
        string m = "Your OTP is:" + otp;
        string authKey = "134832AN9sHl5pu585e3153";
        //Multiple mobiles numbers separated by comma
        //Sender ID,While using route4 sender id should be 6 characters long.
        string senderId = "LOGC4U";
        //Your message to send, Add URL encoding here.
        string message = HttpUtility.UrlEncode(m);
        //Prepare you post parameters
        StringBuilder sbPostData = new StringBuilder();
        sbPostData.AppendFormat("authkey={0}", authKey);
        sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        sbPostData.AppendFormat("&message={0}", message);
        sbPostData.AppendFormat("&sender={0}", senderId);
        sbPostData.AppendFormat("&route={0}", "4");
        try
        {
            //Call Send SMS API
            string sendSMSUri = "https://control.msg91.com/api/sendhttp.php";
            //Create HTTPWebrequest
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
            //Prepare and Add URL Encoded data
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(sbPostData.ToString());
            //Specify post method
            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/x-www-form-urlencoded";
            httpWReq.ContentLength = data.Length;
            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            //Get the response
            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string responseString = reader.ReadToEnd();

            //Close the response
            reader.Close();
            response.Close();
        }
        catch
        {

        }
        con.Close();
    }
}

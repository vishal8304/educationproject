﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using EducationDataAccess.Models;
using EducationDataAccess.Data;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using System.IO;

public class PostController : ApiController
{
    [HttpPost]
    public bool SaveImage(string ImgStr, string ImgName)
    {
        String path = HttpContext.Current.Server.MapPath("./Posts"); //Path

        //Check if directory exist
        if (!System.IO.Directory.Exists(path))
        {
            System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
        }

        string imageName = ImgName + ".jpg";

        //set the image path
        string imgPath = Path.Combine(path, imageName);

        byte[] imageBytes = Convert.FromBase64String(ImgStr);

        File.WriteAllBytes(imgPath, imageBytes);

        return true;
    }

    [HttpPost]
    public Message Sharepost(Sharepost post)
    {
        var msg = new Message();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd1 = new SqlCommand("select * from Savepost where Email='" + post.Email + "' and shareId =" + post.shareId, con);
        SqlDataReader dr = cmd1.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            dr.Close();
            SqlCommand cmd = new SqlCommand("delete from Savepost where shareId=" + post.shareId, con);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                msg.msg = "Unshared";
            }
        }
        else
        {
            dr.Close();
            SqlCommand cmd = new SqlCommand("insert into Savepost(shareId,Email) values (@shareId,@Email)", con);
            cmd.Parameters.AddWithValue("@Email", post.Email);
            cmd.Parameters.AddWithValue("@shareId", post.shareId);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                msg.msg = "Shared";
            }
        }
        con.Close();
        return msg;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class ssctestseries : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlabel();
            bindgrid();
        }
    }

    public void bindgrid()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        var a = Request.QueryString["set"];
        SqlCommand cmd = new SqlCommand("Select name,[set],sum(marks) as totalmarks,sum(time) as totaltime, count(Question) as count, dbo.GetTotalMarksBySet(@a, name, @email) as TotalMarksObt from ontimertest where [set] = @a GROUP BY[set], name", con);
        cmd.Parameters.Add(new SqlParameter("@a", a));
        cmd.Parameters.Add(new SqlParameter("@email", Request.Cookies["Email"].Value));
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        grid.DataSource = dt;
        grid.DataBind();
        con.Close();
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }

    protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var a = Request.QueryString["set"];
        DataRowView drv = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {

            }
            else
            {
                HyperLink Hlink = (HyperLink)e.Row.FindControl("Hlink");
                Label lbl = (Label)e.Row.FindControl("lblobt");
                Label lblname = (Label)e.Row.FindControl("lblname");
                if (lbl.Text == "")
                {
                    Hlink.NavigateUrl= "sscinstruct.aspx?set="+lblname.Text+"&section="+a+"";
                    Hlink.Text = "Start Test";
                }
                else
                {
                    Hlink.NavigateUrl = "solution.aspx?ssconline=" + lblname.Text + "&section=" + a + "";
                    Hlink.Text = "View Solution";
                }
                Session["lbl1"] = lbl.Text;
                Session["lbl2"] = lblname.Text;
            }

        }

    }
    
    public void bindlabel()
    {
        try { 
        var a = Request.QueryString["set"];
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        lbltest.Text = a.ToString();
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
        con.Close();
    }
    //public void valMembers()
    //{
    //    try
    //    {
    //        var a = Request.QueryString["set"];
    //        if (con.State == ConnectionState.Closed)
    //        {
    //            con.Open();
    //        }
    //        SqlCommand cmd1 = new SqlCommand("select Email from onusertest where Email='" + Session["usr"].ToString() + "' and [set]='" + a + "' group by name,[Email]", con);
    //        SqlDataReader dr1 = cmd1.ExecuteReader();
    //        dr1.Read();
    //        if (dr1.HasRows)
    //        {
    //            dr1.Close();
    //            SqlDataAdapter da2 = new SqlDataAdapter();
    //            SqlCommand cmd = new SqlCommand("Select name,[set],sum(marks) as totalmarks,sum(time) as totaltime, count(Question) as count, dbo.GetTotalMarksBySet(@a, name, @email) as TotalMarksObt from ontimertest where [set] = @a GROUP BY[set], name", con);
    //            cmd.Parameters.Add(new SqlParameter("@a", a));
    //            cmd.Parameters.Add(new SqlParameter("@email", Session["usr"].ToString()));
    //            da2.SelectCommand = cmd;
    //            DataTable dt2 = new DataTable();
    //            da2.Fill(dt2);
    //            Repeater2.DataSource = dt2;
    //            Repeater2.DataBind();
    //        }
    //        else
    //        {
    //            dr1.Close();
    //            SqlDataAdapter da1 = new SqlDataAdapter("select name,[set],count(Question) AS COUNT from ontimertest where [set]='" + a + "' and name is not null group by name,[set]", con);
    //            DataTable dt1 = new DataTable();
    //            da1.Fill(dt1);
    //            Repeater1.DataSource = dt1;
    //            Repeater1.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Console.Write("Some Problem Occurred", ex);
    //    }
    //    con.Close();
    //}


    //protected void btnSend_Click(object sender, EventArgs e)
    //{
       
    //    var txtfeedback=Request.Form["txtfeedback"];
    //    SqlCommand cmd = new SqlCommand("insert into feedback(feedback,Email,test) values(@feedback,@Email,@test)", con);
    //    cmd.Parameters.AddWithValue("@feedback", txtfeedback);
    //    cmd.Parameters.AddWithValue("@Email", Session["usr"].ToString());
    //    cmd.Parameters.AddWithValue("@test","Online test SSC CGL");
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    int i = cmd.ExecuteNonQuery();
    //    if (i == 1)
    //    {
    //        lblsave.Visible = true;
    //        lblsave.Text = "Sent Successfully";
    //    }
    //    con.Close();
    //}
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Notifications.aspx.cs" Inherits="Notifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row" style="text-align:center">
            <div class="col-md-12" style="margin-left:29%;">
                <h1>Dont miss any latest Updates</h1>
            </div>
            
        </div>
        <br />
        <div class="row" style="text-align:center">
            <div class="col-md-12">
                <h3>We will send you all important exam notifications, articles, quizzes & more... You can manage the settings anytime.</h3>
            </div>
        </div>
        <br />
        <div class="row" style="text-align:center">
            <div class="col-md-12" style="margin-left:45%;">
                <asp:Button ID="btnSubscribe" runat="server" BorderStyle="Outset" Text="SUBSCRIBE" onclick="btnSubscribe_Click" BackColor="Black" ForeColor="White" />
            </div>
        </div>
        <br />
        <div class="row" style="text-align:center">
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="false" ForeColor="#FFCC00" Font-Italic="True"></asp:Label>
            </div>
    </div>
</asp:Content>


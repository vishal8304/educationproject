﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
public partial class Previousyrppr : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindnewtiles();
        }
    }
    
    public void bindnewtiles()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct section from exam where exam is not null and exam!='' group by section,exam", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptsection.DataSource = dt;
        rptsection.DataBind();
        con.Close();
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }

    }

    protected void rptsection_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try { 
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptcontent = (Repeater)e.Item.FindControl("rptcontent");
            Label lblsection = (Label)e.Item.FindControl("lblsection");
            SqlDataAdapter da = new SqlDataAdapter("select distinct exam from exam where section='"+lblsection.Text+"' and exam is not null group by section,exam", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rptcontent.DataSource = dt;
            rptcontent.DataBind();
        }
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
}
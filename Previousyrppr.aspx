﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Previousyrppr.aspx.cs" Inherits="Previousyrppr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .btn{
                background-color: #c12b2b;
    color: white;
    width: 150px;
    height: 80px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" style="text-align:center">
                <h2><b>Previous Year Papers</b></h2>
            </div>
        </div><br />
        
       
        <asp:Repeater ID="rptsection" OnItemDataBound="rptsection_ItemDataBound" runat="server">
            <ItemTemplate>
         <div class="row">
            <div class="col-md-12" style="text-align:center">
                <h3>
                    <asp:Label ID="lblsection" runat="server" Text='<%#Eval("section")%>'></asp:Label>
                </h3>
            </div>
        </div><br />
                <div class="row">
                 <asp:Repeater ID="rptcontent" runat="server">
                     <ItemTemplate>
                         <div class="col-md-3">
                <a href="previouscontent.aspx?bankexam=<%#Eval("exam")%>"  style="text-decoration: none !important;">
                    <div style="width:200px;height:70px;background-color:#ffb52f;text-align:center;margin-top: 20px;"><br />
                        <%#Eval("exam")%>
                    </div>
                </a>
          </div>
                     </ItemTemplate>
                 </asp:Repeater>
             </div>
            </ItemTemplate>
            </asp:Repeater>
        
               
        </div>
    
</asp:Content>


$(document).on('click', '.testSection .button1', function () {
    id = $(this).attr('id'); if (web.test.data.perSectionTime && id == 'examSummary' && web.test.template.currentSubjectId != web.test.data.lastSubjectId) {
        if (confirm('Switch to next section ( if switched you will not be able to return to current section ) ?')) { web.test.template.jumpToNextSection(); }
        return;
    }
    id2 = $('.testSection .helperConainer').find('.btnActive').attr('id'); $('.testSection .helperConainer').find('.btnActive').removeClass('btnActive'); $(this).addClass('btnActive'); $('.testSection .questionContent').fadeOut('fast'); $('.testSection .actionButtons').fadeOut('fast'); $('.' + id2).fadeOut('fast'); $('.' + id).fadeIn('fast');
}); $(document).on('click', '.testSection .backButton', function () { id = $(this).attr('id'); $('.testSection .helperConainer').find('.btnActive').removeClass('btnActive'); $('.' + id).fadeOut('fast'); $('.testSection .questionContent').fadeIn('fast'); $('.testSection .actionButtons').fadeIn('fast'); }); function showHindiTest() { $('.testSection .sysInsTest1').fadeOut('fast'); $('.testSection .sysInsTest1Hindi').fadeOut('fast'); if ($(".testSection .languageIns option:selected").text() == 'English') { $('.testSection .sysInsTest1').fadeIn('fast'); } else { $('.testSection .sysInsTest1Hindi').fadeIn('fast'); } }
function checkResume() { var val = $('#baseAttemptStatus').val(); if (val == 'resume') { web.test.data.setIsResume(1); $('.instructionSection .leftSection .firstPage').css('display', 'none'); $('.leftSection #instPagination').css('display', 'none'); $('.leftSection .resumePage').css('display', 'block'); } else { web.test.data.setIsResume(0); $('.instructionSection .leftSection .firstPage').css('display', 'block'); $('.leftSection #instPagination').css('display', 'block'); $('.leftSection .resumePage').css('display', 'none'); } }
$(document).ready(function () { checkResume(); adjustHeight(); adjustHeightIns(); }); $(window).resize(function () { adjustHeight(); adjustHeightIns(); }); function adjustHeight() { var x = parseInt($('.testSection header').css('height').replace("px", "")) + parseInt($('.testSection footer').css('height').replace("px", "")); $('.testSection .mainContainer').css('height', $(window).height() - x); x = parseInt($('.testSection .leftSection').css('height').replace("px", "")) - parseInt($('.leftSection>fieldset').css('height').replace("px", "")); $('.testSection .leftSection>div.row').css('height', x); x = x - parseInt($('.leftSection .actionButtons').css('height').replace("px", "")); $('.testSection .leftSection>div.row .questionContent').css('height', x); }


function resumeTest() {
    if (!web.test.data.isDataLoaded()) { return alert("Test Data Not Loaded"); }
    $('.instructionSection #disclaimer').prop("checked", true); web.test.data.currentQuesId = 0; data = web.test.data.resumeData; var resumeQuestionId = 0; var resumeSubjectId = 0; var perSectionTime = web.test.data.perSectionTime; web.test.data.perSectionTime = false; web.test.data.sectionalTimeLeft = data.sectionalTimeLeft; var timeLeft; if (perSectionTime && data.sectionalTimeLeft && data.sectionalTimeLeft.length > 0) { $.each(data.sectionalTimeLeft, function (index, value) { if (value > 0 && typeof timeLeft === 'undefined') { timeLeft = value; } }); } else { timeLeft = data.timeLeft; }
    web.test.data.timer = new CountdownTimer({ seconds: timeLeft, onTick: function () { $("span.hour").text(web.test.data.timer.fHours); $("span.minute").text(web.test.data.timer.fHMins); $("span.seconds").text(web.test.data.timer.fSecs); }, onComplete: web.test.base.finishSubmit }); var lastQuestionId = null; $.each(data.questions, function (key, value) {
        var type = web.test.data.questionData[key].type; var s = Number(value.s); lastQuestionId = web.test.data.currentQuesId; if (s >= 2) {
            resumeQuestionId = key; resumeSubjectId = web.test.data.questionData[key].subjectId; if (s === 3 || s === 5) {
                if (type === 'SC') { $('.choice_wrap[index="' + value.c + '"]').find('input').prop('checked', true); }
                if (type === 'MCC') { $.each(value.m, function (index, value) { $('.choice_wrap[index="' + value + '"]').find('input').prop('checked', true); }); }
                if ((type === 'FIB' || type === 'NAT') && value.r) { $('._options').find('input.answerInput').val(value.r); }
            }
            if (s === 3) { web.test.base.nextQuestion(); }
            if (s >= 4) {
                $('.revise').addClass('markedRevise'); var x = $('.quesLinks').find('.current-question'); x.addClass('status-revise'); web.test.base.nextQuestion(); if (x.hasClass('status-skip')) { x.removeClass('status-skip'); }
                $('.revise').removeClass('markedRevise');
            }
            if (s === 2) { web.test.base.nextQuestion(); }
        } else { web.test.base.nextQuestion(); web.test.data.setMarkNotVisited(lastQuestionId); $('.quesLinks #q_li_' + lastQuestionId).removeClass('status-skip').addClass('status-unattempted'); }
        if (value.t !== undefined || value.t !== null) { web.test.data.questionData[lastQuestionId].t = value.t; }
        web.test.template.toolTip();
    }); $('.quesLinks #q_li_' + (web.test.data.currentQuesId)).removeClass('status-skip').addClass('status-unattempted'); web.test.template.toolTip(); web.test.data.setCurrentQuestionId(resumeQuestionId); web.test.template.setCurrentSubjectId(resumeSubjectId); web.test.template.setActiveSectionLink(resumeSubjectId); web.test.template.displayActiveSubjectQuesLinks(resumeSubjectId); web.test.base.setCurrentQuestionData(); web.test.data.perSectionTime = perSectionTime; startTest();
}
function replaceRadio() {
    if ($('._options').attr('typeofquestion') === 'MC') {
        var checkProp; $('._options>tr').each(function () {
            checkProp = ''; if ($(this).find('input').prop('checked'))
                checkProp = "checked"; if ($(this).find('input.mcCheckbox').attr('type')) {
                    if (checkProp == 'checked') { $(this).find('.mcCheckbox').prop('checked', 'checked'); }
                    return;
                }
            $(this).find('input').hide().after('<input type="checkbox" class="mcCheckbox" ' + checkProp + '>');
        });
    }
}
$(document).contextmenu(disableRightClick); function disableRightClick(event) { if (event.button === 2) { alert('Right click is not allowed'); event.preventDefault(); return false; } }
$(document).keydown(disableCtrl); function disableCtrl(ev) { if (ev.ctrlKey) { ev.preventDefault(); } }
function showHindiTestIns()
{$('.instructionSection .sysInsTest1').fadeOut('fast');$('.instructionSection .sysInsTest1Hindi').fadeOut('fast');if($(".instructionSection .language option:selected").text()=='English')
{$('.instructionSection .sysInsTest1').fadeIn('fast');}
else
{$('.instructionSection .sysInsTest1Hindi').fadeIn('fast');}}
function adjustHeightIns()
{var x=parseInt($('.instructionSection header').css('height').replace("px",""))+parseInt($('.instructionSection footer').css('height').replace("px",""));$('.instructionSection .mainContainer').css('height',$(window).height()-x);$('.afterTestFinishedSection .mainContainer').css('height',$(window).height()-50);}
function goBackToPage1()
{$('.instructionSection .secondPage').fadeOut('fast');$('.instructionSection #instPagination2').fadeOut('fast');$('.instructionSection .secondPage2').fadeOut('fast');$('.instructionSection .firstPage').fadeIn('fast');$('.instructionSection #instPagination').fadeIn('fast');showHindiTest();}
function goToNextSection()
{$('.instructionSection .firstPage').fadeOut('fast');$('.instructionSection #instPagination').fadeOut('fast');$('.instructionSection .secondPage').fadeIn('fast');$('.instructionSection #instPagination2').fadeIn('fast');$('.instructionSection .secondPage2').fadeIn('fast');}
function startTest()
{if(!$('.instructionSection #disclaimer').is(":checked"))
{alert("Please tick the checkbox to continue")
return;}
else
{$('.instructionSection').hide();$('.testSection').show(1,'linear',function(){adjustHeight();adjustHeightIns();web.test.base.startTimer();web.test.base.submitScore=1;});web.test.data.setDefaultLanguage($('.instructionSection .testLanguage option:selected').val());web.test.template.handlers();web.test.template.examSummary();if(web.test.data.perSectionTime){$('.helperConainer a#examSummary.button1').text('Submit section');$('.helperConainer a#QASection.button1').css('display','none');}
web.test.template.displayCurrentQuestion();}}
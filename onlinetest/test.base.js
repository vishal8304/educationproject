web.test.base = web.test.base || {}; web.test.base.finished = 0; web.test.base.submitScore = 0; web.test.base.initialise = function () { }
web.test.base.handlers = function () { $('._previous[test-option="button"]').unbind('click').bind('click', web.test.base.previousQuestion); $('._pause-timer[test-option="timer"]').unbind('click').bind('click', web.test.base.pauseTimer); $('#finish_btn').unbind('click').bind('click', web.test.base.finishSubmit); setTimeout(web.test.base.submitUserCurrentScore, 10000); }
web.test.base.previousQuestion = function () { web.test.base.saveQuestionStatus(); web.test.template.processOnNextAndPrev(); web.test.data.currentQuesId -= 1; web.test.base.setCurrentQuestionData(); web.test.template.previousQuestion($(this)); }
web.test.base.saveQuestionStatus = function () { web.test.base.storeOptions(); web.test.base.storeReviseMark(); web.test.base.markVisited(); web.test.data.endTime = web.test.data.timer.secondsRemaining; web.test.base.setTimeStampOfQuestion(web.test.data.startTime, web.test.data.endTime); }
web.test.base.checkInput = function () {
    var type = $('._options').attr('typeofquestion'); var answer; var re; if (type === 'SC') { return { isValid: true }; }
    if (type === 'MCC') { return { isValid: true }; }
    if (type === 'FIB') {
        answer = $('._options').find('.answerInput').val(); var re = /^\w{1,200}$/g; if (re.test(answer) || answer === '') { return { isValid: true }; }
        return { isValid: false, message: 'Answer must be one word of length between 1 and 200' };
    }
    if (type === 'NAT') {
        answer = $('._options').find('.answerInput').val(); if (Number(answer) || answer === '') { return { isValid: true }; }
        return { isValid: false, message: 'Answer must be a valid number' };
    }
    return { isValid: false, message: 'Not able to get type of question' };
}

web.test.base.nextQuestion = function (actionOnNextAndPrev) {
    var evalInput = web.test.base.checkInput();
    if (!evalInput.isValid) { var message = evalInput.message || ''; return alert(message); }
    web.test.base.saveQuestionStatus();
    web.test.template.processOnNextAndPrev(actionOnNextAndPrev);
    web.test.template.toolTip();
    web.test.template.examSummary();
    if (web.test.data.currentQuesId === web.test.data.totalNumberOfQuestions - 1) { return; }

    var setTimer = false;

    if (web.test.data.perSectionTime) {
        var currentQuesId = web.test.data.currentQuesId;
        var len = web.test.data.subjectData[web.test.template.currentSubjectId].section[0].questions.length;
        var lastQuesIdOfSubject = web.test.data.subjectData[web.test.template.currentSubjectId].section[0].questions[len - 1].id;
        if (currentQuesId === (lastQuesIdOfSubject - 1) && (web.test.data.currentQuesId !== web.test.data.totalNumberOfQuestions - 1))
        {
            if (confirm('Switch to next section ( if switched you will not be able to return to current section ) ?')) { setTimer = true; } else {
                return;
            }
        }
    }
    web.test.data.currentQuesId += 1; if (web.test.data.currentQuesId > web.test.data.totalNumberOfQuestions - 1) { web.test.data.currentQuesId = web.test.data.totalNumberOfQuestions - 1; }
    web.test.base.setCurrentQuestionData(); if (setTimer) { web.test.data.sectionalTimeLeft[web.test.template.currentSubjectId - 1] = 0; web.test.data.timerTime = web.test.data.subjectData[web.test.template.currentSubjectId].time; web.test.data.setTimer(); web.test.base.startTimer(); }
}
web.test.base.submitUserCurrentScore = function () {
    if (web.test.base.submitScore === 1) { web.test.base.submitCurrentScore(); }
    var t = $('#time').val(); var time; if (t) { time = t; }
    return setTimeout(web.test.base.submitUserCurrentScore, time);
}
web.test.base.jumpToQuestion = function (questionId) {
    web.test.data.currentQuesId = questionId; if (web.test.data.currentQuesId > web.test.data.totalNumberOfQuestions - 1) { web.test.data.currentQuesId = web.test.data.totalNumberOfQuestions - 1; }
    web.test.base.setCurrentQuestionData();
}
web.test.base.getCurrentQuestionData = function () { return web.test.data.getCurrentQuestionData(); }
web.test.base.setCurrentQuestionData = function () { web.test.template.displayCurrentQuestion(); web.test.template.setCurrentQuestionData(); }
web.test.base.checkRevise = function () { var questionData = web.test.base.getCurrentQuestionData(); web.test.template.checkRevise(questionData); }
web.test.base.storeOptions = function () { web.test.template.storeOptions(); }
web.test.base.storeReviseMark = function () {
    if (web.test.template.storeReviseMark()) { return web.test.data.setMarkReview(web.test.data.currentQuesId, true); }
    web.test.data.setMarkReview(web.test.data.currentQuesId, false);
}
web.test.base.markVisited = function () { web.test.data.setMarkVisited(web.test.data.currentQuesId); }
web.test.base.isQuestionMarkedRevise = function (questionId) { var questionData = web.test.data.getQuestionDataOfSelected(questionId); return (questionData.s && (questionData.s === 4 || questionData.s === 5)) }
web.test.base.isQuestionMarkedVisited = function (questionId) { var questionData = web.test.data.getQuestionDataOfSelected(questionId); return (questionData.s && questionData.s === 2); }
web.test.base.isQuestionAnswered = function (questionId) {
    var questionData = web.test.data.getQuestionDataOfSelected(questionId); var type = questionData.type; if (type === 'SC') { return (questionData.c > -1); }
    if (type === 'MCC') { return (questionData.m && questionData.m.length > 0); }
    if (type === 'FIB' || type === 'NAT') { return questionData.r || false; }
    return false;
}
web.test.base.submitCurrentScore = function () {
    web.test.data.endTime = web.test.data.timer.secondsRemaining; web.test.base.setTimeStampOfQuestion(web.test.data.startTime, web.test.data.endTime); const url = '/appTestPackage/saveAttemptProgress'; var projectId = "0"; var timeRemaining = web.test.data.timer.secondsRemaining; var perSectionTime = web.test.data.perSectionTime; if (perSectionTime && web.test.data.subjectData[web.test.template.currentSubjectId].time !== undefined) { web.test.data.sectionalTimeLeft[web.test.template.currentSubjectId] = timeRemaining; }
    if ($("#projectId")) { projectId = $("#projectId").val(); topicId = $("#topicId").val(); groupId = $("#groupId").val(); }
    var sendData = []; for (i = 0; i < web.test.data.questionData.length; i++) { sendData[i] = minify(web.test.data.questionData[i]); }
    $.ajax({ url: url, method: "POST", data: JSON.stringify({ questions: sendData, authToken: web.test.data.authToken, unitToken: web.test.data.unitToken, timeLeft: timeRemaining, isComplet: false, attemptSource: 'web', sectionalTimeLeft: web.test.data.sectionalTimeLeft }), dataType: 'json', contentType: 'application/json', success: function (data) { $("#internetConnected").show(); $("#internetDisconnected").hide(); }, error: function (xhr, status, error) { $("#internetConnected").hide(); $("#internetDisconnected").show(); } });
}
function minify(data) {
    if (data.type === 'SC') { return { c: data.c, s: data.s, t: data.t }; }
    if (data.type === 'MCC') { return { m: data.c, s: data.s, t: data.t }; }
    if (data.type === 'FIB' || data.type === 'NAT') { return { r: data.c, s: data.s, t: data.t }; }
    return {};
}
web.test.base.finishSubmit = function () {
    if (!web.test.template.finishSubmit()) { return; }
    $('body .spinnerContainer').toggleClass('hidden'); web.test.base.storeOptions(); web.test.base.storeReviseMark(); web.test.base.markVisited(); web.test.data.endTime = web.test.data.timer.secondsRemaining; web.test.base.setTimeStampOfQuestion(web.test.data.startTime, web.test.data.endTime); web.test.base.setTimeStampOfQuestion(web.test.data.startTime, web.test.data.endTime); web.test.data.timer.pause(); web.test.base.submitScore = 0; const url = '/appTestPackage/saveAttemptProgress'; var projectId = "0"; var sendData = []; var perSectionTime = web.test.data.perSectionTime; if (perSectionTime && typeof web.test.data.subjectData[web.test.template.currentSubjectId].time !== undefined) { web.test.data.sectionalTimeLeft[web.test.template.currentSubjectId] = 0; }
    for (i = 0; i < web.test.data.questionData.length; i++) { sendData[i] = minify(web.test.data.questionData[i]); }
    $("#finish_btn").hide(); $.ajax({
        url: url, type: "POST", cache: 'false', data: JSON.stringify({ questions: sendData, authToken: web.test.data.authToken, unitToken: web.test.data.unitToken, isComplete: true, attemptSource: 'web', sectionalTimeLeft: web.test.data.sectionalTimeLeft }), dataType: 'json', contentType: 'application/json', success: function (data) {
            $('body .spinnerContainer').toggleClass('hidden'); web.test.base.finished = 1; var result; try { result = JSON.parse(data.result).mockTestScores; } catch (e) { result = {}; console.log(e); }
            web.test.base.afterFinishSubmit(result);
        }, error: function (xhr, status, error) { console.log(error); $('body .spinnerContainer').toggleClass('hidden'); $("#finish_btn").show(); alert('Unable to submit test, please check you internet connection'); }
    });
}
web.test.base.setTimeStampOfQuestion = function (start, end) { web.test.data.setQuestionTimeStamp(web.test.data.currentQuesId, start, end); }
web.test.base.startTimer = function () { web.test.data.timer.start(); return true; }
web.test.base.pauseTimer = function () {
    valAllowPause = $("#allowPause").val(); if (valAllowPause == 1) { web.test.data.timer.pause(); }
    return true;
}
web.test.base.resumeTimer = function () { web.test.data.timer.resume(); return true; }
web.test.base.hideTimer = function () { var _timer = $('#timer'); if (_timer.hasClass('timer-hidden')) { _timer.show().removeClass('timer-hidden'); return false; } else { _timer.hide().addClass('timer-hidden'); return true; } }
window.onbeforeunload = function (e) { if (web.test.base.finished == 0) { return 'Navigating Away from this page will loose your marked options. Once closed you will not be able to give the test again.'; } }
web.test.template = web.test.template || {}; web.test.template.currentSubjectId = web.test.template.currentSubjectId || {}; web.test.template.initialise = function () { web.test.template.currentSubjectId = 0; web.test.template.renderTemplate(); web.test.template.examSummary(); }
web.test.template.previousQuestion = function (element) { $('._next').removeAttr('disabled'); if (web.test.data.currentQuesId === 0) { element.attr('disabled', 'disabled'); } }
web.test.template.nextQuestion = function (element) { if (web.test.data.currentQuesId === web.test.data.totalNumberOfQuestions - 1) { } }
web.test.template.setCurrentQuestionData = function () {
    $('._options').find('.selected').attr("checked", "checked"); if (web.test.data.currentQuesId === 0) { $('._next').removeAttr('disabled'); } else if (web.test.data.currentQuesId === web.test.data.totalNumberOfQuestions - 1) { } else { $('._next').removeAttr('disabled'); }
    web.test.base.checkRevise(); $('._next').attr("data-question_id", web.test.data.currentQuestionId);
}
web.test.template.checkRevise = function (questionData) {
    if (questionData.s === 4 || questionData.s === 5) { $('#inp_revise').addClass('markedRevise'); return true; }
    $('#inp_revise').removeClass('markedRevise'); return false;
}
web.test.template.storeOptions = function () { var answer = -1; var options = $('._options .choice_wrap'); $.each(options, function (index, value) { if ($(value).find('input').is(':checked')) { answer = index; } }); web.test.data.setOptions(web.test.data.currentQuesId, answer); }
web.test.template.storeReviseMark = function () {
    if ($('#inp_revise').hasClass('markedRevise')) { return true; }
    return false;
}
web.test.template.jumpToNextSection = function () { web.test.data.sectionalTimeLeft[web.test.template.currentSubjectId] = 0; web.test.data.timerTime = web.test.data.subjectData[web.test.template.currentSubjectId + 1].time; web.test.data.setTimer(); web.test.base.startTimer(); web.test.template.displaySubjectQuestion(web.test.template.currentSubjectId + 1); }
web.test.template.finishSubmit = function () { if (web.test.data.timer.fHours == 0 && web.test.data.timer.fHMins == 0 && web.test.data.timer.fSecs == 0) { if (web.test.data.perSectionTime && web.test.template.currentSubjectId != web.test.data.lastSubjectId) { web.test.template.jumpToNextSection(); return false; } else { alert("Time Up"); return true; } } else { if (!confirm("Are you sure want to submit")) { return false; } else { $('.examSummary .querySubmitBox').append('<p class="loading">Submitting..</p><p class="loading">Please be patient it may take some time</p>'); return true; } } }
$(document).on('click', '.CRBtn', function () { $('._options').find('input').attr('checked', false).removeClass('selected'); $('.quesLinks').find('.current-question').removeClass('status-answer').addClass('status-skip'); web.test.base.storeOptions(); web.test.template.toolTip(); web.test.template.examSummary(); }); web.test.template.handlers = function () {
    $('._next').unbind('click').bind('click', web.test.base.nextQuestion.bind(this, 'rmRevise')); $('.quesLinks').undelegate('.questions_list a.q', 'click').delegate('.questions_list a.q', 'click', web.test.template.jumpToQuestion); $('.questions_nav').undelegate('.questions_filter', 'click').delegate('.questions_filter', 'click', web.test.template.toggle_questions_filter); $('.mask').unbind('click').bind('click', function () { web.test.template.toogleDrawer(); }); $('.revise').click(function (event) {
        $(this).addClass('markedRevise'); var x = $('.quesLinks').find('.current-question'); if (!x.hasClass('status-answer')) { x.addClass('status-revise'); }
        web.test.base.nextQuestion(); if (x.hasClass('status-skip')) { x.removeClass('status-skip'); }
        $(this).removeClass('markedRevise'); web.test.template.toolTip();
    }); $('.questions_nav').undelegate('.filter_options div[type="filter"]', 'click').delegate('.filter_options div[type="filter"]', 'click', web.test.template.filterItems); $('.0').unbind('click').bind('click', function () {
        valAllowPause = $("#allowPause").val(); if (valAllowPause == 1) { web.test.base.pauseTimer(); }
        $('.finish').removeClass('hide').show(); $('.finish_mask').removeClass('hide').show();
    }); $('.test-link').click(function () {
        if (web.test.data.perSectionTime) { return; }
        var id = $(this).attr('id'); web.test.template.setActiveSectionLink(id); web.test.base.saveQuestionStatus(); web.test.template.displaySubjectQuestion(id);
    }); $(".finish .close").unbind('click').bind('click', function () { $('.finish').addClass('hide').hide(); $('.finish_mask').addClass('hide').hide(); web.test.base.resumeTimer(); })
    $("div.finish_mask").unbind('click').bind('click', function (ev) {
        if ($(ev.target).hasClass("finish_mask"))
            $(".finish .close").trigger("click");
    }); $('.continue.btn').unbind('click').bind('click', function () { $(".finish .close").trigger("click"); });
}
web.test.template.setActiveSectionLink = function (id) { $('fieldset').find('.active-link').removeClass('active-link'); $('fieldset').find('#' + id + '.test-link ').addClass('active-link'); }
web.test.template.displayActiveSubjectQuesLinks = function (id) { $(".questionsUL").removeClass("show").hide(); $("#questionsUL" + id).removeClass("hide").show(); }
web.test.template.displaySubjectQuestion = function (id) { web.test.template.currentSubjectId = id; web.test.base.jumpToQuestion(web.test.data.subjectFirstQuestionId[id]); web.test.template.displayActiveSubjectQuesLinks(id); web.test.template.setActiveSectionLink(id); }
web.test.template.renderTemplate = function () {
    var contentLeftData = ""; var contentRealData = ""; var subjectLinks = ""; var i = 0; var j = 0; var k = 0; var optionSr = 'A'; var optionId; var questionId; questionId = web.test.data.currentQuesId; currentSubjectId = web.test.template.currentSubjectId; QASection = ''; var selectedLang = web.test.data.getDefaultLanguage(); var o = 1; $.each(web.test.data.subjectData, function (key, value) {
        QASection += '<h1>Section : <span>' + value.name + '</span></h1>'; var x = value.section[0]; var cmText; var question; $.each(x.questions, function (key, value) {
            cmText = ''; question = value.text; if (value.hasOwnProperty('commonText')) { cmText = value.commonText; }
            if (selectedLang !== '' && selectedLang !== 'EN' && value.supportedLangData.hasOwnProperty(selectedLang)) { cmText = (value.supportedLangData[selectedLang].commonText) ? value.supportedLangData[selectedLang].commonText : ''; question = value.supportedLangData[selectedLang].text; }
            QASection += '<div class="row"><div class=col-xs-12>' + cmText + '</div><div class="col-xs-1">Q. <span>' + (o++) + '</span></div><div class="col-xs-11">' + question + '</div><div class="row">'; var u = 1; QASection += '</div></div>';
        });
    }); $('.QA').html(QASection); $.each(web.test.data.subjectData, function (key, value) { subjectLinks += '<a href="javascript:" class="test-link section-link ' + (i == 0 ? "active-link" : "") + '" id="' + i++ + '">' + value.name + '<span class="toolTip"><p><strong>' + value.name + '</strong></p><div class="row"><div class="col-xs-8">Answered</div><div class="col-xs-4 ansd"></div><div class="col-xs-8">Not Answered</div><div class="col-xs-4 nansd"></div><div class="col-xs-8">Marked for Review</div><div class="col-xs-4 mfr"></div><div class="col-xs-8">Not Visited</div><div class="col-xs-4 nv"></div></div></span></a>'; }); k = 0; $.each(web.test.data.subjectData, function (key, value) {
        $.each(value.section, function (key1, value1) {
            if (key == web.test.template.currentSubjectId) { contentLeftData += '<span class="questions_list show questionsUL" id="questionsUL' + key + '">'; } else { contentLeftData += '<span class="questions_list hide questionsUL" id="questionsUL' + key + '">'; }
            $.each(value1.questions, function (key2, value2) { contentLeftData += '<a href="javascript:" class="quesLinksa questionlistLinks q status-unattempted" id="q_li_' + questionId + '" data-question_id="' + questionId + '">' + (k + 1) + '</a>'; k++; questionId++; }); contentLeftData += '</span>';
        });
    }); $('fieldset>p').html(subjectLinks); $('.masterTestName').html(web.test.data.masterTestName); web.test.data.setTimer(); $('.quesLinks').html(contentLeftData); $('.tests-list').find('.active').removeClass('active'); $('.tests-list').find('#' + web.test.template.currentSubjectId).parent().addClass('active'); $('.test-heading').find('p').html($('.tests-list').find('.active').text()); web.test.base.setCurrentQuestionData();
}
web.test.template.finishLoad = function () { $('.loader').addClass('hide').hide(); $('.finish_mask').addClass('hide').hide(); }
web.test.template.setCountOfAttemptedQuestion = function () { attempted = 0; id = $('.tests-list').find('.active').children().attr('id'); $('#questionsUL' + id).find(".status-answer").each(function () { attempted++; }); $('.attempted-count').html(attempted); }
web.test.template.toolTip = function () { id = $('fieldset').find('.active-link').attr('id'); testData = web.test.template.getTestStat(id); var span = $('.active-link').find('.toolTip'); span.find('.ansd').html(testData.answered); span.find('.nansd').html(testData.notAnswered); span.find('.mfr').html(testData.markedForReview); span.find('.nv').html(testData.notVisited); }
web.test.template.examSummary = function (data) {
    data = data || {}; var examSummaryData = ''; var totAnswered = 0, totNotAnswered = 0, totMarkedForReview = 0, totNotVisited = 0, totTotalQuestions = 0, i = 0; var totCorrect = (data.correct !== undefined) ? data.correct : null; var totWrong = (data.wrong !== undefined) ? data.wrong : null; var totScore = (data.score !== undefined) ? data.score : null; var sectionsScore = (data.sectionScores !== undefined) ? data.sectionScores : []; var sectionWiseScore = []; $('.testSection fieldset p a').each(function () {
        var wrong, correct, score; if (sectionsScore.length > 0) { sectionWiseScore = sectionsScore[i]; wrong = sectionWiseScore.wrong; correct = sectionWiseScore.correct; score = sectionWiseScore.score; }
        id = $(this).attr('id'); sectionName = $(this).find('.toolTip strong').text(); testData = web.test.template.getTestStat(id); examSummaryData += '<tr><td>' + sectionName + '</td><td>' + testData.totalQuestions + '</td><td>' + testData.answered + '</td><td>' + testData.notAnswered + '</td><td>' + testData.markedForReview + '</td><td>' + testData.notVisited + '</td>' + ((wrong !== undefined) ? '<td>' + wrong + '</td>' : '') + ((correct !== undefined) ? '<td>' + correct + '</td>' : '') + ((score !== undefined) ? '<td>' + score + '</td>' : '') + '</tr>'; totAnswered += testData.answered; totNotVisited += testData.notVisited; totNotAnswered += testData.notAnswered; totMarkedForReview += testData.markedForReview; totTotalQuestions += testData.totalQuestions; i++;
    }); if (i > 1) { examSummaryData += '<tr class="totalRow"><td>Total</td><td>' + totTotalQuestions + '</td><td>' + totAnswered + '</td><td>' + totNotAnswered + '</td><td>' + totMarkedForReview + '</td><td>' + totNotVisited + '</td>' + ((totWrong !== null) ? '<td>' + totWrong + '</td>' : '') + ((totCorrect !== null) ? '<td>' + totCorrect + '</td>' : '') + ((totScore !== null) ? '<td>' + totScore + '</td>' : '') + '</tr>'; }
    $('.testSection .examSummary .examSummaryBody').html(examSummaryData); $('.afterTestFinishedSection .examSummary .examSummaryBody').html(examSummaryData);
}
web.test.template.getTestStat = function (id) {
    var answered = 0, notAnswered = 0, markedForReview = 0, notVisited = 0, totalQuestions = 0, markedForReviewAndAnswered = 0; answered = $('#questionsUL' + id).find(".status-answer").length; markedForReview = $('#questionsUL' + id).find(".status-revise").length; notVisited = $('#questionsUL' + id).find(".status-unattempted").length; totalQuestions = $('#questionsUL' + id).find('.q').length; markedForReviewAndAnswered = $('#questionsUL' + id).find('.status-answer.status-revise').length; markedForReview -= markedForReviewAndAnswered
    notAnswered = totalQuestions - answered - notVisited - markedForReview; testData = { 'totalQuestions': totalQuestions, 'answered': answered, 'notAnswered': notAnswered, 'markedForReview': markedForReview, 'notVisited': notVisited }; return testData;
}
web.test.base.afterFinishSubmit = function (responseData) {
    web.test.template.examSummary(responseData); if (responseData) {
        if (responseData.correct !== undefined) { $('.evalCorrect').toggleClass('hidden'); }
        if (responseData.wrong !== undefined) { $('.evalWrong').toggleClass('hidden'); }
        if (responseData.score !== undefined) { $('.evalScore').toggleClass('hidden'); }
    }
    web.test.base.submitScore = 0; $('.instructionSection').hide(0, function () { $('.testSection').hide(0, function () { $('.afterTestFinishedSection').show(0); }); });
}
web.test.template.setCurrentSubjectId = function (currentSubjectId) { web.test.template.currentSubjectId = currentSubjectId; }
web.test.template.displayCurrentQuestion = function () {
    var defaultlang = web.test.data.getDefaultLanguage(); var selectedLang = defaultlang; $('.q').removeClass('current-question'); $('.q[data-question_id="' + web.test.data.currentQuesId + '"]').addClass('current-question'); var questionData = web.test.base.getCurrentQuestionData(); if (questionData.subjectId != web.test.template.currentSubjectId) { web.test.template.displaySubjectQuestion(questionData.subjectId); }
    if (defaultlang !== 'EN' && web.test.data.supportedLangData[defaultlang] && web.test.data.supportedLangData[defaultlang].testName) { $('header h1').html(web.test.data.supportedLangData[defaultlang].testName); } else { $('header h1').html(web.test.data.masterTestName); }
    $('._options').hide(); var x = $('.quesLinks').find('.current-question'); if (x.hasClass('status-unattempted')) { x.removeClass('status-unattempted').addClass('status-skip'); }
    questionText = questionData.text; var optionData = questionData.options; var cmmText = (questionData.commonText) ? questionData.commonText : ''; if (selectedLang !== '' && selectedLang !== 'EN' && questionData.supportedLangData.hasOwnProperty(selectedLang)) { questionText = questionData.supportedLangData[selectedLang].text; cmmText = questionData.supportedLangData[selectedLang].commonText || ""; optionData = questionData.supportedLangData[selectedLang].options; }
    questionType = 'Single Choice'; if (questionData.type == 'SC' || questionData.type == 'singleChoice') { questionType = 'Single Choice'; }
    if (questionData.type == 'MC' || questionData.type == 'multipleChoice') { questionType = 'Multiple Choice'; }
    if (questionData.type == 'Fill') { questionType = 'Fill in the blank' }
    $('.question_number').text(web.test.data.currentQuesId + 1); $('.subject_name').html(questionData.subject); $('.maxMarks').html(questionData.positiveMarks); if (typeof questionData.negativeMarks === 'undefined') { negativeMarks = 0; } else { negativeMarks = questionData.negativeMarks; }
    if (negativeMarks > 0) { negativeMarks = "-" + negativeMarks; }
    if (questionData.isNotLinked) { questionText = '<p class="cmmText">' + cmmText + '</p>' + questionText; }
    questionText += '<p style="font-size:11px !important;color:#666;">' + questionType + ' (' + questionData.positiveMarks + ', ' + negativeMarks + ')</p>'
    $('._question').html(questionText).attr('questionId', questionData.id); $('.negativeMarks').html(negativeMarks); if (!questionData.isNotLinked && cmmText !== '') { $('.commonText').show(); $('.commonText p').html(cmmText); $('.quesAnsContent').css("flex-basis", "50%"); } else { $('.commonText').hide(); $('.quesAnsContent').css("flex-basis", "100%"); }
    var optionHtmlData = ''; var contentActionBarData = ''; var optionSr = 'A'; var i = 0; var optionFlag = 0; var supportedLangData = (questionData.hasOwnProperty('supportedLangData') && questionData.supportedLangData) ? questionData.supportedLangData : {}; var supportedLangHtml = "<option data-langCode='EN'>English</option>"; var languages = web.test.data.getSupportedLanguages(); $.each(supportedLangData, function (key, value) { if (languages.hasOwnProperty(key)) { supportedLangHtml += '<option data-langCode=' + key + ' ' + ((key == selectedLang) ? 'selected' : '') + '>' + languages[key] + '</option>'; } }); $('.questionContent .language').html(supportedLangHtml); $.each(optionData, function (key, value) {
        optionId = value.id; optionFlag = 0; if (questionData.c === key) { optionFlag = 1; }
        if (optionFlag && optionFlag === 1) { classSelected = "selected"; } else { classSelected = ""; }
        if (!(questionData.type == 'SC' || questionData.type == 'singleChoice' || questionData.type == 'MC' || questionData.type == 'multipleChoice')) { questionData.type = 'SC'; }
        if (questionData.type == 'SC' || questionData.type == 'singleChoice') { optionHtmlData += '<tr class="choice_wrap" index="' + i + '"><td style="vertical-align:middle;width:25px;"><input id="option' + i + '" type="radio" ' + (questionData.type == 'SC' || questionData.type == 'singleChoice' ? 'name="options"' : '') + ' ' + (questionData.c === key ? "checked" : "") + ' class="' + (questionData.c === key ? "selected" : "") + '"></td><!--<td><span class="choice_label">' + (optionSr) + '</span></td>--><td><label for="option' + i + '">' + value + '</label></td></tr>'; optionSr = String.fromCharCode(optionSr.charCodeAt(0) + 1); }
        i++;
    }); $('.question_wrap').find('.choices').attr('typeOfQuestion', questionData.type); $('._options').html(optionHtmlData); $('._options').attr('typeOfQuestion', questionData.type); $('._options').hide(1, function () { $(this).show(1); }); replaceRadio(); $('.currentSection').text(web.test.data.subjectData[web.test.template.currentSubjectId].name); setTimeout(function () { web.test.data.startTime = web.test.data.timer.secondsRemaining; }, 600); web.test.base.markVisited(); web.test.template.toolTip(); web.test.template.examSummary();
}
web.test.template.processOnNextAndPrev = function (action) {
    var questionId = web.test.data.currentQuesId; var qno = $("#q_li_" + questionId); var current_status = "unattempted"; if (qno.hasClass("status-answer")) { current_status = "answer"; }
    if (qno.hasClass("status-skip")) { current_status = "skip"; }
    qno.removeClass("status-" + current_status).removeClass("review"); if (web.test.base.isQuestionAnswered(questionId)) { qno.addClass('status-answer'); } else { qno.addClass('status-skip'); }
    if (web.test.base.isQuestionMarkedRevise(questionId)) { qno.addClass('status-revise'); } else { qno.removeClass('status-revise'); }
    if (web.test.base.isQuestionMarkedRevise(questionId) && web.test.base.isQuestionAnswered(questionId) && action === 'rmRevise') { qno.removeClass('status-revise'); web.test.template.storeOptions(); }
}
web.test.template.showFilteredQuestion = function (actions) { var all_questions = $("li.q"); if (actions == "all") { all_questions.show(); $('.sections_list').find('.q-no-q').addClass('hide').hide(); } else { all_questions.hide(); var questions = $("div.question_no." + actions).parent().parent(); questions.show(); $.each($('.sections_list'), function () { if ($(this).find("li.q:visible").length <= 0) { $(this).find('.q-no-q').find('.q-status-current').html(actions); $(this).find('.q-no-q').removeClass('hide').show(); } else { $(this).find('.q-no-q').addClass('hide').hide(); } }); } }
web.test.template.filterItems = function () { var type = $(this).attr('id'); web.test.template.showFilteredQuestion(type); web.test.template.toggle_questions_filter(); }
web.test.template.toogleDrawer = function () {
    if (web.test.isDrawerOpen) {
        $("div.mask").hide(); $("div.questions_nav").animate({ left: "-301px" }, 500, function () {
            web.test.isDrawerOpen = false; $("div.icon img").attr("src", "/images/mainsite/drawer-closed.png"); if (web.test.isFilterOpen)
                web.test.template.toggle_questions_filter();
        });
    } else { $("div.mask").show(); $("div.questions_nav").animate({ left: "0px" }, 500, function () { web.test.isDrawerOpen = true; $("div.icon img").attr("src", "/images/mainsite/drawer-open.png"); }); }
}
web.test.template.jumpToQuestion = function () {
    var questionId = Number($(this).attr('data-question_id')); if ($(this).hasClass('status-unattempted')) { $(this).removeClass('status-unattempted').addClass('status-skip'); }
    web.test.base.jumpToQuestion(questionId);
}
$(".questionContent select.language").change(function () { var langCode = $('.questionContent select.language option:selected').attr('data-langCode'); web.test.data.setDefaultLanguage(langCode); web.test.base.setCurrentQuestionData(); }); web.test.template.toggle_questions_filter = function () {
    if (web.test.isFilterOpen) { $("div.filter_options").slideUp(500, function () { web.test.isFilterOpen = false; }); return; }
    $("div.filter_options").slideDown(500, function () { web.test.isFilterOpen = true; });
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class bankOnline : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Cookies["Email"] == null)
            {
                Response.Redirect("Index.aspx");
            }
            unm();
            
        }
        if (!SM1.IsInAsyncPostBack)
        {
            var a = Request.QueryString["set"];
            var b = Request.QueryString["section"];
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            if (a == null || b == null)
            {
                Response.Redirect("Onlinetiles.aspx");
            }
            else
            {
                SqlCommand cmdtime = new SqlCommand("select max(time) from onbanktest where name='" + a + "' and [set]='"+ b+"'", con);
                int time = Convert.ToInt32(cmdtime.ExecuteScalar());
                Session["timeout"] = DateTime.Now.AddMinutes(time).ToString();
            }
        }
        profile();
    }
    public void unm()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Name from [User] where Email='" + Request.Cookies["Email"].Value + "'", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                lbluname.Text = dr["Name"].ToString();
            }
            dr.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }

    public void profile()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from [User] where Email='"+Request.Cookies["Email"].Value+"'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            img1.Src = ResolveUrl(dr["profilepath"].ToString());
            img2.Src = ResolveUrl(dr["profilepath"].ToString());
            //img3.Src = ResolveUrl(dr["profilepath"].ToString());
            lblname.Text = dr["Name"].ToString();
            lblEmail.Text = dr["Email"].ToString();
            lblcontact.Text = dr["Mob"].ToString();
        }
        else
        {
            img1.Src = ResolveUrl("onlinetest/NewCandidateImage.jpg");
        }
        dr.Close();
        con.Close();
    }

    public class onbanktest
    {
        public int RowNumber { get; set; }
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Optionval5 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string descr { get; set; }
    }

    protected void timer1_tick(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            int a = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).TotalMinutes);
            int b = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).Seconds);
            if (a <= 0 && b <= 0)
            {
                Session["timeup"] = "Timeout";
                Response.Redirect("timeout.aspx");
            }
            else
            {
                if (0 > DateTime.Compare(DateTime.Now, DateTime.Parse(Session["timeout"].ToString())))
                {
                    Label1.Text = string.Format("Time Left: {0}:{1}", a.ToString(), b.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught: {0}", ex);
        }
    }

    [WebMethod]
    public static List<onbanktest> populatelist(string set, string section)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + section + "') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }

    [WebMethod]
    public static List<onbanktest> populateqs(string set, string section)
    {

        DataTable dt1 = new DataTable();
        List<onbanktest> objdept1 = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#,* FROM onbanktest where name='" + set + "' and [set]='" + section + "') select * from MyTable", con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    try
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            objdept1.Add(new onbanktest
                            {
                                Id = Convert.ToInt32(dt1.Rows[i]["Row#"]),
                                Question = dt1.Rows[i]["Question"].ToString(),
                                Optionval1 = dt1.Rows[i]["Optionval1"].ToString(),
                                Optionval2 = dt1.Rows[i]["Optionval2"].ToString(),
                                Optionval3 = dt1.Rows[i]["Optionval3"].ToString(),
                                Optionval4 = dt1.Rows[i]["Optionval4"].ToString(),
                                Optionval5 = dt1.Rows[i]["Optionval5"].ToString(),
                                path = dt1.Rows[i]["path"].ToString()
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("", ex.ToString());
                    }
                }

                return objdept1;
            }
        }

    }
    [WebMethod]
    public static List<onbanktest> getqsfromlist(string val)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest) select * from MyTable where Row#=" + val, con))
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
            }

            return objdept;
        }

    }
    [WebMethod]
    public static List<onbanktest> getenglishqs(string English, string set, string section)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + section + "' and section='English') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }
    [WebMethod]
    public static List<onbanktest> getintlliqs(string General_Intelligence, string set, string section)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + section + "' and section='Reasoning') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }
    [WebMethod]
    public static List<onbanktest> getreasoningqs(string Reasoning, string set, string section)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + section + "' and section='General Awareness') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }
    [WebMethod]
    public static List<onbanktest> getaptitudeqs(string Quantitative_Aptitude, string set, string section)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + section + "' and section='Quantitative Aptitude') select * from MyTable where Row#=" + 1, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new onbanktest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                            Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                            path = dt.Rows[i]["path"].ToString()
                        });
                    }
                }
                return objdept;
            }
        }
    }
    [WebMethod]
    public static List<onbanktest> Save(string radio, string Email, string set, string decode, string hidden, string counter)
    {
        DataTable dt = new DataTable();
        List<onbanktest> objdept = new List<onbanktest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + decode + "') select top 1 Row# from MyTable order by Id DESC", con);
            int countindex = Convert.ToInt32(cmd1.ExecuteScalar());
            int c = Convert.ToInt32(counter);
            if (c <= countindex)
            {
                using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM onbanktest where name='" + set + "' and [set]='" + decode + "') select * from MyTable where Row#=" + (c+1), con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objdept.Add(new onbanktest
                            {
                                RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                                Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                                Question = dt.Rows[i]["Question"].ToString(),
                                Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                                Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                                Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                                Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                                Optionval5 = dt.Rows[i]["Optionval5"].ToString(),
                                path = dt.Rows[i]["path"].ToString()
                            });
                        }
                    }
                }
                using (SqlCommand cmd2 = new SqlCommand("insert into useronbanktest(selectedval,Email,Id,section,name) values(@t1,@t2,@t3,@t4,@t5)", con))
                {
                    cmd2.Parameters.AddWithValue("@t1", radio);
                    cmd2.Parameters.AddWithValue("@t2", Email);
                    cmd2.Parameters.AddWithValue("@t3", hidden);
                    cmd2.Parameters.AddWithValue("@t4", decode);
                    cmd2.Parameters.AddWithValue("@t5", set);
                    int Result = cmd2.ExecuteNonQuery();
                    if (Result == 1)
                    {
                        string qry3 = "update b set [status]='passed',marksobt=2 FROM onbanktest a inner join useronbanktest b on a.Id = b.Id where a.Correctval=b.selectedval update b set[status] = 'failed',marksobt = -0.5 FROM onbanktest a inner join useronbanktest b on a.Id = b.Id where a.Correctval != b.selectedval";
                        SqlCommand cmd3 = new SqlCommand(qry3, con);
                        int j = cmd3.ExecuteNonQuery();
                    }
                }
            }
            return objdept;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using EducationDataAccess.Data;

public partial class sscOnline : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Visible = true;
        var set = Request.QueryString["set"];
        var section = Request.QueryString["section"];
        if (!IsPostBack)
            {
                //Check for null
                if (Request.Cookies["Email"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                //Methods
                unm();
                profile();
                timer();
            }

        //Ajax Script Manager Call
        //if (con != null && con.State == ConnectionState.Closed)
        //{
        //    con.Open();
        //}
        //SqlCommand cmd = new SqlCommand("select * from QuestionSet" , con);
        //SqlDataReader dr = cmd.ExecuteReader();
        //dr.Read();
        //if (dr.HasRows)
        //{
        //    string str = dr["Question"].ToString();
        //    literal1.Text = System.Net.WebUtility.HtmlDecode(str);
        //}
    }
        

    public void timer()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
                if (!SM1.IsInAsyncPostBack)
                {

                    var a = Request.QueryString["set"];
                    var b = Request.QueryString["section"];
                    if (a == null || b == null)
                    {
                        Response.Redirect("Onlinetiles.aspx");
                    }
                    else
                    {
                    SqlCommand cmd = new SqlCommand("select * from ontimertest where name='" + a + "' and [set]='" + b + "'", con);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.Read())
                    {
                        //Label2.Text = sdr["marks"].ToString();
                    }
                    sdr.Close();
                    SqlCommand cmdtime = new SqlCommand("select max(time) from ontimertest where name='" + a + "' and [set]='" + b + "'", con);
                    var t = cmdtime.ExecuteScalar();
                    int time = Convert.ToInt32(t);
                    Session["timeout"] = DateTime.Now.AddMinutes(time).ToString();
                    con.Close();
                    timer1.Enabled = true;
                    }
                }
            
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }


    }
    //Method Call
    public void profile()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from [User] where Email='"+Request.Cookies["Email"].Value+"'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            img1.Attributes["src"] = ResolveUrl("onlinetest/Student.jpg");
            //img2.Attributes["src"] = ResolveUrl(dr["profilepath"].ToString());
            lblname.Text = dr["Name"].ToString();
            lblEmail.Text = dr["Email"].ToString();
            lblcontact.Text = dr["Mob"].ToString();
        }
        else
        {
            img1.Attributes["src"] = ResolveUrl("onlinetest/NewCandidateImage.jpg");
        }
        dr.Close();
        con.Close();
    }

    //Method Call
    public void unm()
    {
        
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Name from [User] where Email='" + Request.Cookies["Email"].Value + "'", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                lbluname.Text = dr["Name"].ToString();
            }
            dr.Close();
            con.Close();
        
    }

    //DataSet Class
    public class ontimertest
    {
        public int RowNumber { get; set; }
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string desc { get; set; }
    }

    //For Ajax Time Call
    protected void timer1_tick(object sender, EventArgs e)
    {  
        try
        {

            int a = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).TotalMinutes);
            int b = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).Seconds);
           // Label1.Text = "timer";
            if (a <= 0 && b <= 0)
            {
                Response.Redirect("solution.aspx?set"+a+"&section"+b+"");

            }
            else
            {
                if (0 > DateTime.Compare(DateTime.Now, DateTime.Parse(Session["timeout"].ToString())))
                {

                    Label1.Text = string.Format("Time Left: {0}:{1}", a.ToString(), b.ToString());

                }
            }

            

        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught: {0}", ex);
        }
    }

    [WebMethod]
    public static List<ontimertest> GetQuestionsBySections(string section1, string set, string section)
    {
        DataTable dt = new DataTable();
        List<ontimertest> objdept = new List<ontimertest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            string query = "WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + section + "') select * from MyTable where Row#=" + 1;
            if (!string.IsNullOrEmpty(section1))
            {
                query = "WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + section + "' and section='" + section1 + "') select * from MyTable where Row#=" + 1;
            }
             using (SqlCommand cmd = new SqlCommand(query, con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new ontimertest
                        {
                            RowNumber = Convert.ToInt32(dt.Rows[i]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Question = dt.Rows[i]["Question"].ToString(),
                            Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[i]["Optionval4"].ToString()
                        });
                    }
                }
                con.Close();
                return objdept;

            }

        }
    }

    ////For English Questions
    //[WebMethod]
    //public static List<ontimertest> getenglishqs(string English, string set, string section)
    //{
    //    DataTable dt = new DataTable();
    //    List<ontimertest> objdept = new List<ontimertest>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + section + "' and section='English') select * from MyTable where Row#=" + 1, con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 1; i < dt.Rows.Count; i++)
    //                {
    //                    objdept.Add(new ontimertest
    //                    {
    //                        RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
    //                        Id = Convert.ToInt32(dt.Rows[i]["Id"]),
    //                        Question = dt.Rows[i]["Question"].ToString(),
    //                        Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
    //                        Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
    //                        Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
    //                        Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
    //                        path = dt.Rows[i]["path"].ToString()
    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept;

    //        }

    //    }
    //}

    ////For IQ's Level 
    //[WebMethod]
    //public static List<ontimertest> getintlliqs(string General_Intelligence, string set, string section)
    //{
    //    DataTable dt = new DataTable();
    //    List<ontimertest> objdept = new List<ontimertest>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + section + "') select * from MyTable where Row#=" + 1, con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    objdept.Add(new ontimertest
    //                    {
    //                        RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
    //                        Id = Convert.ToInt32(dt.Rows[i]["Id"]),
    //                        Question = dt.Rows[i]["Question"].ToString(),
    //                        Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
    //                        Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
    //                        Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
    //                        Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
    //                        path = dt.Rows[i]["path"].ToString()
    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept;
    //        }
    //    }
    //}

    ////For Reasoning Questions
    //[WebMethod]
    //public static List<ontimertest> getreasoningqs(string Reasoning, string set, string section)
    //{
    //    DataTable dt = new DataTable();
    //    List<ontimertest> objdept = new List<ontimertest>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + section + "' and section='General Studies') select * from MyTable where Row#=" + 1, con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    objdept.Add(new ontimertest
    //                    {
    //                        RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
    //                        Id = Convert.ToInt32(dt.Rows[i]["Id"]),
    //                        Question = dt.Rows[i]["Question"].ToString(),
    //                        Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
    //                        Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
    //                        Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
    //                        Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
    //                        path = dt.Rows[i]["path"].ToString()
    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept;
    //        }
    //    }
    //}

    ////For Aptitude Questions
    //[WebMethod]
    //public static List<ontimertest> getaptitudeqs(string Quantitative_Aptitude, string set, string section)
    //{
    //    DataTable dt = new DataTable();
    //    List<ontimertest> objdept = new List<ontimertest>();
    //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='"+set+"' and [set]='"+section+"' and section='Quantitative Aptitude') select * from MyTable where Row#="+ 1, con))
    //        {
    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    objdept.Add(new ontimertest
    //                    {
    //                        RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
    //                        Id = Convert.ToInt32(dt.Rows[i]["Id"]),
    //                        Question = dt.Rows[i]["Question"].ToString(),
    //                        Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
    //                        Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
    //                        Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
    //                        Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
    //                        path = dt.Rows[i]["path"].ToString()
    //                    });
    //                }
    //            }
    //            con.Close();
    //            return objdept;
    //        }
    //    }
    //}

    //Populate Questions


    [WebMethod]
    public static List<ontimertest> populateqs(string set, string section)
    {
        DataTable dt1 = new DataTable();
        List<ontimertest> objdept1 = new List<ontimertest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#,* FROM ontimertest where name='" + set + "' and [set]='" + section + "') select * from MyTable", con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        objdept1.Add(new ontimertest
                        {
                            RowNumber = Convert.ToInt32(dt1.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt1.Rows[i]["Row#"]),
                            Question = dt1.Rows[i]["Question"].ToString(),
                            Optionval1 = dt1.Rows[i]["Optionval1"].ToString(),
                            Optionval2 = dt1.Rows[i]["Optionval2"].ToString(),
                            Optionval3 = dt1.Rows[i]["Optionval3"].ToString(),
                            Optionval4 = dt1.Rows[i]["Optionval4"].ToString(),
                        });
                    }
                }
                con.Close();
                return objdept1;
            }
        }
    }

    //Get Questions

    [WebMethod]
    public static ontimertest getqsfromlist(string val, string set, string section)
    {
        DataTable dt = new DataTable();
        ontimertest objdept = new ontimertest();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT ROW_NUMBER() OVER(ORDER BY Id) AS Row#, *FROM ontimertest where name='" + set + "' and [set]='" + section + "') select * from MyTable where Row#=" + val, con))
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    objdept = new ontimertest
                        {
                            RowNumber= Convert.ToInt32(dt.Rows[0]["Row#"]),
                            Id = Convert.ToInt32(dt.Rows[0]["Id"]),
                            Question = dt.Rows[0]["Question"].ToString(),
                            Optionval1 = dt.Rows[0]["Optionval1"].ToString(),
                            Optionval2 = dt.Rows[0]["Optionval2"].ToString(),
                            Optionval3 = dt.Rows[0]["Optionval3"].ToString(),
                            Optionval4 = dt.Rows[0]["Optionval4"].ToString(),
                            path = dt.Rows[0]["path"].ToString()
                        };
                    
                }
            }
            con.Close();
            return objdept;
        }
    }
    //Save Question
    [WebMethod]
    public static List<ontimertest> Save(string radio, string Email, string set, string decode, string hidden, string counter)
    {
        DataTable dt = new DataTable();
        List<ontimertest> objdept = new List<ontimertest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + decode + "') select top 1 Row# from MyTable order by Id DESC", con);
            int countindex = Convert.ToInt32(cmd1.ExecuteScalar());
            int c = Convert.ToInt32(counter);
            if (c <= countindex)
            {
                using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + decode + "') select * from MyTable where Row#=" + (c+1), con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0 ; i < dt.Rows.Count; i++)
                        {
                            objdept.Add(new ontimertest
                            {
                                RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                                Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                                Question = dt.Rows[i]["Question"].ToString(),
                                Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                                Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                                Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                                Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                                path = dt.Rows[i]["path"].ToString()
                            });
                            
                        }
                    }
                  
                }

                using (SqlCommand cmd2 = new SqlCommand("insert into onusertest(selectedval,Email,Id,[set],name) values(@t1,@t2,@t3,@t4,@t5)", con))
                {
                    cmd2.Parameters.AddWithValue("@t1", radio);
                    cmd2.Parameters.AddWithValue("@t2", Email);
                    cmd2.Parameters.AddWithValue("@t3", hidden);
                    cmd2.Parameters.AddWithValue("@t4", decode);
                    cmd2.Parameters.AddWithValue("@t5", set);
                    int Result = cmd2.ExecuteNonQuery();
                    if (Result == 1)
                    {
                        string qry3 = "update b set [status]='passed',marksobt=2 FROM ontimertest a inner join onusertest b on a.Id = b.Id where a.Correctval=b.selectedval update b set[status] = 'failed',marksobt = -0.5 FROM ontimertest a inner join onusertest b on a.Id = b.Id where a.Correctval != b.selectedval";
                        SqlCommand cmd3 = new SqlCommand(qry3, con);
                        int j = cmd3.ExecuteNonQuery();
                    }
                      con.Close();
                }      
            }
         }
        return objdept;
    }


    //Skip Call
    [WebMethod]
    public static List<ontimertest> Skip(string set, string decode, string hidden, string counter)
    {
        DataTable dt = new DataTable();
        List<ontimertest> objdept = new List<ontimertest>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("WITH MyTable AS(SELECT  ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + decode + "') select top 1 Row# from MyTable order by Id DESC", con);
            int countindex = Convert.ToInt32(cmd1.ExecuteScalar());
            int c = Convert.ToInt32(counter);
            if (c <= countindex)
            {
                using (SqlCommand cmd = new SqlCommand("WITH MyTable AS(SELECT ROW_NUMBER() OVER(ORDER BY Id) AS Row#, * FROM ontimertest where name='" + set + "' and [set]='" + decode + "') select * from MyTable where Row#=" + counter, con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 1; i < dt.Rows.Count; i++)
                        {
                            objdept.Add(new ontimertest
                            {
                                RowNumber = Convert.ToInt32(dt.Rows[0]["Row#"]),
                                Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                                Question = dt.Rows[i]["Question"].ToString(),
                                Optionval1 = dt.Rows[i]["Optionval1"].ToString(),
                                Optionval2 = dt.Rows[i]["Optionval2"].ToString(),
                                Optionval3 = dt.Rows[i]["Optionval3"].ToString(),
                                Optionval4 = dt.Rows[i]["Optionval4"].ToString(),
                                path = dt.Rows[i]["path"].ToString()
                            });
                        }
                    }

                }
            }
            con.Close();
            return objdept;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        var set = Request.QueryString["set"];
        var decode = Request.QueryString["section"];
        Response.Redirect("solution.aspx?ssconline=" + set + "&section=" + decode);
    }
}

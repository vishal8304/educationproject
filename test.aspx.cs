﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services;

public partial class test : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
            if (!IsPostBack)
            {
                bindinstruct();
                bindgrid();
            }
    }

    public void bindgrid()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select name,max(marks) as totalmarks,max(time) as totaltime, count(Ques) as count, dbo.[Getsscquizmarks](name, @email) as TotalMarksObt from test Where Convert(Date,CreateDate) = Convert(Date,GetDate()) GROUP BY name", con);
            cmd.Parameters.Add(new SqlParameter("@email", Request.Cookies["Email"].Value));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            grid.DataSource = dt;
            grid.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }
   
    public void bindinstruct()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select top 1 * from quizinstruction where section='SSC' order by Id desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Content"].ToString();
            lblinstruct.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        dr.Close();
        con.Close();
    }

    public class test1
    {
        public int Id { get; set; }
        public string Setno { get; set; }
        public string Ques { get; set; }
        public string descr { get; set; }
        public string qspath { get; set; }
        public string path { get; set; }
        public string solvedio { get; set; }
    }
    [WebMethod]
    public static List<test1> btnsscquiz()
    {
        DataTable dt = new DataTable();
        List<test1> objdept = new List<test1>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select * from test", con))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objdept.Add(new test1
                        {
                            Id = Convert.ToInt16(dt.Rows[i]["Id"].ToString()),
                            path = dt.Rows[i]["path"].ToString(),
                            Ques = dt.Rows[i]["Ques"].ToString(),
                            qspath = dt.Rows[i]["qspath"].ToString(),
                            solvedio = dt.Rows[i]["solvedio"].ToString(),
                            descr = dt.Rows[i]["descr"].ToString()
                        });
                    }
                }
                con.Close();
                return objdept;
            }
        }
    }

    protected void grid_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {

            }
            else
            {
                HyperLink Hlink = (HyperLink)e.Row.FindControl("Hlink");
                Label lbl = (Label)e.Row.FindControl("lblobt");
                Label lblname = (Label)e.Row.FindControl("lblname");
                if (lbl.Text == "")
                {
                    var plainTextBytes = HttpUtility.HtmlEncode(lblname.Text);
                    Hlink.NavigateUrl = "quiz.aspx?name=" + plainTextBytes + "";
                    Hlink.Text = "Start Quiz";
                }
                else
                {
                    Hlink.NavigateUrl = "solution.aspx?sscquiz=" + lblname.Text + "";
                    Hlink.Text = "View Solution";
                }
            }
        }
    }
}

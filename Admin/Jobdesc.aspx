﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Jobdesc.aspx.cs" Inherits="Admin_Jobdesc" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
               <strong>Update Description for Current Opening</strong>
            </div><div class="col-md-7">
                <asp:Label ID="lblconfirm" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                  </div>
        </div><br />
       
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-11">
                 <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
                 <%--<asp:TextBox runat="server" ID="txtdesc" TextMode="MultiLine" Width="600px" Height="200px"></asp:TextBox>--%>
            </div>
        </div>
       <br />
        <div class="row" style="text-align:center">
            <div class="col-md-12">
                <asp:Button ID="btnupload" runat="server" Text="Upload" OnClick="btnupload_Click" />
            </div>
        </div><br />
       <br />
      
</div>
     <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
</asp:Content>


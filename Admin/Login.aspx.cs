﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;

public partial class Admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            string qry = "select * from Admin where Username=@t1 and Password=@t2";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.Parameters.AddWithValue("@t1", txtUsrnm.Text);
            cmd.Parameters.AddWithValue("@t2", txtPwd.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                HttpCookie mycookie = new HttpCookie("Login");
                mycookie["unm"] = "admin";
                //mycookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(mycookie);
                Server.Transfer("Adminindex.aspx");
            }
            else
            {
                lblConfirm.Visible = true;
                lblConfirm.Text = "Username and Password mismatch";
            }
            con.Close();
        }
        catch(ThreadAbortException ex1)
        {

        }
        catch (Exception ex)
        {
            Console.WriteLine("Error occurred", ex);
        }

    }
}
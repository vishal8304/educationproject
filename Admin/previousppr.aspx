﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="previousppr.aspx.cs" Inherits="Admin_previousppr" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        #div{
            border-bottom-color: black;
    border-style: groove;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" style="text-align:center">
               <h3> Previous Year Papers</h3>
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-2">
                Add section:
            </div>
            <div class="col-md-3">
                 <input type="text" name="addsection" />
            </div>
            <div class="col-md-7">
                <asp:Button ID="btnsavesection" runat="server" OnClick="btnsavesection_Click" Text="Save" />
            </div>
        </div><br />
        <div class="row" id="div">
            <div class="col-md-1">
                Section:
            </div>
            <div class="col-md-2">
               <asp:DropDownList ID="ddlsection" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-2">
                Add Exam:
            </div>
            <div class="col-md-3">
                <input type="text" name="ddlexam" />
            </div>
            <div class="col-md-1">
                
            </div>
            <div class="col-md-3">
                <asp:Label ID="lblsaved" Visible="false" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div><br />
       
        <div class="row">
            <div class="col-md-12">
                <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> <br />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12" style="text-align:center">
                <asp:Button ID="btnsave" ClientIDMode="Static" runat="server" Text="Save" OnClick="btnsave_Click" />
            </div>
            
        </div><br />
      
        </div>
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
    
    <script>
        $("#btnsave").click(function () {
            var dropdown = $('#ddlsection').find(":selected").text();
            if (dropdown == 'Choose') {
                alert("Please choose section");
                return false;
            }
        });
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editmaths.aspx.cs" Inherits="Admin_Editmaths" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .text{
            width:140px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--<div class="container-fluid">
     <div class="row">
         <div class="col-md-12">
     <h3 style="background-color: #FFFFFF; font-weight: bold; font-style: italic; font-variant: small-caps; text-transform: uppercase; color: #000000; width: 1216px; times: ;, ">Online Test </h3>  
    <div>  
         Insert No. of Row Record   
        <asp:TextBox ID="txtAddNoOfRecord" runat="server" Width="30px" style="margin-left: 5px"></asp:TextBox>  
           
        <asp:Button ID="btnAddRow" runat="server" Text="Add Rows" OnClick="btnAddRow_Click"  />  
        <%--<asp:DropDownList ID="ddltime" runat="server" ClientIDMode="Static"></asp:DropDownList>--%>
        <%--<br />  
        <br />  
    </div>--%>  
    <%--<asp:GridView ID="GridAddEmp" runat="server" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
        <Columns>  
            <asp:TemplateField HeaderText="No">  
                <ItemTemplate>  
                    <%#Container.DataItemIndex +1 %>  
                </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="Question">  
                <ItemTemplate>  
                    <asp:TextBox ID="txtqs" ClientIDMode="Static" runat="server"></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="Option1">  
                <ItemTemplate>  
                    <asp:TextBox ID="opt1" ClientIDMode="Static" runat="server" Width="120px"></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="Option2">  
                <ItemTemplate>  
                    <asp:TextBox ID="opt2" ClientIDMode="Static" runat="server" Width="120px"></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
             <asp:TemplateField HeaderText="Option3">  
                <ItemTemplate>  
                    <asp:TextBox ID="opt3" ClientIDMode="Static" runat="server" Width="120px"></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
             <asp:TemplateField HeaderText="Option4">  
                <ItemTemplate>  
                    <asp:TextBox ID="opt4" ClientIDMode="Static" runat="server" Width="120px"></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
             <asp:TemplateField HeaderText="Correct">  
                <ItemTemplate>  
                    <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                    </asp:DropDownList>
                    <%--<asp:TextBox ID="opt2" runat="server"></asp:TextBox>--%>  
               <%-- </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="File">  
                <ItemTemplate>  
                    <asp:FileUpload ID="fileupload1" runat="server" ClientIDMode="Static" />
                </ItemTemplate>   
            </asp:TemplateField>
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
    </asp:GridView>--%> 
   <%-- <div style="padding:10px 0px;">  
        <asp:Panel ID="PanelData" runat="server" Visible="false">  
            <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnSave_Click" /><br />  
             <asp:Label ID="lblMsg" ClientIDMode="Static" runat="server" ></asp:Label>  
        </asp:Panel>  
    </div>  </div>
    </div>
    <div>
        <b>Database Records</b>  
        <div>  
            <asp:GridView ID="GridEmpData" runat="server" AutoGenerateColumns="False" CellPadding="3" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" DataKeyNames="Id" AllowPaging="True" OnPageIndexChanging="GridEmpData_PageIndexChanging" PageSize="5">  
                
                <Columns>
                    <asp:BoundField DataField="Question" HeaderText="Question" />
                    <asp:BoundField DataField="Optionval1" HeaderText="A" >
                    <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval2" HeaderText="B" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval3" HeaderText="C" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval4" HeaderText="D" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:ImageField DataImageUrlField="path" HeaderText="Image">
                        <ControlStyle Height="120px" Width="120px" />
                        <ItemStyle Height="28px" Width="28px" />
                    </asp:ImageField>
                </Columns>
                
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />  
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />  
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />  
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />  
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />  
                <SortedAscendingCellStyle BackColor="#FFF1D4" />  
                <SortedAscendingHeaderStyle BackColor="#B95C30" />  
                <SortedDescendingCellStyle BackColor="#F1E5CE" />  
                <SortedDescendingHeaderStyle BackColor="#93451F" />  
            </asp:GridView>  
        </div>  
    </div>    
   --%>
   <%--  </div>--%>
</asp:Content>


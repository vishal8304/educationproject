﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Testpaper : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddldrop();
            BindTime();
        }
    }

    private void BindTime()
    {
        List<string> minutes = new List<string>();

        for (int i = 0; i <= 300; i++)
        {
            minutes.Add(i.ToString("00"));
        }

        ddltime.DataSource = minutes;
        ddltime.DataBind();
    }

    public void ddldrop()
    {
        try{ 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
          using (SqlCommand cmd = new SqlCommand("SELECT distinct Chapter FROM Testpaper where Sub='"+ subject.Text +"'",con))
            {
                cmd.CommandType = CommandType.Text;
                ddl.DataSource = cmd.ExecuteReader();
            ddl.DataTextField = "Chapter";
            ddl.DataValueField = "Chapter";
            ddl.DataBind();
                con.Close();
            }

        ddl.Items.Insert(0, new ListItem("--Select--", "0"));
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string chk = Request.Form["txtchptr"];
        if (chk == "" && ddl.SelectedIndex==0)
        {
                lblSubmit.Visible = true;
                lblSubmit.Text = "Plz provide chapter name";
        }
        else if(chk=="" && ddl.SelectedIndex!=0)
        { 
        string qs = HttpUtility.HtmlEncode(CKEditor1.Text);
        string opt1 = HttpUtility.HtmlEncode(CKEditorControl1.Text);
        string opt2 = HttpUtility.HtmlEncode(CKEditorControl2.Text);
        string opt3 = HttpUtility.HtmlEncode(CKEditorControl3.Text);
        string opt4 = HttpUtility.HtmlEncode(CKEditorControl4.Text);
        string opt5 = HttpUtility.HtmlEncode(CKEditorControl5.Text);
        string sol = HttpUtility.HtmlEncode(CKEditorControl6.Text);
        SqlCommand cmd = new SqlCommand("insert into testpaper(qs,opt1,opt2,opt3,opt4,opt5,corval,solution,Sub,Chapter,[time],marks) values(@qs,@opt1,@opt2,@opt3,@opt4,@opt5,@corval,@solution,@sub,@chapter,@time,@marks)", con);
        cmd.Parameters.AddWithValue("@qs",qs);
        cmd.Parameters.AddWithValue("@opt1", opt1);
        cmd.Parameters.AddWithValue("@opt2", opt2);
        cmd.Parameters.AddWithValue("@opt3", opt3);
        cmd.Parameters.AddWithValue("@opt4", opt4);
        cmd.Parameters.AddWithValue("@opt5", opt5);
        cmd.Parameters.AddWithValue("@corval", Request.Form["corval"]);
        cmd.Parameters.AddWithValue("@solution", sol);
        cmd.Parameters.AddWithValue("@sub", subject.Text);
        cmd.Parameters.AddWithValue("@chapter", ddl.SelectedItem.Value);
        cmd.Parameters.AddWithValue("@time", ddltime.Text);
        cmd.Parameters.AddWithValue("@marks", txtMarks.Text);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblSubmit.Visible = true;
            lblSubmit.Text = "Submitted Successfully";
                    ddldrop();
                }
            }
            else if (chk != "" && ddl.SelectedIndex == 0)
            {
                string qs = HttpUtility.HtmlEncode(CKEditor1.Text);
                string opt1 = HttpUtility.HtmlEncode(CKEditorControl1.Text);
                string opt2 = HttpUtility.HtmlEncode(CKEditorControl2.Text);
                string opt3 = HttpUtility.HtmlEncode(CKEditorControl3.Text);
                string opt4 = HttpUtility.HtmlEncode(CKEditorControl4.Text);
                string opt5 = HttpUtility.HtmlEncode(CKEditorControl5.Text);
                string sol = HttpUtility.HtmlEncode(CKEditorControl6.Text);
                SqlCommand cmd = new SqlCommand("insert into testpaper(qs,opt1,opt2,opt3,opt4,opt5,corval,solution,Sub,Chapter,[time],marks) values(@qs,@opt1,@opt2,@opt3,@opt4,@opt5,@corval,@solution,@sub,@chapter,@time,@marks)", con);
                cmd.Parameters.AddWithValue("@qs", qs);
                cmd.Parameters.AddWithValue("@opt1", opt1);
                cmd.Parameters.AddWithValue("@opt2", opt2);
                cmd.Parameters.AddWithValue("@opt3", opt3);
                cmd.Parameters.AddWithValue("@opt4", opt4);
                cmd.Parameters.AddWithValue("@opt5", opt5);
                cmd.Parameters.AddWithValue("@corval", Request.Form["corval"]);
                cmd.Parameters.AddWithValue("@solution", sol);
                cmd.Parameters.AddWithValue("@sub", subject.Text);
                cmd.Parameters.AddWithValue("@chapter", chk);
                cmd.Parameters.AddWithValue("@time", ddltime.Text);
                cmd.Parameters.AddWithValue("@marks", txtMarks.Text);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblSubmit.Visible = true;
                    lblSubmit.Text = "Submitted Successfully";
                    ddldrop();
                }
            }
            con.Close();
        
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        try { 
        var sub = Request.Form["subject"];
        if(subject.Text == "--Select--")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Plz select subject to Add Question')", true);
        }
        ddl.Visible = true;
        ddldrop();
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
}
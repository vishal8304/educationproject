﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class Admin_Addjob : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddrop();
        }
    }
    public void binddrop()
    {
        try { 
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select tile from Jobs where tile is not null", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddltile.DataSource = dt;
                ddltile.DataTextField = "tile";
                ddltile.DataValueField = "tile";
                ddltile.DataBind();
                ddltile.Items.Insert(0, "--Select--");
            }
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }

    public void Update()
    {
       try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select Top 1 * from Jobs where tile='"+ddltile.Text+"'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Content"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
            dr.Close();
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
       
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        if (txttiles.Text!="")
        {
                string str = HttpUtility.HtmlEncode(CKEditor1.Text);
                string qry = "insert into Jobs values(@t1,@tiles)";
                SqlCommand cmd = new SqlCommand(qry, con);
                cmd.Parameters.AddWithValue("@t1", str);
                cmd.Parameters.AddWithValue("@tiles", txttiles.Text);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblconfirm.Visible = true;
                    lblconfirm.Text = "Inserted Successfully";
                }
            }
            else if(ddltile.SelectedIndex>1)
            {
                string str = HttpUtility.HtmlEncode(CKEditor1.Text);
                string qry = "update Jobs set [Content]=@t1 where tile=@tiles";
                SqlCommand cmd = new SqlCommand(qry, con);
                cmd.Parameters.AddWithValue("@t1", str);
                cmd.Parameters.AddWithValue("@tiles", ddltile.Text);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblconfirm.Visible = true;
                    lblconfirm.Text = "Updated Successfully";
                }
            }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        Update();
        txttiles.Text = "";
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        if (ddltile.SelectedIndex > 0)
        {
            string str = HttpUtility.HtmlEncode(CKEditor1.Text);
            string qry = "delete from Jobs where tile='"+ddltile.Text+"'";
            SqlCommand cmd = new SqlCommand(qry, con);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblconfirm.Visible = true;
                lblconfirm.Text = "Deleted successfully Successfully";
            }
        }
    }
}
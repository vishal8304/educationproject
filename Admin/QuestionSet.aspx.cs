﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuestionSet : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblconfirm.Visible = false;
    }


    protected void btnupload_Click(object sender, EventArgs e)
    {
        if(con != null && con.State == ConnectionState.Closed)
        {

            con.Open();
        }
        string str = HttpUtility.HtmlEncode(CKEditor1.Text);
        string qry = "insert into QuestionSet values(@t1)";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.Parameters.AddWithValue("@t1", str);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblconfirm.Visible = true;
            lblconfirm.Text = "Inserted Successfully";
        }
    }
}
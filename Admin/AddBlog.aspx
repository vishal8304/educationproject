﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="AddBlog.aspx.cs" Inherits="Admin_AddBlog" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <a href="Blog.aspx" class="btn btn-primary">View Blog</a>
    <div id="main">
        <h2 style="text-align:center">Add Blog Content</h2>
        <div class="row" style="margin-top:1%">
           <div class="col-md-3">Add Image :</div>
            <div class="col-md-5">
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
            </div>
        </div>
              <div class="row" style="margin-top:1%">
           <div class="col-md-3">Add Url :</div>
            <div class="col-md-5">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="margin-top:1%">
           <div class="col-md-3">Add Heading Text :</div>
            <div class="col-md-5">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
         <div class="row" style="margin-top:1%" >
           <div class="col-md-3">Add Short Description:</div>
            <div class="col-md-5">
                <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"  TextMode="MultiLine" Columns="20" Rows="10"></asp:TextBox>
            </div>
        </div>
          <div class="row" style="margin-top:1%">
              Add Description:
        <div class="col-md-12">
            <CKEditor:CKEditorControl ID="CKEditor1" BasePath="/ckeditor/" runat="server">
   </CKEditor:CKEditorControl>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12" style="margin-top:1%">
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click"/>
        </div>
    </div>
    </div>
</asp:Content>


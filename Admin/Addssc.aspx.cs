﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Addssc : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        
    }
    
    

    protected void btnsave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        string section = Request.Form["choose"];
        string Topic = Request.Form["Topic"];
        SqlCommand cmd = new SqlCommand("insert into SSC(section,Chapter,content,Topic) values(@t1,@t2,@t3,@t4)", con);
        cmd.Parameters.AddWithValue("@t1", section);
        cmd.Parameters.AddWithValue("@t2", Request.Form["Chapters"].ToString());
        cmd.Parameters.AddWithValue("@t3", content);
        cmd.Parameters.AddWithValue("@t4", Topic);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            Label2.Visible = true;
            Label2.Text = "Saved Successfully";
        }
    }
}
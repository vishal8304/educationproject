﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editstudybank.aspx.cs" Inherits="Admin_Editstudybank" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
            <div class="col-md-1">
                Section:
            </div>
            <div class="col-md-3">
                <asp:DropDownList ID="ddlsection" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlsection_SelectedIndexChanged">
                    <asp:ListItem>Choose</asp:ListItem>
                    <asp:ListItem>Quantitative Aptitude</asp:ListItem>
                    <asp:ListItem>English</asp:ListItem>
                    <asp:ListItem>Reasoning</asp:ListItem>
                    <asp:ListItem>General Awareness</asp:ListItem>
                    <asp:ListItem>Computer</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                Chapter:
            </div>
            <div class="col-md-2">
          <asp:DropDownList ID="ddlchptr" ClientIDMode="Static" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlchptr_SelectedIndexChanged">
              <asp:ListItem>--Select--</asp:ListItem>
          </asp:DropDownList>
            </div>
        <div class="col-md-1">
            Topic:
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddltopic" ClientIDMode="Static" runat="server">
              <asp:ListItem>--Select--</asp:ListItem>
          </asp:DropDownList>
        </div>
            <div class="col-md-1">
              <asp:Button ID="btnEdit" ClientIDMode="Static" Text="Edit" runat="server" OnClick="btnEdit_Click" />
            </div>
         <div class="col-md-1">
              <asp:Button ID="btndel" ClientIDMode="Static" Text="Delete" runat="server" OnClick="btndel_Click" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
                <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-5" style="text-align:right">
                <asp:Button ID="btnsave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnsave_Click"/>
            </div>
            <div class="col-md-7">
                <asp:Label ID="lblsaved" runat="server" Visible="false" ForeColor="DarkBlue"></asp:Label>
            </div>
        </div>
     <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
     <script>
         $("#btnEdit").click(function () {
             var dropdown = $('#ddlchptr').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please choose Chapter");
                return false;
            }
        });
         </script>
    <script>
        $("#btndel").click(function () {
             var dropdown = $('#ddlchptr').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please choose Chapter");
                return false;
            }
        });
         </script>
</asp:Content>


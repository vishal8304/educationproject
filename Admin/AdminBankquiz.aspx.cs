﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;

public partial class Admin_AdminBankquiz : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            connect();
        }
    }
    public void connect()
    {
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from Bankquiz order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridView1.DataSource = grid;
        GridView1.DataBind();
    }

    private void FillEmpData()
    {
        using (SqlCommand cmd = new SqlCommand("sp_FillData", con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            if (con.State != System.Data.ConnectionState.Open)
            {
                con.Open();
            }
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        connect();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        string qry = "alter table onlinetest nocheck constraint all delete from onlinetest where Id = @t1 delete from Bankquiz where Id=@t1 alter table onlinetest check constraint all";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        cmd1.Parameters.AddWithValue("@t1", eno);
        int i = cmd1.ExecuteNonQuery();
        connect();
        //}catch(Exception ex)
        //{
        //    Console.Write("", ex);
        //}
    }
}
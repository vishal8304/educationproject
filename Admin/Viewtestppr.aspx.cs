﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Viewtestppr : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            connect();
        }
    }

    public void connect()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from testpaper order by Id desc", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        gvEmployee.DataSource = dt;
        gvEmployee.DataBind();
        con.Close();
        }catch(Exception ex)
        {
            Console.WriteLine("",ex);
        }
    }

    protected void btnfetch_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        if(ddlselect.Text== "SSC Quiz")
        {
            foreach (GridViewRow row in this.gvEmployee.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (row.Cells[1].FindControl("chkSelect") as CheckBox);
                    if (chk.Checked)
                    {
                        Label lblqs = (Label)row.FindControl("lblqs");
                    Label lblopt1 = (Label)row.FindControl("lblopt1");
                    Label lblopt2 = (Label)row.FindControl("lblopt2");
                    Label lblopt3 = (Label)row.FindControl("lblopt3");
                    Label lblopt4 = (Label)row.FindControl("lblopt4");
                    Label lblopt5 = (Label)row.FindControl("lblopt5");
                    Label lblcorval = (Label)row.FindControl("lblcorval");
                    Label lblsol = (Label)row.FindControl("lblsol");
                    Label lblSub = (Label)row.FindControl("lblSub");
                    Label lblchptr = (Label)row.FindControl("lblchptr");
                   
                        SqlCommand cmd = new SqlCommand("INSERT INTO test(Ques,val1,val2,val3,val4,Correctoption,descr,section,name) VALUES(@Question,@Optionval1,@Optionval2,@Optionval3,@Optionval4,@Correctval,@descr,@section,@name)", con);
                        cmd.Parameters.AddWithValue("@Question", System.Web.HttpUtility.HtmlEncode(lblqs.Text));
                        cmd.Parameters.AddWithValue("@Optionval1", System.Web.HttpUtility.HtmlEncode(lblopt1.Text));
                        cmd.Parameters.AddWithValue("@Optionval2", System.Web.HttpUtility.HtmlEncode(lblopt2.Text));
                        cmd.Parameters.AddWithValue("@Optionval3", lblopt3.Text);
                        cmd.Parameters.AddWithValue("@Optionval4", lblopt4.Text);
                        cmd.Parameters.AddWithValue("@Correctval", lblcorval.Text);
                        cmd.Parameters.AddWithValue("@descr", lblsol.Text);
                        cmd.Parameters.AddWithValue("@section", lblSub.Text);
                        cmd.Parameters.AddWithValue("@name", lblchptr.Text);
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        cmd.ExecuteScalar();
                        lblConfirm.Visible = true;
                        lblConfirm.Text = "Data inserted successfully";
                        con.Close();
                    }
                }
            }
        }
        else if(ddlselect.Text=="Bank Quiz")
        {

        }
    }

    protected void gvEmployee_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(gvEmployee.DataKeys[e.RowIndex].Value);
        SqlCommand cmd = new SqlCommand("delete from testpaper where Id="+eno, con);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblConfirm.Visible = true;
            lblConfirm.Text = "Deleted Successfully";
        }
    }
}
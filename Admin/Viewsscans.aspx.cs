﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Viewsscans : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            connect();
        }
    }

    public void connect()
    {
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from mcqquery order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridEmpData.DataSource = grid;
        GridEmpData.DataBind();
    }

    protected void GridEmpData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridEmpData.DataKeys[e.RowIndex].Value);
        SqlCommand cmd = new SqlCommand("alter table Usermcq nocheck constraint all delete from mcqquery where Id = @t1 delete from Usermcq where Id=@t1 alter table Usermcq check constraint all", con);
        cmd.Parameters.AddWithValue("@t1", eno);
        int i = cmd.ExecuteNonQuery();
        connect();
    }

    protected void GridEmpData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridEmpData.PageIndex = e.NewPageIndex;
        connect();
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Addnewtopic.aspx.cs" Inherits="Admin_Addnewtopic" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>New Topic</h3>
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-2">
                Topic Name:
            </div>
            <div class="col-md-10">
                <input type="text" name="topic" required="required" />
            </div>
        </div><br />
        <div class="row" id="div">
            <div class="col-md-12">
                <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-5" style="text-align:right">
                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click"/>
            </div>
            <div class="col-md-7">
                <asp:Label ID="lblsaved" runat="server" Visible="false" ForeColor="DarkBlue"></asp:Label>
            </div>
        </div>
        </div>
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
     <script>
        $("#btnadd").click(function () {
            var dropdown = $('#choose').find(":selected").text();
            if (dropdown == 'Choose') {
                alert("Please choose section");
                return false;
            }
        });
    </script>
</asp:Content>


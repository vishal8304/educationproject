﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using System.Text;

public partial class Admin_Onlinetest : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindTime();
            ddlssc();

        }
    }

    //Filling Drop Down
    public void ddlssc()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct [set] from ssconline where [set] is not null", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddlsscset.DataSource = dt;
            ddlsscset.DataTextField = "set";
            ddlsscset.DataValueField = "set";
            ddlsscset.DataBind();
            ddlsscset.Items.Insert(0, "--Select--");

        }
    }

    public void getName()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct name from ssconline where [set] is not null", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddlsscset.DataSource = dt;
            ddlsscset.DataTextField = "set";
            ddlsscset.DataValueField = "set";
            ddlsscset.DataBind();
            ddlsscset.Items.Insert(0, "--Select--");

        }

    }


    //protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = rdb1.Checked || rdb2.Checked || rdb3.Checked || rdb4.Checked;
    //}

    //Add row click 
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddRowsToGrid();
    }

    //add row method
    private void AddRowsToGrid()
    {
        try
        {
            List<int> Row_Number = new List<int>();
            int rows = 0;
            int.TryParse(txtAddNoOfRecord.Text.Trim(), out rows);

            for (int R = 0; R < rows; R++)
            {
                Row_Number.Add(R);
            }
            GridAddEmp.DataSource = Row_Number;
            GridAddEmp.DataBind();


        }
        catch (Exception ex1)
        {
            Console.Write("", ex1);
        }
    }

    //Time bind in the drop down
    private void BindTime()
    {
        List<string> minutes = new List<string>();

        for (int i = 0; i <= 300; i++)
        {
            minutes.Add(i.ToString("00"));
        }

        ddltime.DataSource = minutes;
        ddltime.DataBind();
    }


    //Click on save it inserts the data into online Instruction table
    protected void btnSave_Click(object sender, EventArgs e)
    {

        SqlConnection con = new SqlConnection(connection);
        if (CKEditor1.Text != "")
        {
            string str = HttpUtility.HtmlEncode(CKEditor1.Text);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insert into onlineinstruct(content,section) values(@t1,@t2)", con);
            cmd.Parameters.AddWithValue("@t1", str);
            cmd.Parameters.AddWithValue("@t2", "SSC");
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblsave.Visible = true;
                lblsave.Text = "Updated Successfully";
            }
        }
        else { }


        foreach (GridViewRow row in this.GridAddEmp.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtqs = (TextBox)row.FindControl("txtqs");
                TextBox txtopt1 = (TextBox)row.FindControl("opt1");
                TextBox txtopt2 = (TextBox)row.FindControl("opt2");
                TextBox txtopt3 = (TextBox)row.FindControl("opt3");
                TextBox txtopt4 = (TextBox)row.FindControl("opt4");
                TextBox description = (TextBox)row.FindControl("txtdesc");
                DropDownList txtcorans = (DropDownList)row.FindControl("ddlopt");
                DropDownList ddlsection = (DropDownList)row.FindControl("ddlsection");
                FileUpload fileupload1 = (FileUpload)row.FindControl("fileupload1");
                FileUpload fileupload2 = (FileUpload)row.FindControl("fileupload2");
                FileUpload fileupload3 = (FileUpload)row.FindControl("filevedio");
                SqlCommand cmd = new SqlCommand("INSERT INTO ontimertest(Question,Optionval1,Optionval2,Optionval3,Optionval4,Correctval,descr,[set],[name],marks,[time]) VALUES(@Question,@Optionval1,@Optionval2,@Optionval3,@Optionval4,@Correctval,@descr,'" + ddlsscset.Text + "','" + ddlname.Text + "','" + txtmarks.Text + "','" + ddltime.Text + "')", con);
                cmd.Parameters.AddWithValue("@Question", txtqs.Text);
                cmd.Parameters.AddWithValue("@Optionval1", txtopt1.Text);
                cmd.Parameters.AddWithValue("@Optionval2", txtopt2.Text);
                cmd.Parameters.AddWithValue("@Optionval3", txtopt3.Text);
                cmd.Parameters.AddWithValue("@Optionval4", txtopt4.Text);
                cmd.Parameters.AddWithValue("@Correctval", txtcorans.Text);
                cmd.Parameters.AddWithValue("@descr", description.Text);
                               if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                cmd.ExecuteScalar();
                lblMsg.Visible = true;
                lblMsg.Text = "Data inserted successfully";
                con.Close();
                //ddlssc();
            }
        }

    }

    //Button Save Set using DropDown
    protected void btnsaveset_Click(object sender, EventArgs e)
    {
        if (txtset.Text != "")
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select [set] from ssconline where [set]='" + txtset.Text + "'", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                lblset.Visible = true;
                lblset.Text = "This set already exists";
            }
            else
            {
                dr.Close();
                SqlCommand cmd1 = new SqlCommand("insert into ssconline([set]) values('" + txtset.Text.Trim() + "')", con);
                int i = cmd1.ExecuteNonQuery();
                if (i == 1)
                {
                    lblset.Visible = true;
                    lblset.Text = "Set Added successfully";
                    txtset.Text = "";
                    ddlssc();
                }
            }
        }
        else
        {
            lblset.Visible = true;
            lblset.Text = "Please add some text first";
        }

    }
}
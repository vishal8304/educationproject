﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_About : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblsave.Visible = false;
            text();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (CKEditor1.Text != "")
            {
                string str = HttpUtility.HtmlEncode(CKEditor1.Text);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand cmd = new SqlCommand("insert into AboutMaster(About) values(@t1)", con);
                cmd.Parameters.AddWithValue("@t1", str);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblsave.Visible = true;
                    lblsave.Text = "Updated Successfully";
                }
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            con.Close();
        }
    }

    public void text()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("select Top 1 * from AboutMaster", con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                string str = dr["About"].ToString();
                CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }
}
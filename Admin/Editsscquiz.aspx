﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editsscquiz.aspx.cs" Inherits="Admin_Editsscquiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <h3>Edit SSC Quiz</h3>
     <div class="row">
           
            <div class="col-md-2">
                Test Name:
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlname" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnfilter" OnClick="btnfilter_Click" runat="server" Text="Filter" style="margin-left: 52%;" />
            </div>
         <div class="col-md-2">
             <asp:Button ID="btndel" runat="server" Text="Delete" OnClick="btndel_Click" />
         </div>
         <div class="col-md-4">
             <asp:Label ID="lblmsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
         </div>
        </div><br />
    <asp:Panel ID="hide" runat="server">
    <asp:GridView ID="GridView1" OnRowEditing="GridView1_RowEditing" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" runat="server" Width="100%" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="Id" ForeColor="Black" GridLines="Vertical" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting">
           <%-- <AlternatingRowStyle BackColor="White" />--%>
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
               
                <asp:TemplateField>
                    <EditItemTemplate>
                        <div class="row">
                            <div class="col-md-12">Question:
                        <asp:TextBox ID="txtqs" runat="server" TextMode="MultiLine" Width="100%" Height="100px" Text='<%#Bind("Ques") %>'></asp:TextBox>
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-9">
                        <asp:Image ID="qsimg" runat="server" ImageUrl='<%#Eval("qspath")%>' Height="100px" Width="100%" />
                            </div>
                            <div class="col-md-3">
                                <asp:FileUpload ID="qsfile" runat="server" />
                            </div><br />
                        </div><br />
                        <div class="row">
                        <div class="col-md-6">
                            Option1:
                            <asp:TextBox ID="txt1" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("val1") %>'></asp:TextBox>
                        </div>
                            <div class="col-md-6">
                                Option2:
                                 <asp:TextBox ID="txt2" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("val2") %>'></asp:TextBox>
                            </div>
                        </div><br />
                         <div class="row">
                        <div class="col-md-6">
                            Option3:
                            <asp:TextBox ID="txt3" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("val3") %>'></asp:TextBox>
                        </div>
                            <div class="col-md-6">
                                Option4:
                                 <asp:TextBox ID="txt4" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("val4") %>'></asp:TextBox>
                            </div>
                        </div><br />
                          <div class="row">
                <div class="col-md-6">
                     <strong>Correct Option: </strong>
                    <asp:textbox ID="txtcorval" runat="server" Text='<%#Bind("Correctoption") %>'></asp:textbox>
                    <%--<asp:BoundField DataField="Correctoption" HeaderText="Correct" />--%>
                </div>
     <div class="col-md-6">
                  <strong>Section: </strong>
                    <asp:textbox ID="txtsection" runat="server" Text='<%#Bind("section") %>'></asp:textbox>
                    <%--<asp:BoundField DataField="Setno" HeaderText="Field" />--%>
                </div>
            </div><br />
                         <div class="row">
                            <div class="col-md-12">
                                <strong>Solution:</strong>
                                 <asp:Textbox ID="txtsol" TextMode="MultiLine" Width="100%" Height="100px" runat="server" Text='<%#Bind("descr") %>'></asp:Textbox>
                            </div>
                        </div><br />
                        <div class="row">
                        <div class="col-md-9">
                              <asp:Image ID="solimg" runat="server" ImageUrl='<%# Eval("path") %>' Height="100px" Width="100%" />  
                        </div>
                        <div class="col-md-3">
                              <asp:FileUpload ID="Fileimage" runat="server" />  
                        </div>
                            </div><br />
                       
                        <div class="row">
                        <div class="col-md-12" style="text-align:right">
                            <asp:Button ID="btnupdate" runat="server" Text="Update" CommandName="Update" />
                             <asp:Button ID="btnEdit" runat="server" Text="Cancel" CommandName="Cancel" />
                        </div>
                            </div>
                    </EditItemTemplate>
                    <ItemTemplate>
            <div class="row">
               
                 <div class="col-md-12">
                    <strong>Question</strong>
                     <strong> <%#Container.DataItemIndex+1 %>:</strong>
                    <asp:label ID="lblqs" runat="server" Text='<%#Eval("Ques") %>'></asp:label>
                    <%--<asp:BoundField DataField="Ques" HeaderText="Question" />--%>
                </div>
                 </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                 <asp:Image ID="imgquiz" runat="server" ImageUrl='<%#Eval("qspath") %>' Height="150px" Width="80%" />
                    <%--  <asp:ImageField DataImageUrlField="path" HeaderText="Image">
                    <ControlStyle Height="120px" Width="120px" />
                        <ItemStyle Height="28px" Width="28px" />
                </asp:ImageField>
           --%>
                            </div>
                        </div><br />
<div class="row">
                <div class="col-md-6">
                    <strong>Option1: </strong>
                    <asp:Label ID="lblopt1" runat="server" Text='<%#Eval("val1") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val1" HeaderText="A" />--%>
                </div>
     <div class="col-md-6">
          <strong>Option2: </strong>
                    <asp:Label ID="lblopt2" runat="server" Text='<%#Eval("val2") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="val2" HeaderText="B" />--%>
                </div>
            </div><br />
                        <div class="row">
                <div class="col-md-6">
                     <strong>Option3: </strong>
                    <asp:Label ID="lblopt3" runat="server" Text='<%#Eval("val3") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val3" HeaderText="C" />--%>
                </div>
     <div class="col-md-6">
          <strong>Option4: </strong>
                    <asp:Label ID="lblopt4" runat="server" Text='<%#Eval("val4") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="val4" HeaderText="D" />--%>
                </div>
            </div><br />
                         <div class="row">
                <div class="col-md-6">
                     <strong>Correct Option: </strong>
                    <asp:Label ID="lblcoropt" runat="server" Text='<%#Eval("Correctoption") %>'></asp:Label>
                    <%--<asp:BoundField DataField="Correctoption" HeaderText="Correct" />--%>
                </div>
     <div class="col-md-6">
                  <strong>Section: </strong>
                    <asp:Label ID="lblset" runat="server" Text='<%#Eval("section") %>'></asp:Label>
                    <%--<asp:BoundField DataField="Setno" HeaderText="Field" />--%>
              
                </div>
            </div><br />
                         <div class="row">
                            <div class="col-md-12">
                                 <strong>Solution</strong>
                    <asp:label ID="lblsol" runat="server" Text='<%#Eval("descr") %>'></asp:label>
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                  <asp:Image ID="imgsol" runat="server" ImageUrl='<%#Eval("path") %>' Height="150px" Width="80%" />
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                 <video width="400" controls>
  <source src="<%#Eval("solvedio") %>" type="video/mp4">
</video>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-1">
                                <asp:LinkButton ID="lblEdit" ForeColor="Brown" runat="server" CommandName="Edit">Edit </asp:LinkButton>
                            </div>
                            <div class="col-md-1">
                                <asp:LinkButton ID="lbldel" ForeColor="Brown" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this entry?');">Delete </asp:LinkButton>
                            </div>
                        </div>
            </ItemTemplate>
                  <HeaderTemplate>
                      Quiz Questions
                  </HeaderTemplate>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                </asp:TemplateField>
                 </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
        </asp:Panel>
</asp:Content>


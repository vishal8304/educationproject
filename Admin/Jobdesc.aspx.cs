﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Admin_Jobdesc : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Update();
        }
    }
    public void Update()
    {

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select Top 1 * from Jobdesc order by Id desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Content"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string str = HttpUtility.HtmlEncode(CKEditor1.Text);
        string qry = "insert into Jobdesc values(@t1)";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.Parameters.AddWithValue("@t1", str);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblconfirm.Visible = true;
            lblconfirm.Text = "Inserted Successfully";
        }

        }catch(Exception ex)
        {
            lblconfirm.Visible = true;
            lblconfirm.Text = ex.ToString();
        }

    }
}
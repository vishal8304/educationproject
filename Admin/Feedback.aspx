﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Feedback.aspx.cs" Inherits="Admin_Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .Grid {background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse; font-family:Calibri; color: #474747;}

.Grid td {

      padding: 2px;

      border: solid 1px #c1c1c1; }

.Grid th  {

      padding : 4px 2px;

      color: #fff;

      background: #363670 url(Images/grid-header.png) repeat-x top;

      border-left: solid 1px #525252;

      font-size: 0.9em; }

.Grid .alt {

      background: #fcfcfc url(Images/grid-alt.png) repeat-x top; }

.Grid .pgr {background: #363670 url(Images/grid-pgr.png) repeat-x top; }

.Grid .pgr table { margin: 3px 0; }

.Grid .pgr td { border-width: 0; padding: 0 6px; border-left: solid 1px #666; font-weight: bold; color: #fff; line-height: 12px; }  

.Grid .pgr a { color: Gray; text-decoration: none; }

.Grid .pgr a:hover { color: #000; text-decoration: none; }

   
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">

     <asp:GridView ID="gvEmployee" runat="server" AutoGenerateColumns="false" Width="600px"

                      AllowPaging="true" PageSize="20" OnPageIndexChanging="gvEmployee_PageIndexChanging"

                      CssClass="Grid"  style="width:100%"                  

                      AlternatingRowStyle-CssClass="alt"

                      PagerStyle-CssClass="pgr" DataKeyNames="feedback" >        

         <Columns>

         <asp:BoundField DataField="Email" HeaderText="Email ID" />

         <asp:BoundField DataField="feedback" HeaderText="Feedback" />

         <asp:BoundField DataField="test" HeaderText="Test Name" />

         </Columns>

        </asp:GridView>
            
        </div>
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class Admin_Dailyvocab : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddRowsToGrid();
    }
    private void AddRowsToGrid()
    {
        List<int> Row_Number = new List<int>();
        int rows = 0;
        int.TryParse(text.Text.Trim(), out rows);

        for (int R = 0; R < rows; R++)
        {
            Row_Number.Add(R);
        }
        GridAddEmp.DataSource = Row_Number;
        GridAddEmp.DataBind();
        if (GridAddEmp.Rows.Count > 0)
        {
            btnsaveqs.Visible = true;
            lblsaveqs.Visible = false;
            GridAddEmp.Visible = true;
        }
        else
        {
            btnsaveqs.Visible = false;
        }
    }
    
    //protected void btnword_Click(object sender, EventArgs e)
    //{
    //    AddwordToGrid();
    //}
    //private void AddwordToGrid()
    //{
    //    List<int> Row_Number = new List<int>();
    //    int rows = 0;
    //    int.TryParse(txtword.Text.Trim(), out rows);

    //    for (int R = 0; R < rows; R++)
    //    {
    //        Row_Number.Add(R);
    //    }
    //    grdaddword.DataSource = Row_Number;
    //    grdaddword.DataBind();
    //    if (grdaddword.Rows.Count > 0)
    //    {
    //        btnsaveword.Visible = true;
    //        lblsaved.Visible = false;
    //        grdaddword.Visible = true;
    //    }
    //    else
    //    {
    //        btnsaveword.Visible = false;
    //    }

    //}

    //protected void btnsaveword_Click(object sender, EventArgs e)
    //{
    //    SqlConnection con = new SqlConnection(connection);
    //    { 
    //    foreach (GridViewRow row in this.grdaddword.Rows)
    //    {
    //        if (row.RowType == DataControlRowType.DataRow)
    //        {
    //            TextBox txtword = (TextBox)row.FindControl("txtwords");
    //            TextBox txtdesc = (TextBox)row.FindControl("desc");
    //            SqlCommand cmd = new SqlCommand("INSERT INTO Vocabwords(word,worddesc,[Date]) VALUES(@word,@worddesc,@date)", con);
    //            cmd.Parameters.AddWithValue("@word", txtword.Text);
    //            cmd.Parameters.AddWithValue("@worddesc", txtdesc.Text);
    //            cmd.Parameters.AddWithValue("@date", System.DateTime.Now.ToString());
    //                if (con.State != ConnectionState.Open)
    //            {
    //                con.Open();
    //            }
    //            cmd.ExecuteScalar();
    //                lblsaved.Visible = true;
    //                grdaddword.Visible = false;

    //            con.Close();
    //            }
    //        }
    //    }
    //}

    protected void btnsaveqs_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        {
            foreach (GridViewRow row in this.GridAddEmp.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    TextBox txtqs = (TextBox)row.FindControl("txtqs");
                    TextBox txt1 = (TextBox)row.FindControl("opt1");
                    TextBox txt2 = (TextBox)row.FindControl("opt2");
                    TextBox txt3 = (TextBox)row.FindControl("opt3");
                    TextBox txt4 = (TextBox)row.FindControl("opt4");
                    TextBox txtsol = (TextBox)row.FindControl("txtsol");
                    DropDownList txtcorans = (DropDownList)row.FindControl("ddlopt");
                    FileUpload qsfileupload = (FileUpload)row.FindControl("qsfileupload");
                    FileUpload solfileupload = (FileUpload)row.FindControl("solfileupload");
                    FileUpload vdfile = (FileUpload)row.FindControl("vdfile");
                    SqlCommand cmd = new SqlCommand("INSERT INTO Vocab(Question,val1,val2,val3,val4,correctval,solution,Dated) VALUES(@question,@val1,@val2,@val3,@val4,@corval,@solution,@Dated)", con);
                    cmd.Parameters.AddWithValue("@question", txtqs.Text);
                    cmd.Parameters.AddWithValue("@val1",txt1.Text);
                    cmd.Parameters.AddWithValue("@val2", txt2.Text);
                    cmd.Parameters.AddWithValue("@val3", txt3.Text);
                    cmd.Parameters.AddWithValue("@val4", txt4.Text);
                    cmd.Parameters.AddWithValue("@corval", txtcorans.Text);
                    cmd.Parameters.AddWithValue("@solution", txtsol.Text);
                    cmd.Parameters.AddWithValue("@Dated", DateTime.Now);
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    cmd.ExecuteScalar();
                    lblsaveqs.Visible = true;
                    GridAddEmp.Visible = false;
                    con.Close();
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("insert into vocabcomprehension values(@t1)", con);
        cmd.Parameters.AddWithValue("@t1", content);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lbldone.Visible = true;
            lbldone.Text = "Comprehension Added Successfully";
        }
    }
}
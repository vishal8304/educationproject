﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editpreviousppr.aspx.cs" Inherits="Admin_Editpreviousppr" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <h3>Edit Previous Year Papers</h3>
        </div>
    </div><br />
    <div class="row" style="background-color:red">
        <div class="col-md-1">
            Section:
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlsection" runat="server"></asp:DropDownList>
        </div>
        <div class="col-md-9">
            <asp:Button ID="btndelsection" runat="server" Text="Delete" OnClick="btndel_Click1" />
        </div>
    </div><br />
      <div class="row">
            <div class="col-md-1">
                Tile:</div>
          <div class="col-md-2"><asp:DropDownList ID="ddlpprs" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click"/>
            </div>
            <div class="col-md-7">
                <asp:Button ID="btndel" runat="server" Text="Delete" OnClick="btndel_Click" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
                <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
            </div>
        </div><br />
    <div class="row">
        <div class="col-md-5" style="text-align:right">
            <asp:button runat="server" ID="btnUpdate" Text="Update" OnClick="btnUpdate_Click" />
        </div>
        <div class="col-md-7">
            <asp:Label ID="lblupdate" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        </div>
    </div><br />
      <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
</asp:Content>


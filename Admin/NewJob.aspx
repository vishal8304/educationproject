﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="NewJob.aspx.cs" Inherits="Admin_NewJob" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 style="text-align:center"> Create Links for Result, Admit Card, New Jobs</h2>
    <div class="row" style="margin:auto; width:80%; margin-top:2%">
    <div class="row" style="margin-bottom: 1% !important">
        <div class="col-md-3">Add Link(in URL form)</div>
<div class="col-md-3">
    <asp:TextBox ID="TextBox1" runat="server" PlaceHolder="Ex: www.xyx.com"></asp:TextBox></div>

    </div>
       <div class="row" style="margin-bottom: 1% !important">
        <div class="col-md-3">Add Text</div>
<div class="col-md-3">
    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></div>

    </div>
        <div class="row" style="margin-bottom: 1% !important">
        <div class="col-md-3">Select Section</div>
<div class="col-md-3">
    <asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem>New Job</asp:ListItem>
        <asp:ListItem>Admit Card</asp:ListItem>
        <asp:ListItem>Result</asp:ListItem>

    </asp:DropDownList>

    </div>
            </div>
    <div class="row">
        <div class="col-md-3"></div>
<div class="col-md-3">
    <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />

    </div>
        </div>
        </div>
</asp:Content>


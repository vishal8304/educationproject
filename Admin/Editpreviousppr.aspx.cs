﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Admin_Editpreviousppr : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlist();
            bindsection();
        }
    }

    public void bindsection()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select DISTINCT section from exam where section is not null", con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            ddlsection.DataSource = ds;
            ddlsection.DataTextField = "section";
            ddlsection.DataValueField = "section";
            ddlsection.DataBind();
            ddlsection.Items.Insert(0, new ListItem("Choose", "Choose"));
            con.Close();
            da.Dispose();
            ds.Dispose();
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
    public void bindlist()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select DISTINCT exam from previousppr where exam is not null", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        ddlpprs.DataSource = ds;
        ddlpprs.DataTextField = "exam";
        ddlpprs.DataValueField = "exam";
        ddlpprs.DataBind();
        ddlpprs.Items.Insert(0, new ListItem("Choose", "Choose"));
        con.Close();
        da.Dispose();
        ds.Dispose();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        try { 
        string ddllist = ddlpprs.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from previousppr where [exam]=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["content"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        dr.Close();
        con.Close();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btndel_Click(object sender, EventArgs e)
    {
        try { 
        string ddllist = ddlpprs.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("delete from previousppr where exam=@t1 delete from exam where exam=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        int i=cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblupdate.Visible = true;
            lblupdate.Text = "Deleted Successfully";
        }
        con.Close();
        bindsection();
        bindlist();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string ddllist = ddlpprs.SelectedItem.Text;
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        SqlCommand cmd1 = new SqlCommand("update previousppr set content=@t1 where exam=@t2", con);
        cmd1.Parameters.AddWithValue("@t1", content);
        cmd1.Parameters.AddWithValue("@t2", ddllist);
        int i = cmd1.ExecuteNonQuery();
        if (i == 1)
        {
            lblupdate.Visible = true;
            lblupdate.Text = "Updated Successfully";
        }
        con.Close();
        bindsection();
        bindlist();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }

    protected void btndel_Click1(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd1 = new SqlCommand("select exam from exam where section='"+ddlsection.Text+"'", con);
        SqlDataReader dr = cmd1.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
           
            var exam = dr["exam"].ToString();
            SqlCommand cmd = new SqlCommand("delete from exam where section='" + ddlsection.Text + "' delete from previousppr where exam='"+exam+"'", con);
            dr.Close();
            int i = cmd.ExecuteNonQuery();
            lblupdate.Visible = true;
            lblupdate.Text = "Section deleted successfully";
            
        }
        con.Close();
        bindsection();
        bindlist();
    }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
}
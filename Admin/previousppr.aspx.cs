﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class Admin_previousppr : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindsection();
        }
    }
    public void bindsection()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select DISTINCT section from exam where [section] is not null", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        ddlsection.DataSource = ds;
        ddlsection.DataTextField = "section";
        ddlsection.DataValueField = "section";
        ddlsection.DataBind();
        ddlsection.Items.Insert(0, new ListItem("Choose", "Choose"));
        }catch(Exception ex)
        {
            Console.WriteLine("",ex);
        }
    }
    
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string section = ddlsection.SelectedItem.Value;
        var ddlexam = Request.Form["ddlexam"];
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        SqlCommand cmd1 = new SqlCommand("insert into previousppr values(@t1,@t2) insert into exam values(@t3,@t1) ", con);
        cmd1.Parameters.AddWithValue("@t1",ddlexam);
        cmd1.Parameters.AddWithValue("@t2", content);
        cmd1.Parameters.AddWithValue("@t3", section);
        int i = cmd1.ExecuteNonQuery();
       
            lblsaved.Visible = true;
            lblsaved.Text = "Saved Successfully";
    }catch(Exception ex)
        {
            Console.WriteLine("",ex);
        }
    }

    protected void btnsavesection_Click(object sender, EventArgs e)
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd1 = new SqlCommand("insert into exam(section) values(@t2)", con);
        cmd1.Parameters.AddWithValue("@t2", Request.Form["addsection"]);
        int i = cmd1.ExecuteNonQuery();
        if (i == 1)
        {
            lblsaved.Visible = true;
            lblsaved.Text = "Saved successfully";
            bindsection();
           
        }
    }catch(Exception ex)
        {
            Console.WriteLine("",ex);
        }
    }
}
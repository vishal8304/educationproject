﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using System.Text;

public partial class Admin_Editbankonline : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        hide.Visible = false;
        if (!IsPostBack)
        {
            connect();
            bindtiles();
            bindname();
        }
    }
    public void connect()
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from onbanktest", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridEmpData.DataSource = grid;
        GridEmpData.DataBind();
    }
    public void bindtiles()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select distinct [set] from onbanktest where [set] is not null", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddllist.DataSource = dt;
            ddllist.DataTextField = "set";
            ddllist.DataValueField = "set";
            ddllist.DataBind();
            ddllist.Items.Insert(0, "--Select--");

        }
    }
    public void bindname()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da1 = new SqlDataAdapter("select distinct name from onbanktest where [name] is not null", con);
        DataTable dt1 = new DataTable();
        da1.Fill(dt1);
        if (dt1.Rows.Count > 0)
        {
            ddlname.DataSource = dt1;
            ddlname.DataTextField = "name";
            ddlname.DataValueField = "name";
            ddlname.DataBind();
            ddlname.Items.Insert(0, "--Select--");

        }
    }
    private void FillEmpData()
    {
        using (SqlConnection con = new SqlConnection(connection))
        {
            using (SqlCommand cmd = new SqlCommand("sp_FillData", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                GridEmpData.DataSource = dt;
                GridEmpData.DataBind();
            }
        }
    }
    protected void GridEmpData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridEmpData.PageIndex = e.NewPageIndex;
        connect();
    }

    protected void GridEmpData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridEmpData.DataKeys[e.RowIndex].Value);
        SqlCommand cmd = new SqlCommand("delete from onbanktest where Id = @t1", con);
        cmd.Parameters.AddWithValue("@t1", eno);
        int i = cmd.ExecuteNonQuery();
        connect();
    }

    protected void GridEmpData_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridEmpData.EditIndex = e.NewEditIndex;
        connect();
    }

    protected void GridEmpData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridEmpData.EditIndex = -1;
        connect();
    }

    protected void GridEmpData_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridEmpData.DataKeys[e.RowIndex].Value);
        string qry = "update onbanktest set Question=@t1,Optionval1=@t2,Optionval2=@t3,Optionval3=@t4,Optionval4=@t5,Optionval5=@t6,Correctval=@t7,path=@t8,section=@t9,descr=@t10,solimg=@t11 where Id=@t12";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        TextBox txtqs = GridEmpData.Rows[e.RowIndex].FindControl("txtqs") as TextBox;
        TextBox txt1 = GridEmpData.Rows[e.RowIndex].FindControl("txt1") as TextBox;
        TextBox txt2 = GridEmpData.Rows[e.RowIndex].FindControl("txt2") as TextBox;
        TextBox txt3 = GridEmpData.Rows[e.RowIndex].FindControl("txt3") as TextBox;
        TextBox txt4 = GridEmpData.Rows[e.RowIndex].FindControl("txt4") as TextBox;
        TextBox txt5 = GridEmpData.Rows[e.RowIndex].FindControl("txt5") as TextBox;
        TextBox txtcorval = GridEmpData.Rows[e.RowIndex].FindControl("txtcorval") as TextBox;
        TextBox txtsection = GridEmpData.Rows[e.RowIndex].FindControl("txtsection") as TextBox;
        TextBox txtsol = GridEmpData.Rows[e.RowIndex].FindControl("txtsol") as TextBox;
        Image img = (Image)GridEmpData.Rows[e.RowIndex].FindControl("qsimg");
        Image solimg = (Image)GridEmpData.Rows[e.RowIndex].FindControl("solimg");
        FileUpload qsfile = (FileUpload)GridEmpData.Rows[e.RowIndex].FindControl("qsfile");
        FileUpload filevideo = (FileUpload)GridEmpData.Rows[e.RowIndex].FindControl("filevideo");
        FileUpload Fileimage = (FileUpload)GridEmpData.Rows[e.RowIndex].FindControl("Fileimage");
        cmd1.Parameters.AddWithValue("@t1", txtqs.Text);
        cmd1.Parameters.AddWithValue("@t2", txt1.Text);
        cmd1.Parameters.AddWithValue("@t3", txt2.Text);
        cmd1.Parameters.AddWithValue("@t4", txt3.Text);
        cmd1.Parameters.AddWithValue("@t5", txt4.Text);
        cmd1.Parameters.AddWithValue("@t6", txt5.Text);
        cmd1.Parameters.AddWithValue("@t7", txtcorval.Text);
        string fp = "";
        if (qsfile.HasFile)
        {
            fp = "/testupload/" + qsfile.FileName.ToString();
            qsfile.PostedFile.SaveAs(Server.MapPath("/testupload/") + qsfile.FileName.ToString());
            cmd1.Parameters.AddWithValue("@t8", fp);
        }
        else
        {
            fp = img.ImageUrl;
            cmd1.Parameters.AddWithValue("@t8", fp);
        }
        cmd1.Parameters.AddWithValue("@t9", txtsection.Text);
        cmd1.Parameters.AddWithValue("@t10", txtsol.Text);
        string fp1;
        if (Fileimage.HasFile)
        {
            fp1 = "/testupload/" + Fileimage.FileName.ToString();
            Fileimage.PostedFile.SaveAs(Server.MapPath("/testupload/") + Fileimage.FileName.ToString());
            cmd1.Parameters.AddWithValue("@t11", fp1);
        }
        else
        {
            fp1 = solimg.ImageUrl;
            cmd1.Parameters.AddWithValue("@t11", fp1);
        }
        cmd1.Parameters.AddWithValue("@t12", eno);
        int i = cmd1.ExecuteNonQuery();
        GridEmpData.EditIndex = -1;
        connect();
    }

    protected void btnfilter_Click(object sender, EventArgs e)
    {
        hide.Visible = true;
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from onbanktest where [set]='" + ddllist.Text + "' and name='" + ddlname.Text + "' order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridEmpData.DataSource = grid;
        GridEmpData.DataBind();
    }

    protected void btndel_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("alter table useronbanktest nocheck constraint all delete from onbanktest where [set]='" + ddllist.Text + "' alter table useronbanktest check constraint all", con);
        int i = cmd.ExecuteNonQuery();
        lblmsg.Visible = true;
        lblmsg.Text = "Tile Deleted Successfully";
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Viewsscans.aspx.cs" Inherits="Admin_Viewsscans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .wrap{
            overflow-wrap: break-word;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
<div class="row">
    <div class="col-md-12">

    <asp:GridView ID="GridEmpData" OnPageIndexChanging="GridEmpData_PageIndexChanging" Width="100%" runat="server" AutoGenerateColumns="False"  BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" DataKeyNames="Id" AllowPaging="True" PageSize="5" OnRowDeleting="GridEmpData_RowDeleting"><AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
                 <asp:TemplateField>
	<ItemTemplate>
		<asp:LinkButton ID="lbldel" ForeColor="Brown" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this entry?');">Delete </asp:LinkButton>
	</ItemTemplate>
</asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        </div>
            <div class="row">
               
                 <div class="col-md-1">
                    <strong><asp:label id="lblqss" runat="server" Text="Question:" visible='<%#Eval("Ques").ToString()==""?false:true %>'></asp:label></strong>
                     </div>
                <div class="col-md-11 wrap">
                   <asp:Label ID="lblqs" runat="server" Text='<%#Eval("Ques")%>' visible='<%#Eval("Ques").ToString().Trim()==""?false:true %>'></asp:Label>
                </div>
                 </div>
                        <br />
                        <div class="row">
                            <div class="col-md-11">
                                <asp:Image ID="qsimg" runat="server" Visible='<%#Eval("picpath").ToString()==""?false:true %>' ImageUrl='<%#Eval("picpath")%>' Height="200px" Width="80%" />
                            </div>
                            <div class="col-md-1"></div>
                        </div><br />
                        <div class="row">
                <div class="col-md-6 wrap">
                    <strong><asp:label id="lbl1" runat="server" Text="Option1:" visible='<%#Eval("opta").ToString()==""?false:true %>'></asp:label> </strong>
                    <asp:Label ID="lblopt1" runat="server" visible='<%#Eval("opta").ToString()==""?false:true %>' Text='<%#Eval("opta") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val1" HeaderText="A" />--%>
                </div>
     <div class="col-md-6 wrap">
          <strong><asp:label id="lbl2" runat="server" Text="Option2:" visible='<%#Eval("optb").ToString()==""?false:true %>'></asp:label></strong>
                    <asp:Label ID="lblopt2" visible='<%#Eval("optb").ToString()==""?false:true %>' runat="server" Text='<%#Eval("optb") %>'></asp:Label>


                </div>
            </div><br />
                         <div class="row">
                <div class="col-md-6 wrap">
                     <strong><asp:label id="lbl3" runat="server" Text="Option3:" visible='<%#Eval("optc").ToString()==""?false:true %>'></asp:label> </strong>
                    <asp:Label ID="lblopt3" runat="server" visible='<%#Eval("optc").ToString()==""?false:true %>' Text='<%#Eval("optc") %>'></asp:Label>
                </div>
     <div class="col-md-6 wrap">
          <strong><asp:label id="lbl4" runat="server" Text="Option4:" visible='<%#Eval("optd").ToString()==""?false:true %>'></asp:label> </strong>
                    <asp:Label ID="lblopt4" runat="server" visible='<%#Eval("optd").ToString()==""?false:true %>' Text='<%#Eval("optd") %>'></asp:Label>
                </div>
            </div><br />
              <div class="row">
                  <div class="col-md-6 wrap">
          <strong><asp:label id="lbl5" runat="server" Text="Option5:" visible='<%#Eval("opte").ToString()==""?false:true %>'></asp:label></strong>
                    <asp:Label ID="lblopt5" runat="server" visible='<%#Eval("optd").ToString()==""?false:true %>' Text='<%#Eval("opte") %>'></asp:Label>
                </div>
                <div class="col-md-6">
                    
                </div></div>
                   <div class="row">
                       <div class="col-md-4 wrap">
                            <strong>Correct Option: </strong>
                    <asp:Label ID="lblcoropt" runat="server" Text='<%#Eval("corval") %>'></asp:Label>
                       </div>
     <div class="col-md-3 wrap">
                  <asp:Label ID="lblexam" runat="server" Text='<%#Eval("exam").ToString().Trim() %>'></asp:Label>
                </div>
                   <div class="col-md-5 wrap">
                  <asp:Label ID="lblsubject" runat="server" Text='<%#Eval("subject").ToString().Trim() %>'></asp:Label>
                </div>
            </div><br />
                       </ItemTemplate>
                  <HeaderTemplate>
                    Posts
                  </HeaderTemplate>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                </asp:TemplateField>
                 </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>  
        
    </div>
  </div>
</div>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_AdminMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Request.Cookies["Login"] != null)
            {

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        //    if (Session["admin"] ==null)
        //{
        //    Response.Redirect("Login.aspx");
        //}
        }
    }

    protected void btnlogout_Click(object sender, EventArgs e)
    {
        HttpCookie mycookie = new HttpCookie("Login");
        mycookie.Expires = DateTime.Now.AddDays(-1d);
        Response.Cookies.Add(mycookie);
        //Session.Remove("admin");
        Response.Redirect("Adminindex.aspx");
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="edit_questions.aspx.cs" Inherits="Admin_edit_questions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <strong><asp:Label ID="lblupload" runat="server" Text="Label" Visible="False" ForeColor="#CC3300"></asp:Label></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"><strong>Question:</strong></div>
            <div class="col-md-9">
                <asp:TextBox ID="txtqs" runat="server" Height="66px" TextMode="MultiLine" Width="342px"></asp:TextBox>
                
            </div>
        </div><br />
        <div class="row">




            <div class="col-md-9"></div>
        </div><br />
        <div class="row">
            <div class="col-md-6">
                <asp:TextBox ID="txtOption1" runat="server"></asp:TextBox>
                <asp:RadioButton ID="rdb1" runat="server" GroupName="a" />
            </div>
            <div class="col-md-6"><asp:TextBox ID="txtOption2" runat="server"></asp:TextBox>
                 <asp:RadioButton ID="rdb2" runat="server" GroupName="a" /></div>
        </div><br />
        <div class="row">
            <div class="col-md-6">
                <asp:TextBox ID="txtOption3" runat="server"></asp:TextBox>
                 <asp:RadioButton ID="rdb3" runat="server" GroupName="a" />
            </div>
            <div class="col-md-6"><asp:TextBox ID="txtOption4" runat="server"></asp:TextBox>
                 <asp:RadioButton ID="rdb4" runat="server" GroupName="a" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <asp:Button ID="btnupload" runat="server" Text="Upload" OnClick="btnupload_Click" /></div>
        </div>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>

</asp:Content>


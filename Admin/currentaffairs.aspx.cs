﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_currentaffairs : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddl();

        }
       
    }

    public void bindddl()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from CurrentAffair where [Date] is not null", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        ddldates.DataSource = ds;
        ddldates.DataTextField = "Date";
        ddldates.DataValueField = "Id";
        ddldates.DataBind();
        ddldates.Items.Insert(0, new ListItem("Current","Current"));
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }
    public void text()
    {
        try { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from CurrentAffair where [Date]=@t1", con);
        cmd.Parameters.AddWithValue("@t1",ddldates.SelectedItem.Text);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["content"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }
   
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if(con.State==ConnectionState.Closed)
        {
            con.Open();
        }
        if (ddldates.SelectedIndex == 0)
        {
            string str = HttpUtility.HtmlEncode(CKEditor1.Text);
            string option = Request.Form["Choose"];
            SqlCommand cmd = new SqlCommand("insert into CurrentAffair(content,section,[Date]) values(@t1,@t2,@t4)", con);
            cmd.Parameters.AddWithValue("@t1", str);
            cmd.Parameters.AddWithValue("@t2", option);
            cmd.Parameters.AddWithValue("@t4", System.DateTime.Now.ToString());
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblsaved.Visible = true;
                lblsaved.Text = "Data Saved Successfully";
            }
        }
        else
        {
            string str1 = HttpUtility.HtmlEncode(CKEditor1.Text);
            string option = Request.Form["Choose"];
            SqlCommand cmd = new SqlCommand("update CurrentAffair set content=@t1,section=@t2 where [Date]=@t4", con);
            cmd.Parameters.AddWithValue("@t1", str1);
            cmd.Parameters.AddWithValue("@t2", option);
            cmd.Parameters.AddWithValue("@t4", ddldates.SelectedItem.Text);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblsaved.Visible = true;
                lblsaved.Text = "Data Updated Successfully";
                bindddl();
            }
        }
        
    }

   
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        text();
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        string ddllist = ddldates.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("delete from CurrentAffair where Date=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblsaved.Visible = true;
            lblsaved.Text = "Deleted Successfully";
        }
    }


    protected void ddldates_SelectedIndexChanged(object sender, EventArgs e)
    {
        string ddllist = ddldates.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from CurrentAffair where [Date]=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["content"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Dailyvocab.aspx.cs" Inherits="Admin_Dailyvocab" %>
   <%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .text{
            width:140px;
        }

        .textbox{
                 width: 369px;
    height: 169px;
        }
        
    </style>
</asp:Content>
 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>

</div>
    <div class="container-fluid">
        <div class="row" style="text-align:center">
            <div class="col-md-12">
               <h2>Daily Vocab</h2> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12" style="text-align:center">
                <asp:Label ID="lbldone" runat="server" Visible="false" ForeColor="#FF3300"></asp:Label> 
            </div>
        </div><br />
            
<h4 style="margin-left: 2%; margin-bottom: 2%;">Add Comprehension</h4>
        <div class="row">
            <div class="col-md-10">
                  <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> 
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-danger" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-3">
                Add No of Questions:
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="text" TextMode="Number" runat="server" Width="70px"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnadd" runat="server" Text="ADD" onclick="btnAddRow_Click"/>
            </div>
           
             <div class="col-md-5">
                <asp:Label ID="lblsaveqs" runat="server" Visible="false" Text="Saved Successfully"></asp:Label>
            </div>
            </div><br />
         <asp:GridView ID="GridAddEmp" runat="server"  width="100%" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
         <Columns>  
            <asp:TemplateField HeaderText="Daily Vocab Question/s">  
                <ItemTemplate> <br />
                    <div class="row" style="margin-left:15px">
                        <div class="col-md-1">
                            <%#Container.DataItemIndex +1 %>
                        </div>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtqs" ClientIDMode="Static" runat="server" Width="100%"></asp:TextBox>  
                        </div>
                    </div> 
              <div class="row" style="margin-left:15px;">
                    <div class="col-md-6">A.
                         <asp:TextBox ID="opt1" ClientIDMode="Static" runat="server" Width="414px"></asp:TextBox>
                    </div>
                    <div class="col-md-6">B.
                    <asp:TextBox ID="opt2" ClientIDMode="Static" runat="server" Width="414px"></asp:TextBox>  

                    </div>
                </div>
                 <div class="row"  style="margin-left:15px">
                        <div class="col-md-6">C.
                    <asp:TextBox ID="opt3" ClientIDMode="Static" runat="server" Width="414px"></asp:TextBox>  
                        </div>
                        <div class="col-md-6">D.
                    <asp:TextBox ID="opt4" ClientIDMode="Static" runat="server" Width="414px"></asp:TextBox>  

                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-1">
                            Solution:
                        </div>
                        <div class="col-md-12">
                            <asp:TextBox ID="txtsol" Width="100%" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div><br />
                    <div class="row">
                    <div class="col-md-3">
                        Correct Option
                            <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                    </div><br />
                </ItemTemplate>   
            </asp:TemplateField>
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
    </asp:GridView>  
       <br />
        <div class="row">
         <div class="col-md-12" style="text-align:center">
                <asp:Button ID="btnsaveqs" runat="server" Visible="false" Text="Save" onclick="btnsaveqs_Click"/>
            </div>
            </div><br />
        <asp:GridView ID="grdaddword" runat="server" width="100%" CssClass="grid" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
        <Columns>  
            <asp:TemplateField HeaderText="No">  
                <ItemTemplate>  
                    <%#Container.DataItemIndex +1 %>  
                </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="Word" ControlStyle-CssClass="textbox">  
                <ItemTemplate>  
                    <asp:TextBox ID="txtwords" ClientIDMode="Static" runat="server" ></asp:TextBox>  
                </ItemTemplate>  
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="Description">  
                <ItemTemplate>  
                    <asp:TextBox ID="desc" ClientIDMode="Static" runat="server" Width="454px" Columns="5" Rows="4" TextMode="MultiLine"></asp:TextBox>  
                </ItemTemplate>    
            </asp:TemplateField>  
           
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
    </asp:GridView><br />
        <div class="row">
            <%-- <div class="col-md-12" style="text-align:center">
                <asp:Button ID="btnsaveword" runat="server" Visible="false" Text="Save" onclick="btnsaveword_Click"/>
            </div>--%>
        </div><br />
    </div>
         <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
</asp:Content>


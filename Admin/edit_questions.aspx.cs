﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_edit_questions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnupload_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        con.Open();
        string qry = "insert into quiz(Question,option1,option2,option3,option4,Correctval) values(@t1,@t2,@t3,@t4,@t5,@t6)";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.Parameters.AddWithValue("@t1", txtqs.Text);
        cmd.Parameters.AddWithValue("@t2", txtOption1.Text.Trim());
        cmd.Parameters.AddWithValue("@t3", txtOption2.Text.Trim());
        cmd.Parameters.AddWithValue("@t4", txtOption3.Text.Trim());
        cmd.Parameters.AddWithValue("@t5", txtOption4.Text.Trim());
        string val = "";
        if(rdb1.Checked==true)
        {
            val = txtOption1.Text.Trim();
        }
        else if(rdb2.Checked==true)
        {
            val = txtOption2.Text.Trim();
        }
        else if(rdb3.Checked==true)
        {
            val = txtOption3.Text.Trim();
        }
        else if (rdb4.Checked == true)
        {
            val = txtOption4.Text.Trim();
        }
        cmd.Parameters.AddWithValue("@t6", val);
        int i = cmd.ExecuteNonQuery();
        if(i==1)
        {
            lblupload.Visible = true;
            lblupload.Text = "Question Uploaded Successfully";
        }
    }
}
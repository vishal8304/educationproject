﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Drawing;

public partial class Admin_registeredusers : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            connect();
        }
    }

    public void connect()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from [User]", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        gridregistered.DataSource = dt;
        gridregistered.DataBind();
    }

    protected void gridregistered_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridregistered.PageIndex = e.NewPageIndex;
        connect();
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from [User] where Name='"+Request.Form["txtname"].TrimEnd().TrimStart() +"'", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        gridregistered.DataSource = dt;
        gridregistered.DataBind();
    }

    [WebMethod]
    public static List<string> Getname(string Name)
    {
        List<string> result = new List<string>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select distinct Name from [User] where Name LIKE '%'+@Searchdata+'%'", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@Searchdata", Name);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(string.Format("{0}/{1}", dr["Name"], dr["Name"]));
                }
                return result;
            }
        }
    }

    protected void gridregistered_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Expert")
        {
            string ID = e.CommandArgument.ToString().TrimEnd().TrimStart();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand comd = new SqlCommand("select expert from [User] where Email='" + ID + "' and expert='true'", con);
            SqlDataReader dr = comd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                dr.Close();
                SqlCommand cmd = new SqlCommand("update [User] set expert='false' where Email='" + ID + "'", con);
                int result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    connect();
                }
                con.Close();
            }
            else
            {
                dr.Close();
                SqlCommand cmd = new SqlCommand("update [User] set expert='true' where Email='" + ID + "'", con);
                int result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    connect();
                }
                con.Close();
            }
            
        }
        else if(e.CommandName == "Freetest")
        {
            string ID = e.CommandArgument.ToString().TrimEnd().TrimStart();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand comd = new SqlCommand("select freetest from [User] where Email='" + ID + "' and freetest='true'", con);
            SqlDataReader dr = comd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                dr.Close();
                SqlCommand cmd = new SqlCommand("update [User] set freetest='false' where Email='" + ID + "'", con);
                int result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    connect();
                }
                con.Close();
            }
            else
            {
                dr.Close();
                SqlCommand cmd = new SqlCommand("update [User] set freetest='true' where Email='" + ID + "'", con);
                int result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    connect();
                }
                con.Close();
            }
            
        }
    }

    //protected void gridregistered_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        LinkButton lnkexpert = (LinkButton)e.Row.FindControl("lnkexpert");
    //        LinkButton lnkfreetest = (LinkButton)e.Row.FindControl("lnkfreetest");
    //        if (lnkexpert.Text=="Allow")
    //        {
    //            lnkexpert.BackColor = Color.Red;
    //            lnkexpert.ForeColor = Color.White;
    //        }
    //        else
    //        {
    //            lnkexpert.BackColor = Color.Green;
    //            lnkexpert.ForeColor = Color.White;
    //        }
    //        if(lnkfreetest.Text== "Allow")
    //        {
    //            lnkfreetest.BackColor = Color.Red;
    //            lnkfreetest.ForeColor = Color.White;
    //        }
    //        else
    //        {
    //            lnkfreetest.BackColor = Color.Green;
    //            lnkfreetest.ForeColor = Color.White;
    //        }
    //    }
    //}
}
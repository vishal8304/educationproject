﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;

public partial class Admin_Edit_quiz : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindTime();

        }
    }

    private void BindTime()
    {
        List<string> minutes = new List<string>();

        for (int i = 0; i <= 300; i++)
        {
            minutes.Add(i.ToString("00"));
        }

        ddltime.DataSource = minutes;
        ddltime.DataBind();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddRowsToGrid();
    }
    private void AddRowsToGrid()
    {
        List<int> Row_Number = new List<int>();
        int rows = 0;
        int.TryParse(txtAddNoOfRecord.Text.Trim(), out rows);

        for (int R = 0; R < rows; R++)
        {
            Row_Number.Add(R);
        }
        GridAddEmp.DataSource = Row_Number;
        GridAddEmp.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();

        }
        try
        {
            if (CKEditor1.Text != "")
            {
                string str = HttpUtility.HtmlEncode(CKEditor1.Text);
                SqlCommand cmd = new SqlCommand("insert into quizinstruction(content,section) values(@t1,@t2)", con);
                cmd.Parameters.AddWithValue("@t1", str);
                cmd.Parameters.AddWithValue("@t2", "SSC");
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblsave.Visible = true;
                    lblsave.Text = "Updated Successfully";
                    CKEditor1.Text = "";
                }
            }
            foreach (GridViewRow row in this.GridAddEmp.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    TextBox txtqs = (TextBox)row.FindControl("txtqs");
                    TextBox txtopt1 = (TextBox)row.FindControl("opt1");
                    TextBox txtopt2 = (TextBox)row.FindControl("opt2");
                    TextBox txtopt3 = (TextBox)row.FindControl("opt3");
                    TextBox txtopt4 = (TextBox)row.FindControl("opt4");
                    DropDownList txtcorans = (DropDownList)row.FindControl("ddlopt");
                    TextBox txtdesc = (TextBox)row.FindControl("txtdesc");
                    SqlCommand cmd = new SqlCommand("INSERT INTO test(Ques,val1,val2,val3,val4,Correctoption,[time],descr,marks,name,CreateDate) VALUES(@Question,@Optionval1,@Optionval2,@Optionval3,@Optionval4,@Correctval,@time,@descr,@marks,@name,@CreateDate)", con);
                    cmd.Parameters.AddWithValue("@Question", txtqs.Text);
                    cmd.Parameters.AddWithValue("@Optionval1", txtopt1.Text);
                    cmd.Parameters.AddWithValue("@Optionval2", txtopt2.Text);
                    cmd.Parameters.AddWithValue("@Optionval3", txtopt3.Text);
                    cmd.Parameters.AddWithValue("@Optionval4", txtopt4.Text);
                    cmd.Parameters.AddWithValue("@Correctval", txtcorans.Text);
                    cmd.Parameters.AddWithValue("@time", ddltime.SelectedValue);
                    cmd.Parameters.AddWithValue("@descr", txtdesc.Text);
                    cmd.Parameters.AddWithValue("@marks", txtmarks.Text);
                    cmd.Parameters.AddWithValue("@name", ddlsscset.Text);
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    cmd.ExecuteScalar();
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data inserted successfully";
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("", ex);
        }

    }

}






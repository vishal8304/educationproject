﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="QuestionSet.aspx.cs" Inherits="Admin_QuestionSet" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 style="text-align:center">Question Upload for Online Test Series Section </h2>
    <br></br>
 <div class="row" style="margin-top:2%">
            <div class="col-md-1">
            </div>
            <div class="col-md-11">
                 <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
                 <%--<asp:TextBox runat="server" ID="txtdesc" TextMode="MultiLine" Width="600px" Height="200px"></asp:TextBox>--%>
            </div>
        </div>
       <br />
        <div class="row" style="text-align:center">
            <div class="col-md-4">
                <asp:Button ID="btnupload" ClientIDMode="Static" runat="server" OnClick="btnupload_Click" Text="Upload"  />
            </div>
            <div class="col-md-6">

                 <asp:Label ID="lblconfirm" runat="server" Visible="false" ForeColor="Red"></asp:Label>
            </div>
        </div>

</asp:Content>


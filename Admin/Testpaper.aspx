﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Testpaper.aspx.cs" Inherits="Admin_Testpaper" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12" style="text-align:right">
            <asp:Label ID="lblSubmit" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-2">
            Choose Subject:
        </div>
        <div class="col-md-2">
           <%-- <select name="subject">
                <option value="--Select--">--Select--</option>
                <option value="Maths">Maths</option>
                <option value="English">English</option>
                <option value="Reasoning">Reasoning</option>
                <option value="GS">GS</option>
            </select>--%>
            <asp:DropDownList ID="subject" runat="server">
                <asp:ListItem>Maths</asp:ListItem>
                <asp:ListItem>English</asp:ListItem>
                <asp:ListItem>Reasoning</asp:ListItem>
                <asp:ListItem>GS</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="col-md-4">
            <input type="button" value="Add Chapter" name="btnAdd" id="btnAdd" />
            <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Text="Add Question" />
        </div>
        <div class="col-md-4">
            <input type="text" hidden="hidden" id="txtchptr" name="txtchptr" />
            <asp:DropDownList ID="ddl" ClientIDMode="Static" Visible="false" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row" style="margin-left:15px">
                        <div class="col-md-1">
                            Question:
                        </div>
                         <div class="col-md-11">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
                    </div><br />
                <div class="row" <%--style="margin-left:15px;"--%>>
                    <div class="">
                        Option1:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
             
                    
                    <div class="">
                        Option2:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl2" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
                </div>
    <br />
                    <div class="row" style="margin-left:15px;">
                    <div class="">
                        Option3:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl3" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
             
                
                    <div class="">
                        Option4:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl4" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
                </div><br />
                    <div class="row" style="margin-left:15px;">
                    <div class="">
                        Option5:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl5" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
                
                        <div class="">
                        Solution:
                    </div>
                         <div class="col-md-6">
            <CKEditor:CKEditorControl CssClass="ckedits" ID="CKEditorControl6" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
                    </div><br />
    <div class="row">
        <div class="col-md-2">
            Correct Value:
        </div>
        <div class="col-md-2">
            <select name="corval" id="corval">
                <option value="--Select--">--Select--</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
            </select>
        </div>
        <div class="col-md-1">
            Time:
        </div>
        <div class="col-md-2">
             <asp:DropDownList ID="ddltime" runat="server">
        </asp:DropDownList>
        </div>
       <div class="col-md-1">
           Marks
       </div>
        <div class="col-md-3" style="width:8%;">
<asp:TextBox ID="txtMarks" Width="75px" runat="server" TextMode="Number"></asp:TextBox>
        </div>
    
    
        <div class="col-md-1" style="text-align:center">
            <asp:Button ID="btnSubmit" ClientIDMode="Static" Text="Submit" runat="server" OnClick="btnSubmit_Click" />
        </div>
        
    </div>
     <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script>
        $("#btnAdd").click(function () {
            $("#txtchptr").show();
        });
    </script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
$(function () {
    CKEDITOR.replace('CKEditorControl1', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
$(function () {
    CKEDITOR.replace('CKEditorControl2', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
$(function () {
    CKEDITOR.replace('CKEditorControl3', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
$(function () {
    CKEDITOR.replace('CKEditorControl4', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
$(function () {
    CKEDITOR.replace('CKEditorControl5', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
$(function () {
    CKEDITOR.replace('CKEditorControl6', {
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload'
    });
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('class'));
            });
        });
    </script>
    <script>
        $("#btnSubmit").click(function () {
            debugger
            var dropdown = $('#corval').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please choose Correct Option Value");
                return false;
            }
        });
    </script>
</asp:Content>


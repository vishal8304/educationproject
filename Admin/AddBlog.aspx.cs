﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddBlog : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //BatchType();
        }
    }

    
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (con.State == System.Data.ConnectionState.Closed)
        {
            con.Open();
        }
        string img = "";
        if (FileUpload1.HasFile)
        {
            img = "/BlogImage/" + FileUpload1.FileName.ToString();
            FileUpload1.PostedFile.SaveAs(Server.MapPath("/BlogImage/") + FileUpload1.FileName.ToString());
        }
        var str = HttpUtility.HtmlEncode(CKEditor1.Text);
        SqlCommand cmd = new SqlCommand("insert into BlogMaster (Url,BlogImage,Heading,Short,[Description],Date) values (@t1,@t2,@t3,@t4,@t5,@t6)", con);
        cmd.Parameters.AddWithValue("@t1", TextBox1.Text);
        cmd.Parameters.AddWithValue("@t2", img);
        cmd.Parameters.AddWithValue("@t3", TextBox2.Text);
        cmd.Parameters.AddWithValue("@t4", TextBox3.Text);
        cmd.Parameters.AddWithValue("@t5", str);
        cmd.Parameters.AddWithValue("@t6", DateTime.Now);
        cmd.ExecuteNonQuery();
    }

}
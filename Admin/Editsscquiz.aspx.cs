﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;

public partial class Admin_Editsscquiz : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        hide.Visible = false;
        if (!IsPostBack)
        {
            connect();
            bindname();
        }
    }
    public void connect()
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from test order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridView1.DataSource = grid;
        GridView1.DataBind();
    }
    public void bindname()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da1 = new SqlDataAdapter("select distinct name from test where [name] is not null", con);
        DataTable dt1 = new DataTable();
        da1.Fill(dt1);
        if (dt1.Rows.Count > 0)
        {
            ddlname.DataSource = dt1;
            ddlname.DataTextField = "name";
            ddlname.DataValueField = "name";
            ddlname.DataBind();
            ddlname.Items.Insert(0, "--Select--");

        }
    }
    private void FillEmpData()
    {
        using (SqlConnection con = new SqlConnection(connection))
        {
            using (SqlCommand cmd = new SqlCommand("sp_FillData", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        connect();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //try { 
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        string qry = "alter table onlinetest nocheck constraint all delete from onlinetest where Id = @t1 delete from test where Id=@t1 alter table onlinetest check constraint all";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        cmd1.Parameters.AddWithValue("@t1", eno);
        int i = cmd1.ExecuteNonQuery();
        connect();
        //}catch(Exception ex)
        //{
        //    Console.Write("", ex);
        //}
    }


    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        connect();
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        connect();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        string qry = "update test set Ques=@t1,val1=@t2,val2=@t3,val3=@t4,val4=@t5,Correctoption=@t7,qspath=@t8,section=@t9,descr=@t10,path=@t11 where Id=@t12";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        TextBox txtqs = GridView1.Rows[e.RowIndex].FindControl("txtqs") as TextBox;
        TextBox txt1 = GridView1.Rows[e.RowIndex].FindControl("txt1") as TextBox;
        TextBox txt2 = GridView1.Rows[e.RowIndex].FindControl("txt2") as TextBox;
        TextBox txt3 = GridView1.Rows[e.RowIndex].FindControl("txt3") as TextBox;
        TextBox txt4 = GridView1.Rows[e.RowIndex].FindControl("txt4") as TextBox;
        TextBox txtcorval = GridView1.Rows[e.RowIndex].FindControl("txtcorval") as TextBox;
        TextBox txtsection = GridView1.Rows[e.RowIndex].FindControl("txtsection") as TextBox;
        TextBox txtsol = GridView1.Rows[e.RowIndex].FindControl("txtsol") as TextBox;
        Image img = (Image)GridView1.Rows[e.RowIndex].FindControl("qsimg");
        Image solimg = (Image)GridView1.Rows[e.RowIndex].FindControl("solimg");
        FileUpload qsfile = (FileUpload)GridView1.Rows[e.RowIndex].FindControl("qsfile");
        FileUpload filevideo = (FileUpload)GridView1.Rows[e.RowIndex].FindControl("filevideo");
        FileUpload Fileimage = (FileUpload)GridView1.Rows[e.RowIndex].FindControl("Fileimage");
        cmd1.Parameters.AddWithValue("@t1", txtqs.Text);
        cmd1.Parameters.AddWithValue("@t2", txt1.Text);
        cmd1.Parameters.AddWithValue("@t3", txt2.Text);
        cmd1.Parameters.AddWithValue("@t4", txt3.Text);
        cmd1.Parameters.AddWithValue("@t5", txt4.Text);
        cmd1.Parameters.AddWithValue("@t7", txtcorval.Text);
        string fp = "";
        if (qsfile.HasFile)
        {
            fp = "/testupload/" + qsfile.FileName.ToString();
            qsfile.PostedFile.SaveAs(Server.MapPath("/testupload/") + qsfile.FileName.ToString());
            cmd1.Parameters.AddWithValue("@t8", fp);
        }
        else
        {
            fp = img.ImageUrl;
            cmd1.Parameters.AddWithValue("@t8", fp);
        }
        cmd1.Parameters.AddWithValue("@t9", txtsection.Text);
        cmd1.Parameters.AddWithValue("@t10", txtsol.Text);
        string fp1;
        if (Fileimage.HasFile)
        {
            fp1 = "/testupload/" + Fileimage.FileName.ToString();
            Fileimage.PostedFile.SaveAs(Server.MapPath("/testupload/") + Fileimage.FileName.ToString());
            cmd1.Parameters.AddWithValue("@t11", fp1);
        }
        else
        {
            fp1 = solimg.ImageUrl;
            cmd1.Parameters.AddWithValue("@t11", fp1);
        }
        cmd1.Parameters.AddWithValue("@t12", eno);
        int i = cmd1.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        connect();
    }

    protected void btnfilter_Click(object sender, EventArgs e)
    {
        hide.Visible = true;
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from test where name='" + ddlname.Text + "' order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridView1.DataSource = grid;
        GridView1.DataBind();
    }

    protected void btndel_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("alter table onlinetest nocheck constraint all delete from test where [name]='" + ddlname.Text + "' alter table onlinetest check constraint all", con);
        int i = cmd.ExecuteNonQuery();
        lblmsg.Visible = true;
        lblmsg.Text = "Tile Deleted Successfully";
    }
}
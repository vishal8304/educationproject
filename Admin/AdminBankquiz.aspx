﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="AdminBankquiz.aspx.cs" Inherits="Admin_AdminBankquiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="Id" ForeColor="Black" GridLines="Vertical" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting">
           <%-- <AlternatingRowStyle BackColor="White" />--%>
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
               
                <asp:TemplateField>
                    <ItemTemplate>
            <div class="row">
               
                 <div class="col-md-12">
                    <strong>Question</strong>
                     <strong> <%#Container.DataItemIndex+1 %>:</strong>
                    <asp:label ID="lblqs" runat="server" Text='<%#Eval("Ques") %>'></asp:label>
                    <%--<asp:BoundField DataField="Ques" HeaderText="Question" />--%>
                </div>
                 </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                 <asp:Image ID="imgquiz" runat="server" ImageUrl='<%#Eval("qspath") %>' Height="150px" Width="80%" />
                    <%--  <asp:ImageField DataImageUrlField="path" HeaderText="Image">
                    <ControlStyle Height="120px" Width="120px" />
                        <ItemStyle Height="28px" Width="28px" />
                </asp:ImageField>
           --%>
                            </div>
                        </div><br />
<div class="row">
                <div class="col-md-6">
                    <strong>Option1: </strong>
                    <asp:Label ID="lblopt1" runat="server" Text='<%#Eval("val1") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val1" HeaderText="A" />--%>
                </div>
     <div class="col-md-6">
          <strong>Option2: </strong>
                    <asp:Label ID="lblopt2" runat="server" Text='<%#Eval("val2") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="val2" HeaderText="B" />--%>
                </div>
            </div><br />
                        <div class="row">
                <div class="col-md-6">
                     <strong>Option3: </strong>
                    <asp:Label ID="lblopt3" runat="server" Text='<%#Eval("val3") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val3" HeaderText="C" />--%>
                </div>
     <div class="col-md-6">
          <strong>Option4: </strong>
                    <asp:Label ID="lblopt4" runat="server" Text='<%#Eval("val4") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="val4" HeaderText="D" />--%>
                </div>
            </div><br />
                         <div class="row">
                <div class="col-md-6">
                    <strong>Option5: </strong>
                      <asp:Label ID="Label1" runat="server" Text='<%#Eval("val5") %>'></asp:Label>
                </div>
     <div class="col-md-6">
                 <strong>Correct Option: </strong>
                    <asp:Label ID="lblcoropt" runat="server" Text='<%#Eval("correctoption") %>'></asp:Label>
              
                </div>
            </div><br />
                        <div class="row">
                            <div class="col-md-12">
                                 <strong>Solution</strong>
                    <asp:label ID="lblsol" runat="server" Text='<%#Eval("descr") %>'></asp:label>
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                  <asp:Image ID="imgsol" runat="server" ImageUrl='<%#Eval("solpath") %>' Height="150px" Width="80%" />
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                 <video width="400" controls>
  <source src="<%#Eval("solvideo") %>" type="video/mp4">
</video>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-1">
                                <asp:LinkButton ID="lbldel" ForeColor="Brown" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this entry?');">Delete </asp:LinkButton>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
<%--                
               
            <FooterStyle BackColor="#CCCC99" />
            
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />--%>
            </ItemTemplate>
                  <HeaderTemplate>
                      Quiz Questions
                  </HeaderTemplate>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                </asp:TemplateField>
                 </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
</asp:Content>


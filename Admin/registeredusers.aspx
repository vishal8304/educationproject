﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="registeredusers.aspx.cs" Inherits="Admin_registeredusers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Registered Users</h3>
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-2">
                Filter:
            </div>
            <div class="col-md-3">
            <input type="text" id="txtname" name="txtname" class="txtname"/>
</div>
            <div class="col-md-7">
                <asp:Button ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
                <asp:GridView ID="gridregistered"  Width="100%" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="Email" ForeColor="Black" OnPageIndexChanging="gridregistered_PageIndexChanging" GridLines="Vertical" AllowPaging="True">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" >
                        <ControlStyle Width="100px" />
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Email" HeaderText="Email Id" >
                        <ControlStyle Width="200px" />
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Mob" HeaderText="Mobile" />
                        <asp:BoundField DataField="Addr" HeaderText="Address" >
                        <ControlStyle Width="800px" />
                        <ItemStyle Width="500px" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery2.0.3.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            $(".txtname").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "registeredusers.aspx/Getname",
                        data: "{'Name':'" + $(document.activeElement).val() + "'}",
                        //data: "{'drugs':'" + $('input[name="txtbox[]"]').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },

            });
        });
        
    </script>
</asp:Content>


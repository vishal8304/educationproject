﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Addnewtopic : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        SqlCommand cmd = new SqlCommand("insert into Newtopic values(@t1,@t2,@t3)", con);
        cmd.Parameters.AddWithValue("@t1",Request.Form["topic"]);
        cmd.Parameters.AddWithValue("@t2",content);
        cmd.Parameters.AddWithValue("@t3",System.DateTime.Now.ToString());
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblsaved.Visible = true;
            lblsaved.Text = "Uploaded successfully";
        }

    }
}
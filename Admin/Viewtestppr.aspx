﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Viewtestppr.aspx.cs" Inherits="Admin_Viewtestppr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .Grid {background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse; font-family:Calibri; color: #474747;}

.Grid td {

      padding: 2px;

      border: solid 1px #c1c1c1; }

.Grid th {

      padding : 4px 2px;

      color: #fff;

      background: #363670 url(Images/grid-header.png) repeat-x top;

      border-left: solid 1px #525252;

      font-size: 0.9em; }

.Grid .alt {

      background: #fcfcfc url(Images/grid-alt.png) repeat-x top; }

.Grid .pgr {background: #363670 url(Images/grid-pgr.png) repeat-x top; }

.Grid .pgr table { margin: 3px 0; }

.Grid .pgr td { border-width: 0; padding: 0 6px; border-left: solid 1px #666; font-weight: bold; color: #fff; line-height: 12px; }  

.Grid .pgr a { color: Gray; text-decoration: none; }

.Grid .pgr a:hover { color: #000; text-decoration: none; }

   .wraps{
       word-wrap:break-word;
   }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-3">
            <asp:DropDownList ID="ddlselect" ClientIDMode="Static" runat="server">
          <asp:ListItem>--SELECT--</asp:ListItem>
          <asp:ListItem>SSC Quiz</asp:ListItem>
          <asp:ListItem>SSC Banking</asp:ListItem>
      </asp:DropDownList>
        </div>
        <div class="col-md-9" style="text-align:right">
            <asp:Label ID="lblConfirm" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        </div> 
    </div>
      <div style="overflow:scroll; width: 1020px;">
     <asp:GridView ID="gvEmployee" OnRowDeleting="gvEmployee_RowDeleting" ClientIDMode="Static" runat="server" AutoGenerateColumns="false" Width="100%"

                      CssClass="Grid" style="width:100%" 

                      AlternatingRowStyle-CssClass="alt"

                      PagerStyle-CssClass="pgr" DataKeyNames="Id" > 
            <Columns>
                 <asp:TemplateField>
	<ItemTemplate>
		<asp:LinkButton ID="lbldel" ForeColor="Brown" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this entry?');">Delete </asp:LinkButton>
	</ItemTemplate>
</asp:TemplateField>
                  <asp:TemplateField>  
                        <HeaderTemplate>   
                            <asp:CheckBox ID="chkAllSelect" runat="server" onclick="CheckAll(this);" />  
                        </HeaderTemplate>  
                        <ItemTemplate>  
                            <asp:CheckBox ID="chkSelect" runat="server" />  
                        </ItemTemplate>  
                    </asp:TemplateField>  
                <asp:TemplateField>
                    <ItemTemplate>
                        
            <div class="row">
                <div class="col-md-1">
                    Question:
                </div>
                <div class="col-md-11">
                     <asp:Label ID="lblqs" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("qs")) %>' runat="server"></asp:Label>
                </div>
                 </div>
                      <div class="row">
                          <div class="col-md-1">
                              Option1:
                          </div>
                          <div class="col-md-11">
                              <asp:Label ID="lblopt1" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("opt1")) %>' runat="server"></asp:Label>
                          </div> 
                      </div>
                        <div class="row">
                            <div class="col-md-1">
                                Option2:
                            </div>
                            <div class="col-md-11">
                                <asp:Label ID="lblopt2" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("opt2")) %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Option3:
                            </div>
                            <div class="col-md-11">
                                <asp:Label ID="lblopt3" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("opt3")) %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Option4:
                            </div>
                            <div class="col-md-11">
                                <asp:Label ID="lblopt4" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("opt4")) %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Option5:
                            </div>
                            <div class="col-md-11">
                                <asp:Label ID="lblopt5" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("opt5")) %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Correct Value:
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblcorval" CssClass="wraps" Text='<%#Eval("corval") %>' runat="server"></asp:Label>
                            </div>
                            
                            <div class="col-md-2"> Subject:
                                <asp:Label ID="lblSub" CssClass="wraps" Text='<%#Eval("Sub") %>' runat="server"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                Chapter:
                                <asp:Label ID="lblchptr" CssClass="wraps" Text='<%#Eval("Chapter") %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Solution:
                            </div>
                            <div class="col-md-11">
                                <asp:Label ID="lblsol" CssClass="wraps" Text='<%#System.Web.HttpUtility.HtmlDecode((string)Eval("solution")) %>' runat="server"></asp:Label>
                            </div>
                        </div>
                        
                       </ItemTemplate>
                  
            </asp:TemplateField>
                </Columns>
            </asp:GridView>  
   </div>
   
    <asp:Button ID="btnfetch" runat="server" ClientIDMode="Static" OnClick="btnfetch_Click" Text="Submit" />
     <script type="text/javascript">  
// for check all checkbox  
        function CheckAll(Checkbox) {  
            var GridVwHeaderCheckbox = document.getElementById("<%=gvEmployee.ClientID %>");  
            for (i = 1; i < GridVwHeaderCheckbox.rows.length; i++) {  
                GridVwHeaderCheckbox.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;  
            }  
        }  
    </script> 
    <script>
        $("#btnfetch").click(function () {
            debugger
            var ddlselect= $("#ddlselect option:selected").val();
            if (ddlselect == "--SELECT--")
            {
                alert("Plz choose Field");
                return false;
            }
        });
        
    </script>
</asp:Content>


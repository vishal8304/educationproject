﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="currentaffairs.aspx.cs" ValidateRequest = "false" Inherits="Admin_currentaffairs" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row" style="text-align:center">
            <div class="col-md-12">
               <h2>Current Affairs</h2> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-3">
                Search by Date(with Time):
            </div>
            <div class="col-md-3">
                <asp:DropDownList ID="ddldates" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddldates_SelectedIndexChanged">
                    <asp:ListItem>Choose</asp:ListItem>
                </asp:DropDownList>
            </div>
             <div class="col-md-1">
                <asp:Button ID="btnAdd" runat="server" Text="Edit" OnClick="btnAdd_Click" />
            </div>
            
            <div class="col-md-5">
                <asp:Button ID="btnDel" runat="server" Text="Delete" OnClick="btnDel_Click" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-2">
                Section
            </div>
            <div class="col-md-2">
                <select id="choose" name="Choose">
                    <option value="choose">--Select--</option>
  <option value="Hindi">Hindi</option>
  <option value="English">English</option>
</select>
            </div>
           <div class="col-md-6"></div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
      <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> <br />
                </div>
        </div>
        <div class="row">
            <div class="col-md-5" style="text-align:right">
                <asp:Button ID="btnsave" ClientIDMode="Static" runat="server" Text="Save" OnClick="btnsave_Click" />
            </div>
            <div class="col-md-3">
                <asp:Label ID="lblsaved" Visible="false" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
            <asp:GridView ID="grid1" runat="server">

            </asp:GridView> 
            </div>
        </div>
    </div>
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
    <script>
        $("#btnsave").click(function () {
            var dropdown = $('#choose').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please choose section");
                return false;
            }
        });
    </script>
  
         <%-- <script>  
              $(function () {
                  $('#txtdatefrom').datepicker(
                  {
                      dateFormat: 'dd/mm/yy',
                      changeMonth: true,
                      changeYear: true,
                      yearRange: '1950:2017'
                  });
              });
</script>  
  <script type="text/javascript">
        $(function () {
            $('#txtdateto').datepicker({
                dateFormat: "m/d/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>--%>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Bankonline.aspx.cs" Inherits="Admin_Bankonline" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .text{
            width:140px;
        }

        .textbox{
           width:220px;
            height:150px;
        }
        .grid{
             margin: 0px 13px 19px 0px;
             width:100%;
        }
          
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row">
                 <div class="col-md-2">
                     Add Title:
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtset" runat="server" Width="180px"></asp:TextBox>
                 </div>
                  <div class="col-md-1">
                      <asp:Button ID="btnsaveset" OnClick="btnsaveset_Click" runat="server" Text="Add" />
                  </div>
                  <div class="col-md-3">
                      <asp:Label ID="lblset" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                  </div>
          <div class="col-md-3">
              <asp:Label ID="Label1" ClientIDMode="Static" Visible="false" ForeColor="Red" runat="server" ></asp:Label>
          </div> 
             </div><br />
  <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 style="background-color: #FFFFFF; font-weight: bold; font-style: italic; margin-bottom: 5%; font-variant: small-caps; text-transform: uppercase; color: #000000; text-align: center">SSC Online Test </h3>
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="text-align: center; margin-bottom: 2%;"><strong>Instructions</strong></h3>
                        <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
                        </CKEditor:CKEditorControl>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <%--<asp:Button ID="btninstruct" runat="server" Text="Submit" OnClick="btninstruct_Click" />--%>
                        <asp:Label ID="lblsave" ForeColor="Red" Visible="false" runat="server"></asp:Label>
                    </div>
                </div>
                <br />
                <%-- <div class="row" style="border-top-style:solid;border-color:black;"><br />
                 <div class="col-md-2">
                     Add Set:
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtset" runat="server" Width="180px"></asp:TextBox>
                 </div>
                  <div class="col-md-2">
                      <asp:Button ID="btnsaveset" OnClick="btnsaveset_Click" runat="server" Text="Add" />
                  </div>
                  <div class="col-md-5">
                      <asp:Label ID="lblset" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                  </div>
             </div><br />--%>
               <%-- <div class="row">
                    
                    <asp:DropDownList ID="DropDownList1" runat="server">
                          <asp:ListItem Enabled="true" Text="Select Quiz Type" Value="-1"></asp:ListItem>
    <asp:ListItem Text="SSC" Value="SSC"></asp:ListItem>
    <asp:ListItem Text="BANK" Value="2"></asp:ListItem>
    <asp:ListItem Text="December" Value="12"></asp:ListItem>

                    </asp:DropDownList>
                </div>--%>
                <div class="row">
                    <div class="col-md-1">
                        No. of Questions   
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtAddNoOfRecord" TextMode="Number" runat="server" Width="50px" Style="margin-left: 5px"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btnAddRow" runat="server" Text="Add Rows" OnClick="btnAddRow_Click" />
                    </div>
                    <div class="col-md-1" style="margin-left: 3%;">
                        Time(in Minute):
                    </div>
                    <div class="col-md-1">
                        <asp:DropDownList ID="ddltime" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        Marks:
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtmarks" ClientIDMode="Static" runat="server" Width="40px" Style="margin-left: 5px" TextMode="Number"></asp:TextBox>
                    </div>
                    Selected Title:
                 <asp:DropDownList ID="ddlsscset" ClientIDMode="Static" runat="server"></asp:DropDownList>
                  Test  Name: <asp:textbox ID="ddlname" runat="server"></asp:textbox>
                    <%--<asp:Button ID="btnupdate" runat="server" ClientIDMode="Static" Text="Update" OnClick="btnupdate_Click" />--%>
                    <asp:Label ID="lblupdate" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                    <br />
                    <br />
                </div>

                <asp:GridView ID="GridAddEmp" runat="server" CssClass="grid" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <br />

                                <div class="row" style="margin-left: 15px">
                                    <div class="col-md-1">
                                        <%#Container.DataItemIndex +1 %>
                                    </div>
                                    <div class="col-md-10">
                                        <asp:TextBox ID="txtqs" runat="server" Width="100%"></asp:TextBox>
                                       <%-- <textarea id="txtqs" style="width:100%"></textarea>--%>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px;">
                                    <div class="col-md-6">
                                        A.
                                    <asp:TextBox ID="opt1" runat="server" Width="100%"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6">
                                        B.
                   <asp:TextBox ID="opt2" runat="server" Width="100%"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px">
                                    <div class="col-md-6">
                                        C.
                    <asp:TextBox ID="opt3" runat="server" Width="100%"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6">
                                        D.
              <asp:TextBox ID="opt4" runat="server" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                </div>
                    <%--<div class="row" id="5throw" hidden="hidden" style="margin-left:15px">
                        <div class="col-md-12">E.
                    <asp:TextBox ID="opt5" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                    </div>--%>

                                <%--<asp:TextBox ID="opt2" runat="server"></asp:TextBox>--%>
                                <div class="row">
                                    <div class="col-md-12" style="color: black">
                                        Solution:
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px;">
                                    <div class="col-md-12">
                                       <asp:TextBox ID="txtdesc" runat="server" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <br />
                                <div class="row" style="margin-left: 15px">

                                    <div class="col-md-3">
                                        Correct Option
                            <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                                <asp:ListItem>A</asp:ListItem>
                                <asp:ListItem>B</asp:ListItem>
                                <asp:ListItem>C</asp:ListItem>
                                <asp:ListItem>D</asp:ListItem>
                            </asp:DropDownList>
                                    </div>
                                <br />

                                <br />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </div>


            <%--<asp:GridView ID="GridView1" runat="server" CssClass="grid" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
        <Columns> 
            <asp:TemplateField>  
                <ItemTemplate>    <br />
                    
                    <div class="row" style="margin-left:15px">
                        <div class="col-md-1">
                            <%#Container.DataItemIndex +1 %>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox ID="TextBox1" runat="server" Text
                                ="multiline" Font-Name="editor1" ></asp:TextBox> 
                        </div>
                       <div class="col-md-3">
                           
                       </div>
                    </div>
                <div class="row" style="margin-left:15px;">
                    <div class="col-md-6">A.
                           <asp:TextBox ID="TextBox2" runat="server" Text
                                ="multiline" Font-Name="editor2" ></asp:TextBox> 
                    </div>
                    <div class="col-md-6">B.
                     <asp:TextBox ID="TextBox3" runat="server" Text
                                ="multiline" Font-Name="editor3
                         " ></asp:TextBox> 
                    </div>
                </div>
                    <div class="row"  style="margin-left:15px">
                        <div class="col-md-6">C.
                    <asp:TextBox ID="TextBox4" runat="server" Text
                                ="multiline" Font-Names="editor4" ></asp:TextBox> 
                        </div>
                        <div class="col-md-6">D.
                    <asp:TextBox ID="TextBox5" runat="server" Text
                                ="multiline" Font-Names="editor5" ></asp:TextBox> 

                        </div>
                    </div>
                    <%--<div class="row" id="5throw" hidden="hidden" style="margin-left:15px">
                        <div class="col-md-12">E.
                    <asp:TextBox ID="opt5" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                    </div>--%>

            <%--<asp:TextBox ID="opt2" runat="server"></asp:TextBox>--%>
            <%--<div class="row">
                    <div class="col-md-12" style="color:black">
                        Solution:
                    </div>
                </div>--%>
            <%--     <div class="row" style="margin-left:15px;">
                         <div class="col-md-12">
                             
                              <asp:TextBox ID="TextBox6" runat="server" Text
                                ="multiline" Font-Names="editor4" ></asp:TextBox>                          </div>
                     </div><br />--%>
            <%--     <div class="row" style="margin-left:15px">
                   
                     <div class="col-md-3">Correct Option
                            <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                    </asp:DropDownList>
                        </div>
                        
                        <div class="col-md-1">
                            Section:
                        </div>
                        <div class="col-md-8">
                            <asp:DropDownList ID="ddlsection" runat="server">
                                <asp:ListItem>Quantitative Aptitude</asp:ListItem>
                                <asp:ListItem>English</asp:ListItem>
                                <asp:ListItem>Reasoning</asp:ListItem>
                                <asp:ListItem>General Awareness</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        </div><br />
                     
                    <div class="row">--%>

            <%--<div class="col-md-1">
                            Image:
                        </div>
                        <div class="col-md-4">
                              <CKEditor:CKEditorControl ID="CKEditor2" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
                        </div>--%>
            <%--<div class="col-md-1">
                            Video:
                        </div>
                        <div class="col-md-6">
                             <asp:FileUpload ID="filevedio" runat="server" ClientIDMode="Static" />
                        </div>--%>
            <%--     </div>
                    <br />
                </ItemTemplate>   
            </asp:TemplateField>
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
    </asp:GridView>--%>
        </div>
        <div style="padding: 10px 0px;">
            <asp:Panel ID="PanelData" runat="server">
                <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnSave_Click" /><br />
                <asp:Label ID="lblMsg" Visible="false" ClientIDMode="Static" runat="server"></asp:Label>
            </asp:Panel>
        </div>

        <%--         <div class="row">
            <div class="col-md-12">
                <strong><asp:Label ID="lblupload" runat="server" Text="Label" Visible="False" ForeColor="#CC3300"></asp:Label></strong>
            </div>
        </div>
         <div class="row">--%>
        <%-- <div class="col-md-2">
                 <asp:TextBox ID="txtid" runat="server" ClientIDMode="Static" Width="80px" Enabled="False"></asp:TextBox></div>
            <div class="col-md-2">
                <input type="button" value="Change set" id="btnset" onclick="incrementValue()"/>
            </div>--%>
        <%--<div class="col-md-2">
                 Category
             </div>
             <div class="col-md-2">
                 <asp:DropDownList ID="DropDownList1" ClientIDMode="Static" runat="server">
                     <asp:ListItem>SSC</asp:ListItem>
                     <asp:ListItem>Bank</asp:ListItem>
                 </asp:DropDownList>
             </div>
            <div class="col-md-1">Minutes:</div>
             <div class="col-md-7">
                 <asp:DropDownList ID="ddlmin" ClientIDMode="Static" runat="server"></asp:DropDownList>
             </div>
            
         </div>
         <br />
        <div class="row">
            <div class="col-md-2"><strong>Question:</strong></div>
            <div class="col-md-6">
                <asp:TextBox ID="txtqs" runat="server" Height="66px" ClientIDMode="Static" TextMode="MultiLine" Width="342px"></asp:TextBox>
            </div>
            <div class="col-md-2">--%>
        <%--<asp:FileUpload ID="fileupload1" ClientIDMode="Static" runat="server"/>--%>
        <%-- <input type="file" id="FileUpload1" /></div><br />
            <div class="col-md-2">
<input type="button" id="btnUpload" value="Upload Files"/>
            </div>
        </div><br />
        <div class="row">
           <div class="col-md-3"><strong> Options:</strong></div>
            <div class="col-md-9"></div>
        </div><br />
        <div class="row">
            <div class="col-md-6">
                <asp:TextBox ID="txtOption1" runat="server" ClientIDMode="Static"></asp:TextBox>
                <input type="radio" id="rdb1" name="a"/>
            </div>
            <div class="col-md-6"><asp:TextBox ID="txtOption2" runat="server" ClientIDMode="Static"></asp:TextBox>
                 <input type="radio" id="rdb2" name="a" /></div>
        </div><br />
        <div class="row">
            <div class="col-md-6">
                <asp:TextBox ID="txtOption3" runat="server" ClientIDMode="Static"></asp:TextBox>
                 <input type="radio" id="rdb3" name="a" />
            </div>
            <div class="col-md-6">
                <asp:TextBox ID="txtOption4" runat="server" ClientIDMode="Static"></asp:TextBox>
                 <input type="radio" id="rdb4" name="a" />
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <input type="button" id="uploadTest" value="Upload"/></div>
        </div>--%>
        <br />
        <script src="../js/jquery-2.1.4.min.js"></script>
        <script src="../js/increment.js"></script>
        <script src="../js/uploaddata.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
        <%--<script type="text/javascript">
$(function () {
$('#btnUpload').click(function () {
var fileUpload = $('#FileUpload1').get(0);
var files = fileUpload.files;
var test = new FormData();
for (var i = 0; i < files.length; i++) {
test.append(files[i].name, files[i]);
}
$.ajax({
url: "UploadHandler.ashx",
type: "POST",
contentType: false,
processData: false,
data: test,
// dataType: "json",
success: function (result) {
alert(result);
},
error: function (err) {
alert(err.statusText);
}
});
});
})
</script>--%>
        <%-- <script>
        
        $(function () {
            $("#ddlcategory").change(function () {
                var ddl = ($('option:selected', this).text());
                if (ddl == 'BANK') {
                    $("#5throw").show();
                }
                else {
                    $("#5throw").hide();
                }
            });
        });
    </script>--%>
        <script src="../ckeditor/ckeditor.js"></script>
        <script>
            var roxyFileman = '/fileman/index.html';
            $(function () {
                CKEDITOR.replace('CKEditor1', {
                    filebrowserBrowseUrl: roxyFileman,
                    filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                    removeDialogTabs: 'link:upload;image:upload'
                });
            });
        </script>
        <script>
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        </script>
        <script src="../ckeditor/ckeditor.js"></script>
        <script>
            $("#btnSave").click(function () {
                var dropdown = $('#ddlsscset').find(":selected").text();
                if (dropdown == '--Select--') {
                    alert("Please choose Test Name");
                    return false;
                }
            });
        </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Addjob.aspx.cs" Inherits="Admin_Addjob" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .fieldset{
            padding: 12px;
                border-width: medium;
                    border: 2px solid silver;
        }
        .text{
            width:230px;
        }
        
    </style>
    <style type="text/css">
        .GridViewEditRow input[type=text] {width:5px;} /* size textboxes */
.GridViewEditRow select {width:5px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
               <strong>Current Opening</strong>
            </div><div class="col-md-9">
                <asp:Label ID="lblconfirm" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                  </div>
        </div><br />
        <div class="row">
            <div class="col-md-2">
                Add Tiles:
            </div>
            <div class="col-md-5">
                <asp:TextBox ID="txttiles" ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddltile" ClientIDMode="Static" runat="server">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnEdit" ClientIDMode="Static" Text="Edit" OnClick="btnEdit_Click" runat="server" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnDel" ClientIDMode="Static" Text="Delete" OnClick="btnDel_Click" runat="server" />
            </div>
        </div><br />
    
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-11">
                 <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
                 <%--<asp:TextBox runat="server" ID="txtdesc" TextMode="MultiLine" Width="600px" Height="200px"></asp:TextBox>--%>
            </div>
        </div>
       <br />
        <div class="row" style="text-align:center">
            <div class="col-md-12">
                <asp:Button ID="btnupload" ClientIDMode="Static" runat="server" Text="Upload" OnClick="btnupload_Click" />
            </div>
        </div><br />
       <br />
      
</div>
     <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
      <script>
          $("#btnEdit").click(function () {
              var dropdown = $('#ddltile').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please select tile first");
                return false;
            }
          });
           </script>
    <script>
          $("#btnDel").click(function () {
              var dropdown = $('#ddltile').find(":selected").text();
              if (dropdown == '--Select--') {
                  alert("Please select tile first");
                  return false;
              }
          });
    </script>
     
</asp:Content>


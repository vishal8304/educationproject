﻿<%@ WebHandler Language="C#" Class="UploadHandler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public class UploadHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        con.Open();
        string qry="insert into test([path]) values(@t1)";
        SqlCommand cmd = new SqlCommand(qry,con);
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                fname=Path.Combine(context.Server.MapPath("~/uploads/"), fname);
                file.SaveAs(fname);
                cmd.Parameters.AddWithValue("@t1",fname);
                int j = cmd.ExecuteNonQuery();
                
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("File Uploaded Successfully!");
    }
    public bool IsReusable {
        get {
            return false;
        }
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="EditBanking.aspx.cs" Inherits="Admin_EditBanking" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .text{
            width:140px;
        }

        .textbox{
            width:170px;
            height:120px;
        }
        .grid{
             margin: 0px 13px 19px -36px;
        }
           
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
     <div class="row">
         <div class="col-md-12">
     <h3 style="background-color: #FFFFFF; font-weight: bold; font-style: italic; font-variant: small-caps; text-transform: uppercase; color: #000000; width: 1216px; times: ;, ">Bank </h3>  
             </div>
         </div><br />
          <div class="row">
            <div class="col-md-1">
                Section:
            </div>
            <div class="col-md-3">
              <select name="choose" id="choose">
                    <option value="Choose">Choose</option>
                    <option value="Quantitative Aptitude">Quantitative Aptitude</option>
                    <option value="English">English</option>
                    <option value="Reasoning">Reasoning</option>
                    <option value="General Awareness">General Awareness</option>
                    <option value="Computer">Computer</option>
                </select>
            </div>
            <div class="col-md-1">
                Chapter:
            </div>
            <div class="col-md-3">
           <input type="text" required="required" name="Chapters" />
            </div>
            <div class="col-md-1">
               Topic:
            </div>
              <div class="col-md-3">
                  <input type="text" required="required" name="Topic" />
              </div>
        </div><br />
        <div class="row">
            <div class="col-md-12">
                <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl> 
            </div>
        </div><br />
        <div class="row">
            <div class="col-md-5" style="text-align:right">
                <asp:Button ID="btnsave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnsave_Click"/>
            </div>
            <div class="col-md-7">
                <asp:Label ID="Label2" runat="server" Visible="false" ForeColor="DarkBlue"></asp:Label>
            </div>
        </div>
   <%-- <div>  
         Insert No. of Row Record   
        <asp:TextBox ID="txtAddNoOfRecord" runat="server" Width="30px" style="margin-left: 5px"></asp:TextBox>  
           
        <asp:Button ID="btnAddRow" runat="server" Text="Add Rows" OnClick="btnAddRow_Click"  />  
       
        <br />  
        <br />  
    </div>  
    <asp:GridView ID="GridAddEmp" width="750px"  runat="server" CssClass="grid" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
         <Columns>  
            <asp:TemplateField HeaderText="Bank Questions">  
                <ItemTemplate> <br />
                    <div class="row" style="margin-left:15px">
                        <div class="col-md-1">
                            <%#Container.DataItemIndex +1 %>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtqs" ClientIDMode="Static" runat="server" Width="500px" height="80px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                    </div> 
       <div class="row" style="margin-left:15px;">
                    <div class="col-md-6">A.
                         <asp:TextBox ID="opt1" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="col-md-6">B.
                    <asp:TextBox ID="opt2" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  

                    </div>
                </div>--%>
                  <%--  <div class="row"  style="margin-left:15px">
                        <div class="col-md-6">C.
                    <asp:TextBox ID="opt3" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                        <div class="col-md-6">D.
                    <asp:TextBox ID="opt4" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  

                        </div>
                        </div>
            <div class="row"  style="margin-l
    
    eft:15px">
                        <div class="col-md-12">E.
                    <asp:TextBox ID="opt5" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                </div>
                    <div class="row"  style="margin-left:15px">
                        <div class="col-md-4">Correct Option
                            <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                    </asp:DropDownList>
                        </div>
                        <div class="col-md-8">
                             <asp:FileUpload ID="fileupload1" runat="server" ClientIDMode="Static" />
                        </div>
                </div><br />
           </ItemTemplate>   
            </asp:TemplateField>
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
       
    </asp:GridView> --%>
   <%-- <div style="padding:10px 0px;">  
        <asp:Panel ID="PanelData" runat="server" Visible="false">  
            <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnSave_Click" /><br />  
             <asp:Label ID="lblMsg" ClientIDMode="Static" runat="server" ></asp:Label>  
        </asp:Panel>  
    </div>  </div>
    </div>
    <div>
        <b>Database Records</b>  
        <div>  
            <asp:GridView ID="GridEmpData" runat="server" AutoGenerateColumns="False" CellPadding="3" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" DataKeyNames="BankId" AllowPaging="True" PageSize="5">  
                 <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
            <div class="row">
               
                 <div class="col-md-12">
                    <strong>Question</strong>
                     <strong> <%#Container.DataItemIndex+1 %>:</strong>
                    <asp:Label ID="lblqs" runat="server" Text='<%#Eval("Question") %>'></asp:Label>--%>
                    <%--<asp:BoundField DataField="Ques" HeaderText="Question" />--%>
               <%-- </div>
                 </div>
                        <br />
                         <div class="row">
                            <div class="col-md-1">
                                </div>
                             <div class="col-md-10">
                                 <asp:Image ID="imgquiz" runat="server" ImageUrl='<%#Eval("picpath") %>' Height="100px" Width="100%" />--%>
                    <%--  <asp:ImageField DataImageUrlField="path" HeaderText="Image">
                    <ControlStyle Height="120px" Width="120px" />
                        <ItemStyle Height="28px" Width="28px" />
                </asp:ImageField>
           --%>
                        <%--    </div>
                             <div class="col-md-1"></div>
                        </div><br />
                        <div class="row">
                <div class="col-md-6">
                    <strong>Option1: </strong>
                    <asp:Label ID="lblopt1" runat="server" Text='<%#Eval("val1") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val1" HeaderText="A" />--%>
                
     <%--<div class="col-md-6">
          <strong>Option2: </strong>
                    <asp:Label ID="lblopt2" runat="server" Text='<%#Eval("val2") %>'></asp:Label>--%>
                    <%-- <asp:BoundField DataField="val2" HeaderText="B" />--%>
                <%--</div>
            </div><br />
                         <div class="row">
                <div class="col-md-6">
                     <strong>Option3: </strong>
                    <asp:Label ID="lblopt3" runat="server" Text='<%#Eval("val3") %>'></asp:Label>--%>
                    <%--<asp:BoundField DataField="val3" HeaderText="C" />--%>
                <%--</div>
     <div class="col-md-6">
          <strong>Option4: </strong>
                    <asp:Label ID="lblopt4" runat="server" Text='<%#Eval("val4") %>'></asp:Label>--%>
                    <%-- <asp:BoundField DataField="val4" HeaderText="D" />--%>
               <%-- </div>
            </div><br />
              <div class="row">
                  <div class="col-md-6">
                   <strong>Option5: </strong>
                   <asp:Label ID="lblopt5" runat="server" Text='<%#Eval("val5") %>'></asp:Label>
                </div>
                <div class="col-md-6">
                     <strong>Correct Option: </strong>
                    <asp:Label ID="lblcoropt" runat="server" Text='<%#Eval("correctval") %>'></asp:Label>--%>
                    <%--<asp:BoundField DataField="Correctoption" HeaderText="Correct" />--%>
               <%-- </div>
     
            </div><br />
                       </ItemTemplate>
                  <HeaderTemplate>
                      Bank Questions
                  </HeaderTemplate>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                </asp:TemplateField>
                 </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
               
            </asp:GridView>  --%>
        </div>  
     <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../js/jquery-2.1.4.min.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
     <script>
         $("#btnsave").click(function () {
            var dropdown = $('#choose').find(":selected").text();
            if (dropdown == 'Choose') {
                alert("Please choose section");
                return false;
            }
        });
         </script>
</asp:Content>


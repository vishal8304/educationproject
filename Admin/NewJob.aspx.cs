﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_NewJob : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (con != null && con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("insert into NewJob Values(@t1, @t2, @t3)",con);
        cmd.Parameters.AddWithValue("@t1", TextBox1.Text);
        cmd.Parameters.AddWithValue("@t2", TextBox2.Text);
        cmd.Parameters.AddWithValue("@t3", DropDownList1.SelectedValue);
        cmd.ExecuteNonQuery();

    }
}
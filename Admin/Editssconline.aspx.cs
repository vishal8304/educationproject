﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using System.Text;

public partial class Admin_Editssconline : System.Web.UI.Page
{
    string connection = ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        hide.Visible = false;
        if (!IsPostBack)
        {
            connect();
            //bindtiles();
            bindname();
        }
    }
    public void connect()
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from ontimertest order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridEmpData.DataSource = grid;
        GridEmpData.DataBind();
    }

    //public void bindtiles()
    //{
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
    //    if (con.State != ConnectionState.Open)
    //    {
    //        con.Open();
    //    }
    //    SqlDataAdapter da = new SqlDataAdapter("select distinct [set] from ontimertest", con);
    //    DataTable dt = new DataTable();
    //    da.Fill(dt);
    //    if (dt.Rows.Count > 0)
    //    {
    //        ddllist.DataSource = dt;
    //        ddllist.DataTextField = "set";
    //        ddllist.DataValueField = "set";
    //        ddllist.DataBind();
    //        ddllist.Items.Insert(0, "--Select--");
    //    }
    //}
    public void bindname()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["datacon"].ConnectionString);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da1 = new SqlDataAdapter("select distinct name from ontimertest", con);
        DataTable dt1 = new DataTable();
        da1.Fill(dt1);
        if (dt1.Rows.Count > 0)
        {
            ddlname.DataSource = dt1;
            ddlname.DataTextField = "name";
            ddlname.DataValueField = "name";
            ddlname.DataBind();
            ddlname.Items.Insert(0, "--Select--");
        }
    }
    protected void GridEmpData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridEmpData.DataKeys[e.RowIndex].Value);
        SqlCommand cmd = new SqlCommand("alter table onusertest nocheck constraint all delete from onusertest where Id = @t1 delete from ontimertest where Id=@t1 alter table onusertest check constraint all", con);
        cmd.Parameters.AddWithValue("@t1", eno);
        int i = cmd.ExecuteNonQuery();
        connect();
    }
    private void FillEmpData()
    {
        using (SqlConnection con = new SqlConnection(connection))
        {
            using (SqlCommand cmd = new SqlCommand("sp_FillData", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                GridEmpData.DataSource = dt;
                GridEmpData.DataBind();
            }
        }
    }

    protected void GridEmpData_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        GridEmpData.PageIndex = e.NewPageIndex;
        connect();
    }

    protected void GridEmpData_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridEmpData.EditIndex = e.NewEditIndex;
        connect();
    }

    protected void GridEmpData_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        int eno = Convert.ToInt32(GridEmpData.DataKeys[e.RowIndex].Value);
        string qry = "update ontimertest set Question=@t1,Optionval1=@t2,Optionval2=@t3,Optionval3=@t4,Optionval4=@t5,Correctval=@t6 where Id=@t12";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        TextBox txtqs = GridEmpData.Rows[e.RowIndex].FindControl("txtqs") as TextBox;
        TextBox txt1 = GridEmpData.Rows[e.RowIndex].FindControl("txt1") as TextBox;
        TextBox txt2 = GridEmpData.Rows[e.RowIndex].FindControl("txt2") as TextBox;
        TextBox txt3 = GridEmpData.Rows[e.RowIndex].FindControl("txt3") as TextBox;
        TextBox txt4 = GridEmpData.Rows[e.RowIndex].FindControl("txt4") as TextBox;
        TextBox txtcorval = GridEmpData.Rows[e.RowIndex].FindControl("txtcorval") as TextBox;
        TextBox txtsection = GridEmpData.Rows[e.RowIndex].FindControl("txtsection") as TextBox;
        TextBox txtsol = GridEmpData.Rows[e.RowIndex].FindControl("txtsol") as TextBox;
        cmd1.Parameters.AddWithValue("@t1", txtqs.Text); 
        cmd1.Parameters.AddWithValue("@t2", txt1.Text);
        cmd1.Parameters.AddWithValue("@t3", txt2.Text);
        cmd1.Parameters.AddWithValue("@t4", txt3.Text);
        cmd1.Parameters.AddWithValue("@t5", txt4.Text);
        cmd1.Parameters.AddWithValue("@t6", txtcorval.Text);
        cmd1.Parameters.AddWithValue("@t12", eno);
        int i = cmd1.ExecuteNonQuery();
        GridEmpData.EditIndex = -1;
        connect();
    }

    protected void GridEmpData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridEmpData.EditIndex = -1;
        connect();
    }

    protected void btnfilter_Click(object sender, EventArgs e)
    {
        hide.Visible = true;
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from ontimertest where name='"+ddlname.Text+"' order by Id desc", con);
        DataTable grid = new DataTable();
        da.Fill(grid);
        GridEmpData.DataSource = grid;
        GridEmpData.DataBind();
        
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(connection);
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("", con);
        int i = cmd.ExecuteNonQuery();
            lblmsg.Visible = true;
            lblmsg.Text = "Test Deleted Successfully";
    }
}
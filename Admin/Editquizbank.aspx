﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editquizbank.aspx.cs" Inherits="Admin_Editquizbank" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .text{
            width:140px;
        }

        .textbox{
           width:220px;
            height:150px;
        }
        .grid{
             margin: 0px 13px 19px 0px;
             width:100%;
        }
          
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container-fluid">
         
        <div class="row">
         <div class="col-md-12">
     <h3 style="background-color: #FFFFFF; font-weight: bold; font-style: italic; font-variant: small-caps; text-transform: uppercase; color: #000000; width: 1216px; ">Bank Quiz </h3>  
              
    <div class="row">
       <div class="col-md-2">
          <strong> Instruction:</strong>
       </div>
        <div class="col-md-10">
            <CKEditor:CKEditorControl ID="CKEditor1" ClientIDMode="Static" runat="server">
</CKEditor:CKEditorControl>
        </div>
         
    </div><br />
             <div class="row">
                 <div class="col-md-12" style="text-align:center">
            <%--<asp:Button ID="btninstruct" runat="server" Text="Submit" OnClick="btninstruct_Click" />--%>
                     <asp:Label ID="lblsave" ForeColor="Red" Visible="false" runat="server"></asp:Label>
                 </div>
             </div><br />
              <%--<div class="row" style="border-top-style:solid;border-color:black;"><br />
                 <div class="col-md-2">
                     Add Set:
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtset" runat="server" Width="180px"></asp:TextBox>
                 </div>
                  <div class="col-md-2">
                      <asp:Button ID="btnsaveset" OnClick="btnsaveset_Click" runat="server" Text="Add" />
                  </div>
                  <div class="col-md-5">
                      <asp:Label ID="lblset" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                  </div>
             </div><br />--%>
             
             <div>  
         Insert No. of Row Record   
        <asp:TextBox ID="txtAddNoOfRecord" TextMode="Number" runat="server" Width="30px" style="margin-left: 5px"></asp:TextBox>  
           
        <asp:Button ID="btnAddRow" runat="server" Text="Add Rows" OnClick="btnAddRow_Click"  />  
        Time:
        <asp:DropDownList ID="ddltime" runat="server" ClientIDMode="Static"></asp:DropDownList>
         Marks:
        <asp:TextBox ID="txtmarks" ClientIDMode="Static" runat="server" Width="40px" style="margin-left: 5px" TextMode="Number"></asp:TextBox>  
                 Test Name:
                 <asp:textbox ID="ddlsscset" ClientIDMode="Static" runat="server"></asp:textbox>
                 <%--<asp:Button ID="btnupdate" runat="server" ClientIDMode="Static" Text="Update" OnClick="btnupdate_Click" />--%>
                 <asp:Label ID="lblupdate" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        <br />
        <br />
    </div>
    <asp:GridView ID="GridAddEmp" runat="server" CssClass="grid" AutoGenerateColumns="False" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">  
        <Columns> 
            <asp:TemplateField>  
                <ItemTemplate>    <br />
                    
                    <div class="row" style="margin-left:15px">
                        <div class="col-md-1">
                            <%#Container.DataItemIndex +1 %>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtqs" ClientIDMode="Static" runat="server" Width="100%" height="100px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                       <div class="col-md-3">
                           <asp:FileUpload ID="fileupload2" runat="server" />
                       </div>
                    </div>
                <div class="row" style="margin-left:15px;">
                    <div class="col-md-6">A.
                         <asp:TextBox ID="opt1" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="col-md-6">B.
                    <asp:TextBox ID="opt2" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  

                    </div>
                </div>
                    <div class="row"  style="margin-left:15px">
                        <div class="col-md-6">C.
                    <asp:TextBox ID="opt3" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                        <div class="col-md-6">D.
                    <asp:TextBox ID="opt4" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  

                        </div>
                    </div>
                    <div class="row" style="margin-left:15px">
                        <div class="col-md-12">E.
                    <asp:TextBox ID="opt5" ClientIDMode="Static" runat="server" Width="200px" height="50px" TextMode="MultiLine"></asp:TextBox>  
                        </div>
                    </div>
                    
                    <%--<asp:TextBox ID="opt2" runat="server"></asp:TextBox>--%>  
                <div class="row">
                    <div class="col-md-12" style="color:black">
                        Solution:
                    </div>
                </div>
                     <div class="row" style="margin-left:15px;">
                         <div class="col-md-12">
                             
                             <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="100%" height="150px"></asp:TextBox>
                         </div>
                     </div><br />
                    <div class="row" style="margin-left:15px">
                     <div class="col-md-3">Correct Option
                            <asp:DropDownList ID="ddlopt" runat="server" ClientIDMode="Static">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                        <asp:ListItem>E</asp:ListItem>
                    </asp:DropDownList>
                        </div>
                        <%--<div class="col-md-1">
                            Set:
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlset" runat="server"></asp:DropDownList>
                        </div>
                       <div class="col-md-1">
                           Name:
                       </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlname" runat="server"></asp:DropDownList>
                        </div>--%>
                        <div class="col-md-1">
                            Section:
                        </div>
                        <div class="col-md-8">
                            <asp:DropDownList ID="ddlsection" runat="server">
                                <asp:ListItem>Quantitative Aptitude</asp:ListItem>
                                <asp:ListItem>English</asp:ListItem>
                                <asp:ListItem>Reasoning</asp:ListItem>
                                <asp:ListItem>General Awareness</asp:ListItem>
                                <asp:ListItem>Computer</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        </div><br />
                     
                    <div class="row">
                        
                        <div class="col-md-1">
                            Image:
                        </div>
                        <div class="col-md-4">
                             <asp:FileUpload ID="fileupload1" runat="server" ClientIDMode="Static" />
                        </div>
                        <div class="col-md-1">
                            Video:
                        </div>
                        <div class="col-md-6">
                             <asp:FileUpload ID="filevedio" runat="server" ClientIDMode="Static" />
                        </div>
                </div>
                    <br />
                </ItemTemplate>   
            </asp:TemplateField>
        </Columns>  
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />  
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />  
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />  
        <RowStyle BackColor="White" ForeColor="#330099" />  
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />  
        <SortedAscendingCellStyle BackColor="#FEFCEB" />  
        <SortedAscendingHeaderStyle BackColor="#AF0101" />  
        <SortedDescendingCellStyle BackColor="#F6F0C0" />  
        <SortedDescendingHeaderStyle BackColor="#7E0000" />  
    </asp:GridView>  
    <div style="padding:10px 0px;">  
        <asp:Panel ID="PanelData" runat="server">  
            <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" Text="Save" OnClick="btnSave_Click" /><br />  
             <asp:Label ID="lblMsg" Visible="false" ClientIDMode="Static" runat="server" ></asp:Label>
        </asp:Panel>  
    </div>  </div>
    </div><br />
        
        <br />
    </div>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/increment.js"></script>
    <script src="../js/uploaddata.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
<%--<script type="text/javascript">
$(function () {
$('#btnUpload').click(function () {
var fileUpload = $('#FileUpload1').get(0);
var files = fileUpload.files;
var test = new FormData();
for (var i = 0; i < files.length; i++) {
test.append(files[i].name, files[i]);
}
$.ajax({
url: "UploadHandler.ashx",
type: "POST",
contentType: false,
processData: false,
data: test,
// dataType: "json",
success: function (result) {
alert(result);
},
error: function (err) {
alert(err.statusText);
}
});
});
})
</script>--%>
   <%-- <script>
        
        $(function () {
            $("#ddlcategory").change(function () {
                var ddl = ($('option:selected', this).text());
                if (ddl == 'BANK') {
                    $("#5throw").show();
                }
                else {
                    $("#5throw").hide();
                }
            });
        });
    </script>--%>
    <script> 
var roxyFileman = '/fileman/index.html'; 
$(function(){
    CKEDITOR.replace('CKEditor1', {
        filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                removeDialogTabs: 'link:upload;image:upload'}); 
});
 </script>
    <script>
        $(function () {
            $('.editors').each(function () {
                CKEDITOR.replace($(this).attr('id'));
            });
        });
    </script>
    <script>
        $("#btnSave").click(function () {
            var dropdown = $('#ddlsscset').find(":selected").text();
            if (dropdown == '--Select--') {
                alert("Please choose Test Name");
                return false;
            }
        });
    </script>
</asp:Content>


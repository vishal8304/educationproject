﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Editstudyssc : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            if (ddltopic.SelectedIndex == 0)
            {
                SqlCommand cmd = new SqlCommand("select Top 1 * from SSC where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "'", con);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    string str = dr["content"].ToString();
                    CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
                }
                dr.Close();
                con.Close();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select Top 1 * from SSC where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "' and Topic='"+ddltopic.Text+"'", con);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    string str = dr["content"].ToString();
                    CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
                }
                dr.Close();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }

    protected void btndel_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        if (ddlchptr.SelectedIndex > 0)
        {
            string str = HttpUtility.HtmlEncode(CKEditor1.Text);
            if (ddltopic.SelectedIndex == 0)
            {
                string qry = "delete from SSC where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "'";
                SqlCommand cmd = new SqlCommand(qry, con);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblsaved.Visible = true;
                    lblsaved.Text = "Deleted successfully Successfully";
                }
            }
            else
            {
                string qry = "delete from SSC where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "' and Topic='"+ddltopic.Text+"'";
                SqlCommand cmd = new SqlCommand(qry, con);
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    lblsaved.Visible = true;
                    lblsaved.Text = "Deleted successfully";
                }
            }
            con.Close();
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string str = HttpUtility.HtmlEncode(CKEditor1.Text);
        if (ddltopic.SelectedIndex == 0)
        {
            string qry = "update SSC set [content]=@t1 where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.Parameters.AddWithValue("@t1", str);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblsaved.Visible = true;
                lblsaved.Text = "Updated Successfully";
            }
        }
        else
        {
            string qry = "update SSC set [content]=@t1 where Chapter='" + ddlchptr.Text + "' and section='" + ddlsection.Text + "' and Topic='"+ddltopic.Text+"'";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.Parameters.AddWithValue("@t1", str);
            int i = cmd.ExecuteNonQuery();
            if (i == 1)
            {
                lblsaved.Visible = true;
                lblsaved.Text = "Updated Successfully";
            }
        }
        con.Close();
    }

    protected void ddlsection_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select Chapter from SSC where section='"+ddlsection.SelectedItem.Text+"'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddlchptr.DataSource = dt;
                ddlchptr.DataTextField = "Chapter";
                ddlchptr.DataValueField = "Chapter";
                ddlchptr.DataBind();
                ddlchptr.Items.Insert(0, "--Select--");
            }
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }

    protected void ddlchptr_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select Topic from SSC where section='" + ddlsection.SelectedItem.Text + "' and Chapter='"+ddlchptr.Text+"'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                ddltopic.DataSource = dt;
                ddltopic.DataTextField = "Topic";
                ddltopic.DataValueField = "Topic";
                ddltopic.DataBind();
                ddltopic.Items.Insert(0, "--Select--");
            }
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write("", ex);
        }
    }
}
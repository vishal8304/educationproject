﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Editssconline.aspx.cs" Inherits="Admin_Editssconline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <div class="row">
        <%--    <div class="col-md-1">
                Tiles:
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddllist" runat="server"></asp:DropDownList>
            </div>--%>
            <div class="col-md-2">
                Test Name:
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlname" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-1" style="margin-left: 6%;">
                <asp:Button ID="btnfilter" OnClick="btnfilter_Click" runat="server" Text="Filter" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btndelete" runat="server" Text="Delete Tile" ClientIDMode="Static" OnClick="btndelete_Click" />
            </div>
            <div class="col-md-3">
             <asp:Label ID="lblmsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                </div>
        </div><br />
       <asp:Panel  ID="hide" runat="server">
        <b>Database Records</b>  
        <div>  
            <asp:GridView ID="GridEmpData" runat="server" Width="100%" OnPageIndexChanging="GridEmpData_PageIndexChanging1" OnRowEditing="GridEmpData_RowEditing" OnRowUpdating="GridEmpData_RowUpdating" OnRowCancelingEdit="GridEmpData_RowCancelingEdit" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="Id" ForeColor="Black" GridLines="Vertical" AllowPaging="True" OnRowDeleting="GridEmpData_RowDeleting">
           <%-- <AlternatingRowStyle BackColor="White" />--%>
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <div class="row">
                            <div class="col-md-12">Question:
                        <asp:TextBox ID="txtqs" runat="server" TextMode="MultiLine" Width="100%" Height="100px" Text='<%#Bind("Question") %>'></asp:TextBox>
                            </div>
                        </div><br />
                        <div class="row">
                        <div class="col-md-6">
                            Option1:
                            <asp:TextBox ID="txt1" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("Optionval1") %>'></asp:TextBox>
                        </div>
                            <div class="col-md-6">
                                Option2:
                                 <asp:TextBox ID="txt2" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("Optionval2") %>'></asp:TextBox>
                            </div>
                        </div><br />
                         <div class="row">
                        <div class="col-md-6">
                            Option3:
                            <asp:TextBox ID="txt3" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("Optionval3") %>'></asp:TextBox>
                        </div>
                            <div class="col-md-6">
                                Option4:
                                 <asp:TextBox ID="txt4" runat="server" TextMode="MultiLine" Width="100%" Height="70px" Text='<%#Bind("Optionval4") %>'></asp:TextBox>
                            </div>
                        </div><br />
                          <div class="row">
                <div class="col-md-6">
                     <strong>Correct Option: </strong>
                    <asp:textbox ID="txtcorval" runat="server" Text='<%#Bind("Correctval") %>'></asp:textbox>
                    <%--<asp:BoundField DataField="Correctoption" HeaderText="Correct" />--%>
                </div>
     <div class="col-md-6">
                  <strong>Section: </strong>
                    <asp:textbox ID="txtsection" runat="server" Text='<%#Bind("section") %>'></asp:textbox>
                    <%--<asp:BoundField DataField="Setno" HeaderText="Field" />--%>
                </div>
            </div><br />
                         <div class="row">
                            <div class="col-md-12">
                                <strong>Solution:</strong>
                                 <asp:Textbox ID="txtsol" TextMode="MultiLine" Width="100%" Height="100px" runat="server" Text='<%#Bind("descr") %>'></asp:Textbox>
                            </div>
                        </div><br />
                        <div class="row">
                        <div class="col-md-12" style="text-align:right">
                            <asp:Button ID="btnupdate" runat="server" Text="Update" CommandName="Update" />
                             <asp:Button ID="btnEdit" runat="server" Text="Cancel" CommandName="Cancel" />
                        </div>
                            </div>
                    </EditItemTemplate>
                    <ItemTemplate>
            <div class="row">
                 <div class="col-md-12">
                    <strong>Question</strong>
                     <strong> <%#Container.DataItemIndex+1 %>:</strong>
                    <asp:Label ID="lblqs" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="Question" HeaderText="Question" />--%>
                </div>
                 </div>
                        <br />
<div class="row">
                <div class="col-md-6">
                    <strong>Option1: </strong>
                    <asp:Label ID="lblopt1" runat="server" Text='<%#Eval("Optionval1") %>'></asp:Label>
                    <%--<asp:BoundField DataField="Optionval1" HeaderText="A" >--%>
                </div>
     <div class="col-md-6">
          <strong>Option2: </strong>
                    <asp:Label ID="lblopt2" runat="server" Text='<%#Eval("Optionval2") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="Optionval2" HeaderText="B" >--%>
                </div>
            </div><br />
                        <div class="row">
                <div class="col-md-6">
                     <strong>Option3: </strong>
                    <asp:Label ID="lblopt3" runat="server" Text='<%#Eval("Optionval3") %>'></asp:Label>
                    <%--<asp:BoundField DataField="val3" HeaderText="C" />--%>
                </div>
     <div class="col-md-6">
          <strong>Option4: </strong>
                    <asp:Label ID="lblopt4" runat="server" Text='<%#Eval("Optionval4") %>'></asp:Label>
                    <%-- <asp:BoundField DataField="val4" HeaderText="D" />--%>
                </div>
            </div><br />
                         <div class="row">
                <div class="col-md-6">
                     <strong>Correct Option: </strong>
                    <asp:Label ID="lblcoropt" runat="server" Text='<%#Eval("Correctval") %>'></asp:Label>
                    <%--<asp:BoundField DataField="Correctoption" HeaderText="Correct" />--%>
                </div>
     <div class="col-md-6">
                  <strong>Section: </strong>
                    <asp:Label ID="lblset" runat="server" Text='<%#Eval("section") %>'></asp:Label>
                    <%--<asp:BoundField DataField="Setno" HeaderText="Field" />--%>
                </div>
            </div><br />
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Solution:</strong>
                                 <asp:Label ID="lblsol" runat="server" Text='<%#Eval("descr") %>'></asp:Label>
                            </div>
                        </div><br />
                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-1">
                                <asp:LinkButton ID="lbldel" ForeColor="Brown" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this entry?');">Delete </asp:LinkButton>
                            </div>
                              <div class="col-md-1">
                                <asp:LinkButton ID="lbledit" ForeColor="Brown" runat="server" CommandName="Edit">Edit </asp:LinkButton>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
            </ItemTemplate>
                  <HeaderTemplate>
                      Quiz Questions
                  </HeaderTemplate>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White"/>
                </asp:TemplateField>
                 </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView><br />
            <%--<asp:GridView ID="GridEmpData" runat="server" AutoGenerateColumns="False" CellPadding="3" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" DataKeyNames="Id" AllowPaging="True" OnPageIndexChanging="GridEmpData_PageIndexChanging" PageSize="5">  
                
                <Columns>
                    <asp:BoundField DataField="Question" HeaderText="Question" />
                    <asp:BoundField DataField="Optionval1" HeaderText="A" >
                    <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval2" HeaderText="B" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval3" HeaderText="C" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Optionval4" HeaderText="D" >
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="time" HeaderText="Time" />
                    <asp:ImageField DataImageUrlField="path" HeaderText="Image">
                        <ControlStyle Height="120px" Width="120px" />
                        <ItemStyle Height="28px" Width="28px" />
                    </asp:ImageField>
                </Columns>
                
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />  
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />  
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />  
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />  
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />  
                <SortedAscendingCellStyle BackColor="#FFF1D4" />  
                <SortedAscendingHeaderStyle BackColor="#B95C30" />  
                <SortedDescendingCellStyle BackColor="#F1E5CE" />  
                <SortedDescendingHeaderStyle BackColor="#93451F" />  
            </asp:GridView>--%>  
        </div> 
           </asp:Panel> 
    </div>
</asp:Content>


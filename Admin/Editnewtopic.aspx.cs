﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Editnewtopic : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlist();
        }
    }
    public void bindlist()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select DISTINCT Topic from Newtopic", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        ddlpprs.DataSource = ds;
        ddlpprs.DataTextField = "Topic";
        ddlpprs.DataValueField = "Topic";
        ddlpprs.DataBind();
        ddlpprs.Items.Insert(0, new ListItem("Choose", "Choose"));
    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        string ddllist = ddlpprs.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from Newtopic where [Topic]=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Description"].ToString();
            CKEditor1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
    }

    protected void btndel_Click(object sender, EventArgs e)
    {
        string ddllist = ddlpprs.SelectedItem.Text;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("delete from Newtopic where Topic=@t1", con);
        cmd.Parameters.AddWithValue("@t1", ddllist);
        int i = cmd.ExecuteNonQuery();
        if (i == 1)
        {
            lblupdate.Visible = true;
            lblupdate.Text = "Deleted Successfully";
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        string ddllist = ddlpprs.SelectedItem.Text;
        string content = HttpUtility.HtmlEncode(CKEditor1.Text);
        SqlCommand cmd1 = new SqlCommand("update Newtopic set Description=@t1 where Topic=@t2", con);
        cmd1.Parameters.AddWithValue("@t1", content);
        cmd1.Parameters.AddWithValue("@t2", ddllist);
        int i = cmd1.ExecuteNonQuery();
        if (i == 1)
        {
            lblupdate.Visible = true;
            lblupdate.Text = "Updated Successfully";
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="AddBanner.aspx.cs" Inherits="Admin_AddBanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-3">
            Select Image
        </div>
        <div class="col-md-3">
            <asp:FileUpload ID="FileUpload1" runat="server" />
        </div>
        <div class="col-md-3">
            <asp:Button ID="Button1" runat="server" Text="Upload" OnClick="Button1_Click" />
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
          
            <asp:GridView ID="GridView1" DataKeyNames="Id" CssClass ="Grid" runat="server" AutoGenerateColumns = "false" OnRowDeleting="OnRowDeleting" >
    <Columns>
         <asp:TemplateField HeaderText="Sr. No.">
            <ItemTemplate>
                <label id="lblID"><%#Eval("Id") %></label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Banner Images">
            <ItemTemplate>
                <img src="<%# Eval("Banner") %>"   width="200px" height="200px"/>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ShowDeleteButton="True" ButtonType="Button" />
    </Columns>
</asp:GridView>
                
        </div>
    </div>

</asp:Content>


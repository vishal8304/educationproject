﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewJob : System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con != null && con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select * from NewJob where Section = 'Admit Card'", con);
        SqlDataReader sdr = cmd.ExecuteReader();
        if (sdr.HasRows)
        {
            Repeater1.DataSource = sdr;
            Repeater1.DataBind();
        }
    }
}
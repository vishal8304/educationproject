﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Notifications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubscribe_Click(object sender, EventArgs e)
    {
        
            var user = Request.Cookies["Email"].Value;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            con.Open();
            string qry = "select * from notifications where Email=@t1";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.Parameters.AddWithValue("@t1", user);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Label1.Visible = true;
                Label1.Text = "You are already subscribed";
            }
            else
            {
                string query = "insert into notifications(Email) values(@t2)";
                SqlCommand cmd1 = new SqlCommand(query, con);
                cmd1.Parameters.AddWithValue("@t2", user);
                int j = cmd1.ExecuteNonQuery();
                if (j == 1)
                {
                    Label1.Visible = true;
                    Label1.Text = "You have successfully subscribed";
                }
            }
        }

    }

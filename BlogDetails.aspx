﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.master" AutoEventWireup="true" CodeFile="BlogDetails.aspx.cs" Inherits="BlogDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="styleBlog.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnplace" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="content" class="site-content">
    <div class="container">
    <div class="inner-wrapper">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
<article id="post-5399" class="post-5399 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized tag-ceat-tyre-for-baleno-in-noida tag-ceat-tyre-for-ciaz-in-noida tag-ceat-tyre-for-creta-in-noida tag-ceat-tyre-for-dzire-in-noida tag-ceat-tyre-for-honda-in-noida tag-ceat-tyre-for-wagonr-in-noida tag-ceat-tyre-for-xcent-in-noida">

	<div class="entry-head">
		<div class="entry-footer">
			<span class="posted-on">
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></span>		</div>

					<div class="featured-thumb">
				<img width="660" height="550" src="<%=BlogImage %>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Ceat tyre for baleno in Noida" srcset="<%=BlogImage %> 660w, <%=BlogImage %> 150w, <%=BlogImage %> 300w" sizes="(max-width: 660px) 100vw, 660px">			</div>
			</div>

	<div class="content-wrap">
		<div class="content-wrap-inner">
			
			<header class="entry-header">
				<h1 class="entry-title"> <asp:Label ID="Label1" runat="server" ></asp:Label></h1>			</header>

			<div class="entry-content">
				<asp:Literal ID="Literal1" runat="server">
    </asp:Literal>

			</div><!-- .entry-content -->
		</div>
	</div>

</article><!-- #post-## -->
            </main>
            </div>
        </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contentplace3" Runat="Server">
</asp:Content>


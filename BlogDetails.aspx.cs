﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BlogDetails : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    public string BlogImage;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetBlog();
        }
    }

    public void GetBlog()
    {

        string Url = Request.QueryString["Url"];
        try
        {
            con.Open();
            string qry = "select Top 1 * from BlogMaster where Url='" + Url + "' order by Id desc";
            SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataReader sdr = cmd.ExecuteReader();
            sdr.Read();
            if (sdr.HasRows)
            {
                Literal1.Text = Server.HtmlDecode(sdr["Description"].ToString() + "");
                Label1.Text = sdr["Heading"].ToString();
                BlogImage = sdr["BlogImage"].ToString();
                Label2.Text = sdr["Date"].ToString();
            }
        }
        catch (Exception ex)
        {


        }
    }
}
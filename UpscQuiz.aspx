﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="UpscQuiz.aspx.cs" Inherits="UpscQuiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .label2 {
            font-size: 15px;
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="contact">
        <div class="container">

            <h3 class="w3l_header w3_agileits_header1"><span>Quiz
                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></span></h3>
            <asp:HiddenField runat="server" Value='<%#Eval("Id") %>' ID="setNumber" />
            <div class="row">
                <div class="col-md-4">
                    <asp:Label ID="lblshow" runat="server" Visible="false" Text="Result:" BackColor="Yellow"></asp:Label>
                </div>
                <div class="col-md-8">
                    <asp:Label ID="lblresult" runat="server" Visible="false" BackColor="Yellow"></asp:Label>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                        <asp:ScriptManager ID="SM1" runat="server"></asp:ScriptManager>

                        <asp:Timer ID="timer1" runat="server" Interval="1000" Enabled="false" OnTick="timer1_tick"></asp:Timer>

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="timer1" EventName="tick" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                </div>
                <asp:Repeater ID="rptqs" runat="server">
                    <ItemTemplate>
                        <div class="row" style="border-bottom-color: lightsteelblue; border-width: medium; border-style: solid; box-shadow: rgb(128, 128, 128);">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:HiddenField ClientIDMode="Static" runat="server" Value='<%#Eval("Id") %>' ID="sno" />
                                        <strong>
                                            <asp:Label ID="lblqs" ClientIDMode="Static" runat="server" Text='<%#Eval("Ques") %>'></asp:Label></strong>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Image ID="imgquiz" runat="server" Visible='<%#Eval("qspath").ToString().Length>0?true:false %>' ImageUrl='<%#Eval("qspath") %>' Height="200px" Width="500px" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-1">
                                        <asp:RadioButton ID="rdb1" CssClass="label2" runat="server" value="A" GroupName="a" />A
                                    </div>
                                    <div class="col-md-11">
                                        <asp:Label ID="lblsub1" runat="server" CssClass="label2" Text='<%#Eval("val1") %>'></asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-1">
                                        <asp:RadioButton ID="rdb2" runat="server" CssClass="label2" value="B" GroupName="a" />B
                                    </div>
                                    <div class="col-md-11">
                                        <asp:Label ID="lblsub2" runat="server" CssClass="label2" Text='<%#Eval("val2") %>'></asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-1">
                                        <asp:RadioButton ID="rdb3" runat="server" CssClass="label2" value="C" GroupName="a" />C
                                    </div>
                                    <div class="col-md-11">
                                        <asp:Label ID="lblsub3" runat="server" CssClass="label2" Text='<%#Eval("val3") %>'></asp:Label>
                                        <%--<input type="radio" name="a" opt="<%#Eval("val3") %>"/>--%>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-1">
                                        <asp:RadioButton ID="rdb4" runat="server" CssClass="label2" value="D" GroupName="a" />D
                                    </div>
                                    <div class="col-md-11">
                                        <asp:Label ID="lblsub4" runat="server" CssClass="label2" Text='<%#Eval("val4") %>'></asp:Label>
                                    </div>
                                </div>
                                <br />
                                <%--<asp:DataList ID="DataList1" runat="server">
                    <ItemTemplate>
                    <div class="row">
                    <div class="col-md-5">
                        <asp:HiddenField  runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' ID="id1"/>
                        <asp:Label ID="lblsub1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val1") %>'></asp:Label> </div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb1" runat="server" GroupName="a"/></div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val2") %>'></asp:Label></div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb2" runat="server" GroupName="a"/></div>
                    </div>
                    <div class="row">
                    <div class="col-md-5">
                        <asp:Label ID="lblsub3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val3") %>'></asp:Label> </div>
                        <div class="col-md-1">
                            <asp:RadioButton ID="rdb3" runat="server" GroupName="a"/>
                            </div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val4") %>'></asp:Label></div>
                        <div class="col-md-1">
                            <asp:RadioButton ID="rdb4" runat="server" GroupName="a"/>
                    </div></div>
</ItemTemplate>
                </asp:DataList>--%>
                                <br />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="row" style="text-align: center">
                    <div class="col-md-12">
                        <%-- <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click"/>--%>
                        <input type="button" value="Submit" id="btnSubmit" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="Admin/js/jquery.min.js"></script>
    <%-- <script>
        function element() {
            $("#btnstartquiz").click(function () {
                return 0;
            });

        }
        if (element() == 0) {
            $("input[type='radio']").attr("disabled", );
        } else {
            $("input[type='radio']").attr("disabled", true);
        }
        //$("#btnstartquiz").click(function () {
        //    $("input[type='radio']").attr("disabled", false);
        //})
    </script>--%>
    <%--<script>
        $(document).ready(function () {
            $("input[type='radio']").attr("disabled", true);
            
        })
    </script>--%>
    <%--<script src="js/timerdata.js"></script>--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequest);
        function beginRequest() {
            prm._scrollPosition = null;
        }
    </script>
    <script>
        $("#btnSubmit").click(function () {
            $("input[type='radio']:checked").each(function () {
                var current = $(this);
                var opt = current.val();
                var Email = "";
                var cookieArray = document.cookie.split("; ");
                for (var i = 0; i < cookieArray.length; i++) {
                    var nameValueArray = cookieArray[i].split("=");
                    if (nameValueArray[0] == "Email") {
                        Email = nameValueArray[1];
                    }
                }
                var id = current.parent().parent().parent().parent().find("#sno").val();
                function getUrlVars() {

                    var vars = [], hash;
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                    for (var i = 0; i < hashes.length; i++) {
                        hash = hashes[i].split('=');
                        vars.push(hash[0]);
                        vars[hash[0]] = hash[1];
                    }
                    return vars;
                }
                var section = getUrlVars()["section"];
                var sectionL = decodeURI(section);
                var name = getUrlVars()["name"];
                var decode = decodeURI(name);
                $.ajax({
                    type: "POST",
                    url: "UpscQuiz.aspx/SaveAnswer",
                    data: "{ 'opt':'" + opt + "', 'Email': '" + Email + "', 'id': '" + id + "','name': '" + decode + "', 'section':'" + sectionL + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        if (r.d == 'true') {
                <% //Session["BankQuiz"] = "BankQuiz"; %>

                //alert("Submitted successfullyy");
            }
        },
                error: function (result) {
                    alert("Error");
                }
            });
            });
            alert("Submitted successfully");
            window.location.href = 'UpscTest.aspx';
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentplace3" runat="Server">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class testseries : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            valMembers();
            literal();
            bindlabel();
        }
    }
    public void literal()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select Top 1 * from onlineinstruct where section='SSC'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Content"].ToString();
            literal1.Text = System.Net.WebUtility.HtmlDecode(str);
        }
    }
    public void bindlabel()
    {
        try { 
        var a = Request.QueryString["set"];
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        lbltest.Text = a.ToString();
        }catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }
    public void valMembers()
    {
        try
        {
            var a = Request.QueryString["set"];
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("select Email from onusertest where Email='" + Session["usr"].ToString() + "' and [set]='" + a + "' group by name,[Email]", con);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            dr1.Read();
            if (dr1.HasRows)
            {
                dr1.Close();
                SqlDataAdapter da2 = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand("Select name,[set],sum(marks) as totalmarks,sum(time) as totaltime, count(Question) as count, dbo.GetTotalMarksBySet(@a, name, @email) as TotalMarksObt from ontimertest where [set] = @a GROUP BY[set], name", con);
                cmd.Parameters.Add(new SqlParameter("@a", a));
                cmd.Parameters.Add(new SqlParameter("@email", Session["usr"].ToString()));
                da2.SelectCommand = cmd;
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                Repeater2.DataSource = dt2;
                Repeater2.DataBind();
            }
            else
            {
                dr1.Close();
                SqlDataAdapter da1 = new SqlDataAdapter("select name,[set],count(Question) AS COUNT from ontimertest where [set]='" + a + "' and name is not null group by name,[set]", con);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                Repeater1.DataSource = dt1;
                Repeater1.DataBind();
            }
        }
        catch (Exception ex)
        {
            Console.Write("Some Problem Occurred", ex);
        }
    }

    //public void connect()
    //{
    //    if (con.State == ConnectionState.Closed)
    //    {
    //        con.Open();
    //    }
    //    SqlDataAdapter da = new SqlDataAdapter("select distinct section from ontimertest where section IS NOT NULL", con);
    //    DataTable dt = new DataTable();
    //    da.Fill(dt);
    //    rpttest.DataSource = dt;
    //    rpttest.DataBind();
    //}
    public class ontimertest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Optionval1 { get; set; }
        public string Optionval2 { get; set; }
        public string Optionval3 { get; set; }
        public string Optionval4 { get; set; }
        public string Correctval { get; set; }
        public string time { get; set; }
        public string path { get; set; }
        public string section { get; set; }
        public string descr { get; set; }
        public string solimg { get; set; }
        public string solved { get; set; }
    }
    
}

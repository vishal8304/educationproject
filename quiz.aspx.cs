﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class quiz : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    int a, b;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnstart();
            valMembers();
            Label2.Text = Request.QueryString["name"].ToString();
        }
    }
    
    public void valMembers()
    {
       
        var setNo = HttpUtility.UrlDecode(Request.QueryString["name"]);
        //setNumber.Value = setNo;
        //string val = HttpContext.Current.Session["test"].ToString();
        da = new SqlDataAdapter("select * from test where name = '" + setNo + "'", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptqs.DataSource = dt;
        rptqs.DataBind();
    }

    public void btnstart()
    {
        try { 
        var setNo = Request.QueryString["name"];
            if (setNo == null)
            {
                Response.Redirect("test.aspx");
            }
            else
            {

        //setNumber.Value = setNo;
        DataTable dt = new DataTable();
        con.Open();
        SqlCommand sqlCmd = new SqlCommand("select max(time) from test where name = '" + setNo + "'", con);
        int time = Convert.ToInt32(sqlCmd.ExecuteScalar());
        Session["timeout"] = DateTime.Now.AddMinutes(time).ToString();
        con.Close();
        timer1.Enabled = true;
            }
        }
        
        catch(Exception ex)
        {
            Console.Write("", ex);
        }
    }
    protected void timer1_tick(object sender, EventArgs e)
    {
        try
        {
            int a = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).TotalMinutes);
            int b = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).Seconds);
            if (a <= 0 && b <= 0)
            {
                Response.Redirect("timeout.aspx");

            }
            else
            {
                if (0 > DateTime.Compare(DateTime.Now, DateTime.Parse(Session["timeout"].ToString())))
                {

                    Label1.Text = string.Format("Time Left: {0}:{1}", a.ToString(), b.ToString());

                }
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught: {0}", ex);
        }
    }
    

    [WebMethod]
    public static string SaveAnswer(string opt, string Email, string id,string name)
    {
        string returnvalue = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("quizinsert", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@selectedval", opt);
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@Id", id);
        cmd.Parameters.AddWithValue("@name", name);
        int Result = cmd.ExecuteNonQuery();
        if (Result == 1)
        {
            SqlCommand comd = new SqlCommand("select max(marks) as marked,count(Ques) as qs from test where name='"+name+"'", con);
            SqlDataReader dr = comd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                double marked = Convert.ToDouble(dr["marked"].ToString());
                double qs = Convert.ToDouble(dr["qs"].ToString());
                double calc = Math.Round((marked / qs),2);
                dr.Close();
                string qry3 = "update b set [status]='passed', marksobt='"+calc+ "' FROM test a inner join onlinetest b on a.Id = b.Id where a.Correctoption=b.selectedval update b set [status] = 'failed',marksobt = -0.5 FROM test a inner join onlinetest b on a.Id = b.Id where a.Correctoption != b.selectedval";
                SqlCommand cmd3 = new SqlCommand(qry3, con);
                cmd3.ExecuteNonQuery();
               
            }
            
            
        }
      
        con.Close();
        return returnvalue = Result == 1 ? "true" : "false";

    }

   


    protected void btnstopquiz_Click(object sender, EventArgs e)
    {
        
    }

    
}
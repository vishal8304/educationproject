﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Banking : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    protected void Page_Load(object sender, EventArgs e)
    {
        try { 
        if (!IsPostBack)
        {
                var a = Request.QueryString["BankId"];
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
                
                SqlCommand cmd = new SqlCommand("select * from Banking where BankId=@t1", con);
                cmd.Parameters.AddWithValue("@t1", a);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    string str = dr["Content"].ToString();
                    literal1.Text = System.Net.WebUtility.HtmlDecode(str);
                }
            con.Close();
            //valMembers();
            //if (Session["usr"] == null)
            //{
            //    Response.Redirect("Login.aspx");
            //}
        }
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
    //public void valMembers()
    //{
    //    var setNo = Request.QueryString["setNo"];
    //    setNumber.Value = setNo;
    //    //string val = HttpContext.Current.Session["test"].ToString();
    //    da = new SqlDataAdapter("select * from Banking", con);
    //    DataTable dt = new DataTable();
    //    da.Fill(dt);
    //    rptqs.DataSource = dt;
    //    rptqs.DataBind();
    //}

    [WebMethod]
    public static string SaveAnswer(string radio, string Email, string id)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("insert into userbank(selectedval,Email,BankId) values(@selectedval,@Email,@Id)", con);
        cmd.Parameters.AddWithValue("@selectedval", radio);
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@Id", id);
        int Result = cmd.ExecuteNonQuery();
        if (Result == 1)
        {
            string qry3 = "update a set [status]='passed' FROM userbank a inner join Banking b on a.BankId = b.BankId where a.selectedval=b.correctval";
            SqlCommand cmd3 = new SqlCommand(qry3, con);
            int j = cmd3.ExecuteNonQuery();
            //return j.ToString();
        }
        con.Close();
        return Result > 0 ? "true" : "error";
    }
}
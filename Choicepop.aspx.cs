﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class Choicepop : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        con.Open();
        if (rdbssc.Checked == true)
        {
            HttpCookie mycookie = new HttpCookie("rdb");
            mycookie["rdb"] = "SSC";
            mycookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(mycookie);
            Response.Redirect("Home.aspx");

            //Session["rdb"] = "SSC";
        }
        else if (rdbbank.Checked == true)
        {
            HttpCookie mycookie = new HttpCookie("rdb");
            mycookie["rdb"] = "Bank";
            mycookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(mycookie);
            Response.Redirect("Home.aspx");
        }
    }
}
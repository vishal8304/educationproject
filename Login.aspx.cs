﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

public partial class Login : System.Web.UI.Page
{ 
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.Form.DefaultButton = "btnSubmit";
        try { 
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        con.Open();
        string qry = "select * from [User] where [Email]=@t1 and [password]=@t2";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.Parameters.AddWithValue("@t1", txtUsrnm.Text);
        cmd.Parameters.AddWithValue("@t2", txtPwd.Text);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if(dr.HasRows)
        {
            Session["usr"] = txtUsrnm.Text;
            Response.Redirect("Home.aspx");

        }
        else
        {
            lblInfo.Visible = true;
            lblInfo.Text = "Email Id or Password mismatch";
        }
        }catch(Exception ex4)
        {
            Console.Write("", ex4);
        }
    }

    //protected void btnchangetheme_Click(object sender, EventArgs e)
    //{
    //    EnableTheming = true;

    //}

    protected void btnforgotpwd_Click(object sender, EventArgs e)
    {
        try { 
        string username = "";
        string password = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd4 = new SqlCommand("select Email, password from [User] where Email=@email", con);
        cmd4.Parameters.AddWithValue("@email", txtUsrnm.Text);
        using (SqlDataReader sdr = cmd4.ExecuteReader())
        {
            if (sdr.Read())
            {
                username = sdr["Email"].ToString();
                password = sdr["password"].ToString();

            }
        }
        con.Close();

        if (!string.IsNullOrEmpty(password))
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("ruchikaj6@gmail.com");
            msg.To.Add(txtUsrnm.Text);
            msg.Subject = " Recover your Password";
            msg.Body = ("Your Username is:" + username + "<br/><br/>" + "Your Password is:" + password);
            msg.IsBodyHtml = true;
            SmtpClient smt = new SmtpClient();
            smt.Host = "smtp.gmail.com";
            System.Net.NetworkCredential ntwd = new NetworkCredential();
            ntwd.UserName = "ruchikaj6@gmail.com"; //Your Email ID  
            ntwd.Password = "password1290@"; // Your Password  
            smt.UseDefaultCredentials = true;
            smt.Credentials = ntwd;
            smt.Port = 587;
            smt.EnableSsl = true;
            smt.Send(msg);
            lblInfo.Visible = true;
            lblInfo.Text = "Username and Password Sent Successfully";
            lblInfo.ForeColor = System.Drawing.Color.ForestGreen;
        }
        else
        {
            lblInfo.Visible = true;
            lblInfo.Text = "Please enter a valid Email Address";
            lblInfo.ForeColor = System.Drawing.Color.Red;
        }
        }catch(Exception ex)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ex.ToString();
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Onlinetiles.aspx.cs" Inherits="Onlinetiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
    .zoom {
  /*padding: 50px;*/
  background-color: green;
  transition: transform .2s;
  width: 200px;
  height: 200px;
  margin: 2%;
}

.zoom:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
         <div class="row" style="margin-left: 40%;
    margin-top: 2%;">
             <div class="col-md-12">
                <h2>SSC TEST SERIES</h2> 
             </div>
         </div>
         <br />
         <%--Repeater has been used to show data from database--%>
         <div class="row">
            
                 <asp:Repeater ID="rpttiles" runat="server">
                     <ItemTemplate>
                          <div class="col-md-3">
                         <div class="row">
            <div class="col-sm-12">

                <%--Redirecting to Test Series page--%>
                <a href="ssctestseries.aspx?set=<%#Eval("set")%>"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:greenyellow;text-align:center" class="zoom"><br /><br />
                       <b><%#Eval("set")%></b>
                    </div>
                </a>
            </div></div> </div>
                     </ItemTemplate>
                 </asp:Repeater>
            
         </div>
         <br />
         <hr />
         <%--Repeater has been used to show data from database--%>
       <div class="row" style="margin-left: 40%;
    margin-top: 2%;">
             <div class="col-md-12">
                <h2>BANK TEST SERIES</h2> 
             </div>
         </div>
         <br />
         <div class="row">
            
                 <asp:Repeater ID="rptbanktiles" runat="server">
                     <ItemTemplate>
                          <div class="col-md-3">
                         <div class="row">
            <div class="col-sm-12">
                 <%--Redirecting to Bank Test Series page--%>
                <a href="Banktestseries.aspx?set=<%#Eval("set")%>"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:greenyellow;text-align:center" class="zoom"><br /><br />
                        <b><%#Eval("set")%></b>
                    </div>
                </a>
            </div></div><br /></div>
                     </ItemTemplate>
                 </asp:Repeater>
             
         </div><br />
 </div>
</asp:Content>


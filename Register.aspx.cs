﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class Register : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //else { 
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        //con.Open();
        //string qry = "insert into [User] (Name,Email,[password]) values(@t1,@t2,@t3)";
        //SqlCommand cmd = new SqlCommand(qry, con);
        //cmd.Parameters.AddWithValue("@t1", txtName.Text);
        //cmd.Parameters.AddWithValue("@t2", txtEmail.Text);
        //cmd.Parameters.AddWithValue("@t3", txtPwd.Text);
        //int i = cmd.ExecuteNonQuery();
        //if(i==1)
        //{
        //    lblConfirm.Visible = true;
        //    lblConfirm.Text = "You have Successfully Registered";
        //    txtEmail.Text = "";
        //    txtName.Text = "";
        //    txtPwd.Text = "";
        //}
        //}
    }

    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        if (txtEmail.Text != "" && txtName.Text != "" && txtPwd.Text != "")
        {

            string email = txtEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                SqlCommand newcmd = new SqlCommand("select Email from [User] where Email='" + email + "'", con);
                SqlDataReader dr = newcmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    lblConfirm.Visible = true;
                    lblConfirm.Text = "You are already Registered";
                }
                else
                {
                    dr.Close();
                    string qry = "insert into [User] (Name,Email,[password],Mob,Addr) values(@t1,@t2,@t3,@t4,@t5)";
                    SqlCommand cmd = new SqlCommand(qry, con);
                    cmd.Parameters.AddWithValue("@t1", txtName.Text);
                    cmd.Parameters.AddWithValue("@t2", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@t3", txtcpwd.Text);
                    cmd.Parameters.AddWithValue("@t4", txtmob.Text);
                    cmd.Parameters.AddWithValue("@t5", txtadrr.Text);
                    int i = cmd.ExecuteNonQuery();
                    if (i == 1)
                    {
                        lblrequired.Visible = false;
                        lblConfirm.Visible = true;
                        lblConfirm.Text = "You have Successfully Registered";
                        txtEmail.Text = "";
                        txtName.Text = "";
                        txtPwd.Text = "";
                    }
                }
            }
            else
            {
                lblrequired.Visible = true;
                lblrequired.Text = email + " is Invalid Email Address";
            }
        }
    }
}
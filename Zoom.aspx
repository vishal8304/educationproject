﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Zoom.aspx.cs" Inherits="Zoom" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
#thover{
  position:fixed;
  background:#000;
  width:100%;
  height:100%;
  opacity: .6
}
 
#tpopup{
  position:absolute;
  width:684px;
  height:340px;
  background:#151414;
  left:45%;
  top:35%;
  border-radius:5px;
  padding:60px 0;
  margin-left:-320px; /* width/2 + padding-left */
  margin-top:-150px; /* height/2 + padding-top */
  text-align:center;
  box-shadow:0 0 10px 0 #000;
}
#tclose{
  position:absolute;
  background:black;
  color:white;
  right:-15px;
  top:-15px;
  border-radius:50%;
  width:30px;
  height:30px;
  line-height:30px;
  text-align:center;
  font-size:8px;
  font-weight:bold;
  font-family:'Arial Black', Arial, sans-serif;
  cursor:pointer;
  box-shadow:0 0 10px 0 #000;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="thover"></div>
        <div id="tpopup">
    <img id="myImg" src="" runat="server" alt="Trolltunga, Norway" width="600" height="350"/>
             <div id="tclose"> <a href="Home.aspx" >X</a></div>    
  </div>
<!-- The Modal -->



    </form>
</body>
   <script>
    $(document).ready(function () {

        $("#thover").click(function () {
            $(this).fadeOut();
            $("#tpopup").fadeOut();
        });


      
    });
</script>
</html>

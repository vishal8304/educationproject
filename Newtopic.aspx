﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Newtopic.aspx.cs" Inherits="Newtopic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style>
        .pnl{
            background-color:rgba(132, 128, 128, 0.67);
            border-style:groove;
        }
        .label2{
  font-size: 15px;
  vertical-align: middle;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                <aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
               <li>
                   <asp:Panel ID="pnltopic" runat="server" CssClass="pnl">
                   <asp:Repeater ID="rptwords" runat="server">
                       <ItemTemplate>
                           <div class="row">
                               <div class="col-md-12">
                                      <b> <a href="Newtopic.aspx?Topic=<%#Eval("Topic") %>"><%#Eval("Topic") %></a></b>
                               </div>
                           </div>
                       </ItemTemplate>
                   </asp:Repeater> </asp:Panel>
               </li>
            </ul>
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
            </div>
                    <div class="col-md-9">
                        <asp:Label ID="lbldesc" runat="server">
                        </asp:Label>
                    </div>
                </div>
            </div>
        </div><br />
    </div>
</asp:Content>


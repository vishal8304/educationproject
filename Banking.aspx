﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Banking.aspx.cs" Inherits="Banking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<div class="container" style="background-color:white; width:70%">
            <div class="row">
                <div class="col-md-12">
            <h3 class="w3l_header w3_agileits_header1"><span>BANKING</span></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
            <asp:Literal ID="literal1" runat="server"></asp:Literal>
                </div>
            </div>
                       <%-- <asp:HiddenField  runat="server"  Value='<%#Eval("BankId") %>' ID="setNumber"/>
                <div class="row">
                    <div class="col-md-4">
                                               <asp:Label ID="lblshow" runat="server" Visible="false" Text="Result:" BackColor="Yellow"></asp:Label>
                    </div>
                    <div class="col-md-8">
                        <asp:Label ID="lblresult" runat="server" Visible="false" BackColor="Yellow"></asp:Label>
                    </div>
                
    <asp:Repeater ID="rptqs" runat="server">
        <ItemTemplate>
                 <div class="row" style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <asp:HiddenField ClientIDMode="Static"  runat="server" Value='<%#Eval("BankId") %>' ID="sno"/>
                        <strong><asp:Label ID="lblqs" ClientIDMode="Static" runat="server"  Text='<%#Eval("Question") %>'></asp:Label></strong>
                </div>
                    </div>
                 <div class="row">
                                    <div class="col-md-12">
                                        <asp:Image ID="testimg" runat="server" ImageUrl='<%#Eval("picpath") %>' Height="200px" Width="500px"/>
                                    </div>
                                </div>
                <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb1" CssClass="label2" runat="server" value='A' GroupName="a" /> </div>
                    <div class="col-md-11">
                        <asp:Label ID="lblsub1" runat="server" CssClass="label2" Text='<%#Eval("val1") %>'></asp:Label>
                    </div>
                    </div>
                <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb2" runat="server" CssClass="label2" value='B' GroupName="a"  />
                        </div>
                    <div class="col-md-11">
                        <asp:Label ID="lblsub2" runat="server" CssClass="label2" Text='<%#Eval("val2") %>'></asp:Label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb3" runat="server" CssClass="label2" value='C'  GroupName="a" />
                        </div>
                        <div class="col-md-11">
                            <asp:Label ID="lblsub3" runat="server" CssClass="label2" Text='<%#Eval("val3") %>'></asp:Label>
                            </div>
                        </div>
                <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb4" runat="server" CssClass="label2" value='D' GroupName="a" />
                        </div>
                        <div class="col-md-11">
                            <asp:Label ID="lblsub4" runat="server" CssClass="label2" Text='<%#Eval("val4") %>' ></asp:Label>
                    </div></div>
                <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb5" runat="server" CssClass="label2" value='E'  GroupName="a" />
                        </div>
                        <div class="col-md-11">
                            <asp:Label ID="lblsub5" runat="server" CssClass="label2" Text='<%#Eval("val5") %>'></asp:Label>
                            </div>
                    </div>--%>

                <%--<asp:DataList ID="DataList1" runat="server">
                    <ItemTemplate>
                    <div class="row">
                    <div class="col-md-5">
                        <asp:HiddenField  runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' ID="id1"/>
                        <asp:Label ID="lblsub1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val1") %>'></asp:Label> </div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb1" runat="server" GroupName="a"/></div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val2") %>'></asp:Label></div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb2" runat="server" GroupName="a"/></div>
                    </div>
                    <div class="row">
                    <div class="col-md-5">
                        <asp:Label ID="lblsub3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val3") %>'></asp:Label> </div>
                        <div class="col-md-1">
                            <asp:RadioButton ID="rdb3" runat="server" GroupName="a"/>
                            </div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "val4") %>'></asp:Label></div>
                        <div class="col-md-1">
                            <asp:RadioButton ID="rdb4" runat="server" GroupName="a"/>
                    </div></div>
</ItemTemplate>
                </asp:DataList>--%>
                <%--<br />
                      </div>
                     </div>
		 </ItemTemplate>
    </asp:Repeater>
              <div class="row" style="text-align:center">
                    <div class="col-md-12">
                        <input type="button" value="Submit" id="btnSubmit" />
                    </div>
                </div>  
        </div>--%>
	 </div>
     <script src="Admin/js/jquery.min.js"></script>

    <script>
        $("#btnSubmit").click(function () {
            var current = $(this);
            if ($("input[type='radio']").is(':checked')) {
                var radio = current.parent().parent().parent().find("input[type=radio]:checked").val();
            }
            else {
                alert('Plz choose your option');
            }
            var Email = "";
            var cookieArray = document.cookie.split("; ");
            for (var i = 0; i < cookieArray.length; i++) {
                var nameValueArray = cookieArray[i].split("=");
                if (nameValueArray[0] == "Email") {
                    Email = nameValueArray[1];
                }
            }
            //var Email = '<%= Session["usr"] %>';
            var id = current.parent().parent().parent().parent().find("#sno").val();
            $.ajax({
        type: "POST",
        url: "Banking.aspx/SaveAnswer",
        data:  "{'radio':'" + radio + "','Email':'" + Email + "','id':'" + id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var obj = data.d;
            if (obj == 'true') {
                <% Session["Bank"] = "Bank"; %>
                window.location.href = 'timeout.aspx';
                //alert("Submitted Successfully");
            }
            else
                alert(obj);
        },
        error: function (result) {
            alert("Error Occured, Try Again");
        }
            });
        });
        </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Studybank.aspx.cs" Inherits="Studybank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12" style="text-align:center">
             <h3>Banking</h3>
        </div>
    </div><br />
     <div class="row">
         <div class="col-md-1"></div>
            <div class="col-sm-2">
                <a href="Studybank.aspx?section=Quantitative Aptitude"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#ffb52f;text-align:center"><br /><br />
                        Quantitative Aptitude
                    </div>
                </a>
            </div>
          <div class="col-sm-2">
                <a href="Studybank.aspx?section=English"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#33cc99;text-align:center"><br /><br />
                        English
                    </div>
                </a>
            </div>
          <div class="col-sm-2">
                <a href="Studybank.aspx?section=Reasoning"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#eb5424;text-align:center"><br /><br />
                        Reasoning
                    </div>
                </a>
            </div>
          <div class="col-sm-2">
                <a href="Studybank.aspx?section=General Awareness"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#55c2e2;text-align:center"><br /><br />
                        General Awareness
                    </div>
                </a>
            </div>
          <div class="col-sm-2">
                <a href="Studybank.aspx?section=Computer"  style="text-decoration: none !important;">
                    <div style="width:200px;height:100px;background-color:#ffb52f;text-align:center"><br /><br />
                        Computers
                    </div>
                </a>
            </div>
         <div class="col-md-1"></div>
      </div><br /><br />
   <div class="row">
       <div class="col-md-3"></div>
                <div class="col-md-6">
                <asp:Repeater ID="Repeater2" runat="server">
                    <HeaderTemplate>
                        <div class="row" style=" border-style:solid; border-bottom:none;">
                            <div class="col-md-1">Sno</div>
                            <div class="col-md-4" style="text-align:center">
                                Section
                            </div>
                            <div class="col-md-7">
                                Chapters
                            </div>
                        </div>
                    </HeaderTemplate>
        <ItemTemplate>
						 <div class="row"  style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
                             <div class="col-md-1">
                                  <asp:Label ID="Label1" runat="server" Text='<%#Container.ItemIndex+1 %>'></asp:Label>
                             </div>
                            
                             <div class="col-md-4" style="text-align:center">
                               <strong><asp:Label ID="lblno" runat="server" Text='<%#Eval("section") %>'></asp:Label></strong>  
                             </div>
                             <div class="col-md-3">
                                <strong><asp:Label ID="lbltotalqs" runat="server" Text='<%#Eval("Chapter") %>'></asp:Label></strong>
                             </div>
                            
                                 <%--<div class="col-md-1">--%>
                                     <%--   Max marks:
                                 </div>
                             <div class="col-md-1">
                                 <asp:Label ID="lblmaxmarks" runat="server" Text='<%#Eval("Setno") %>'></asp:Label>
                             </div>--%>
                             <div class="col-md-4" style="text-align:right">
                                        <a href="Banking.aspx?BankId=<%#Eval("BankId") %>"><%#Eval("Topic").ToString()==""?"Click here":Eval("Topic") %></a>
                </div>
                         </div>
    </ItemTemplate>
                </asp:Repeater></div>
       <div class="col-md-3"></div>
            </div><br />
</asp:Content>


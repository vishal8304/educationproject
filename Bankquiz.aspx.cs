﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class Bankquiz : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    SqlDataAdapter da;
    int a, b;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnstart();
            valMembers();
            Label3.Text= Request.QueryString["name"].ToString();
        }
    }
    public void valMembers()
    {
        //try { 
        var name = Request.QueryString["name"];
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        da = new SqlDataAdapter("select * from Bankquiz where name='"+name+"'", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        rptqs.DataSource = dt;
        rptqs.DataBind();
        //}catch(Exception ex)
        //{
        //    Console.WriteLine("", ex);
        //}
    }
    public void btnstart()
    {
        try { 
        var name = Request.QueryString["name"];
       
        DataTable dt = new DataTable();
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand sqlCmd = new SqlCommand("select max(time) from Bankquiz where name ='" + name + "'", con);
        
        int time = Convert.ToInt32(sqlCmd.ExecuteScalar());
        
             Session["timeout"] = DateTime.Now.AddMinutes(time).ToString();
        
        timer1.Enabled = true;
        con.Close();
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
    protected void timer1_tick(object sender, EventArgs e)
    {
        try
        {
            int a = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).TotalMinutes);
            int b = ((Int32)DateTime.Parse(Session["timeout"].ToString()).Subtract(DateTime.Now).Seconds);
            if (a <= 0 && b <= 0)
            {
                Response.Redirect("timeout.aspx");

            }
            else
            {
                if (0 > DateTime.Compare(DateTime.Now, DateTime.Parse(Session["timeout"].ToString())))
                {

                    Label1.Text = string.Format("Time Left: {0}:{1}", a.ToString(), b.ToString());

                }
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught: {0}", ex);
        }
    }
    [WebMethod]
    public static string SaveAnswer(string opt, string Email, string id,string name)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("insert into userbankquiz(selectedval,Email,ID,name) values(@selectedval,@Email,@Id,@name)", con);
        cmd.Parameters.AddWithValue("@selectedval", opt);
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@Id", id);
        cmd.Parameters.AddWithValue("@name", name);
        int Result = cmd.ExecuteNonQuery();
        if (Result == 1)
        {
            SqlCommand comd = new SqlCommand("select max(marks) as marked,count(Ques) as qs from Bankquiz where name='" + name + "'", con);
            SqlDataReader dr = comd.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                double marked = Convert.ToDouble(dr["marked"].ToString());
                double qs = Convert.ToDouble(dr["qs"].ToString());
                double calc = Math.Round((marked / qs), 2);
                dr.Close();
                string qry3 = "update b set [status]='passed' , marksobt='"+calc+"' FROM Bankquiz a inner join userbankquiz b on a.Id = b.Id where a.correctoption=b.selectedval update b set[status] = 'passed',marksobt = -0.5 FROM Bankquiz a inner join userbankquiz b on a.Id = b.Id where a.correctoption != b.selectedval";
                SqlCommand cmd3 = new SqlCommand(qry3, con);
                cmd3.ExecuteNonQuery();
            }
        }
        con.Close();
        return Result.ToString();
    }


    
}
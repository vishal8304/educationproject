﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewJob.aspx.cs" Inherits="NewJob" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row" style="">
            <div class="col-md-4">
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <%#Eval("Section") %>
            </ItemTemplate>
            <ItemTemplate>
                <a target="_blank" href="<%#Eval("Link") %>"><%#Eval("Text") %></a>
            </ItemTemplate>
        </asp:Repeater>
            </div>
             <div class="col-md-4">
        <asp:Repeater ID="Repeater2" runat="server">
          
        </asp:Repeater>
            </div>
             <div class="col-md-4">
        <asp:Repeater ID="Repeater3" runat="server">
            <ItemTemplate>
                
            </ItemTemplate>

        </asp:Repeater>
            </div>
            </div>

    </form>
</body>
</html>

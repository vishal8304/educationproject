﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.Services;

public partial class Studybank : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindbank();
        }
    }
    public void bindbank()
    {
        try { 
        var section = Request.QueryString["section"];
        if (section == "Quantitative Aptitude" || section == "" || section == null)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select * from Banking where section='Quantitative Aptitude' group by Chapter,BankId,section,Topic,content order by BankId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
            con.Close();
        }
        else if (section == "English")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select BankId,Chapter,section,Topic,content from Banking where section='English' group by Chapter,BankId,section,Topic,content order by BankId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
                con.Close();
            }
        else if (section == "Reasoning")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select BankId,Chapter,section,Topic,content from Banking where section='Reasoning' group by Chapter,BankId,section,Topic,content order by BankId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
                con.Close();
            }
        else if (section == "General Awareness")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select BankId,Chapter,section,Topic,content from Banking where section='General Awareness' group by Chapter,BankId,section,Topic,content order by BankId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
                con.Close();
            }
        else if (section == "Computer")
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("select BankId,Chapter,section,Topic,content from Banking where section='Computer' group by Chapter,BankId,section,Topic,content order by BankId", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
                con.Close();
        }
        }catch(Exception ex)
        {
            Console.WriteLine("", ex);
        }
    }
}
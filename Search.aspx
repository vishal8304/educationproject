﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<link href="enq/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
<link href="enq/style.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="stylesheet" href="enq/ken-burns.css" type="text/css" media="all"/>
<link rel="stylesheet" href="enq/animate.min.css" type="text/css" media="all"/>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <%--  <div>

            <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" style="font-size:1.3em;" role="tablist">
                    <li role="presentation" class="home active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Post</a></li>
                    <li role="presentation" class="exam"><a href="#exam" aria-controls="exam" role="tab" data-toggle="tab">Exam</a></li>
                    <li role="presentation" class="declaration"><a href="#declaration" aria-controls="declaration" role="tab" data-toggle="tab">User</a></li>
                </ul>
    <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <h1 class="section-title">Post</h1>
                        <div class="form-group">
                            <div class="col-md-12 control-label">--%>
                           <div class="contact">
		<div class="container">
    <div class="w3layouts_mail_grid">
        <div class="agileits_mail_grid_right agileits_w3layouts_mail_grid_right">
   
            <div class="agileits_mail_grid_right1 agile_mail_grid_right1">
    <asp:Repeater ID="rptqs" runat="server">
        <ItemTemplate>
                 <div class="row">
            <div class="col-md-12">
                
                 <div class="row">
                    <div class="col-md-1">
                        <span class="glyphicon glyphicon-user"></span>
                   </div>
                     <div class="col-md-11"><h4><strong><asp:Label ID="lblnm" runat="server" Text='<%#Eval("Name") %>'></asp:Label></strong></h4>
                     </div>
                        </div>
                <div class="row">
                    <div class="col-md-12">
                    <div class="image-box">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("picpath") %>' Width="320" Height="300"/>
                </div>
                </div>
                    </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblqs" runat="server" Text='<%#Eval("Post") %>'></asp:Label>
                      </div>
			 </div>
                <div class="row">
                    <div class="col-md-3">
                        <asp:Label ID="lblsub1" runat="server" Text='<%#Eval("exam") %>'></asp:Label> </div>
                    <div class="col-md-1">
                        <asp:Label ID="lblsub2" runat="server" Text="|"></asp:Label></div>
                    <div class="col-md-8">
                        <asp:Label ID="lblsub3" runat="server" Text='<%#Eval("subject") %>'></asp:Label></div>
                </div>
                
                     <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
        <%--<asp:UpdatePanel runat="server" id="UpdatePanel" updatemode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger controlid="btnLike" eventname="Click" />
        </Triggers>
            <ContentTemplate>
                <asp:Label runat="server" id="lblLikes" />
                <asp:Button runat="server" id="btnLike" onclick="btnLike_Click" text="Like" />               
            </ContentTemplate>
        </asp:UpdatePanel>--%>
                <div class="row">
                    <div class="col-md-2">
                        <asp:button ID="btnlike" runat="server" Text="Like" style="width:100px;" OnClick="btnlike_Click"></asp:button></div>
   <%-- <i class=""></i>
    <span class="like-count"></span>--%>

                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <input id="btncomment" type="button" value="Comment"/>
                    </div>
                    <div class="col-md-5">
                         <asp:Button ID="btnshare" runat="server" Text="SHARE" onclick="btnshare_Click"/>
                    </div>
                </div>
                
                <div class="row" hidden="hidden" id="divcom">
                    <div class="col-md-1"></div>
                    <div class="col-md-8">
                        <input id="txtcom" name="txtcom" type="text" style="width:550px;"/>
                    </div>
                    <div class="col-md-3">
                        <input type="button" id="btnEnter" value="Enter" style="width:100px;"/>
                    </div>
                </div>
                      </div>
                     </div>
                   </div>
		 </ItemTemplate>
    </asp:Repeater>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Repeater ID="rptcom" runat="server">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-12">
                                         <asp:Label ID="lblcom" runat="server" Text='<%#Eval("Comments") %>'></asp:Label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
        </div>
	 </div>
             </div></div>
                        </div>
                        <%--</div>
                            </div>
                        </div>--%>
                    <%--<div role="tabpanel" class="tab-pane" id="exam">
                        <h1 class="section-title">Exam</h1>
                        <div class="form-group">
                               <asp:Repeater ID="rptrexam" runat="server"></asp:Repeater>
                            </div>
                       </div>
                    <div role="tabpanel" class="tab-pane" id="declaration">
                        <h1 class="section-title">User</h1>
                        <div class="form-group">
                            <div class="col-md-12">
                               <asp:Repeater ID="rptrusr" runat="server"></asp:Repeater>
                            </div>
                            
                        </div>
                       </div>--%>
                        
                      <%--</div>
     </div>--%>
<%--<link rel="stylesheet" href="enq/validationEngine.jquery.css" type="text/css"/>
<script src="enq/js_checks.js" type="text/javascript"></script>
<%--<script src="enq/custom.js"></script>--%>
<%--<link rel="stylesheet" type="text/css" href="enq/bootstrap-datepicker.min.css"/>--%>
    <%--<script> 
	$(document).ready(function() {
        $("#form1").validationEngine({autoHidePrompt:true});
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $('.datepicker').datepicker({
            startDate: '-3d'
        });
        $( 'input[name="photo_check"]:radio' ).change(function(){
            if($(this).val() == "yes")
                $(".applicant_photo").css("display","block");
            else
                $(".applicant_photo").css("display","none");
        });
        $( 'input[name="document_check"]:radio' ).change(function(){
            if($(this).val() == "yes")
                $(".document_check").css("display","block");
            else
                $(".document_check").css("display","none");
        });
        
        $('#step_1').click(function(e){
            if($('applicant_email').val() == $('applicant_confirm_email').val()) {
                $('.exam').addClass('active');
                $('.home').removeClass('active');
                $('#exam').addClass('active');
                $('#home').removeClass('active');
            } else {
                alert("Applicant email and confirm email are not same...!");
            }
        });
        $('#step_2').click(function(e){
            $('.declaration').addClass('active');
            $('.exam').removeClass('active');
            $('#declaration').addClass('active');
            $('#exam').removeClass('active');
        });
        $("#person_detail_1").change(function(){
            if($("#person_detail_1"). prop("checked") == true)
                $(".person_detail_1").css("display","block");
            else
                $(".person_detail_1").css("display","none");
        });
        $("#person_detail_2").change(function(){
            if($("#person_detail_2"). prop("checked") == true)
                $(".person_detail_2").css("display","block");
            else
                $(".person_detail_2").css("display","none");
        });
        $("#reference_by").change(function(){
            if($(this).val() == "reference"){
                $("#reference_name").css("display","block");
            } else {
                $("#reference_name").css("display","none");
            }
        });
	});
</script>--%>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
   <script>
        $('#btncomment').click(function () {
            $('#divcom').show();
            $('#btncomment').hide();
            return false;
        });
    </script>

    <script type="text/javascript">
    window.onload = initAll;

function initAll() {

    document.getElementById("#btnEnter").onsubmit = addNode;

}

function addNode() {

    var inText = document.getElementById("txtcom").value;

var newText = document.createTextNode(inText);

var newGraf = document.createElement("p");

newGraf.appendChild(newText);

return false;

}
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Newtopic : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dataCon"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            desc();
            bindrepeater();
        }
    }

    public void desc()
    {
        var a = Request.QueryString["Topic"];
        if (a == null)
        { 
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("select Top 1 * from Newtopic order by [Date] desc", con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            string str = dr["Description"].ToString();
            lbldesc.Text = System.Net.WebUtility.HtmlDecode(str);
        }
        dr.Close();
        }
        else
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd1 = new SqlCommand("select * from Newtopic where Topic=@t1", con);
            cmd1.Parameters.AddWithValue("@t1", a);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            dr1.Read();
            if (dr1.HasRows)
            {
                string str = dr1["Description"].ToString();
                lbldesc.Text = System.Net.WebUtility.HtmlDecode(str);
            }
            dr1.Close();
        }
    }

    public void bindrepeater()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlDataAdapter da = new SqlDataAdapter("select * from Newtopic order by Id desc", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        rptwords.DataSource = dt;
        rptwords.DataBind();
    }
}
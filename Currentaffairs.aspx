﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.master" AutoEventWireup="true" CodeFile="Currentaffairs.aspx.cs" Inherits="Currentaffairs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .pnl {
            background-color: rgba(132, 128, 128, 0.67);
            border-style: groove;
        }

        .label2 {
            font-size: 15px;
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3 class="w3l_header w3_agileits_header1" style="text-align: center"><span>Current Affairs</span></h3>
    <div class="row" style="margin-top:2%;">
        <div class="col-md-3">
            <aside>
                <div id="sidebar" class="nav-collapse">
                    <!-- sidebar menu start-->
                    <%-- <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
               <li>--%>
                    <asp:Panel ID="pnlword" runat="server" CssClass="pnl">
                        <asp:Repeater ID="rptwords" runat="server">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-12">
                                        <b><a href='Currentaffairs.aspx?Date=<%#Eval("Date", "{0:dd-MMM-yyyy}")%>'><%#Eval("Date", "{0:dd-MMM-yyyy}")%></a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <%-- </li>
            </ul>
        </div>--%>
                    <!-- sidebar menu end-->
                </div>
            </aside>

        </div>
        <div class="col-md-9" style="">
            <table class="table table-bordered">
                <tr>
                    <td>
                        <asp:Literal ID="literal1" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <%--  <asp:Repeater ID="rptqs" runat="server">
        <ItemTemplate>
                 <div class="row" style="border-bottom-color:lightsteelblue; border-width:medium; border-style:solid; box-shadow:rgb(128, 128, 128);">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <asp:HiddenField ClientIDMode="Static"  runat="server" Value='<%#Eval("Id") %>' ID="sno"/>
                        <strong><asp:Label ID="lblqs" ClientIDMode="Static" runat="server"  Text='<%#Eval("Question") %>'></asp:Label></strong>
                </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Image ID="imgquiz" runat="server" ImageUrl='<%#Eval("path") %>' Height="200px" Width="500px" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb1" CssClass="label2" runat="server" value="A" GroupName="a" />A </div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub1" runat="server" CssClass="label2" Text='<%#Eval("Optionval1") %>'></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb2" runat="server" CssClass="label2" value="B" GroupName="a"  />B
                        </div>
                    <div class="col-md-5">
                        <asp:Label ID="lblsub2" runat="server" CssClass="label2" Text='<%#Eval("Optionval2") %>'></asp:Label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb3" runat="server" CssClass="label2" value="C"  GroupName="a" />C
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="lblsub3" runat="server" CssClass="label2" Text='<%#Eval("Optionval3") %>'></asp:Label>
                            <%--<input type="radio" name="a" opt="<%#Eval("val3") %>"/>--%>
    <%-- </div>
                    <div class="col-md-1">
                        <asp:RadioButton ID="rdb4" runat="server" CssClass="label2" value="D" GroupName="a" />D
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="lblsub4" runat="server" CssClass="label2" Text='<%#Eval("Optionval4") %>' ></asp:Label>
                    </div></div>
                <br />
                      </div>
                     </div>
		 </ItemTemplate>
    </asp:Repeater><br />--%>
    <%--<div class="row" style="text-align:center">
                    <div class="col-md-12">
                       <%-- <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click"/>--%>
    <%--<input type="button" value="Submit" id="btnSubmit" />
                    </div>
                </div> --%>
    <script src="Admin/js/jquery.min.js"></script>
    <script>
        $("#btnSubmit").click(function () {
            $("input[type='radio']:checked").each(function () {
                var current = $(this);
                var opt = current.val();
                var Email = "";
                var cookieArray = document.cookie.split("; ");
                for (var i = 0; i < cookieArray.length; i++) {
                    var nameValueArray = cookieArray[i].split("=");
                    if (nameValueArray[0] == "Email") {
                        Email = nameValueArray[1];
                    }
                }
                var id = current.parent().parent().parent().parent().find("#sno").val();
                $.ajax({
                    type: "POST",
                    url: "Currentaffairs.aspx/SaveAnswer",
                    data: "{ 'opt':'" + opt + "', 'Email': '" + Email + "', 'id': '" + id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        if (r.d == 'true') {
                <% Session["Current"] = "Current"; %>
                window.location.href = 'timeout.aspx';
                //alert("Submitted successfullyy");
            }
        },
             error: function (result) {
                 alert("Error");
             }
         });
            });
        });
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequest);
        function beginRequest() {
            prm._scrollPosition = null;
        }
    </script>

</asp:Content>

